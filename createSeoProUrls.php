<?php
header("Content-Type: text/html; charset=UTF-8"); // Где-нибудь в начале PHP скрипта
require_once('config.php');

error_reporting(E_ALL);
ini_set('display_errors', 1);

function translit($s) {
    $s = (string) $s;
    $s = strip_tags($s);
    $s = str_replace(array("\n", "\r"), " ", $s);
    $s = preg_replace("/\s+/", ' ', $s);
    $s = trim($s); 
    $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); 
    $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
    $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s);
    $s = str_replace(" ", "-", $s); 
    return $s;
}

function urlAliasGenerator() {
    $mysqli = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

    if ($mysqli->connect_errno) {
        return false;
    }

    $mysqli->query( "SET CHARSET utf8" );
    
    $sqlProducts = "SELECT opd.product_id, opd.name FROM oc_product op, oc_product_description opd WHERE opd.product_id = op.product_id AND op.status = 1";
    
    if (!$enabledProducts = $mysqli->query($sqlProducts)) {
        return false;
    }

    $products = $enabledProducts->fetch_all();
    for ($i = 0; $i < count($products); $i++) {
        $product = $products[$i];
        print_r($product);
        $result = $mysqli->query("INSERT IGNORE INTO oc_url_alias (query, keyword, seomanager) VALUES ('product_id=" . $product[0] . "', '" . translit($product[1]) . "', 0)");
    }
    return true;
}

urlAliasGenerator();