<?php
// *	@copyright	OPENCART.PRO 2011 - 2015.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class Pagination_front {
	public $total = 0;
	public $page = 1;
	public $limit = 20;
	public $num_links = 8;
	public $url = '';
	public $text_first = '';
	public $text_last = '';
	public $text_next = 'Вперед';
	public $text_prev = 'Назад';

	public function render() {
		$total = $this->total;

		if ($this->page < 1) {
			$page = 1;
		} else {
			$page = $this->page;
		}

		if (!(int)$this->limit) {
			$limit = 10;
		} else {
			$limit = $this->limit;
		}

		$num_links = $this->num_links;
		$num_pages = ceil($total / $limit);

		$this->url = str_replace('%7Bpage%7D', '{page}', $this->url);

		$output = '<div class="pagination">';

		if ($page > 1) {
			//$output .= '<a href="' . str_replace('{page}', 1, $this->url) . '">' . $this->text_first . '</a></li>';
			$output .= '<a class="btn" href="' . str_replace('{page}', $page - 1, $this->url) . '">' . $this->text_prev . '</a>';
		}

		if ($num_pages > 1) {
			if ($num_pages <= $num_links) {
				$start = 1;
				$end = $num_pages;
			} else {
				$start = $page - floor($num_links / 2);
				$end = $page + floor($num_links / 2);

				if ($start < 1) {
					$end += abs($start) + 1;
					$start = 1;
				}

				if ($end > $num_pages) {
					$start -= ($end - $num_pages);
					$end = $num_pages;
				}
			}

			for ($i = $start; $i <= $end; $i++) {
				if ($page == $i) {
					$output .= '<a class="btn active">' . $i . '</a>';
				} else {
					$output .= '<a class="btn" href="' . str_replace('{page}', $i, $this->url) . '">' . $i . '</a>';
				}
			}
		}

		if ($page < $num_pages) {
			$output .= '<a class="btn" href="' . str_replace('{page}', $page + 1, $this->url) . '">' . $this->text_next . '</a>';
			//$output .= '<li><a href="' . str_replace('{page}', $num_pages, $this->url) . '">' . $this->text_last . '</a></li>';
		}

		$output .= '</div>';

		if ($num_pages > 1) {
			return $output;
		} else {
			return '';
		}
	}
}