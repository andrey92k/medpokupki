<?php
// *	@copyright	OPENCART.PRO 2011 - 2015.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

namespace DB;
final class MySQL {
	private $link;

	public function __construct($hostname, $username, $password, $database, $port = '3306') {
		if (!$this->link = mysql_connect($hostname . ':' . $port, $username, $password)) {
			trigger_error('Error: Could not make a database link using ' . $username . '@' . $hostname);
			exit();
		}

		if (!mysql_select_db($database, $this->link)) {
			trigger_error('Error: Could not connect to database ' . $database);
			exit();
		}

		mysql_query("SET NAMES 'utf8'", $this->link);
		mysql_query("SET CHARACTER SET utf8", $this->link);
		mysql_query("SET CHARACTER_SET_CONNECTION=utf8", $this->link);
		mysql_query("SET SQL_MODE = ''", $this->link);
		// get local time on Web/PHP server
$localtime = strtotime(date('Y-m-d H:i:s'));

//get local time in GMT/UTC (i.e GMT/UTC is set as +0:00 on database and other timezones are set as +/- hours of this)
$gm_localtime = strtotime(gmdate('Y-m-d H:i:s'));

//find offset in hours 
$diff_hours = ($localtime - $gm_localtime) / 3600;

//Then the Database server needs to be set to this Offset to store/retrieve values as local ones﻿
$adjust = "SET time_zone = '";

if ($diff_hours > 0) {
   $adjust .= "+" . ceil($diff_hours);
} elseif ($diff_hours < 0) {
   $adjust .= floor($diff_hours);
} else {
   $adjust .= "+0";
}   
$adjust .= ":00'";

mysql_query($adjust, $this->link);
	}

	public function query($sql) {
		if ($this->link) {
			$resource = mysql_query($sql, $this->link);

			if ($resource) {
				if (is_resource($resource)) {
					$i = 0;

					$data = array();

					while ($result = mysql_fetch_assoc($resource)) {
						$data[$i] = $result;

						$i++;
					}

					mysql_free_result($resource);

					$query = new \stdClass();
					$query->row = isset($data[0]) ? $data[0] : array();
					$query->rows = $data;
					$query->num_rows = $i;

					unset($data);

					return $query;
				} else {
					return true;
				}
			} else {
				$trace = debug_backtrace();

				trigger_error('Error: ' . mysql_error($this->link) . '<br />Error No: ' . mysql_errno($this->link) . '<br /> Error in: <b>' . $trace[1]['file'] . '</b> line <b>' . $trace[1]['line'] . '</b><br />' . $sql);
			}
		}
	}

	public function escape($value) {
		if ($this->link) {
			return mysql_real_escape_string($value, $this->link);
		}
	}

	public function countAffected() {
		if ($this->link) {
			return mysql_affected_rows($this->link);
		}
	}

	public function getLastId() {
		if ($this->link) {
			return mysql_insert_id($this->link);
		}
	}

	public function __destruct() {
		if ($this->link) {
			mysql_close($this->link);
		}
	}
}