</div>

            <?php if (isset ($yandex_money_metrika_active) && $yandex_money_metrika_active){ ?>
                <?php echo $yandex_metrika; ?>
                <script type="text/javascript">
                    var old_addCart = cart.add;
                    cart.add = function (product_id, quantity)
                    {
                        var params_cart = new Array();
                        params_cart['name'] = 'product id = '+product_id;
                        params_cart['quantity'] = quantity;
                        params_cart['price'] = 0;
                        old_addCart(product_id, quantity);
                        metrikaReach('metrikaCart', params_cart);
                    }
    
                    $('#button-cart').on('click', function() {
                        var params_cart = new Array();
                        params_cart['name'] = 'product id = '+ $('#product input[name="product_id"]').val();
                        params_cart['quantity'] = $('#product input[name="quantity"]').val();
                        params_cart['price'] = 0;
                        metrikaReach('metrikaCart', params_cart);
                    });
    
                    function metrikaReach(goal_name, params) {
                    for (var i in window) {
                        if (/^yaCounter\d+/.test(i)) {
                            window[i].reachGoal(goal_name, params);
                        }
                    }
                }
                </script>
            <?php } ?>
    <footer>
        <div class="container">
            <section class="top">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo">
                            <img src="catalog/view/theme/default/stylesheet/images/logo-footer.jpg">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3 col-sm-4 col-xs-12 footer-col">
                                <p></p>
                                <p></p>
                                <span class="titlefour">КАТАЛОГ</span>
                                <ul class="menu">
                                    <?php foreach ($categories as $category) { ?>
                                        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                                    <? } ?>
                                </ul>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-12 footer-col">
                                <p></p>
                                <p></p>
                                <span class="titlefour">КЛИЕНТ</span>
                                <ul class="menu">
                                    <li><a href="https://medpokupki.ru/kontakti">Обратная связь</a></li>
                                 <!--  <li><a href="">Отследить свой заказ</a></li>
                                    <li><a href="">Задать вопрос фармацевту</a></li> -->
                                    <li><a href="index.php?route=account/order">Личный кабинет</a></li>
                                    <li><a href="https://med.andreaszak.ru/index.php?route=account/wishlist">Мое избранное</a></li>
                                </ul>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-12 footer-col">
                                <p></p>
                                <p></p>
                                <span class="titlefour">АПТЕКА</span>
                                <ul class="menu">
                                    <li><a href="https://medpokupki.ru/o-nas">О нас</a></li>
                                    <li><a href="https://medpokupki.ru/dostavka-i-oplata">Доставка и оплата</a></li>
                                    <li><a href="https://medpokupki.ru/blog">Блог</a></li>
                                    <li><a href="https://medpokupki.ru/news">Новости</a></li>
                                    <li><a href="https://medpokupki.ru/kontakti">Контакты</a></li>
                                </ul>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-12 footer-col">
                                <p></p>
                                <p></p>
                                <span class="titlefour">КОНТАКТЫ</span>
                                <p class="adress"><b>Адрес:</b> 443124, Самарская обл.,<br> г. Самара, ул. Солнечная, д. 16, офис 2</p>
                                <p><b>8 800 551 4063 - Бесплатный номер по всей России</b></p>
                                <p><b>(846) 219 2737 - Самара</b></p>
                                <div class="social-icons-wrapper">
							       <a href="https://www.instagram.com/medpokupki/"> <img class="icon social-icon social-icon-instagram" src="catalog/view/theme/default/stylesheet/images/social_icon_2.png"></a>
							       <a href="https://vk.com/medpokupki"> <img class="icon social-icon social-icon-vk" src="/catalog/view/theme/default/stylesheet/images/vk_icon_1.png"></a>
							       <br>
							       <!--LiveInternet counter--><script type="text/javascript">
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t26.10;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,150))+";"+Math.random()+
"' alt='' title='LiveInternet: показано число посетителей за"+
" сегодня' "+
"border='0' width='88' height='15'><\/a>")
</script><!--/LiveInternet-->

					            </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <section class="bottom">
                <p>© 2017 Интернет-магазин “МедПокупки” - аптечный интернет-магазин</p>
                <img class="payments" src="image/payment/yandex_money/yamoney.jpg">
            </section>
        </div>
    </footer>



    <script src="catalog/view/theme/default/stylesheet/scripts/slider_price.js"></script>
    <script src="catalog/view/theme/default/stylesheet/scripts/jquery.scrollbox.js"></script>
    <script src="catalog/view/theme/default/stylesheet/scripts/jquery.ui.touch-punch.js"></script>
    <script src="catalog/view/theme/default/stylesheet/scripts/main.js"></script>


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'f5ydO2BAPU';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter48413435 = new Ya.Metrika2({
                    id:48413435,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<script>
$(function () { 
    $('header .top nav > a').each(function () {
        var location = window.location.href;
        var link = this.href; 
        if(location == link) {
            $(this).addClass('active');
        }
    });
});
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/48413435" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- BEGIN LEADGENIC CODE {literal} -->
  <!-- Put this script tag before the </body> tag of your page -->
  <script type="text/javascript" async src="https://gate.leadgenic.ru/getscript?site=5b332ce00cf2e5dc32089735"></script>
<!-- {/literal} END LEADGENIC CODE -->


				<?php if ($buyoneclick_status) { ?>
					<div id="boc_order" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<form id="boc_form" action="" role="form">
									<fieldset>
								<div class="modal-header">
									<button class="close" type="button" data-dismiss="modal">×</button>
									<h2 id="boc_order_title" class="modal-title"><?php echo $buyoneclick_title; ?></h2>
								</div>
								<div class="modal-body">
									<div id="boc_product_field" class="col-xs-12"></div>
									<div class="col-xs-12"><hr /></div>
									<div class="col-xs-12">
										<div style="display:none">
											<input id="boc_admin_email" type="text" name="boc_admin_email" value="<?php echo $buyoneclick_admin_email; ?>">
										</div>
										<div style="display:none">
											<input id="boc_product_id" type="text" name="boc_product_id">
										</div>
										<?php if ($buyoneclick_field1_status) { ?>
											<div class="input-group<?php if ($buyoneclick_field1_required) { echo ' has-warning';} ?>">
												<span class="input-group-addon"><i class="fa fa-fw fa-user" aria-hidden="true"></i></span>
												<input id="boc_name" class="form-control<?php if ($buyoneclick_field1_required) { echo ' required';} ?>" type="text" name="boc_name" placeholder="<?php echo $buyoneclick_field1_title; ?>">
											</div>
											<br />
										<?php } ?>
										<?php if ($buyoneclick_field2_status) { ?>
											<div class="input-group<?php if ($buyoneclick_field2_required) { echo ' has-warning';} ?>">
												<span class="input-group-addon"><i class="fa fa-fw fa-phone-square" aria-hidden="true"></i></span>
												<input id="boc_phone" class="form-control<?php if ($buyoneclick_field2_required) { echo ' required';} ?>" type="tel" name="boc_phone" placeholder="<?php if ($buyoneclick_validation_status) { echo $buyoneclick_validation_type; } else { echo $buyoneclick_field2_title; } ?>"<?php if ($buyoneclick_validation_status) {echo ' data-pattern="true"';} else {echo ' data-pattern="false"';} ?>>
											</div>
											<br />
										<?php } ?>
										<?php if ($buyoneclick_field3_status) { ?>
											<div class="input-group<?php if ($buyoneclick_field3_required) { echo ' has-warning';} ?>">
												<span class="input-group-addon"><i class="fa fa-fw fa-envelope" aria-hidden="true"></i></span>
												<input id="boc_email" class="form-control<?php if ($buyoneclick_field3_required) { echo ' required';} ?>" type="email" name="boc_email" placeholder="<?php echo $buyoneclick_field3_title; ?>">
											</div>
											<br />
										<?php } ?>
										<?php if ($buyoneclick_field4_status) { ?>
											<div class="form-group<?php if ($buyoneclick_field4_required) { echo ' has-warning';} ?>">
												<textarea id="boc_message" class="form-control<?php if ($buyoneclick_field4_required) { echo ' required';} ?>" name="boc_message" rows="3" placeholder="<?php echo $buyoneclick_field4_title; ?>" ></textarea>
											</div>
										<?php } ?>
										<?php if ($buyoneclick_agree_status !=0) { ?>
											<div class="checkbox">
												<label>
													<input id="boc_agree" type="checkbox" name="boc_agree" class="required" value="1"> <?=$buyoneclick_text_agree;?>
												</label>
											</div>
										<?php } ?>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="modal-footer">
									<div class="col-sm-2 hidden-xs">
									</div>
									<div class="col-sm-8 col-xs-12">
										<button type="submit" id="boc_submit" class="btn btn-block btn-primary text-uppercase"><?php echo $buyoneclick_button_order; ?></button>
									</div>
									<div class="col-sm-2 hidden-xs">
									</div>
								</div>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
					<div id="boc_success" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-body">
									<div class="text-center"><?php echo $buyoneclick_success_field; ?></div>
								</div>
							</div>
						</div>
					</div>
					<script type="text/javascript"><!--
					$('.boc_order_btn').on('click', function() {
						$.ajax({
							url: 'index.php?route=common/buyoneclick/info',
							type: 'post',
							data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
							beforeSend: function() {
						//		$('.boc_order_btn').button('loading');
							},
							complete: function() {
						//		$('.boc_order_btn').button('reset');
							},
							success: function(data) {
								console.log('1'+data);
								$('#boc_product_field').html(data);
							},
							error: function(xhr, ajaxOptions, thrownError) {
								console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
							}
						});
					});
					$('.boc_order_category_btn').on('click', function() {
						var for_post = {};
						for_post.product_id = $(this).attr('data-product_id');
						$.ajax({
							url: 'index.php?route=common/buyoneclick/info',
							type: 'post',
							data: for_post,
							beforeSend: function() {
						//		$('.boc_order_btn').button('loading');
							},
							complete: function() {
						//		$('.boc_order_btn').button('reset');
							},
							success: function(data) {
								console.log(data);
								$('#boc_product_field').html(data);
							},
							error: function(xhr, ajaxOptions, thrownError) {
								console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
							}
						});
					});					
					//--></script>
				<?php } ?>
				
</body></html>