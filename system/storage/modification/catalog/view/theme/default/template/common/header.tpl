
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
	
	<!--<![endif]-->
	<head>
		<meta charset="UTF-8" />
		<meta name="yandex-verification" content="853df9cc2aaddbf4" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $title; ?></title>
		<?php if ($robots) { ?>
			<meta name="robots" content="<?php echo $robots; ?>" />
		<?php } ?>
		<base href="<?php echo $base; ?>" />
		<?php if ($description) { ?>
			<meta name="description" content="<?php echo $description; ?>" />
		<?php } ?>
		<?php if ($keywords) { ?>
			<meta name="keywords" content= "<?php echo $keywords; ?>" />
		<?php } ?>
		
		<!-- Bootstrap -->
		<link href="catalog/view/theme/default/stylesheet/bootstrap-3.3.6/css/bootstrap.min.css" rel="stylesheet" />
		<!-- Bootstrap -->
		<!-- Font-awesome -->
		<link href="catalog/view/theme/default/stylesheet/font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet" />
		
		<link href="catalog/view/theme/default/stylesheet/font-awesome-5/css/v4-shims.css" type="text/css" rel="stylesheet" media="screen" />
		
		<link href="catalog/view/theme/default/stylesheet/font-awesome-5/css/fontawesome-all.css" type="text/css" rel="stylesheet" media="screen" />
		

		<link href="catalog/view/theme/default/stylesheet/icomoon/style.css" rel="stylesheet" />
		<link href="catalog/view/theme/default/stylesheet/styles/jquery-scrollbox-master.css" rel="stylesheet" />
		<!-- Font-awesome -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
		<!-- Site styles -->
		<link href="catalog/view/theme/default/stylesheet/styles/style.css" rel="stylesheet" />
		<link href="catalog/view/theme/default/stylesheet/styles/jquery-scrollbox-master.css" rel="stylesheet" />
		<link href="catalog/view/theme/default/stylesheet/styles/site_components.css" rel="stylesheet" />
		<link href="catalog/view/theme/default/stylesheet/styles/header.css" rel="stylesheet" />
		<link href="catalog/view/theme/default/stylesheet/styles/footer.css" rel="stylesheet" />
		<link href="catalog/view/theme/default/stylesheet/styles/add.css" rel="stylesheet" />
		<link rel="stylesheet" href="callme/callme.css" type="text/css">
		
		
		<link rel="stylesheet" href="catalog/view/javascript/jquery/ui/jquery-ui.min.css" type="text/css">
		
<?php if (getNitroPersistence('GoogleJQuery')) { ?>
                <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
            <?php } else { ?>
		<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<?php } ?>
		
		<!--<script src="catalog/view/theme/default/stylesheet/scripts/jquery-1.12.3.min.js"></script>-->
		
		<script src="catalog/view/javascript/jquery/ui/jquery-ui.min.js"></script>
		
		<script src="catalog/view/javascript/mf/jquery-ui.min.js" type="text/javascript" ></script>
		
		
		<script src="catalog/view/theme/default/stylesheet/bootstrap-3.3.6/js/bootstrap.min.js"></script> 
		<script type="text/javascript" src="callme/jquery.storage.js"></script>
		<script type="text/javascript" src="callme/callme.js"></script>
		<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
		<script src="//yastatic.net/share2/share.js"></script>

		<script>
$(function(){ 
  $("#myTab a").click(function(e){
    e.preventDefault();
    $(this).tab('show');
  });
});
</script>
		<!-- Site styles -->
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="scripts/respond.min.js"></script>
			<script src="scripts/html5shiv.min.js"></script>
		<![endif]-->
		
<link href="catalog/view/theme/default/stylesheet/geoip.css" rel="stylesheet">
		<?php foreach ($styles as $style) { ?>
			<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
		<?php } ?>
		<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
		<?php foreach ($links as $link) { ?>
			<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
		<?php } ?>
		<?php foreach ($scripts as $script) { ?>
			<script src="<?php echo $script; ?>" type="text/javascript"></script>
		<?php } ?>
		<?php foreach ($analytics as $analytic) { ?>
			<?php echo $analytic; ?>
		<?php } ?>
		<meta name="yandex-verification" content="b887c1070cdc7910" />
		
		<!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/v4-shims.css">-->

		<link rel="stylesheet" href="catalog/view/theme/default/stylesheet/custom-1.css?ver=<?php echo time(); ?>" type="text/css">

		<link rel="stylesheet" href="catalog/view/theme/default/stylesheet/custom.css?ver=<?php echo time(); ?>" type="text/css">

<script src="catalog/view/javascript/jquery/jquery.geoip-module.js" type="text/javascript"></script>

				<?php if ($buyoneclick_status || $buyoneclick_status_category) { ?>
					<script src="catalog/view/javascript/buyoneclick.js" type="text/javascript"></script>
					<?php if ($buyoneclick_validation_status) { ?>
						<script src="catalog/view/javascript/jquery.mask.min.js" type="text/javascript"></script>
						<script>
							$(document).ready(function(){
								$('#boc_phone').mask('<?php echo $buyoneclick_validation_type; ?>');
							});
						</script>
					<?php } ?>
					<?php if ($buyoneclick_ya_status || $buyoneclick_google_status) { ?>
						<script>
							function clickAnalytic(){
								<?php if ($buyoneclick_ya_status) { ?>
									yaCounter<?=$buyoneclick_ya_counter?>.reachGoal('<?=$buyoneclick_ya_identificator?>');
								<?php } ?>
								<?php if ($buyoneclick_google_status) { ?>
									ga('send', 'event', '<?=$buyoneclick_google_category?>', '<?=$buyoneclick_google_action?>');
								<?php } ?>
								return true;
							}
						</script>
					<?php } ?>
					<?php if ($buyoneclick_style_status) { ?><link href="catalog/view/theme/default/stylesheet/buyoneclick.css" rel="stylesheet"><?php } ?>
				<?php } ?>
            

			<script src="catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js" type="text/javascript"></script>
			<script src="catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
			<script src="catalog/view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
			<script src="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
			<link href="catalog/view/javascript/jquery/magnific/magnific-popup.css" rel="stylesheet">
			<link href="catalog/view/javascript/jquery/owl-carousel/owl.carousel.css" type="text/css" rel="stylesheet" media="screen">
			<link href="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen">
			<link href="catalog/view/theme/default/stylesheet/quickview.css" rel="stylesheet">
			<script type="text/javascript">
			var color_schem = '1';
			var loading_masked_img = '<img src="catalog/view/theme/default/image/ring-alt-'+ color_schem +'.svg" />';
			function loading_masked(action) {
				if (action) {
					$('.loading_masked').html(loading_masked_img);
					$('.loading_masked').show();
				} else {
					$('.loading_masked').html('');
					$('.loading_masked').hide();
				}
			}
			function creatOverlayLoadPage(action) {
				if (action) {
					$('#messageLoadPage').html(loading_masked_img);
					$('#messageLoadPage').show();
				} else {
					$('#messageLoadPage').html('');
					$('#messageLoadPage').hide();
				}
			}
			function quickview_open(id) {
			$('body').prepend('<div id="messageLoadPage"></div><div class="mfp-bg-quickview"></div>');
				$.ajax({
					type:'post',
					data:'quickviewpost=1',
					url:'index.php?route=product/product&product_id='+id,	
					beforeSend: function() {
						creatOverlayLoadPage(true); 
					},
					complete: function() {
						$('.mfp-bg-quickview').hide();
						$('#messageLoadPage').hide();
						creatOverlayLoadPage(false); 
					},	
					success:function (data) {
						$('.mfp-bg-quickview').hide();
						$data = $(data);
						var new_data = $data.find('#quickview-container').html();							
						$.magnificPopup.open({
							tLoading: loading_masked_img,
							items: {
								src: new_data,
							},
							type: 'inline'
						});
					}
			});							
			}
			</script>
			
	</head>
	<body class="<?php echo $class; ?>">
		<div class="container page_container">
			<!--header-->
			<header>
				<section class="top">
				    <div id="top">
			            <div class="row">
							<div class="col-sm-3 geo-ip-module"><?php echo $geoip; ?></div>
							<div class="col-sm-9">
								<nav class="clearfix pull-right">
									<a href="o-nas">О нас</a>
									<a href="dostavka-i-oplata">Доставки и оплата</a>
									<a href="blog">Блог</a>
									<a href="news">Новости</a>
									<a href="kontakti">Контакты</a>
									<a class="dropdown" href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-head"></i><?php echo $text_account; ?> <span class="caret"></span></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<?php if ($logged) { ?>
											<li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
											<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
											<li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
											<?php } else { ?>
											<li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
											<li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
										<?php } ?>
									</ul>
								</nav>
							</div>
						</div>
				    </div>
					<div class="row flex-center">
						<div class="col-md-3 col-sm-6 col-xs-12 col-lg-3">
							<a href="/" class="logo">
								<img src="catalog/view/theme/default/stylesheet/images/logo.png" />
							</a>
						</div>
								<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2 p-0 w-20">
									<div class="phone">
										<h4>8 800 551 40 63</h4>
										<div class="jivo-btn jivo-online-btn jivo-btn-light" onclick="jivo_api.open();" style="font-family: Helvetica, Arial;font-size: 12px;background-color: #5dbfcd;border-radius: 11px;-moz-border-radius: 11px;-webkit-border-radius: 11px;height: 28px;line-height: 28px; color: #fff; padding: 0 14px 0 14px;font-weight: normal;font-style: normal"><div class="jivo-btn-icon" style="background-image: url(//static.jivosite.com/button/chat_light.png);"></div>Чат с фармацевтом</div><div class="jivo-btn jivo-offline-btn jivo-btn-light" onclick="jivo_api.open();" style="font-family: Helvetica, Arial;font-size: 12px;background-color: #5dbece;border-radius: 11px;-moz-border-radius: 11px;-webkit-border-radius: 11px;height: 28px;line-height: 28px;padding: 0 14px 0 14px;display: none;font-weight: normal;font-style: normal"><div class="jivo-btn-icon" style="background-image: url(//static.jivosite.com/button/mail_light.png); style="background-color: #4db8c8""></div>Оставьте сообщение!</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 p-0 w-27">
								    <div class="search">
								        <?php echo $search; ?>
                                    </div>
                                </div>
								<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 p-0 flex-between w-28">
								 
								 
								    <a href="/index.php?route=product/compare" class="compare">
                                        <i class="fa fa-bars"></i>
                                    </a>
								 
                                    <a href="<?php echo $wishlist; ?>"
                                       title="<?php echo $text_wishlist; ?>" class="item-btn">
                                        <i class="fa fa-heart"></i>
                                        <span id="wishlist-total"><?php echo $text_wishlist; ?></span>
                                    </a>
                                    
									<div class="cart">                                 
                                        <a class="title clearfix" href="index.php?route=checkout/cart">
                                            <img src="catalog/view/theme/default/stylesheet/images/cart_icon.png" />
											
											<?php echo $cart; ?>
											
										</a>
									</div>
								</div>
							
						</div>
						
						
						
					</section>
					
					

						<?php if ($categories) { ?>
						
    					
						
						<nav class="catalog">
						    <div class="row">
						    
						    <div class="navbar-header">
						    
						        <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i> Категории</button>
		    				    <!--<span id="category" class="visible-xs"><?php echo $text_category; ?></span>-->
                                  
                                </div>
							
								<div class="catalog_menu_toggle collapse navbar-collapse navbar-ex1-collapse">
								<ul class="catalog-menu nav navbar-nav">
									<span class="category"><?php foreach ($categories as $category) { ?>
									
									    <?php // var_dump($category) ?>
									
										<?php if ($category['children']) { ?></span>
										<li class="dropdown">
											<a <? if ($mobile == '0') { ?> class="dropdown-toggle" data-toggle="dropdown" href="<?php echo $category['href']; ?>"<? } ?>><?php echo $category['name']; ?></a>
											<ul class=" menu dropdown-menu">
											    <div class="row menu-wrapper"> 
											        <div class="col-sm-8 col-xs-12">
												<?php foreach ($category['children'] as $child) { ?>
													<li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
												<?php } ?>
												    </div>
												    <div class="col-sm-4 hidden-xs">
												        <div class="element">
                                                            <div class="product ">
                                                                <?php if ($category['product']['special']) { ?>
                                									<div class="action">
                                										<div class="discount"><? echo round((($category['product']['price']-$category['product']['special'])/$category['product']['price'])*100);?>%</div>
                                									</div>
                                								<? } ?>
                                                                
                                                            <?php /*var_dump($category['product'])*/ ?>    
                                                                
                                                              <a href="<?php echo $category['product']['href'];?>" class="img-container bg-item" 
                                                                data-image-src="<?php echo $category['product']['img'];?>" style="background-image: url(&quot;<?php echo $category['product']['href'];?>&quot;);"></a>
                                                            <div class="description">
                                                            <a href="<?php echo $category['product']['href'];?>"><?php echo $category['product']['name'];?></a>
                                                                        <div class="price">
                                                                        <?php if ($category['product']['special']){?>
                                                                            <strong><?php echo $category['product']['special'];?></strong><?php echo $category['product']['price'];?></i>
                                                                            <?php }
                                                                            else
                                                                            {?>
                                                                            <strong><?php echo $category['product']['price'];?></strong></i>
                                                                            <?php }?>
                                                                          </div>
                                                            <button onclick="
                        									cart.add('<?php echo $category['product']['product_id']; ?>'); 
                        									$('.quantity-<?php echo $category['product']['product_id']; ?>').val('1'); 
                        									$('.quantity-<?php echo $category['product']['product_id']; ?>').parent().parent().show(); 
                        									$('.quantity-<?php echo $category['product']['product_id']; ?>').parent().parent().prev().hide(); 
                        									$('.quantity-<?php echo $category['product']['product_id']; ?>').parent().parent().addClass('cartid');
                        									$('.quantity-<?php echo $category['product']['product_id']; ?>').parent().parent().parent().parent().addClass('added');
                        									" class="btn btn-primary text-uppercase" <? if ($category['product']['cart_id']) { ?>style="display:none;"<? } ?>>В корзину</button>
                        									
                        									
                        									<div class="added" <? if ($category['product']['cart_id']) { ?>style="display:block;"<? } ?>>
                        										<p>Добавлен <a href="index.php?route=checkout/cart">в корзину</a></p>
                        										<div class="quantity_count">
                        											<button class="btn quantity-down" type="button"><i class="fa fa-minus"></i></button>
                        											<input cartid="<?php echo $category['product']['cart_id']; ?>" prodid="quantity-<?php echo $category['product']['product_id']; ?>" disabled="disabled" type="text" name="quantity" class="form-control quantity-form quantity-<?php echo $category['product']['product_id']; ?>" value="<?php echo $category['product']['quan']; ?>">
                        											<button class="btn quantity-up" type="button"><i class="fa fa-plus"></i></button>
                        											<button onclick="wishlist.add('<?php echo $category['product']['product_id']; ?>');" class="btn btn-like"><i class="icon-heart2"></i></button>
                        										</div>
                        									</div>
                                                            </div>
                                                            </div>
                                                            </div>
												    </div>
												</div>
												<div class="row brands">
												    <div class="col-sm-12"> 
                            <?php foreach($category['mans'] as $m){?>
	                               <div class="element">
                                    <a rel="nofollow" title="<?php echo $m['name'];?>" href="<?php echo $m['href'];?>">
                                        <img src="<?php echo $m['img'];?>" title="<?php echo $m['name'];?>" alt="<?php echo $m['name'];?>">
                                    </a>
                                 </div>
                            <?php }?>
					   							</div>
                        </div>
											</ul>
										</li>
										<? } else { ?>
										<li>
											<a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
										</li>
									<? } ?>
									<? } ?>
									    <li id="brands" class="dropdown">
									        <a target="_blank" href="/brands/">БРЕНДЫ</a>
									    </li>
									    <li id="promo" class="dropdown">
									        <a href="akcii">АКЦИИ</a>
									    </li>
								</ul>
							</div>	
							</div>
						<? } ?>
					</nav>      
					
					
					
					
				</header>
				<!--header-->
						