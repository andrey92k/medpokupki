<?php echo $header; ?><?php if( ! empty( $mfilter_json ) ) { echo '<div id="mfilter-json" style="display:none">' . base64_encode( $mfilter_json ) . '</div>'; } ?>
<section class="inner_page">
  <nav class="bread_crumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <? if ($breadcrumb == end($breadcrumbs)) { ?>
    <span><?php echo $breadcrumb['text']; ?></span>
    <? } else { ?>
    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <? } ?>
    <?php } ?>
  </nav>
  <div class="page_title text-uppercase">
    <h1><? echo $heading_title; ?></h1>
  </div>
  <div class="row">
    <div class="col-sm-4 col-md-3">
      <div class="sidebar">



        <? echo $column_left; ?>
      </div>
    </div>


    <div class="col-sm-8 col-md-9" id="mfilter-content-container">

      <?php if ($products) { ?>
      <div class="catalog_page_sort clearfix">
        <div class="pull-left">
          <span>Сортировать по:</span>
          <div class="custom_select dropdown">
            <button class="form-control dropdown-toggle" data-toggle="dropdown">
              <?php foreach ($sorts as $sort1) { ?>
              <?php if ($sort1['value'] == $sort . '-' . $order) { ?>
              <span class="text"><?php echo $sort1['text']; ?></span>
              <?php } ?>
              <?php } ?>
            </button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
              <?php foreach ($sorts as $sorts) { ?>
              <?php if ($sorts['value'] != $sort . '-' . $order) { ?>
              <li><a tabindex="-1" href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a></li>
              <?php } ?>
              <?php } ?>
            </ul>
          </div>
        </div>
        <div class="pull-right">
          <span>Показывать на странице по:</span>
          <?php foreach ($limits as $limits) { ?>

          <?php if ($limits['value'] == $limit) { ?>
          <?php if ($limits['value'] == 99999) { ?>
          <a href="<?php echo $limits['href']; ?>" class="active">Все</a>
          <? } else { ?>
          <a href="<?php echo $limits['href']; ?>" class="active"><?php echo $limits['text']; ?></a>
          <? } ?>

          <?php } else { ?>
          <?php if ($limits['value'] == 99999) { ?>
          <a href="<?php echo $limits['href']; ?>">Все</a>
          <? } else { ?>
          <a href="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></a>
          <? } ?>
          <?php } ?>
          <?php } ?>
        </div>
      </div>
      <section class="section_block products">
        <div class="row">                            
          <?php foreach ($products as $product) { ?>
          <div class="col-sm-6 col-md-4">
            <div class="product <? if ($product['cart_id']) { ?>added<? } ?>">
			
			<?php if ($product['stock_status'] == 'В наличии в аптеке') { ?>
									<div style="position: absolute;top: 0;left: 0;z-index: 2;text-align: right;" class="in_stock_sticker">
										<img src="catalog/view/theme/default/stylesheet/images/in_stock.png" />
									</div>
								<? } ?>
								
              <?php if ($product['special']) { ?>
              <div class="action">
                <div class="discount"><? echo round((($product['price']-$product['special'])/$product['price'])*100);?>%</div>
              </div>
              <? } ?>
              <a href="<?php echo $product['href']; ?>" class="img-container bg-item" data-image-src="<?php echo $product['thumb']; ?>"></a>
	
				<?php if($config_on_off_search_page_quickview =='1'){?>
					<div class="quickview"><a class="btn btn-quickview" onclick="quickview_open(<?php echo $product['product_id']?>);"><i class="fa fa-external-link fa-fw"></i><?php echo $config_quickview_btn_name[$lang_id]['config_quickview_btn_name']; ?></a></div>
				<?php } ?>
			
              <div class="description">
                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                <p><?php echo $product['description']; ?></p>
                <?php if ($product['price']) { ?>
                <div class="price">
                  <?php if (!$product['special']) { ?>
                  <strong><?php echo $product['price']; ?></strong>
                  <?php } else { ?>
                  <strong><?php echo $product['special']; ?></strong> <i><?php echo $product['price']; ?></i>
                  <?php } ?>
                </div>
                <?php } ?>
                <button onclick="
                cart.add('<?php echo $product['product_id']; ?>'); 
                $('.quantity-<?php echo $product['product_id']; ?>').val('1'); 
                $('.quantity-<?php echo $product['product_id']; ?>').parent().parent().show(); 
                $('.quantity-<?php echo $product['product_id']; ?>').parent().parent().prev().hide(); 
                $('.quantity-<?php echo $product['product_id']; ?>').parent().parent().addClass('cartid');
                $('.quantity-<?php echo $product['product_id']; ?>').parent().parent().parent().parent().addClass('added');
                " class="btn btn-primary text-uppercase" <? if ($product['cart_id']) { ?>style="display:none;"<? } ?>>В корзину</button>

				
				<?php if ($buyoneclick_status_category) { ?>
					<button type="button" class="btn-block boc_order_category_btn" <?php if ($buyoneclick_ya_status || $buyoneclick_google_status) { ?> onClick="clickAnalytic(); return true;" <?php } ?> data-toggle="modal" data-target="#boc_order" data-product="<?php echo $product['name'] ?>" data-product_id="<?php echo $product['product_id']; ?>"><?php echo $buyoneclick_name; ?></button>
				<?php } ?>
			


                <div class="added" <? if ($product['cart_id']) { ?>style="display:block;"<? } ?>>
                  <p>Добавлен <a href="index.php?route=checkout/cart">в корзину</a></p>
                  <div class="quantity_count">
                    <button class="btn quantity-down" type="button"><i class="fa fa-minus"></i></button>
                    <input cartid="<?php echo $product['cart_id']; ?>" prodid="quantity-<?php echo $product['product_id']; ?>" disabled="disabled" type="text" name="quantity" class="form-control quantity-form quantity-<?php echo $product['product_id']; ?>" value="<?php echo $product['quan']; ?>">
                    <button class="btn quantity-up" type="button"><i class="fa fa-plus"></i></button>
                    <button onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="btn btn-like"><i class="icon-heart2"></i></button>
                  </div>
                </div>

              </div>
            </div>
          </div>
          <? } ?>
        </div>
        <? echo $pagination; ?>
      </section>
      <? } else { ?>
      <p>Ничего не найдено. В нашей базе свыше 20 тысяч товаров, скорее всего у нас этот товар называется по другому. Попробуйте обратиться к нашему консультанту в правом нижнем углу сайта.</p>
      <? } ?>
    </div>
  </div>
</section>
<?php echo $footer; ?>
