<?php
// *	@copyright	OPENCART.PRO 2011 - 2015.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerModuleFeatured extends Controller {
	public function index($setting) {
		$this->load->language('module/featured');

			$data['config_on_off_featured_quickview'] = $this->config->get('config_on_off_featured_quickview');
		

		$data['heading_title'] = $setting['name'];
		$data['information_description'] = html_entity_decode($setting['information_description']);

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');

			$data['lang_id'] =	(int)$this->config->get('config_language_id');
			$data['config_quickview_btn_name'] = $this->config->get('config_quickview_btn_name');
		
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');


				// BuyOneClick
					$this->load->model('setting/setting');
					$current_language_id = $this->config->get('config_language_id');
					$data['buyoneclick_name'] = $this->config->get('buyoneclick_name_'.$current_language_id);
					$data['buyoneclick_status_module'] = $this->config->get('buyoneclick_status_module');

					$data['buyoneclick_ya_status'] = $this->config->get('buyoneclick_ya_status');
					$data['buyoneclick_google_status'] = $this->config->get('buyoneclick_google_status');
				// BuyOneClickEnd
				
		$data['products'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}

		if (!empty($setting['product'])) {
			$products = array_slice($setting['product'], 0, (int)$setting['limit']);

			foreach ($products as $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);

				if ($product_info) {
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}

					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}

					$cart_array = $this->cart->getProducts($product_info['product_id']);
					$cart_id = false;
					$cart_quan =false;
					foreach ($cart_array as $cart_elem) {
						if ($cart_elem['product_id'] == $product_info['product_id'])
						{
							$cart_id = $cart_elem['cart_id'];
							$cart_quan = $cart_elem['quantity'];
						}					
					}


					$data['products'][] = array(
						'product_id'  => $product_info['product_id'],
						'stock_status'       => $product_info['stock_status'],
						'cart_id'   => $cart_id,
						'quan'      => $cart_quan,
						'thumb'       => $image,
						'name'        => $product_info['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $rating,
						'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					);
				}
			}
		}

		if ($data['products']) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/featured.tpl')) {
				if ($setting['name'] == "В корзине") {
					return $this->load->view($this->config->get('config_template') . '/template/module/featuredkorzina.tpl', $data);
				} else {
					return $this->load->view($this->config->get('config_template') . '/template/module/featured.tpl', $data);	
				}
				
			} else {
				if ($setting['name'] == "В корзине") {
					return $this->load->view($this->config->get('config_template') . '/template/module/featuredkorzina.tpl', $data);
				} else {
					return $this->load->view($this->config->get('config_template') . '/template/module/featured.tpl', $data);	
				}
			}
		}
	}
}