<?php
	/*
		@author	Dmitriy Kubarev
		@link	http://www.simpleopencart.com
		@link	http://www.opencart.com/index.php?route=extension/extension/info&extension_id=4811
	*/
	
	include_once(DIR_SYSTEM . 'library/simple/simple_controller.php');
	
	class ControllerCheckoutSimpleCheckoutShipping extends SimpleController {
		private $_templateData = array();
		
		public function index() {
			
			$this->response->addHeader("Cache-Control: post-check=0, pre-check=0", false);
			$this->response->addHeader("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			$this->response->addHeader("Pragma: no-cache");
			
			if (!$this->cart->hasShipping()) {
				return;
			}
			
			$this->loadLibrary('simple/simplecheckout');
			
			$this->simplecheckout = SimpleCheckout::getInstance($this->registry);
			
			$this->language->load('checkout/simplecheckout');
			
			$get_route = isset($_GET['route']) ? $_GET['route'] : (isset($_GET['_route_']) ? $_GET['_route_'] : '');
			
			if ($get_route == 'checkout/simplecheckout_shipping') {
				$this->simplecheckout->init('shipping_address');
				$this->simplecheckout->init('shipping');
			}
			
			$address = $this->session->data['simple']['shipping_address'];
			
			$this->_templateData['address_empty'] = $this->simplecheckout->isShippingAddressEmpty();
			
			$quote_data = array();
			
			if ($stubs = $this->simplecheckout->getShippingStubs()) {
				foreach ($stubs as $stub) {
					$quote_data[$stub['code']] = $stub;
				}
			}
			
			if ($this->simplecheckout->getOpencartVersion() < 200) {
				$this->load->model('setting/extension');
				
				$results = $this->model_setting_extension->getExtensions('shipping');
				} else {
				$this->load->model('extension/extension');
				
				$results = $this->model_extension_extension->getExtensions('shipping');
			}
			


		/* Shiptor - агрегатор доставки */
		
		if (isset($this->request->post['payment_method'])
		&& isset($this->session->data['payment_methods'][$this->request->post['payment_method']])) {
			$this->session->data['payment_method']['code'] = $this->request->post['payment_method'];
		}
		
		/* Shiptor - агрегатор доставки */

		
			foreach ($results as $result) {
				$display = true;
				if ($this->_templateData['address_empty']) {
					$display = $this->simplecheckout->displayShippingMethodForEmptyAddress($result['code']);
				}
				
				if ($this->config->get($result['code'] . '_status') && $display) {
					$this->load->model('shipping/' . $result['code']);
					
					$quote = $this->{'model_shipping_' . $result['code']}->getQuote($address);
					
					if ($quote) {
						$this->simplecheckout->exportShippingMethods($quote);
						$quote = $this->simplecheckout->prepareShippingMethods($quote);
						if (!empty($quote)) {
							$stubsInfo = !empty($quote_data[$result['code']]['quote']) ? $quote_data[$result['code']]['quote'] : array();
							$realInfo = !empty($quote['quote']) ? $quote['quote'] : array();
							
							$quote['quote'] = $stubsInfo;
							
							foreach ($realInfo as $realId => $realInfo) {
								$quote['quote'][$realId] = $realInfo;
							}
							
							$quote_data[$result['code']] = $quote;
						}
					}
				}
			}
			
			$sort_order = array();
			
			foreach ($quote_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}
			
			array_multisort($sort_order, SORT_ASC, $quote_data);
			
			$this->_templateData['shipping_methods']   = $quote_data;
			$this->_templateData['shipping_method']    = null;
			$this->_templateData['error_shipping']     = $this->language->get('error_shipping');
			$this->_templateData['has_error_shipping'] = false;
			
			$this->_templateData['code'] = '';
			$this->_templateData['checked_code'] = '';
			
			if ($this->request->server['REQUEST_METHOD'] == 'POST' && !empty($this->request->post['shipping_method_checked'])) {


		/* Shiptor - агрегатор доставки */

		$this->getShiptorValidate($this->request->post['shipping_method_checked']);

		/* Shiptor - агрегатор доставки */

		
				$shipping = explode('.', $this->request->post['shipping_method_checked']);
				
				if (isset($shipping[1]) && isset($this->_templateData['shipping_methods'][$shipping[0]]['quote'][$shipping[1]]) && empty($this->_templateData['shipping_methods'][$shipping[0]]['quote'][$shipping[1]]['dummy'])) {
					$this->_templateData['checked_code'] = $this->request->post['shipping_method_checked'];
				}
			}
			
			if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['shipping_method'])) {


		/* Shiptor - агрегатор доставки */

		$this->getShiptorValidate($this->request->post['shipping_method']);

		/* Shiptor - агрегатор доставки */

		
				$shipping = explode('.', $this->request->post['shipping_method']);
				
				if (isset($shipping[1]) && isset($this->_templateData['shipping_methods'][$shipping[0]]['quote'][$shipping[1]]) && empty($this->_templateData['shipping_methods'][$shipping[0]]['quote'][$shipping[1]]['dummy'])) {
					$this->_templateData['shipping_method'] = $this->_templateData['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];
					
					if (isset($this->request->post['shipping_method_current']) && $this->request->post['shipping_method_current'] != $this->request->post['shipping_method']) {
						$this->_templateData['checked_code'] = $this->request->post['shipping_method'];
					}
				}
			}
			
			if ($this->request->server['REQUEST_METHOD'] == 'GET' && isset($this->session->data['shipping_method'])) {
				$user_checked = false;
				if (isset($this->session->data['shipping_method'])) {
					$shipping = explode('.', $this->session->data['shipping_method']['code']);
					$user_checked = true;
				}
				
				if (isset($shipping[1]) && isset($this->_templateData['shipping_methods'][$shipping[0]]['quote'][$shipping[1]]) && empty($this->_templateData['shipping_methods'][$shipping[0]]['quote'][$shipping[1]]['dummy'])) {
					$this->_templateData['shipping_method'] = $this->_templateData['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];
					if ($user_checked) {
						$this->_templateData['checked_code'] = $this->session->data['shipping_method']['code'];
					}
				}
			}
			
			$selectFirst = $this->simplecheckout->getSettingValue('selectFirst', 'shipping');
			$hide = $this->simplecheckout->isBlockHidden('shipping');
			
			if ($hide) {
				$selectFirst = true;
			}
			
			if (!empty($this->_templateData['shipping_methods']) && ($hide || ($selectFirst && $this->_templateData['checked_code'] == ''))) {
				$first = false;
				foreach ($this->_templateData['shipping_methods'] as $method) {
					if (!empty($method['quote'])) {
						$first_method = reset($method['quote']);
						
						if (!empty($first_method) && empty($first_method['dummy'])) {
							$this->_templateData['shipping_method'] = $first_method;
							break;
						}
					}
				}
			}
			
			if (!empty($this->_templateData['shipping_method']['code'])) {
				$this->_templateData['code'] = $this->_templateData['shipping_method']['code'];
				} else {
				$this->_templateData['has_error_shipping'] = true;
				$this->simplecheckout->addError('shipping');
			}
			
			
			
			
			
			$this->_templateData['error_recept_shipping'] = false;
			$this->_templateData['error_weight_product'] = false;
			
			
			$products = $this->cart->getProducts();


			
			foreach ($products as $product)
			{
				if ($product['rec_status_id'])
				{
					$this->_templateData['error_recept_shipping']     = 'В Вашей корзине есть строго-рецептурный товар. Доставка товара невозможна! При самовывозе вы обязаны будете предъявить рецепт на официальном бланке.';
					break;
				}
			}





			foreach ($products as $product)
			{

				if ($product['weight'] > '100')
				{
					$this->_templateData['error_weight_product']     =  $this->cart->getProducts();
					if (isset($this->request->post['product_remove'])) {
					$this->cart->remove($this->request->post['product_remove']);
			}
					break;
				}
			}		
			
			
			
			
			
			
			
			
			
			$this->session->data['shipping_methods'] = $this->_templateData['shipping_methods'];
			$this->session->data['shipping_method'] = $this->_templateData['shipping_method'];
			
			if (empty($this->session->data['shipping_methods'])) {
				unset($this->session->data['shipping_method']);
			}
			
			$this->_templateData['rows'] = $this->simplecheckout->getRows('shipping');
			
			
			
			
			
			$products = $this->cart->getProducts();
			
			$variant = 'all_in_apteka';


			
			foreach ($products as $product)
			{
				if ($product['stock_status_id'] < 7)
				{
					$variant = 'some_not_in_apteka';
					break;
				}
			}
			
			
			// Для тестов
			
			// $variant = 'some_not_in_apteka';
			// $variant = 'all_in_apteka';
			
			// По-умолчанию
			
			$start_from_day = 'tomorrow';
			$utr_dostavka_start_from_day = 'tomorrow';
			$day_of_week = date('D');
			
			// Если все товары в аптеке
			if ($variant == 'all_in_apteka')
			{
				// Если заказали до 17:00
				if (!$this->isNowAfterThisTime('17:00'))
				{
					$start_from_day = 'today';
				}
				// Если заказали после 17:00
				else
				{
					$start_from_day = 'tomorrow';
				}
				
				// Утренняя доставка
				// Если заказали до 18:00
				if (!$this->isNowAfterThisTime('18:00'))
				{
					$utr_dostavka_start_from_day = 'tomorrow';
				}
				// Если заказали после 18:00
				else
				{
					$utr_dostavka_start_from_day = 'tomorrow + 1 day';
				}
			}
			
			// Если какие-то товары у поставщиков
			else
			{
				// Если заказали до 14:00
				if (!$this->isNowAfterThisTime('14:00'))
				{
					// Если заказали в пятницу, либо в выходные
					
					
					if (in_array($day_of_week, array('Fri', 'Sat', 'Sun')))
					{
						$start_from_day = 'next Tuesday';
						$utr_dostavka_start_from_day = 'next Wednesday';
						
					}
					// Если заказали в понедельник-четверг
					else
					{
						$start_from_day = 'tomorrow';
						$utr_dostavka_start_from_day = 'tomorrow + 1 day';
					}
				}
				// Если заказали после 14:00
				else
				{
					// Если заказали в пятницу, либо в выходные
					if (in_array($day_of_week, array('Fri', 'Sat', 'Sun')))
					{
						$start_from_day = 'next Tuesday';
						$utr_dostavka_start_from_day = 'next Wednesday';
					}
					// Если заказали в четверг после 14:00
					elseif ($day_of_week == 'Thu')
					{
						$start_from_day = 'next Monday';
						$utr_dostavka_start_from_day = 'next Tuesday';
					}
					// Если заказали в понедельник-среду
					else
					{
						$start_from_day = 'tomorrow + 1 day';
						$utr_dostavka_start_from_day = 'tomorrow + 2 days';
					}
					
				}
				
				
			}
			
			
			
			$start_from_date = date('d.m.Y', strtotime($start_from_day));
			
			// $this->log->write($start_from_date);
			
			
			
			foreach ($this->_templateData['rows'] as &$field_row)
			{
				if ($start_from_date)
				{
					$field_row = str_ireplace('data-start-day="2018-04-01"', 'data-start-day="' . $start_from_date . '"', $field_row);
				}
				else
				{
					$field_row = str_ireplace('data-start-day="2018-04-01"', 'data-start-day=""', $field_row);
				}
			}	
			
			
			
			
			
			
			
			
			
			
			
			// print_r($this->_templateData['rows']);
			
			if (!$this->simplecheckout->validateFields('shipping')) {
				$this->simplecheckout->addError('shipping');
			}
			
			$this->_templateData['display_header']        = $this->simplecheckout->getSettingValue('displayHeader', 'shipping');
			$this->_templateData['display_error']         = $this->simplecheckout->displayError('shipping');
			$this->_templateData['display_address_empty'] = $this->simplecheckout->getSettingValue('displayAddressEmpty', 'shipping');
			$this->_templateData['has_error']             = $this->simplecheckout->hasError('shipping');
			$this->_templateData['hide']                  = $this->simplecheckout->isBlockHidden('shipping');
			
			$this->_templateData['text_checkout_shipping_method'] = $this->language->get('text_checkout_shipping_method');
			$this->_templateData['text_shipping_address']         = $this->language->get('text_shipping_address');
			$this->_templateData['error_no_shipping']             = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));
			$this->_templateData['display_type']                  = $this->simplecheckout->getShippingDisplayType();
			
			$this->setOutputContent($this->renderPage('checkout/simplecheckout_shipping', $this->_templateData));
		}


		/* Shiptor - агрегатор доставки */

		private function getShiptorValidate($shipping_method) {
			$this->language->load('checkout/simplecheckout');

			$this->load->model('shipping/shiptor');

			if ($shipping = $this->model_shipping_shiptor->getValidate($shipping_method)) {
				if ($shipping['shipping'] == 'P') {
					if (! isset($this->session->data['shiptor']['address']['courier_id']) || ! isset($this->session->data['shiptor']['address']['point_id']) || (isset($this->session->data['shiptor']['address']['courier_id']) && $shipping['courier_id'] != $this->session->data['shiptor']['address']['courier_id'])) {
						$this->_templateData['has_error_shipping'] = true;
						$this->_templateData['error_shipping']     = $this->language->get('error_shipping_point');
						$this->simplecheckout->addError('shipping');
					}
				} else if ($shipping['shipping'] == 'A') {	
					unset($this->session->data['shiptor']['address']);
					$this->session->data['shiptor']['address']['courier_id'] = $shipping['courier_id'];
				} else {
					unset($this->session->data['shiptor']);
				}
			} else {
				unset($this->session->data['shiptor']);
			}
		}

		/* Shiptor - агрегатор доставки */

		



		public function get_ajax_cart()
		{
			
			//Add
			if (isset($this->request->post['product_add'])) {
				$this->cart->add($this->request->post['product_add'], 1);
			}
			
			
			
			
			// Remove
			if (isset($this->request->post['product_remove'])) {
				$this->cart->remove($this->request->post['product_remove']);
			}
			

			var_dump($this->request->post['product_remove']);
			
			// Update
			if (!empty($this->request->post['quantity'])) {
				foreach ($this->request->post['quantity'] as $key => $value) {
					$this->cart->update($key, $value);
				}
			}
			
			
			$json = array();
			
			
			$this->load->model('tool/image');
			$this->load->model('tool/upload');
			$this->load->model('catalog/product');
			
			$data['products'] = array();
			
			$products = $this->cart->getProducts();
			if ($products) {
				foreach ($products as $product) {
					$product_total = 0;
					
					foreach ($products as $product_2) {
						if ($product_2['product_id'] == $product['product_id']) {
							$product_total += $product_2['quantity'];
						}
					}
					
					if ($product['minimum'] > $product_total) {
						$data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
					}
					
					if ($product['image']) {
						$image = $this->model_tool_image->resize($product['image'], 180, 180);
						} else {
						$image = '';
					}
					
					$option_data = array();
					
					foreach ($product['option'] as $option) {
						if ($option['type'] != 'file') {
							$value = $option['value'];
							} else {
							$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);
							
							if ($upload_info) {
								$value = $upload_info['name'];
								} else {
								$value = '';
							}
						}
						
						$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
						);
					}
					$product_info = $this->model_catalog_product->getProduct($product['product_id']);
					// Display prices
					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						$unit_price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));
						
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
						$total = $this->currency->format($unit_price * $product['quantity'], $this->session->data['currency']);
						} else {
						$price = false;
						$total = false;
					}
					
					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
						} else {
						$special = false;
					}
					
					$recurring = '';
					
					if ($product['recurring']) {
						$frequencies = array(
						'day'        => $this->language->get('text_day'),
						'week'       => $this->language->get('text_week'),
						'semi_month' => $this->language->get('text_semi_month'),
						'month'      => $this->language->get('text_month'),
						'year'       => $this->language->get('text_year'),
						);
						
						if ($product['recurring']['trial']) {
							$recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
						}
						
						if ($product['recurring']['duration']) {
							$recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
							} else {
							$recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
						}
					}
					
					
					
					$data['products'][] = array(
					'cart_id'   => $product['cart_id'],
					'product_id'   => $product['product_id'],
					'thumb'     => $image,
					'name'      => $product['name'],
					'model'     => $product['model'],
					'option'    => $option_data,
					'recurring' => $recurring,
					'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, 60) . '...',
					'special'    => $special,
					'quantity'  => $product['quantity'],
					'stock'     => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
					'reward'    => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
					'price'     => $price,
					'total'     => $total,
					'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id'])
					);
				}
				
				$files = glob(DIR_APPLICATION . '/controller/total/*.php');
				
		        if ($files) {
					foreach ($files as $file) {
						$extension = basename($file, '.php');
						
						$data[$extension] = $this->load->controller('total/' . $extension);
					}
				}
				
				// Totals
				$this->load->model('extension/extension');
				
				$total_data = array();
				$total = 0;
				$taxes = $this->cart->getTaxes();
				
				// Display prices
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$sort_order = array();
					
					$results = $this->model_extension_extension->getExtensions('total');
					
					foreach ($results as $key => $value) {
						$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
					}
					
					array_multisort($sort_order, SORT_ASC, $results);
					


		/* Shiptor - агрегатор доставки */
		
		if (isset($this->request->post['payment_method'])
		&& isset($this->session->data['payment_methods'][$this->request->post['payment_method']])) {
			$this->session->data['payment_method']['code'] = $this->request->post['payment_method'];
		}
		
		/* Shiptor - агрегатор доставки */

		
					foreach ($results as $result) {
						if ($this->config->get($result['code'] . '_status')) {
							$this->load->model('total/' . $result['code']);
							
							$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
						}
					}
					
					$sort_order = array();
					
					foreach ($total_data as $key => $value) {
						$sort_order[$key] = $value['sort_order'];
					}
					
					array_multisort($sort_order, SORT_ASC, $total_data);
				} 
				
				
				
				$data['totals'] = array();
				
				foreach ($total_data as $total) {
					$data['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'])
					);
				}
				$data['sub_total'] = $this->currency->format($this->cart->getSubTotal(), $this->session->data['currency']);
				$data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
				$data['footer'] = $this->load->controller('common/footer');
				$data['header'] = $this->load->controller('common/header');
				$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($this->cart->getSubTotal(), $this->session->data['currency']));
				$json['html'] = $this->load->view('default/template/checkout/blocks/cart.tpl', $data);
			}
			else
			{ 
				$json['redirect'] = $this->url->link('checkout/cart');
			}
			
			
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
			
		}
		
		
		public function isNowAfterThisTime($test_time)
		{
			$testTimeStamp = strtotime(date('Y-m-d')  ." ". $test_time);
			
			if (time() > $testTimeStamp)
			{
				return true;
			}
			else
			{
				return false;
			}
			
		}
		
	}						