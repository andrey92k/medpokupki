<?php
	// *	@copyright	OPENCART.PRO 2011 - 2015.
	// *	@forum	http://forum.opencart.pro
	// *	@source		See SOURCE.txt for source and other copyright.
	// *	@license	GNU General Public License version 3; see LICENSE.txt
	
	abstract class Controller {
		protected $registry;
		
		public function __construct($registry) {
			$this->registry = $registry;
			
			if( ! empty( $this->request->get['mfp'] ) ) {
				preg_match( '/path\[([^]]*)\]/', $this->request->get['mfp'], $mf_matches );
				
				if( ! empty( $mf_matches[1] ) ) {
					$this->request->get['path'] = $mf_matches[1];
					
					if( isset( $this->request->get['category_id'] ) || ( isset( $this->request->get['route'] ) && in_array( $this->request->get['route'], array( 'product/search', 'product/special', 'product/manufacturer/info' ) ) ) ) {
						$mf_matches = explode( '_', $mf_matches[1] );
						$this->request->get['category_id'] = end( $mf_matches );
					}
				}
				
				unset( $mf_matches );
			}
			
		}
		
		public function __get($key) {
			return $this->registry->get($key);
		}
		
		public function __set($key, $value) {
			$this->registry->set($key, $value);
		}
		
		public function clearMegaFilterCache()
		{
			$dir	= @ opendir( DIR_SYSTEM . 'cache_mfp' );
			$items	= 0;
			
			while( false !== ( $file = readdir( $dir ) ) ) {
				if( strlen( $file ) > 32 && strpos( $file, 'idx.' ) === 0 ) {
					@ unlink( DIR_SYSTEM . 'cache_mfp' . '/' . $file );
					
					$items++;
				}
			}
			
			closedir( $dir );
		}
	}	