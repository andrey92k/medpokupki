<?php
# Разработчик: Билалов Ильсур
# E-mail: bilalovi@gmail.com
# Skype: oc-designer
# ВК: https://vk.com/ocdesign
# Shiptor - Агрегатор служб доставки

class ModelPaymentShiptorPaycard extends Model {
	public function getMethod($address, $total) {
		$this->load->language('payment/shiptor_paycard');

		$method_data = array();

		if (isset($this->session->data['shipping_method']['code'])) {
			$this->load->model('shipping/shiptor');
			
			if ($shipping = $this->model_shipping_shiptor->getValidate($this->session->data['shipping_method']['code'])) {
				if (isset($this->session->data['shiptor']['payment'][$shipping['courier_id']])) {					
					
					// для курьерской доставки (to-door) оплата картами выводится всегда
					
					if (
						($shipping['shipping'] == 'P' || 'to-door' == $this->session->data['shipping_method']['category'])
						&&
						(($this->session->data['shiptor']['payment'][$shipping['courier_id']] === 'all_types') || ($this->session->data['shiptor']['payment'][$shipping['courier_id']] === 'card'))
					) {
						$method_data = array(
							'code'		=> 'shiptor_paycard',
							'title'		=> $this->language->get('text_title'),
							'terms'		=> '',
							'sort_order'	=> $this->config->get('shiptor_paycard_sort_order'),
						);
					}
				}
			}
		}

		return $method_data;
	}
}