<?php
# Разработчик: Билалов Ильсур
# E-mail: bilalovi@gmail.com
# Skype: oc-designer
# ВК: https://vk.com/ocdesign
# Shiptor - Агрегатор служб доставки

class ModelPaymentShiptorPay extends Model {
	public function getMethod($address, $total) {
		$this->load->language('payment/shiptor_pay');

		$method_data = array();

		if (isset($this->session->data['shipping_method']['code'])) {
			$this->load->model('shipping/shiptor');

			if ($shipping = $this->model_shipping_shiptor->getValidate($this->session->data['shipping_method']['code'])) {
				if (isset($this->session->data['shiptor']['payment'][$shipping['courier_id']])) {
					if (
						($shipping['shipping'] == 'A')
						||
						(($this->session->data['shiptor']['payment'][$shipping['courier_id']] === 'all_types') || ($this->session->data['shiptor']['payment'][$shipping['courier_id']] === 'cod'))
					) {
						$method_data = array(
							'code'		=> 'shiptor_pay',
							'title'		=> $this->language->get('text_title'),
							'terms'		=> '',
							'sort_order'	=> $this->config->get('shiptor_pay_sort_order'),
						);
					}
				}
			}
		}

		return $method_data;
	}
}