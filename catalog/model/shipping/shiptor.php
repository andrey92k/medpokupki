<?php
	# Разработчик: Билалов Ильсур
	# E-mail: bilalovi@gmail.com
	# Skype: oc-designer
	# ВК: https://vk.com/ocdesign
	# Shiptor - Агрегатор служб доставки
	
	class ModelShippingShiptor extends Model {
		private $error;
		private $cleanAreas = array('город','республика','область','ао','край','автономный','округ','г.','обл.');
		
		public function __construct($registry) {
			parent::__construct($registry);
			$registry->set('shiptor', new Shiptor($registry));
			
		}
		
		public function getQuote($address) {	
			$this->language->load('shipping/shiptor');
			if (! $this->config->get('shiptor_status') && ! $this->config->get('shiptor_authorization')) {
				return;
			}
			
			if (mb_strtolower($address['city'], 'UTF-8') == 'самара')
			{
				return;
			}	
			
			
			$params = array();
			# Код города отправителя
			if ($this->config->get('shiptor_kladr_id')) {
				$params['kladr_id_from'] = $this->config->get('shiptor_kladr_id');
			}
			
			// Самара
			$params['kladr_id_from'] = 63000001000;
			
			$address_id = 0;
			if ($this->customer->isLogged()) {
				if($address['address_id']==0 && empty($address['city']))return; // selected new address
				if (!empty($address['city'])) {
					$result = $this->getSuggest($address);
					if (isset($result['kladr_id'])) {
						if (! empty($address['address_id'])) {
							$this->setAddressId($address['address_id'], $result['kladr_id']);
						}
						$this->session->data['shiptor']['kladr_id'] = $result['kladr_id'];
					}
				}
			}
			else {
				if (!isset($this->session->data['shiptor']['kladr_id'])) {
					if (!empty($address['city'])) {
						$result = $this->getSuggest($address);
						if (isset($result['kladr_id'])) {
						
							// if (empty($this->session->data['shiptor']['kladr_id']))
								$this->session->data['shiptor']['kladr_id'] = $result['kladr_id'];
						}
					}
				}
			}
			if(isset($this->session->data['shiptor']['kladr_id']))$address_id = $this->session->data['shiptor']['kladr_id'];
			$params['kladr_id']  = $address_id;
			$params['country_code'] = $address['iso_code_2'];
			if(strlen($params['kladr_id'])<=1)return;
			$products = $this->cart->getProducts();
			
			// если товар один, и указаны его размеры - берем их
			// если товар один, и размеров нет - ставим нули, в библиотеке подставятся дефолтные размеры
			// если товаров несколько - пропускаем блок, в библиотеке подставятся дефолтные размеры
			
			if (1 == sizeof($products)) {
				$product = reset($products);
				
				if (1 == $product['quantity']) {
					$params = array_merge($params,$this->length($product));
				}
			}
			
			# Вес, кг
			$weight = $this->weight();
			
			$params['weight'] = $weight;
			
			$shiptor_total_status = $this->config->get('shiptor_total_status');
			
			$total = $this->cart->getTotal();
			
			if ($total) {
				$params['declared_cost'] = $total;
				
				// проверяем модуль "Shiptor - Учет суммы наложенного платежа"
				
				if (!empty($shiptor_total_status)) {
					
					// "учет" включен
					// первый заход - считаем с наложенным платежом (еще нет данных по способу оплаты)
					// второй заход - считаем в зависимости от способа оплаты
					
					if (!isset($this->session->data['payment_method']['code']) || false !== strpos($this->session->data['payment_method']['code'], 'shiptor_')) {
						$params['cod'] = $total;
						} else {
						$params['cod'] = 0;
					}
					} else {
					// "учет" выключен
					// перерасчета при смене способа оплаты не происходит, всегда считаем с наложкой
					
					$params['cod'] = $total;
				}
			}
			
			$results = $this->shiptor->calculateShipping($params);
			$quote_data = array();
			
			if (! empty($results['methods'])) {			
				# Сортируем способы доставки
				$sort_order = array();
				
				foreach ($results['methods'] as $key => $value) {
					$sort_order[$key] = $this->config->get('shiptor_' . $value['method']['id'] . '_sort_order');
				}
				
				array_multisort($sort_order, SORT_ASC, $results['methods']);
				
				
				// echo "<pre>";
				// print_r($results['methods']);
				// echo "</pre>";
				
				foreach ($results['methods'] as $value) {
					if ($value['status'] != 'unavailable') {
						
						$courier_id = $value['method']['id'];
						
						# получаем доступные виды доставок
						if ($shipping_method = $this->getShippingMethod($courier_id)) {
						
							// Иключаем ненужные методы
							if ( !in_array($shipping_method['category'], array('door-to-door', 'door-to-delivery-point')) )
								continue;
							
							$shipping_method['name'] = $shipping_method['simple_name'] ? $shipping_method['simple_name'] : $shipping_method['name'];
							// echo "<pre>";
							// print_r($shipping_method);
							// echo "</pre>";
							
							if (!$this->config->get('shiptor_' . $shipping_method['id'] . '_status')) {
								
								
								$price = $value['cost']['total']['sum'];
								
								$title = $shipping_method['name'];
								
								if ( in_array($shipping_method['category'], array('to-door', 'door-to-door', 'delivery-point-to-door', 'post-office')) ) {
									//if ($price) {
									if ($shipping_method['category'] == 'to-door') {
										$this->session->data['shiptor']['payment'][$shipping_method['id']] = 'all_types'; // курьерская служба - все способы оплаты
										} else {
										$this->session->data['shiptor']['payment'][$shipping_method['id']] = 'cod'; // оплата наличными
									}
									//} else {
									//	unset($this->session->data['shiptor']['payment'][$shipping_method['id']]);
									//}
									
									$courier = false;
									} else {
									if (! isset($this->session->data['shiptor']['payment'][$shipping_method['id']])) {
										unset($this->session->data['shiptor']['payment'][$shipping_method['id']]);									
									}
									
									$courier = true;
								}
								
								
								
								
								
								$quote_data[$shipping_method['courier'] . '-' . $shipping_method['id']] = array(
								'code'         => 'shiptor.' . $shipping_method['courier'] . '-' . $shipping_method['id'],
								'courier'      => $courier,
								'title'        => $title,
								'description'  => '',
								'surcharge'    => 0,								
								'cost'         => $price,
								'tax_class_id' => 0,
								'text'         => $this->currency->format($price, $this->session->data['currency']),
								'category'     => $shipping_method['category'],
								);
								
								
							}
							else
							{
								$price = $surcharge = 0;
								
								//if (! empty($value['cost']['services'])) {
								//	foreach ($value['cost']['services'] as $service) {
								//		if ($service['service'] == 'shipping') {
								//			$price 	   += $service['sum'];
								//		} else {
								//			$surcharge += $service['sum'];
								//		}
								//	}
								//}
								if (! empty($value['cost']['total'])) {
									$price = $value['cost']['total']['sum'];
								}
								
								# Минимальная цена
								if ($this->config->get('shiptor_' . $shipping_method['id'] . '_total_min')) {
									if ($this->cart->getSubTotal() < $this->config->get('shiptor_' . $shipping_method['id'] . '_total_min')) {
										continue;
									}
								}
								
								#Максимальная цена
								if ($this->config->get('shiptor_' . $shipping_method['id'] . '_total_max')) {
									if ($this->cart->getSubTotal() > $this->config->get('shiptor_' . $shipping_method['id'] . '_total_max')) {
										continue;
									}
								}
								
								# Минимальная вес
								if ($this->config->get('shiptor_' . $shipping_method['id'] . '_total_min_weight')) {
									if ($weight < $this->config->get('shiptor_' . $shipping_method['id'] . '_total_min_weight')) {
										continue;
									}
								}
								
								#Максимальная вес
								if ($this->config->get('shiptor_' . $shipping_method['id'] . '_total_max_weight')) {
									if ($weight > $this->config->get('shiptor_' . $shipping_method['id'] . '_total_max_weight')) {
										continue;
									}
								}
								
								# Фиксированная стоимость
								if ($this->config->get('shiptor_' . $shipping_method['id'] . '_fixed')) {
									$price = $this->config->get('shiptor_' . $shipping_method['id'] . '_fixed');
								}
								
								# Доставка в зависимости от веса
								if ($this->config->get('shiptor_' . $shipping_method['id'] . '_weight_price')) {
									$rates = explode(',', $this->config->get('shiptor_' . $shipping_method['id'] . '_weight_price'));
									$rate_price = array();
									foreach ($rates as $rate) {
										$data = explode(':', $rate);
										$rate_price[(int)$data[0]] = (float)$data[1];
									}
									krsort($rate_price);
									foreach ($rate_price as $p=>$rate) {
										if ($p<=$weight) {
											if (isset($rate)) {
												$price = $rate;
											}
											break;
										}
									}
								}
								
								# Наценка фиксированной суммой 
								if ($this->config->get('shiptor_' . $shipping_method['id'] . '_type') == 'F') {
									if ($price && $this->config->get('shiptor_' . $shipping_method['id'] . '_markup')) {
										$price = ($price + $this->config->get('shiptor_' . $shipping_method['id'] . '_markup'));
									}
									
									if ($this->config->get('shiptor_' . $shipping_method['id'] . '_markup_surcharge')) {
										$surcharge = $surcharge + $this->config->get('shiptor_' . $shipping_method['id'] . '_markup_surcharge');
									}
								}
								
								# Наценка в %
								if ($this->config->get('shiptor_' . $shipping_method['id'] . '_type') == 'P') {
									if ($price && $this->config->get('shiptor_' . $shipping_method['id'] . '_markup')) {
										$price = ($price * (($this->config->get('shiptor_' . $shipping_method['id'] . '_markup') / 100) + 1));
									}
									
									if ($this->config->get('shiptor_' . $shipping_method['id'] . '_markup_surcharge')) {
										$surcharge = ($surcharge * (($this->config->get('shiptor_' . $shipping_method['id'] . '_markup_surcharge') / 100) + 1));
									}
								}
								
								# Бесплатная доставка
								if ($this->config->get('shiptor_' . $shipping_method['id'] . '_total_free')) {
									if ($this->cart->getSubTotal() >= $this->config->get('shiptor_' . $shipping_method['id'] . '_total_free')) {
										$price = 0;
									}
								}
								
								# Настройки по регионам
								$regions = $this->config->get('shiptor_regions');
								
								if (is_array($regions)) {
									foreach ($regions as $id) {					
										if ((int)substr($address_id, 0, -9) == (int)$id) {
											if ($region_info = $this->getRegionId($id)) {
												if ($this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_status')) {
													
													if ($this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_hide')) {
														continue 2;
													}
													
													# Минимальная цена
													if ($this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_total_min')) {
														if ($this->cart->getSubTotal() < $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_total_min')) {
															continue 2;
														}
													}
													
													#Максимальная цена
													if ($this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_total_max')) {
														if ($this->cart->getSubTotal() > $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_total_max')) {
															continue 2;
														}
													}												
													
													# Минимальная вес
													if ($this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_total_min_weight')) {
														if ($weight < $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_total_min_weight')) {
															continue 2;
														}
													}
													
													#Максимальная вес
													if ($this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_total_max_weight')) {
														if ($weight > $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_total_max_weight')) {
															continue 2;
														}
													}
													
													# Фиксированная цена
													if ($this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_fixed')) {
														$price = $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_fixed');
													}
													
													# Доставка в зависимости от веса	
													if ($this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_weight_price')) {
														$rates = explode(',', $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_weight_price'));
														$rate_price = array();
														foreach ($rates as $rate) {
															$data = explode(':', $rate);
															$rate_price[(int)$data[0]] = (float)$data[1];
														}
														krsort($rate_price);
														foreach ($rate_price as $p=>$rate) {
															if ($p<=$weight) {
																if (isset($rate)) {
																	$price = $rate;
																}
																break;
															}
														}
													}
													
													# Наценка фиксированной суммой
													if ($this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_type') == 'F') {
														if ($price && $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_markup')) {
															$price = ($price + $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_markup'));
														}
													}
													
													# Наценка в %
													if ($this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_type') == 'P') {
														if ($price && $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_markup')) {
															$price = $price * (($this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_markup') / 100) + 1);
														}
													}
													
													# Бесплатная доставка
													if ($this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_total_free')) {
														if ($this->cart->getSubTotal() >= $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region_info['id'] . '_total_free')) {
															$price = 0;
														}
													}
												}
											}
											
											break;
										}
									}
								}
								
								if ( in_array($shipping_method['category'], array('to-door', 'door-to-door', 'delivery-point-to-door', 'post-office')) ) {
									//if ($price) {
									if ($shipping_method['category'] == 'to-door') {
										$this->session->data['shiptor']['payment'][$shipping_method['id']] = 'all_types'; // курьерская служба - все способы оплаты
										} else {
										$this->session->data['shiptor']['payment'][$shipping_method['id']] = 'cod'; // оплата наличными
									}
									//} else {
									//	unset($this->session->data['shiptor']['payment'][$shipping_method['id']]);
									//}
									
									$courier = false;
									} else {
									if (! isset($this->session->data['shiptor']['payment'][$shipping_method['id']])) {
										unset($this->session->data['shiptor']['payment'][$shipping_method['id']]);									
									}
									
									$courier = true;
								}
								
								# Доставка Shiptor 1 Days
								if (($shipping_method['courier'] == 'shiptor-one-day') && (time() >= strtotime(date($this->config->get('shiptor_' . $shipping_method['id'] . '_time') . ':00'))))  {
									continue;
								}
								
								$title = $shipping_method['name'];
								
								if ($this->config->get('shiptor_days') && $value['days']) {
									$increase_days = (int)$this->config->get('shiptor_increase_days')<0?0:abs($this->config->get('shiptor_increase_days'));
									foreach(explode(' ',$value['days']) as $day){
										if(stristr($day,'-')){
											$few_days = explode('-',$day);
											$few_days[0] = (int)$few_days[0]+$increase_days;
											$few_days[1] = (int)$few_days[1]+$increase_days;
											$value['days'] = $few_days[0].'-'.$few_days[1].' '.$this->plural($few_days[1],$this->language->get('text_one_day'),$this->language->get('text_many_days'),$this->language->get('text_two_days'));
										}
										else{
											if((int)$day>0){
												$day += $increase_days;
												$value['days'] = $day.' '.$this->plural($day,$this->language->get('text_one_day'),$this->language->get('text_many_days'),$this->language->get('text_two_days'));
											}
										}
									}
									$title = sprintf('%s за %s', $shipping_method['name'], $value['days']);								
								}
								
								$comment = '';
								
								if ($this->config->get('shiptor_' . $shipping_method['id'] . '_comment')) {
									if (isset($value['comment'])) {
										$comment = $value['comment'];
									}
								}
								else{
									if ($this->config->get('shiptor_' . $shipping_method['id'] . '_my_comment')) {
										$comment = $this->config->get('shiptor_' . $shipping_method['id'] . '_my_comment');									
									}
								}
								
								if($price<0)$price = 0;
								
								$quote_data[$shipping_method['courier'] . '-' . $shipping_method['id']] = array(
								'code'         => 'shiptor.' . $shipping_method['courier'] . '-' . $shipping_method['id'],
								'courier'      => $courier,
								'title'        => $title,
								'description'  => $comment,
								'surcharge'    => 0,								
								'cost'         => $price + $surcharge,
								'tax_class_id' => 0,
								'text'         => $this->currency->format($price + $surcharge, $this->session->data['currency']),
								'category'     => $shipping_method['category'],
								);
							}
						}
					}	
				}	
			}
			
			$method_data = array();
			
			if ($quote_data) {
				
				//$quote_data_new = $this->optionAddToCostOgCod($quote_data);
				
				$method_data = array('code' => 'shiptor', 'title' => $this->language->get('text_title'), 'quote' => $quote_data, 'sort_order' => $this->config->get('shiptor_sort_order'), 'error' => false);
			}
			
			return $method_data;
		}
        
		private function optionAddToCostOgCod($quote_data) {
			$quote_data_new = array();
			
			foreach ($quote_data as $key => $val) {
				if (isset($val['cost'])) {
					$val['cost'] += $val['surcharge'];
				}
				
				if (isset($val['text'])) {
					$val['text'] += $val['surcharge'];
					$val['text'] .= ' р.';
				}
				
				$quote_data_new[$key] = $val;
			}
			
			return $quote_data_new;
		}
		
		
		// Вычисляем общий вес с учетом дефолтного значения в настройках и единиц измерения у каждого товара
		
		public function weight() {
			
			$weight = 0;
			$weightId = $this->getKgId();
			foreach ($this->cart->getProducts() as $product) {
				if (0 != $product['weight']) {
					if($this->config->get('config_weight_class_id')==$weightId)$weight += $this->weight->convert($product['weight'], $product['weight_class_id'], $weightId);
					else{
						$preWeight = $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
						$weight += $this->weight->convert($preWeight, $this->config->get('config_weight_class_id'), $weightId);
					}
					} else {
					$weight += $product['quantity'] * $this->config->get('shiptor_weight');
				}
			}
			return $weight;
		}
		
		// получение айдишника килограммов в системе
		private function getKgId(){
			$sql = "SELECT weight_class_id FROM " . DB_PREFIX . "weight_class_description 
			WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'
			AND (unit='kg' OR unit='кг')";
			$query = $this->db->query($sql);
			return $query->row['weight_class_id'];
		}
		
		public function length($product) {
			
			$params = array();
			$lengthId = $this->getCmId();
			if($this->config->get('config_length_class_id')==$lengthId){
				$params['length'] = $this->length->convert($product['length'], $product['length_class_id'], $lengthId);
				$params['width']  = $this->length->convert($product['width'],  $product['length_class_id'], $lengthId);
				$params['height'] = $this->length->convert($product['height'], $product['length_class_id'], $lengthId);
			}
			else{
				$preLength = $this->length->convert($product['length'], $product['length_class_id'], $this->config->get('config_length_class_id'));
				$params['length'] = $this->length->convert($preLength, $this->config->get('config_length_class_id'), $lengthId);
				$preWidth = $this->length->convert($product['width'], $product['length_class_id'], $this->config->get('config_length_class_id'));
				$params['width'] = $this->length->convert($preWidth, $this->config->get('config_length_class_id'), $lengthId);
				$preHeight = $this->length->convert($product['height'], $product['length_class_id'], $this->config->get('config_length_class_id'));
				$params['height'] = $this->length->convert($preHeight, $this->config->get('config_length_class_id'), $lengthId);
			}
			return $params;
		}
		
		// получение айдишника сантиметров в системе
		private function getCmId(){
			$sql = "SELECT length_class_id FROM " . DB_PREFIX . "length_class_description 
			WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'
			AND (unit='cm' OR unit='см')";
			$query = $this->db->query($sql);
			return $query->row['length_class_id'];
		}
		
		# получение данных о доставке
		private function getShippingMethod($courier_id) {
			return $this->db->query('SELECT * FROM `' . DB_PREFIX . 'shipping_shiptor_couriers` WHERE `id` = ' . (int)$courier_id)->row;
		}
		
		# получение данных о регионе
		private function getRegionId($id){
			return $this->db->query('SELECT * FROM `' . DB_PREFIX . 'shipping_shiptor_regions` WHERE `id` = "' . $this->db->escape($id) . '" ORDER BY `id` ASC')->row;
		}
		
		# получаем номер Клад, если покупатель зарегестрировался
		private function getAddressId($address_id){
			$customer_id = $this->customer->getId();
			
			$query = $this->db->query('SELECT `kladr_id` FROM `' . DB_PREFIX . 'shipping_shiptor_address` WHERE `address_id` = ' . (int)$address_id . ' AND `customer_id` = ' . (int)$customer_id);
			
			return $query->row ? $query->row['kladr_id'] : 0;
		}
		
		# Сохраняем для покупателя номер кладр, если номер не был найден в базе
		public function setAddressId($address_id, $kladr_id){
			$customer_id = $this->customer->getId();
			
			$this->db->query('INSERT INTO `' . DB_PREFIX . 'shipping_shiptor_address` SET `address_id` = ' . (int)$address_id . ', `customer_id` = ' . (int)$customer_id . ', `kladr_id` = "' . $this->db->escape($kladr_id) . '" ON DUPLICATE KEY UPDATE `kladr_id` = "' . $this->db->escape($kladr_id) . '"');
		}
		
		# проверка типа доставки
		public function getValidate($quote) {
			$shipping_data = array();
			
			$_courier    = substr(strstr($quote, '.'), 0);	
			$_courier_id = substr(strstr($_courier, '-'), 0);
			$sipping     = substr($quote, 0, -strlen($_courier));
			
			if ($sipping == 'shiptor') {
				$courier_id = preg_replace('/[^0-9]/', '', $_courier_id);
				
				$shipping_method = $this->getShippingMethod($courier_id);
				
				if ($shipping_method) {
					$shipping_data['courier']    = $shipping_method['courier'];
					$shipping_data['courier_id'] = $shipping_method['id'];
					
					if (($shipping_method['category'] != 'to-door') && ($shipping_method['category'] != 'door-to-door') && ($shipping_method['category'] != 'delivery-point-to-door') && ($shipping_method['category'] != 'post-office')) {
						$shipping_data['shipping'] = 'P';					
						} else {
						$shipping_data['shipping'] = 'A';
					}
				}
			}
			
			return $shipping_data;
		}
		
		public function getOrder($order_id) {
			return $this->db->query('SELECT * FROM `' . DB_PREFIX . 'shipping_shiptor_orders` WHERE `order_id` = ' . (int)$order_id)->row;
		}
		
		# Записываем данные заказа
		public function setOrder($order_id) {
			
			$weight = $this->weight();
			
			# Адрес пункта самовывоза
			if (isset($this->session->data['shiptor']['address']['point'])) {
				$this->db->query('UPDATE `' . DB_PREFIX . 'order` SET `payment_address_1` =  "' . $this->db->escape($this->session->data['shiptor']['address']['point']) . '", `shipping_address_1` = "' . $this->db->escape($this->session->data['shiptor']['address']['point']) . '" WHERE `order_id` = ' . (int)$order_id);
			}
			
			$sql = 'INSERT INTO `' . DB_PREFIX . 'shipping_shiptor_orders` SET `order_id` = ' . (int)$order_id;
			
			# ID способа доставки
			if (isset($this->session->data['shiptor']['address']['courier_id'])) {
				$sql .= ', `shipping_method` = ' . (int)$this->session->data['shiptor']['address']['courier_id'];
			}
			
			# ID пункта самовывоза
			if (isset($this->session->data['shiptor']['address']['point_id'])) {		
				$sql .= ', `delivery_point` = ' . (int)$this->session->data['shiptor']['address']['point_id'];
			}
			
			# Адрес пункта самовывоза
			if (isset($this->session->data['shiptor']['address']['point'])) {
				$sql .= ', `address` = "' . $this->db->escape($this->session->data['shiptor']['address']['point']) . '"';
			}
			
			# Код КЛАДР населенного пункта, можно получить из справочника населенных пунктов
			if (isset($this->session->data['shiptor']['kladr_id'])) {	
				$sql .= ', `kladr_id` = "' . $this->db->escape($this->session->data['shiptor']['kladr_id']) . '"';
			}
			
			# Время доставки
			if (isset($this->session->data['shiptor']['terms'])) {
				$sql .= ', `time` = ' . (int)$this->session->data['shiptor']['terms'];
			}
			
			# Вес
			if ($weight) {
				$sql .= ', `weight` = ' . (float)$weight;
			}
			
			$sql .= ' ON DUPLICATE KEY UPDATE `order_id` = ' . (int)$order_id;
			
			if (isset($this->session->data['shiptor']['address']['courier_id'])) {
				$sql .= ', `shipping_method` = ' . (int)$this->session->data['shiptor']['address']['courier_id'];
				} else {
				$sql .= ', `shipping_method` = 0';
			}
			
			if (isset($this->session->data['shiptor']['address']['point_id'])) {		
				$sql .= ', `delivery_point` = ' . (int)$this->session->data['shiptor']['address']['point_id'];
				} else {
				$sql .= ', `delivery_point` = 0';
			}
			
			if (isset($this->session->data['shiptor']['address']['point'])) {
				$sql .= ', `address` = "' . $this->db->escape($this->session->data['shiptor']['address']['point']) . '"';
				} else {
				$sql .= ', `address` = ""';
			}
			
			if (isset($this->session->data['shiptor']['kladr_id'])) {	
				$sql .= ', `kladr_id` = "' . $this->db->escape($this->session->data['shiptor']['kladr_id']) . '"';
				} else {
				$sql .= ', `kladr_id` = ""';
			}
			
			if (isset($this->session->data['shiptor']['terms'])) {
				$sql .= ', `time` = ' . (int)$this->session->data['shiptor']['terms'];
				} else {
				$sql .= ', `time` = 0';
			}
			
			if ($weight) {
				$sql .= ', `weight` = ' . (float)$weight;
				} else {
				$sql .= ', `weight` = 0';
			}
			
			$this->db->query($sql);	
		}
		
		// правильное окончание в зависимости от количества
		private function plural($number, $singular, $plural1, $plural2=null){
			
			$number = abs($number); 
			if(!empty($plural2)){
				$p1 = $number%10;
				$p2 = $number%100;
				if($number == 0)
				return $plural1;
				if($p1==1 && !($p2>=11 && $p2<=19))
				return $singular;
				elseif($p1>=2 && $p1<=4 && !($p2>=11 && $p2<=19))
				return $plural2;
				else
				return $plural1;
			}
			else{
				if($number == 1)
				return $singular;
				else
				return $plural1;
			}
		}
		
		// определение населённого пункта из подсказок, смотря на регион/страну
		private function getSuggest($address = array()){
			
			if(!empty($address['zone_id'])){
				$this->load->model('localisation/zone');
				$zone = $this->model_localisation_zone->getZone($address['zone_id']);
				$zone = mb_strtolower($zone['name']);
				$zone = trim(str_replace($this->cleanAreas,'',$zone));
			}
			$results = $this->shiptor->suggestSettlement(array('query' => trim($address['city']),'country_code'=>$address['iso_code_2']));
			$searchCity = strtolower($address['city']);
			$result = false;
			foreach($results as $suggest){
				if($result==false && $searchCity==strtolower($suggest['name'])){
					if($suggest['readable_parents']!=''){
						$area = mb_strtolower($suggest['readable_parents']);
						$area = trim(str_replace($this->cleanAreas,'',$area));
						$area = explode(',',$area);
						
						if(isset($zone) && trim($area[0])==$zone)return $suggest;
						if($result==false && isset($area[1])) if(isset($zone) && trim($area[1])==$zone)return $suggest;
					}
					else {
						if(isset($zone) && trim(mb_strtolower($suggest['name']))==$zone)return $suggest;
					}
				}
			}
			return $result;
		}
	}		