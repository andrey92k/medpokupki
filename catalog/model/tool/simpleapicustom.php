<?php
	/*
		@author Dmitriy Kubarev
		@link   http://www.simpleopencart.com
		@link   http://www.opencart.com/index.php?route=extension/extension/info&extension_id=4811
	*/
	
	class ModelToolSimpleApiCustom extends Model {
		public function example($filterFieldValue) {
			$values = array();
			
			$values[] = array(
            'id'   => 'my_id',
            'text' => 'my_text'
			);
			
			return $values;
		}
		
		public function checkCaptcha($value, $filter) {
			if (isset($this->session->data['captcha']) && $this->session->data['captcha'] != $value) {
				return false;
			}
			
			return true;
		}
		
		public function getYesNo($filter = '') {
			return array(
            array(
			'id'   => '1',
			'text' => $this->language->get('text_yes')
            ),
            array(
			'id'   => '0',
			'text' => $this->language->get('text_no')
            )
			);
		}
		
		
		public function getDeliveryTimes($filter = '') {
			if (!$filter)
			{
				return false;
			}
			
			
			
			$products = $this->cart->getProducts();
			
			$variant = 'all_in_apteka';
			
			foreach ($products as $product)
			{
				if ($product['stock_status_id'] < 7)
				{
					$variant = 'some_not_in_apteka';
					break;
				}
			}
			
			
			// Для тестов
			
			// $variant = 'some_not_in_apteka';
			// $variant = 'all_in_apteka';
			
			
			// Проверяем выбранную дату (сегодня, завтра или позже)
			
			
			
			// Получаем, сегодня четверг, пятница или выходные
			$day_of_week = date('D');
			
			$todayIsThursday = $todayIsFriday = $todayIsWeekend = false;
			
			if ($day_of_week == 'Thu')
			{
				$todayIsThursday = true;
			}
			
			if ($day_of_week == 'Fri')
			{
				$todayIsFriday = true;
			}
			
			if (in_array($day_of_week, array('Sat', 'Sun')))
			{
				$todayIsWeekend = true;
			}
			
			
			
			$selected_date_timestamp = strtotime($filter);
			
			$today = $tomorrow = $after_tomorrow = false;
			// Сегодня
			if ( $selected_date_timestamp == strtotime(date('Y-m-d')) )
			{
				$today = true;
			}
			// Завтра
			elseif ( $selected_date_timestamp == strtotime(date('Y-m-d') . ' + 1 day') )
			{
				$tomorrow = true;
			}
			// Послезавтра и далее
			else
			{
				$after_tomorrow = true;
			}
			
			
			
			// $this->log->write(strtotime(date('Y-m-d') . ' + 1 day'));
			// $this->log->write(strtotime('2018-04-21'));
			
			
			$nextWorkDay = $afterOneWorkDay = $afterTwoWorkDays = false;
			// Следующий рабочий день
			if ( $todayIsFriday || $todayIsWeekend )
			{
				if ( $selected_date_timestamp == strtotime('next Monday') )
				{
					$nextWorkDay = true;
				}
			}
			else
			{
				if ( $selected_date_timestamp == strtotime('tomorrow') )
				{
					$nextWorkDay = true;
				}
			}
			
			
			// Через один рабочий день
			if ( $todayIsFriday || $todayIsWeekend )
			{
				if ( $selected_date_timestamp == strtotime('next Tuesday') )
				{
					$afterOneWorkDay = true;
				}
			}
			elseif ($todayIsThursday)
			{
				if ( $selected_date_timestamp == strtotime('next Monday') )
				{
					$afterOneWorkDay = true;
				}
			}
			else
			{
				if ( $selected_date_timestamp == strtotime('tomorrow + 1 day') )
				{
					$afterOneWorkDay = true;
				}
			}
			
			
			// Через два рабочих дня и далее
			if ( $todayIsThursday )
			{
				if ( $selected_date_timestamp >= strtotime('next Tuesday') )
				{
					$afterTwoWorkDays = true;
				}
			}
			elseif ( $todayIsFriday || $todayIsWeekend )
			{
				if ( $selected_date_timestamp >= strtotime('next Wednesday') )
				{
					$afterTwoWorkDays = true;
				}
			}
			else
			{
				if ( $selected_date_timestamp >= strtotime('tomorrow + 2 days') )
				{
					$afterTwoWorkDays = true;
				}
			}
			

			$enable_utr = $enable_10 = $enable_14 = $enable_18 = false;
			
			// =========================================
			// Если все товары в аптеке
			if ($variant == 'all_in_apteka')
			{
				// Проверяем утреннюю доставку
				{
					// Если доставка завтра и заказано до 18:00
					if ($tomorrow && !$this->isNowAfterThisTime('18:00'))
					{
						$enable_utr = true;
					}
					
					// Если доставка начиная с послезавтра
					if ($after_tomorrow)
					{
						$enable_utr = true;
					}
				}
				
				// Если доставка завтра и заказано до 20:00
				if ($tomorrow && !$this->isNowAfterThisTime('20:00'))
				{
					$enable_10 = true;
				}
				
				// Или если доставка начиная с послезавтра
				if ($after_tomorrow)
				{
					$enable_10 = true;
				}
				
				
				// Если доставка сегодня и заказано до 13:00
				if ($today && !$this->isNowAfterThisTime('13:00'))
				{
					$enable_14 = true;
				}
				
				// Или если доставка начиная с завтра
				if ($tomorrow || $after_tomorrow)
				{
					$enable_14 = true;
				}
				
				
				// Если доставка сегодня и заказано до 17:00
				if ($today && !$this->isNowAfterThisTime('17:00'))
				{
					$enable_18 = true;
				}
				
				// Или если доставка начиная с завтра
				if ($tomorrow || $after_tomorrow)
				{
					$enable_18 = true;
				}
				
				
			}
			// =========================================
			// Если какие-то товары у поставщиков
			else
			{
				
				
				// Проверяем утреннюю доставку
				{
					// Если доставка через рабочий день и заказано до 14:00
					if ($afterOneWorkDay && !$this->isNowAfterThisTime('14:00'))
					{
						$enable_utr = true;
					}
					
					// Если доставка через два рабочих дня или позже
					if ($afterTwoWorkDays)
					{
						$enable_utr = true;
					}
				}
				
				// Если заказ сделан до 14:00
				if (!$this->isNowAfterThisTime('14:00'))
				{
					if ($nextWorkDay)
					{
						$enable_10 = true;
					}
					
					if ($afterOneWorkDay || $afterTwoWorkDays)
					{
						$enable_10 = true;
						$enable_14 = true;
						$enable_18 = true;
					}	
				}
				// Если заказ после 14:00
				else
				{
					if ($afterOneWorkDay)
					{
						$enable_18 = true;
					}
					
					if ($afterTwoWorkDays)
					{
						$enable_10 = true;
						$enable_14 = true;
						$enable_18 = true;
					}
				}
				
				
				
			}
			
			
			$values = array();
			
			if ($enable_utr)
				$values[] =  array(
				'id'   => '1',
				'text' => '07:00-09:00'
				);
			
			if ($enable_10)
				$values[] =  array(
				'id'   => '2',
				'text' => '10:00-14:00'
				);
			
			if ($enable_14)
				$values[] =  array(
				'id'   => '3',
				'text' => '14:00-18:00'
				);
			
			if ($enable_18)
				$values[] =  array(
				'id'   => '4',
				'text' => '18:00-22:00'
				);
			
			
			return $values;
		}
		
		
		
		public function isNowAfterThisTime($test_time)
		{
			$testTimeStamp = strtotime(date('Y-m-d')  ." ". $test_time);
			
			if (time() > $testTimeStamp)
			{
				return true;
			}
			else
			{
				return false;
			}
			
		}
		
		
	}					