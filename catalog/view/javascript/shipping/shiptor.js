var	width  = ($(window).width() * 3 / 4);

$(document).ready(function(){
	$('[name="shipping_method"]').removeAttr('checked');

	if (width < 1024) {
		$('#shiptor-modal .modal-dialog').removeAttr('style');
	}
});

$(window).resize(function() {
	var width  = ($(window).width() * 3 / 4);

	if (width < 1024) {
		$('#shiptor-modal .modal-dialog').removeAttr('style');
	} else {
		$('#shiptor-modal .modal-dialog').css('width', width + 'px');
	}
});

/* действие при выборе метода доставки */
$(document).on('click', '[name="shipping_method"]', function(){
	$(this).prop('checked', true);

	if($(this).hasClass('shiptor-courier')){
		$('[name="shipping_address[address_1]"], [name="address_1"]').val('');
		if($('input[name=address_same]').is(':checked')){
			$('[name="payment_address[address_1]"]').val('');
		}
	}
	
	if(typeof(reloadAll) == 'function'){
		reloadAll()
	}

	Shiptor.points();
});

var shiptorModal = '<div class="modal fade" id="shiptor-modal" tabindex="-1" role="dialog">'
	+ '<div class="modal-dialog modal-lg" style="width: '+ width + 'px">'
	+ '<div class="modal-content">'
	+ '<div class="modal-header">'
	+	'<button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">×</span></button>'
	+	'<h4 class="modal-title"></h4>'
	+ '</div>'
	+ '<div class="modal-body">'
	+ '<div class="container-fluid">'
	+ '<div class="row">'
	+ '<div class="hidden-xs hidden-sm col-md-3" style="overflow-x: hidden; overflow-y: auto; height: 500px;" id="filter_content"></div>'
	+ '<div class="col-sm-12 col-md-9">'
	+ '<div id="shiptor_map" style="height: 500px;"></div>'
	+ '</div></div></div></div></div></div></div>';

var shiptorMap;

var Shiptor = {
	'points': function() {
		$('#shiptor-modal').remove();

		$('body').append(shiptorModal);

		ymaps.ready(init);
	
		function init() {
			shiptorMap = new ymaps.Map('shiptor_map', { center: [55.76, 37.64], controls: ['zoomControl'], zoom: 8 });

			BalloonContentLayout = ymaps.templateLayoutFactory.createClass(
				'<div style="line-height: 170%; margin: 10px;">' 
				+ '<h4>{{properties.address}}</h4>'  
				+ '[if properties.phones]{{properties.phones}}<br />[endif]' 
				+ '[if properties.work]{{properties.work}}<br />[endif]<br />' 
				+ '[if properties.description]<span style="font-size: 11px;">{{properties.description}}</span><br />[endif]'						
				+ '<button id="shiptor_button"> Выбрать </button>' 
				+ '</div>', {
				build: function() {
					BalloonContentLayout.superclass.build.call(this);
					$('#shiptor_button').bind('click', this.onCounterClick);
				},
				clear: function() {
					$('#shiptor_button').unbind('click', this.onCounterClick);
					BalloonContentLayout.superclass.clear.call(this);
					
					$('[name="shipping_address[address_1]"], [name="address_1"]').val(this._data.object.properties.address);
					if($('input[name=address_same]').is(':checked')){
						$('[name="payment_address[address_1]"]').val(this._data.object.properties.address);
					}
	
					$.post('index.php?route=shipping/shiptor/address', { kladr_id: this._data.object.properties.kladr_id, courier_id: this._data.object.properties.courier_id, id: this._data.object.properties.id });
				},
				onCounterClick: function() {
					$('#shiptor-modal').modal('hide');
				}
			});

			objectManager = new ymaps.ObjectManager({clusterize: true, clusterHasBalloon: true});
			objectManager.clusters.options.set('preset', 'islands#redClusterIcons');
			objectManager.objects.options.set({balloonContentLayout: BalloonContentLayout, preset: 'islands#blueIcon'});
			
			shiptorMap.geoObjects.add(objectManager);
			
			/* действие при закрытии всплывающего окна */
			$('#shiptor-modal').on('hidden.bs.modal', function(e) {
				shiptorMap.destroy();
				
				$(this).remove();
				
				if(typeof(reloadAll) == 'function'){
					reloadAll()
				}
			});
			
			$.post('index.php?route=shipping/shiptor/points', { shipping_method: $('[name="shipping_method"]:checked').val() }).done(function(data) {			
				if (data) {
					objectManager.add(data);

					if (data.position) {
						$('#shiptor-modal').modal('show');
						$('#shiptor-modal .modal-title').text($('[name="shipping_method"]:checked').closest('label').text());
						shiptorMap.setCenter([data.position.location[0], data.position.location[1]], data.features.length==1?13:10);
					}
					
					/* создаем макет левого блока со списком адресов ПВЗ */
					var template = ''

					if (data.features) {
						$.each(data.features, function(i, e) {
							template += '<div class="shiptor-filter list-group-item" data-point-id="' + e.properties.id + '" data-location="' + e.geometry.coordinates + '">';
							template += '<div>' + e.properties.address + '</div>';
							template += 'Оплата: ';

							if (e.properties.cod) { 
								template += '<i class="shiptor-money" title="Оплата наличными"></i>'; 
							}
							if (e.properties.card) { 
								template += '<i class="shiptor-card" title="Оплата картой"></i>'; 
							}
							if(e.properties.no_pay){
								template += '<i class="" title="Не принимает платежи">Не принимает платежи</i>';
							}
							template += '</div>';
						});
					}

					$('#filter_content').html(template);

					/* действие по клику на элемент списка ПВЗ */
					$(document).on('click', '.shiptor-filter', function(event) {
						$('.shiptor-filter').removeClass('shiptor-point-active');
						
						$(this).addClass('shiptor-point-active');
						
						var objectId = $(this).attr('data-point-id'), location = $(this).attr('data-location').split(',');

						objectManager.objects.balloon.open(objectId);
						
						if (objectManager.objects.balloon.isOpen(objectId)) {
							shiptorMap.setCenter(location, 13);
						}
					});
					
					/* действие по клику на метку */
					objectManager.objects.events.add('click', function (e) {
						var objectId = e.get('objectId'), button = $('[data-point-id="' + objectId + '"]');
						
						$('.shiptor-filter').removeClass('shiptor-point-active');
						
						button.addClass('shiptor-point-active');

						$('#filter_content').scrollTo(button, 300);
					});
				}
			});
		};
	},

	time: function(terms) {
		var select = $('#shiptor-time').find('select');
		
		$.post('index.php?route=shipping/shiptor/save', { terms: terms }).done(function(j) {
			select.css('border-color', 'green');

			setTimeout(function(){
				select.removeAttr('style');
			}, 500);
		});
	}
}

$(document).on("keyup", '[name="city"], [name="address[city]"], [name="payment_address[city]"], [name="shipping_address[city]"], [name="register[city]"]', function() {
    var e = $(this)
      , t = 0
      , n = 0
      , a = location.search.split("&");
    for (i in a) {
        console.log(a[i]);
        var s = a[i].split("=");
        (s[0] = "address_id") ? address_id = s[1] : address_id = !1
    }
    e.parent().parent().parent().find("select").each(function() {
        -1 != $(this).attr("name").indexOf("zone_id") && 0 == n && (n = $(this)),
        -1 != $(this).attr("name").indexOf("country_id") && 0 == t && (t = $(this).val())
    }),
    e.is("[loading]") || $(this).kladr({
        type: $.kladr.type.city,
        withParents: !0,
        country: t,
        limit: 30,
        sendBefore: function() {
            e.attr("loading", !0)
        },
        receive: function() {
            e.removeAttr("loading")
        },
        select: function(t) {
            0 != n && n.val(t.zone_id),
            $.post("index.php?route=shipping/shiptor/save", {
                kladr_id: t.kladr_id,
                address_id: address_id
            }),
            e.change()
        },
        labelFormat: function(e, t) {
            var n = ""
              , a = e.name.toLowerCase();
            t = t.name.toLowerCase();
            var i = a.indexOf(t);
            return i = i > 0 ? i : 0,
            e.type_short && (n += e.type_short + ". "),
            t.length < e.name.length ? (n += e.name.substr(0, i),
            n += "<strong>" + e.name.substr(i, t.length) + "</strong>",
            n += e.name.substr(i + t.length, e.name.length - t.length - i)) : n += "<strong>" + e.name + "</strong>",
            e.administrative_area && (n += " <small>" + e.readable_parents + ".</small>"),
            n
        }
    })
});
