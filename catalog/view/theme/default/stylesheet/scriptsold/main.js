﻿/*
$(".product_page .quantity-down").click(function () {
    var currentVal = $(this).siblings(".quantity-form").val();
    $(this).siblings(".quantity-form").val(--currentVal);
    if (currentVal == 0) {
        $(this).prop('disabled', true);
    }
});

$(".product_page .quantity-up").click(function () {
    var currentVal = $(this).siblings(".quantity-form").val();
    $(this).siblings(".quantity-form").val(++currentVal);
    if (currentVal != 0) {
        $(this).siblings(".quantity-down").prop('disabled', false);
    }
});
*/
$(function () {
    $(".custom_select > .dropdown-menu > li > a").click(function () {
        $(this).parents(".dropdown").find(".dropdown-toggle > .text").text($(this).text());
        $(this).val($(this).text());
    });
});



$(".bg-item").each(function () {
    var attr = $(this).attr('data-image-src');
    if (typeof attr !== typeof undefined && attr !== false) {
        $(this).css('background-image', 'url(' + attr + ')');
    }
});

$('.scrollbox-container').scrollbox({
    direction: 'h',
    autoPlay: false,
    onMouseOverPause: false,
    listElement: '.scrollbox',
    listItemElement: '.element'
});

$('.scrollbox-slider > .controls > .control.backward').click(function () {
    $(this).parents(".scrollbox-slider").find(".scrollbox-container").trigger('backward');
});

$('.scrollbox-slider > .controls > .control.forward').click(function () {
    $(this).parents(".scrollbox-slider").find(".scrollbox-container").trigger('forward');
});

$(function () {
    $('#homeCarousel').carousel({
        interval: 3000,
        pause: "false"
    });
    $('#playButton').click(function () {
        $('#homeCarousel').carousel('cycle');
    });
    $('#pauseButton').click(function () {
        $('#homeCarousel').carousel('pause');
    });
});



$(function () {
    $(".catalog > .left > .catalog-menu > li > a").click(function () {
        $(this).parent("li").toggleClass("open");
    });
});

$(function () {
    $(".catalog > .left > .catalo_menu_toggle").click(function () {
        $(this).parent(".left").toggleClass("open");
        $(".catalog > .left > .catalog-menu > li").removeClass("open");
    });
});
/*
$(function () {
    $(".products .product > .description > .btn").click(function () {
        $(this).parents(".product").toggleClass("added");
    });
});

$(function () {
    $(".cart_section > table  td> .btn-remove").click(function () {
        $(this).parents("tr").remove();
    });
});


$(function () {
    // Указываем class блока div где будет ползунок.
    $(".slider_price").slider({
        animate: true, // Анимация. true - включить. false - выключить.
        min: 100, // Минимальный интервал диапазона.
        max: 1000000, // Максимальный интервал диапазона.
        range: true, // Включение двойного ползунка. Если место true поставить 'min', то будет один ползунок.
        step: 100, // Шаг ползунка.
        values: [300000, 800000], // Значения для ползунков. Для первого и второго.
        slide: function (event, ui) { // Действия которые будут происходить по перетаскивания ползунка.
            $(".slider_price_forms .left_count > .form-control").html(ui.values[0]); // Значение первого ползунка.
            $(".slider_price_forms .right_count > .form-control").html(ui.values[1]); // Значение второго ползунка.
        }
    });
});*/

$(document).ready(function(){
    $('.go_to').click( function(){ // ловим клик по ссылке с классом go_to
    var scroll_el = $(this).attr('href'); // возьмем содержимое атрибута href, должен быть селектором, т.е. например начинаться с # или .
        if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
        $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 500); // анимируем скроолинг к элементу scroll_el
        }
        return false; // выключаем стандартное действие
    });
});