<?php echo $header; ?>
<div class="inner_page">
  <nav class="bread_crumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                  <? if ($breadcrumb == end($breadcrumbs)) { ?>
                      <span><?php echo $breadcrumb['text']; ?></span>
                  <? } else { ?>
                     <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                  <? } ?>
                <?php } ?>
            </nav>

          <div class="page_title">
                <h1><? echo $heading_title; ?></h1>
            </div>
            <div class="site_tab personal_data">
            <ul class="nav nav-tabs">
                    <li class="active"><a href="index.php?route=account/order">История заказов</a></li>
                    <li><a href="index.php?route=account/simpleedit">Личные данные </a></li>
                    <li><a href="index.php?route=account/wishlist">Отложенные товары</a></li>
                    <!--<li><a href="#tab_4">Просмотренные товары</a></li>-->
                    <li><a href="index.php?route=product/compare">Сравнение</a></li>
                </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <?php echo $content_top; ?>


    <section class="history_block_2">
      <br/>

                            <h4><i>Заказ № <?php echo $order_id; ?> от <?php echo $date_added; ?></i></h4>
                            <div class="cart_section">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Фото</th>
                                            <th>Наименование и описание:</th>
                                            <th>Цена за шт</th>
                                            <th>Количество</th>
                                            <th>Сумма</th>
                                            <th>Повторить</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php foreach ($products as $product) { ?>
                                        <tr>
                                            <td>
                                                <a href="<?php echo $product['href']; ?>" class="img-container bg-item" data-image-src="<?php echo $product['thumb']; ?>"></a>
                                            </td>
                                            <td>
                                                <div class="description">
                                                    <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                                    <p><? echo $product['description']; ?></p>
                                                </div>
                                            </td>
                                            <td>
                                                <strong class="title">Цена за шт</strong>
                                                <div class="price">
                                                  <? if ($product['special']) { ?>
                                                    <strong><? echo $product['special']; ?></strong>
                                                    <i><? echo $product['price']; ?></i>
                                                    <div class="discount"><? echo round((($product['price']-$product['special'])/$product['price'])*100);?>%</div>
                                                  <? } else { ?>
                                                    <strong><? echo $product['price']; ?></strong>
                                                  <? } ?>
                                                    
                                                </div>
                                            </td>
                                            <td>
                                                <strong class="title">Количество</strong>
                                                <div class="text-center"><h6><?php echo $product['quantity']; ?> шт</h6></div>
                                            </td>
                                            <td>
                                                <strong class="title">Сумма</strong>
                                                <div class="price">
                                                    <strong><?php echo $product['total']; ?></strong>
                                                </div>
                                            </td>
                                            <td>
                                              <a href="<?php echo $product['reorder']; ?>" data-toggle="tooltip" title="<?php echo $button_reorder; ?>" class="btn btn-primary"><i class="fa fa-shopping-cart"></i></a>
                                            </td>
                                        </tr>
                                      <? } ?>
                                    </tbody>
                                </table>
                                <div class="actions">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <!--<button class="btn btn-primary">ТОВАРНЫЙ ЧЕК</button>
                                            <button class="btn btn-primary">ПОВТОРИТЬ ЗАКАЗ</button>-->
                                        </div>
                                        <div class="col-md-6">
                                            <?php foreach ($totals as $total) { ?>
                                            <div class="price">
                                                <span><?php echo $total['title']; ?>:</span><strong><?php echo $total['text']; ?></strong>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>


      <?php if ($comment) { ?>
      <table class="table table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-left"><?php echo $text_comment; ?></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-left"><?php echo $comment; ?></td>
          </tr>
        </tbody>
      </table>
      <?php } ?>
      <?php if ($histories) { ?>
      <h3><?php echo $text_history; ?></h3>
      <table class="table table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-left"><?php echo $column_date_added; ?></td>
            <td class="text-left"><?php echo $column_status; ?></td>
            <td class="text-left"><?php echo $column_comment; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($histories as $history) { ?>
          <tr>
            <td class="text-left"><?php echo $history['date_added']; ?></td>
            <td class="text-left"><?php echo $history['status']; ?></td>
            <td class="text-left"><?php echo $history['comment']; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <?php } ?>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>