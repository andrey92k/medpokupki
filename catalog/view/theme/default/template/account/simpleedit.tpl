<?php if (!$ajax && !$popup && !$as_module) { ?>
<?php $simple_page = 'simpleedit'; include $simple_header; ?>
<div class="site_tab personal_data simoleedit">
<ul class="nav nav-tabs">
                    <li><a href="index.php?route=account/order">История заказов</a></li>
                    <li class="active"><a href="index.php?route=account/simpleedit">Личные данные </a></li>
                    <li><a href="index.php?route=account/wishlist">Отложенные товары</a></li>
                    <!--<li><a href="#tab_4">Просмотренные товары</a></li>-->
                    <li><a href="index.php?route=product/compare">Сравнение</a></li>
                </ul>
                <div class="tab-content">
<div class="personal_data_section">
                            <p class="subtitle">Ваши контактные данные, вы можете изменить их (понадобиться потверждение по email)</p>
                            <div class="row">
                                <div class="col-md-8">
<?php } ?>
    <?php if (!$ajax || ($ajax && $popup)) { ?>
    <script type="text/javascript">
    (function($) {
    <?php if (!$popup && !$ajax) { ?>
        $(function(){
    <?php } ?>
            if (typeof Simplepage === "function") {
                var simplepage = new Simplepage({
                    additionalParams: "<?php echo $additional_params ?>",
                    additionalPath: "<?php echo $additional_path ?>",
                    mainUrl: "<?php echo $action; ?>",
                    mainContainer: "#simplepage_form",
                    scrollToError: <?php echo $scroll_to_error ? 1 : 0 ?>,
                    javascriptCallback: function() {<?php echo $javascript_callback ?>}
                });

                simplepage.init();
            }
    <?php if (!$popup && !$ajax) { ?>
        });
    <?php } ?>
    })(jQuery || $);
    </script>
    <?php } ?>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="simplepage_form">
        <div class="simpleregister" id="simpleedit">
            <?php if ($error_warning) { ?>
            <div class="warning alert alert-danger"><?php echo $error_warning; ?></div>
            <?php } ?>
            <div class="simpleregister-block-content">
                <?php foreach ($rows as $row) { ?>
                  <?php echo $row ?>
                <?php } ?>
            </div>
            <div class="col-md-offset-2">
                
                    <a class="button btn-primary button_oc btn" data-onclick="submit" id="simpleregister_button_confirm"><span><?php echo $button_continue; ?></span></a>
                
            </div>
        </div>
        <?php if ($redirect) { ?>
            <script type="text/javascript">
                location = "<?php echo $redirect ?>";
            </script>
        <?php } ?>
    </form>
<?php if (!$ajax && !$popup && !$as_module) { ?>
</div>
<!--
                                <div class="col-md-4">
                                    <div class="discount">
                                        <h4>Личная скидка:</h4>
                                        <p>Сумма покупок :310 838руб. <br />Кол-во товаров: 50 шт. </p>
                                        <div>
                                            <strong>5%</strong><h5>7%</h5><strong>10%</strong>
                                        </div>
                                        <p>до следующей уровня скидок осталось купить на сумму:</p>
                                        <strong>30 000 руб.</strong>
                                    </div>
                                </div>
-->
</div></div></div></div>
<?php include $simple_footer ?>
<?php } ?>