<?php echo $header; ?>
<div class="inner_page">
  <nav class="bread_crumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                  <? if ($breadcrumb == end($breadcrumbs)) { ?>
                      <span><?php echo $breadcrumb['text']; ?></span>
                  <? } else { ?>
                     <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                  <? } ?>
                <?php } ?>
            </nav>

          <div class="page_title">
                <h1><? echo $heading_title; ?></h1>
            </div>
            <div class="site_tab personal_data">
            <ul class="nav nav-tabs">
                    <li class="active"><a href="index.php?route=account/order">История заказов</a></li>
                    <li><a href="index.php?route=account/simpleedit">Личные данные </a></li>
                    <li><a href="index.php?route=account/wishlist">Отложенные товары</a></li>
                    <!--<li><a href="#tab_4">Просмотренные товары</a></li>-->
                    <li><a href="index.php?route=product/compare">Сравнение</a></li>
                </ul>
  <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div class="tab-content"><?php echo $content_top; ?>

      <div role="tabpanel" class="tab-pane active">
                        <section class="history_block_1">
                            <p class="subtitle"><i>После того, как Вы сделаете заказ, здесь будет отображаться вся история Ваших покупок. Вы можете оплатить заказ онлайн.</i></p>

      <?php if ($orders) { ?>
      <div>
        <table>
          <thead>
            <tr>
              <th>№ заказа</th>
              <th>Дата заказа</th>
              <th>Сумма, руб</th>
              
              
              <th>Кол-во товаров</th>
              <th><?php echo $column_status; ?></th>
              
              
              <th>Комментарий</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($orders as $order) { ?>
            <tr>
              <td><a href="<?php echo $order['href']; ?>"><?php echo $order['order_id']; ?></a></td>
              <td><?php echo $order['date_added']; ?></td>
              <td><?php echo $order['total']; ?></td>
              
              
              <td><?php echo $order['products']; ?></td>
              <td><strong><?php echo $order['status']; ?></strong></td>
              
              
              <td><strong class="red"><?php echo $order['comment']; ?></strong></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <div class="text-right"><?php echo $pagination; ?></div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      
      </div></section>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>