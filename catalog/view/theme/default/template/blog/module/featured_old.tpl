    <? if ($module_id == 44) { ?>
      <!--Articles-->
        <section class="section_block articles">
            <div class="section_title text-center">
                <h1><?php echo $heading_title; ?></h1>
                <p>Полезная информация, статьи, обзоры препаратов и товаров для здоровья</p>
            </div>
            <div class="row">
              <?php $count = 0; foreach ($articles as $article) { $count++; ?>
                <? if ($count == 2) { ?>
                  <div class="col-md-4">
                      <article class="banner">
                          <? echo $information_description; ?>
                      </article>
                  </div>
                  <div class="col-md-4">
                      <article>
                          <a href="<?php echo $article['href']; ?>" class="title"><?php echo $article['name']; ?></a>
                          <p><?php echo $article['description']; ?></p>
                          <a href="<?php echo $article['href']; ?>" class="img-container size-16x9 bg-item" data-image-src="<?php echo $article['thumb']; ?>"></a>
                          <a href="<?php echo $article['href']; ?>" class="btn btn-primary btn-outline">Читать статью</a>
                      </article>
                  </div>
                <?php } else { ?>
                  <div class="col-md-4">
                      <article>
                          <a href="<?php echo $article['href']; ?>" class="title"><?php echo $article['name']; ?></a>
                          <p><?php echo $article['description']; ?></p>
                          <a href="<?php echo $article['href']; ?>" class="img-container size-16x9 bg-item" data-image-src="<?php echo $article['thumb']; ?>"></a>
                          <a href="<?php echo $article['href']; ?>" class="btn btn-primary btn-outline">Читать статью</a>
                      </article>
                  </div>
                <? } ?>
              <?php } ?>
            </div>
        </section>
        <!--Articles-->
<? } else { ?>
  <!--News-->
        <section class="section_block articles news">
            <div class="section_title text-center">
                <h1><?php echo $heading_title; ?></h1>
                <p>Самая свежая и актуальная информация о нашем магазине, ассортименте и т.д.</p>
            </div>
            <div class="row">
              <?php foreach ($articles as $article) { ?>
                <div class="col-md-4">
                    <article>
                        <div class="date"><?php echo $article['date_added']; ?></div>
                        <a href="<?php echo $article['href']; ?>" class="title"><?php echo $article['name']; ?></a>
                        <p><?php echo $article['description']; ?></p>
                        <a href="<?php echo $article['href']; ?>" class="btn btn-primary btn-outline" rel="nofollow">Читать статью</a>
                    </article>
                </div>
              <?php } ?>
            </div>
        </section>
        <!--News-->
<? } ?>
