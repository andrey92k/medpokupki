<?php echo $header; ?>

<section class="inner_page">
            <nav class="bread_crumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                  <? if ($breadcrumb == end($breadcrumbs)) { ?>
                      <span><?php echo $breadcrumb['text']; ?></span>
                  <? } else { ?>
                     <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                  <? } ?>
                <?php } ?>
            </nav>
            <div class="page_title">
                <h1><? echo $heading_title; ?></h1>
                <br />
                <?php echo $description; ?>
                
            </div>
            <div class="articles">
                <div class="row">
                    <?php $count = 0; foreach ($articles as $article) { $count++; ?>
                <? if ($count == 2) { ?>
                  <div class="col-md-4">
                      <article class="banner">
                          <a href="#" class="img-container">
                              <img src="catalog/view/theme/default/stylesheet/images/site_img_10.jpg" /></a>
                      </article>
                  </div>
                  <div class="col-md-4">
                      <article>
                          <a href="<?php echo $article['href']; ?>" class="title"><?php echo $article['name']; ?></a>
                          <p><?php echo $article['description']; ?></p>
                          <a href="<?php echo $article['href']; ?>" class="img-container size-16x9 bg-item" data-image-src="<?php echo $article['thumb']; ?>"></a>
                          <a href="<?php echo $article['href']; ?>" class="btn btn-primary btn-outline">Читать статью</a>
                      </article>
                  </div>
                <?php } else { ?>
                  <div class="col-md-4">
                      <article>
                          <a href="<?php echo $article['href']; ?>" class="title"><?php echo $article['name']; ?></a>
                          <p><?php echo $article['description']; ?></p>
                          <img href="<?php echo $article['href']; ?>" class="img-container size-16x9 bg-item" data-image-src="<?php echo $article['thumb']; ?>" />
                          <a href="<?php echo $article['href']; ?>" class="btn btn-primary btn-outline" rel="nofollow">Читать статью</a>
                      </article>
                  </div>
                <? } ?>
              <?php } ?>

                </div>
                <? echo $pagination; ?>
            </div>
        </section>
        <!--Articles-->
<?php echo $footer; ?>