<?php echo $header; ?>
<div class="container page_container inner_page">
  <nav class="bread_crumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                  <? if ($breadcrumb == end($breadcrumbs)) { ?>
                      <span><?php echo $breadcrumb['text']; ?></span>
                  <? } else { ?>
                     <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                  <? } ?>
                <?php } ?>
            </nav>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div class="page_title text-uppercase">
        <h1><? echo $heading_title; ?></h1>
    </div>
    <div class="order_page">
                <div class="row">


      