</div>
    <footer>
        <div class="container">
            <section class="top">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <p></p>
                                <p></p>
                                <span class="titlefour">КАТАЛОГ</span>
                                <ul class="menu">
                                    <?php foreach ($categories as $category) { ?>
                                        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                                    <? } ?>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <p></p>
                                <p></p>
                                <span class="titlefour">МЕНЮ</span>
                                <ul class="menu">
                                    <li><a href="o-nas">О нас</a></li>
                                    <li><a href="dostavka-i-oplata">Доставка и оплата</a></li>
                                    <li><a href="blog">Блог</a></li>
                                    <li><a href="news">Новости</a></li>
                                    <li><a href="kontakti">Контакты</a></li>
                                    <li><a href="index.php?route=account/order">Личный кабинет</a></li>
                                    <li><a href="publichnaja-oferta">Пользовательское соглашение</a></li>
                                    <li><a href="politika-konfidencialnosti">Политика конфиденциальности</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p></p>
                        <p></p>
                        <span class="titlefour">ПРИНИМАЕМ К ОПЛАТЕ</span>
                        <br>
                        <img class="payments" src="catalog/view/theme/default/stylesheet/images/payments.jpg" />
                        <br>
                        <span class="titlefour">МЫ В СОЦСЕТЯХ</span>
                        <div class="socials">
                            <a rel="nofollow" href="https://vk.com/medpokupki">
                                <img src="catalog/view/theme/default/stylesheet/images/vk_icon_1.png"/></a>
                                 <a rel="nofollow" href="https://www.instagram.com/medpokupki/">
                                <img src="catalog/view/theme/default/stylesheet/images/social_icon_2.png"/></a>
                            
                        </div>
                        <!--LiveInternet counter--><script type="text/javascript">
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t57.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,150))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='88' height='31'><\/a>")
</script><!--/LiveInternet-->
<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
var _tmr = window._tmr || (window._tmr = []);
_tmr.push({id: "3050661", type: "pageView", start: (new Date()).getTime()});
(function (d, w, id) {
  if (d.getElementById(id)) return;
  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
  ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window, "topmailru-code");
</script><noscript><div>
<img src="//top-fwz1.mail.ru/counter?id=3050661;js=na" style="border:0;position:absolute;left:-9999px;" alt="" />
</div></noscript>
<!-- //Rating@Mail.ru counter -->
<!-- Rating@Mail.ru logo -->
<a href="https://top.mail.ru/jump?from=3050661">
<img src="//top-fwz1.mail.ru/counter?id=3050661;t=318;l=1" 
style="border:0;" height="15" width="88" alt="Рейтинг@Mail.ru" /></a>
<!-- //Rating@Mail.ru logo -->

<script async="async" src="https://w.uptolike.com/widgets/v1/zp.js?pid=1765999" type="text/javascript"></script>
                    </div>
                </div>
            </section>
            <section class="bottom">
                <p>© 2017 Интернет-магазин “МедПокупки” - аптечный интернет-магазин</p>
                <p><a href="/">Аптека онлайн</a></p>
            </section>
        </div>
    </footer>



    <script src="catalog/view/theme/default/stylesheet/scripts/slider_price.js"></script>
    <script src="catalog/view/theme/default/stylesheet/scripts/jquery.scrollbox.js"></script>
    <script src="catalog/view/theme/default/stylesheet/scripts/jquery.ui.touch-punch.js"></script>
    <script src="catalog/view/theme/default/stylesheet/scripts/main.js"></script>


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'f5ydO2BAPU';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter48413435 = new Ya.Metrika2({
                    id:48413435,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<script>
$(function () { 
    $('header .top nav > a').each(function () {
        var location = window.location.href;
        var link = this.href; 
        if(location == link) {
            $(this).addClass('active');
        }
    });
});
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/48413435" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- BEGIN LEADGENIC CODE {literal} -->
  <!-- Put this script tag before the </body> tag of your page -->
  <script type="text/javascript" async src="https://gate.leadgenic.ru/getscript?site=5b332ce00cf2e5dc32089735"></script>
<!-- {/literal} END LEADGENIC CODE -->

</body></html>