</div>
    <footer>
        <div class="container">
            <section class="top">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo">
                            <img src="catalog/view/theme/default/stylesheet/images/logo-footer.jpg">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3 col-sm-4 col-xs-12 footer-col">
                                <p></p>
                                <p></p>
                                <span class="titlefour">КАТАЛОГ</span>
                                <ul class="menu">
                                    <?php foreach ($categories as $category) { ?>
                                        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                                    <? } ?>
                                </ul>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-12 footer-col">
                                <p></p>
                                <p></p>
                                <span class="titlefour">КЛИЕНТ</span>
                                <ul class="menu">
                                    <li><a href="https://medpokupki.ru/kontakti">Обратная связь</a></li>
                                 <!--  <li><a href="">Отследить свой заказ</a></li>
                                    <li><a href="">Задать вопрос фармацевту</a></li> -->
                                    <li><a href="index.php?route=account/order">Личный кабинет</a></li>
                                    <li><a href="https://med.andreaszak.ru/index.php?route=account/wishlist">Мое избранное</a></li>
                                </ul>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-12 footer-col">
                                <p></p>
                                <p></p>
                                <span class="titlefour">АПТЕКА</span>
                                <ul class="menu">
                                    <li><a href="https://medpokupki.ru/o-nas">О нас</a></li>
                                    <li><a href="https://medpokupki.ru/dostavka-i-oplata">Доставка и оплата</a></li>
                                    <li><a href="https://medpokupki.ru/blog">Блог</a></li>
                                    <li><a href="https://medpokupki.ru/news">Новости</a></li>
                                    <li><a href="https://medpokupki.ru/kontakti">Контакты</a></li>
                                </ul>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-12 footer-col">
                                <p></p>
                                <p></p>
                                <span class="titlefour">КОНТАКТЫ</span>
                                <p class="adress"><b>Адрес:</b> 443124, Самарская обл.,<br> г. Самара, ул. Солнечная, д. 16, офис 2</p>
                                <p><b>8 800 551 4063 - Бесплатный номер по всей России</b></p>
                                <p><b>(846) 219 2737 - Самара</b></p>
                                <div class="social-icons-wrapper">
							       <a href="https://www.instagram.com/medpokupki/"> <img class="icon social-icon social-icon-instagram" src="catalog/view/theme/default/stylesheet/images/social_icon_2.png"></a>
							       <a href="https://vk.com/medpokupki"> <img class="icon social-icon social-icon-vk" src="/catalog/view/theme/default/stylesheet/images/vk_icon_1.png"></a>
							       <br>
							       <!--LiveInternet counter--><script type="text/javascript">
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t26.10;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,150))+";"+Math.random()+
"' alt='' title='LiveInternet: показано число посетителей за"+
" сегодня' "+
"border='0' width='88' height='15'><\/a>")
</script><!--/LiveInternet-->

					            </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <section class="bottom">
                <p>© 2017 Интернет-магазин “МедПокупки” - аптечный интернет-магазин</p>
                <img class="payments" src="image/payment/yandex_money/yamoney.jpg">
            </section>
        </div>
    </footer>



    <script src="catalog/view/theme/default/stylesheet/scripts/slider_price.js"></script>
    <script src="catalog/view/theme/default/stylesheet/scripts/jquery.scrollbox.js"></script>
    <script src="catalog/view/theme/default/stylesheet/scripts/jquery.ui.touch-punch.js"></script>
    <script src="catalog/view/theme/default/stylesheet/scripts/main.js"></script>


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'f5ydO2BAPU';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter48413435 = new Ya.Metrika2({
                    id:48413435,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<script>
$(function () { 
    $('header .top nav > a').each(function () {
        var location = window.location.href;
        var link = this.href; 
        if(location == link) {
            $(this).addClass('active');
        }
    });
});
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/48413435" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- BEGIN LEADGENIC CODE {literal} -->
  <!-- Put this script tag before the </body> tag of your page -->
  <script type="text/javascript" async src="https://gate.leadgenic.ru/getscript?site=5b332ce00cf2e5dc32089735"></script>
<!-- {/literal} END LEADGENIC CODE -->

</body></html>