<div class="row form-group <?php if ($required) { ?>required<?php } ?>">
	<div class="simplecheckout-table-form-left polya">
		<?php if ($required) { ?>
			<span class="simplecheckout-required">*</span>
		<?php } ?>
		<?php echo $label ?>


	</div>

    <div class="korotkie">
		<?php if ($type == 'select') { ?>
			<?php if ($values) { ?>
		
				<select class="form-control" style="text-align:center; max-width: 150px;" name="<?php echo $name ?>" id="<?php echo $id ?>" <?php echo $reload ? 'data-onchange="reloadAll"' : 'data-reload-payment-form="true"'?> style="display:none;">
					<?php foreach ($values as $info) { ?>
						<option value="<?php echo $info['id'] ?>" <?php echo $value == $info['id'] ? 'selected="selected"' : '' ?>><?php echo $info['text'] ?></option>
					<?php } ?>
				</select>
			<?php } ?>
			<?php } elseif ($type == 'radio') { ?>
					<?php 
						if ($values[0]["id"] = "1") { ?>
						<div class="col-sm-4">
						<label>Выбрать:</label>
						</div>
					<?php } ?>

				<?php foreach ($values as $info) { ?>
				<div class="col-sm-4">		
					<div class="radio">
					<label><input type="radio" name="<?php echo $name ?>" id="<?php echo $id ?>" value="<?php echo $info['id'] ?>" <?php echo $value == $info['id'] ? 'checked="checked"' : '' ?> <?php echo $reload ? 'data-onchange="reloadAll"' : 'data-reload-payment-form="true"'?>><?php echo $info['text'] ?></label>
					</div>
				</div>	
					<?php } ?>
				
					<?php } elseif ($type == 'checkbox') { ?>
					<div>
					<?php foreach ($values as $info) { ?>
					
						<div class="checkbox">
						
						<label><input type="checkbox" name="<?php echo $name ?>[<?php echo $info['id'] ?>]" id="<?php echo $id ?>" value="1" <?php echo !empty($value[$info['id']]) ? 'checked="checked"' : '' ?> <?php echo $reload ? 'data-onchange="reloadAll"' : 'data-reload-payment-form="true"'?>><?php echo $info['text'] ?></label>
						<input type="hidden" name="<?php echo $name ?>[<?php echo $info['id'] ?>]" value="0">
						</div>
					<?php } ?>
					</div>
					<?php } elseif ($type == 'textarea') { ?>
					<textarea class="form-control" name="<?php echo $name ?>" id="<?php echo $id ?>" placeholder="<?php echo $placeholder ?>" <?php echo $reload ? 'data-onchange="reloadAll"' : 'data-reload-payment-form="true"'?>><?php echo $value ?></textarea>
					<?php } elseif ($type == 'captcha') { ?>
					<?php if ($site_key) { ?>
						<script src="https://www.google.com/recaptcha/api.js" type="text/javascript"></script>
						<div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
						<?php } else { ?>
						<input type="text" name="<?php echo $name ?>" id="<?php echo $id ?>" value="" placeholder="<?php echo $placeholder ?>" <?php echo $reload ? 'data-onchange="reloadAll"' : 'data-reload-payment-form="true"'?>>
						<div class="simple-captcha-container"><img src="index.php?<?php echo $additional_path ?>route=common/simple_connector/captcha&t=<?php echo time() ?>" alt="" id="captcha" /></div>
					<?php } ?>
					<?php } elseif ($type == 'file') { ?>
					<input type="button" value="<?php echo $button_upload; ?>" data-file="<?php echo $id ?>" class="button form-control">
					<div id="text_<?php echo $id ?>" style="margin-top:3px;max-width:200px;"><?php echo $filename ?></div>
					<input type="hidden" name="<?php echo $name ?>" id="<?php echo $id ?>" value="<?php echo $value ?>">
					<?php } elseif ($type == 'date') { ?>
					<div class="input-group date" style="text-align:center; max-width: 150px; position:relative;" id="dostavka_date">
					<input class="form-control" type="text" name="<?php echo $name ?>" id="<?php echo $id ?>" value="<?php echo $value ?>" placeholder="<?php echo $placeholder ?>" <?php echo $attrs ?> <?php echo $reload ? 'data-onchange="reloadAll"' : 'data-reload-payment-form="true"'?> readonly="readonly" style="background:white;">
					<!--<span class="input-group-btn">
					<button type="button" class="btn btn-default ui-datepicker-trigger"><i class="fa fa-calendar"></i></button>
					</span>-->
					</div>
					<?php } elseif ($type == 'time') { ?>
					<div class="input-group time">
					<input class="form-control" type="text" name="<?php echo $name ?>" id="<?php echo $id ?>" value="<?php echo $value ?>" placeholder="<?php echo $placeholder ?>" <?php echo $attrs ?> <?php echo $reload ? 'data-onchange="reloadAll"' : 'data-reload-payment-form="true"'?>>
					<span class="input-group-btn">
					<button type="button" class="btn btn-default"><i class="fa fa-clock-o"></i></button>
					</span>
					</div>
					<?php } else { ?>
							<div class="left-colum-checkout col-sm-4">
					<?php if ($required) { ?>
								<span class="simplecheckout-required">*</span>
					<?php } ?>

					<label><?php echo $label ?></label>
							</div>
					<!-- <div class="right-colum-checkout"> -->
					<input  class="col-sm-4  form-control" type="<?php echo $type ?>" name="<?php echo $name ?>" id="<?php echo $id ?>" value="<?php echo $value ?>" placeholder="<?php echo $placeholder ?>" <?php echo $attrs ?> <?php echo $reload ? 'data-onchange="reloadAll"' : 'data-reload-payment-form="true"'?>>
				<?php } ?>
					<!-- </div> -->
					<div class="col-sm-4">
				<?php if (!empty($rules)) { ?>
					<div class="simplecheckout-rule-group" data-for="<?php echo $id ?>">
					<?php foreach ($rules as $rule) { ?>
						<div <?php echo $rule['display'] && !$rule['passed'] ? '' : 'style="display:none;"' ?> data-for="<?php echo $id ?>" data-rule="<?php echo $rule['id'] ?>" class="simplecheckout-error-text simplecheckout-rule" <?php echo $rule['attrs'] ?>><?php echo $rule['text'] ?></div>
					<?php } ?>
					</div>
					
				<?php } ?>
					</div>
				<?php if ($description) { ?>
					<div class="simplecheckout-tooltip" data-for="<?php echo $id ?>"><?php echo $description ?></div>
				<?php } ?>
				</div>
			</div>			