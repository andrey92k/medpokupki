<?php
	
	$last_modified_time = getlastmod();
	header("Cache-Control: public");
	header("Expires: " . date("r", time()+1080000));
	
	if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= $last_modified_time){
		header('HTTP/1.1 304 Not Modified');
		die; /* убили всё, что ниже */
	}
	header('Last-Modified: '.gmdate('D, d M Y H:i:s', $last_modified_time).' GMT');
	
?>
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
	
	<!--<![endif]-->
	<head>
		<meta charset="UTF-8" />
		<meta name="yandex-verification" content="853df9cc2aaddbf4" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $title; ?></title>
		<?php if ($robots) { ?>
			<meta name="robots" content="<?php echo $robots; ?>" />
		<?php } ?>
		<base href="<?php echo $base; ?>" />
		<?php if ($description) { ?>
			<meta name="description" content="<?php echo $description; ?>" />
		<?php } ?>
		<?php if ($keywords) { ?>
			<meta name="keywords" content= "<?php echo $keywords; ?>" />
		<?php } ?>
		
		<!-- Bootstrap -->
		<link href="catalog/view/theme/default/stylesheet/bootstrap-3.3.6/css/bootstrap.min.css" rel="stylesheet" />
		<!-- Bootstrap -->
		<!-- Font-awesome -->
		<link href="catalog/view/theme/default/stylesheet/font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet" />
		<link href="catalog/view/theme/default/stylesheet/icomoon/style.css" rel="stylesheet" />
		<link href="catalog/view/theme/default/stylesheet/styles/jquery-scrollbox-master.css" rel="stylesheet" />
		<!-- Font-awesome -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
		<!-- Site styles -->
		<link href="catalog/view/theme/default/stylesheet/styles/style.css" rel="stylesheet" />
		<link href="catalog/view/theme/default/stylesheet/styles/jquery-scrollbox-master.css" rel="stylesheet" />
		<link href="catalog/view/theme/default/stylesheet/styles/site_components.css" rel="stylesheet" />
		<link href="catalog/view/theme/default/stylesheet/styles/header.css" rel="stylesheet" />
		<link href="catalog/view/theme/default/stylesheet/styles/footer.css" rel="stylesheet" />
		<link href="catalog/view/theme/default/stylesheet/styles/add.css" rel="stylesheet" />
		<link rel="stylesheet" href="callme/callme.css" type="text/css">
		
		
		<link rel="stylesheet" href="catalog/view/javascript/jquery/ui/jquery-ui.min.css" type="text/css">
		
		<script src="catalog/view/theme/default/stylesheet/scripts/jquery-1.12.3.min.js"></script>
		
		<script src="catalog/view/javascript/jquery/ui/jquery-ui.min.js"></script>
		
		
		<script src="catalog/view/theme/default/stylesheet/bootstrap-3.3.6/js/bootstrap.min.js"></script> 
		<script type="text/javascript" src="callme/jquery.storage.js"></script>
		<script type="text/javascript" src="callme/callme.js"></script>
		<!-- Site styles -->
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="scripts/respond.min.js"></script>
			<script src="scripts/html5shiv.min.js"></script>
		<![endif]-->
		
		<?php foreach ($styles as $style) { ?>
			<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
		<?php } ?>
		<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
		<?php foreach ($links as $link) { ?>
			<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
		<?php } ?>
		<?php foreach ($scripts as $script) { ?>
			<script src="<?php echo $script; ?>" type="text/javascript"></script>
		<?php } ?>
		<?php foreach ($analytics as $analytic) { ?>
			<?php echo $analytic; ?>
		<?php } ?>
		<meta name="yandex-verification" content="b887c1070cdc7910" />
	</head>
	<body class="<?php echo $class; ?>">
		<div class="container page_container">
			<!--header-->
			<header>
				<section class="top">
					<div class="row">
						<div class="col-md-3">
							<a href="/" class="logo">
								<img src="catalog/view/theme/default/stylesheet/images/logo.png" />
							</a>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-sm-3 geo-ip-module"><?php echo $geoip; ?></div>
								<div class="col-sm-9">
									<nav class="clearfix">
										<a href="o-nas">О нас</a>
										<a href="dostavka-i-oplata">Доставки и оплата</a>
										<a href="blog">Блог</a>
										<a href="news">Новости</a>
										<a href="kontakti">Контакты</a>
										<a class="dropdown" href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-head"></i><?php echo $text_account; ?> <span class="caret"></span></a>
										<ul class="dropdown-menu dropdown-menu-right">
											<?php if ($logged) { ?>
												<li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
												<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
												<li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
												<?php } else { ?>
												<li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
												<li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
											<?php } ?>
										</ul>
									</nav>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
									<div class="phone">
										<h4>8 800 551 4063</h4>
										<p>Бесплатно по России</p>
									</div>
								</div>
								<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
									<div class="phone">
										<h4>+7 846 219 2737</h4>
										<div class="jivo-btn jivo-online-btn jivo-btn-dark" onclick="jivo_api.open();" style="font-family: Helvetica, Arial;font-size: 12px;background-color: #75e8f0;border-radius: 11px;-moz-border-radius: 11px;-webkit-border-radius: 11px;height: 28px;line-height: 28px;padding: 0 14px 0 14px;font-weight: normal;font-style: normal"><div class="jivo-btn-icon" style="background-image: url(//static.jivosite.com/button/chat_dark.png);"></div>Чат с фармацевтом</div><div class="jivo-btn jivo-offline-btn jivo-btn-dark" onclick="jivo_api.open();" style="font-family: Helvetica, Arial;font-size: 12px;background-color: #5dbece;border-radius: 11px;-moz-border-radius: 11px;-webkit-border-radius: 11px;height: 28px;line-height: 28px;padding: 0 14px 0 14px;display: none;font-weight: normal;font-style: normal"><div class="jivo-btn-icon" style="background-image: url(//static.jivosite.com/button/mail_dark.png);"></div>Оставьте сообщение!</div>
										
									</div>
								</div>
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-6">
									<div class="cart">                                 
                                        <a class="title clearfix" href="index.php?route=checkout/cart">
                                            <img src="catalog/view/theme/default/stylesheet/images/cart_icon.png" />
											
											<?php echo $cart; ?>
											
										</a>
										<a href="index.php?route=checkout/cart" class="btn btn-primary text-uppercase">Оформить заказ</a>
									</div>
								</div>
							</div>
							</div>
						</div>
						
						
						
					</section>
					<nav class="catalog">
						<?php if ($categories) { ?>
							<div class="left">
								<div class="catalog_menu_toggle"><i class="fa fa-bars"></i>Каталог товаров<i class="fa fa-angle-down"></i></div>
								<ul class="catalog-menu">
									<span><?php foreach ($categories as $category) { ?>
										<?php if ($category['children']) { ?></span>
										<li>
											<a <? if ($mobile == '0') { ?>href="<?php echo $category['href']; ?>"<? } ?>><?php echo $category['name']; ?></a>
											<ul class="menu">
												<?php foreach ($category['children'] as $child) { ?>
													<li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
												<?php } ?>
											</ul>
										</li>
										<? } else { ?>
										<li>
											<a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
										</li>
									<? } ?>
									<? } ?>
								</ul>
							</div>
						<? } ?>
						<div class="search">
							<?php echo $search; ?>
						</div>
					</nav>
				</header>
				<!--header-->
						