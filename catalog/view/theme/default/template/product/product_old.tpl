<?php echo $header; ?>


<section class="inner_page" itemscope itemtype="http://schema.org/Product">
            <nav class="bread_crumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                  <? if ($breadcrumb == end($breadcrumbs)) { ?>
                      <span><?php echo $breadcrumb['text']; ?></span>
                  <? } else { ?>
                     <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                  <? } ?>
                <?php } ?>
            </nav>
            <div class="page_title text-uppercase">
                <h1 itemprop="name"><? echo $heading_title; ?></h1>
            </div>
            <div class="product_page">
                <div class="row">
                    <div class="col-md-6">
                        <div class="left_info">
                            <div id="product_slider" class="carousel slide">
                                <ol class="carousel-indicators">
                                    <li data-target="#product_slider" data-slide-to="0" class="active"><a class="img-container bg-item" data-image-src="<?php echo $small; ?>"></a></li>
                                    <? $count = 0; foreach ($images as $image) { $count++; ?>
                                    <li data-target="#product_slider" data-slide-to="<? echo $count; ?>"><a class="img-container bg-item" data-image-src="<? echo $image['thumb']; ?>"></a></li>
                                    <? } ?>
                                   
                                </ol>
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <a class="img-container size-1x1 bg-item" data-image-src="<?php echo $thumb; ?>"></a>
                                    </div>
                                    <? foreach ($images as $image) { ?>
                                    <div class="item">
                                        <a class="img-container size-1x1 bg-item" data-image-src="<? echo $image['big']?>"></a>
                                    </div>
                                    <? } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="right_info">
                            <div class="content">
                                <div class="rating clearfix">
                                  <?php if ($review_status) { ?>
                                  <div class="stars">
                                      <?php for ($i = 1; $i <= 5; $i++) { ?>
                                      <?php if ($rating < $i) { ?>
                                      <i class="fa fa-star pusto"></i>
                                      <?php } else { ?>
                                      <i class="fa fa-star active"></i>
                                      <?php } ?>
                                      <?php } ?>
                                  </div>
                                      <a class="testimonials go_to" href="#review" onclick="$('a[href=\'#tab4\']').trigger('click'); return false;"><?php echo $reviews; ?></a>
                                  <?php } ?>
                                    <?php if($manufacturers) { ?>
                                    <a href="<?php echo $manufacturers; ?>">Все товары бренда (<? echo $product_total; ?>)</a>
                                    <? } ?>
                                </div>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-6">
                                      <?php if ($price) { ?>
                                      <div class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                        <?php if (!$special) { ?>
                                          <strong itemprop="price"><?php echo $price; ?></strong>
										  <meta itemprop="priceCurrency" content="RUB" />
                                        <?php } else { ?>
                                            <strong><?php echo $special; ?></strong>
                                            <div class="discount"><? echo round((($price-$special)/$price)*100);?>%</div>
                                            <br />
                                            <i><?php echo $price; ?></i>
                                        <?php } ?> 
                                      </div>
                                      <?php } ?>
                                    </div>
                                    <div class="col-md-6">
                                      <? if ($thumbmanuf) { ?>
                                        <img class="brand" src="<? echo $thumbmanuf; ?>" />
                                      <? } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="quantity_count">
                                    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                                    <button class="btn quantity-down" type="button"><i class="fa fa-minus"></i></button>
                                    <input type="text" name="quantity" value="<?php echo $minimum; ?>" id="input-quantity" class="form-control quantity-form" />
                                    <button class="btn quantity-up" type="button"><i class="fa fa-plus"></i></button>
                                </div>
                                <button id="button-cart" class="btn btn-primary">В корзину</button>
                            </div>

                            <div class="content">
                                <div class="actions">
                                    <label>
                                        <input id="comparecheck" onclick="compare.add('<?php echo $product_id; ?>');" type="checkbox" <? if($in_compare == 1) { ?>checked="checked" disabled="disabled"<? } ?>/>
                                        <? if($in_compare == 1) { ?>
                                          <a href="index.php?route=product/compare">В сравнение</a>
                                        <? } else { ?>
                                          <span>В сравнение</span>
                                        <? } ?>
                                        
                                    </label>
                                    <?php echo $file_to_product; ?>
                                </div>
                            </div>

                            <div class="content">
                                <p><? echo $kratkoeopis; ?></p>
                                <div class="info_table">
                                    <? if ($naznachenie) { ?>
                                    <div class="row">
                                        <div class="col-xs-5 column"><span>Назначение</span></div>
                                        <div class="col-xs-7 column"><? echo $naznachenie; ?></div>
                                    </div>
                                    <? } ?>
                                    <div class="row">
                                        <div class="col-xs-5 column"><span>Есть штук</span></div>
                                        <div class="col-xs-7 column"><? echo $quantity; ?></div>
                                    </div>
                                    <? if ($strana) { ?>
                                    <div class="row">
                                        <div class="col-xs-5 column"><span>Производитель</span></div>
                                        <div class="col-xs-7 column"><? echo $strana; ?></div>
                                    </div>
                                    <? } ?>
                                    <? if ($stock_status_id == 7) { ?>
                                    <div class="row">
                                        <div class="col-xs-5 column"><span>Наличие</span></div>
                                        <div class="col-xs-7 column">В наличии.</div>
                                    </div>
                                    <? } else if ($stock_status_id == 6) { ?>
                                    <div class="row">
                                        <div class="col-xs-5 column"><span>Наличие</span></div>
                                        <div class="col-xs-7 column">1-2 рабочих дня.</div>
                                    </div>
                                    <? } ?>
									<? if ($rec_status_id == 1) { ?>
                                    <div class="row">
                                        <div id="BusinessCard_BusinessCardControl_OgrnPanel" class="g-row t-line"><p><span style="color: rgb(255, 0, 0);"><span style="background-color: inherit;"><span style="text-decoration: underline;"><span style="font-weight: bold;">Это строго-рецептурный товар. Доставка невозможна. Выдача только при предъявлении рецепта на официальном бланке <a href="http://medpokupki.ru/image/catalog/1071ysample.PNG" target="_blank">по форме №107-1/у</a>. <br>Рецепт остается в аптеке.</span></span></span></span></p>
                                    </div>
                                    <? } else if ($stock_status_id == 0) { ?>
                                    <div class="row">
                                        <div class="col-xs-5 column"><span>Рецепт:</span></div>
                                        <div class="col-xs-7 column">БЛАБЛАБЛА</div>
                                    </div>
                                    <? } ?>
									
									
				
									
									
                                   <? if ($descriptionprov) { ?>
                                    <a class="go_to" href="#instruction" onclick="$('a[href=\'#tab1\']').trigger('click'); return false;">Читать полностью</a>
                                    <? } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <? if ($products) { ?>
                <section class="section_block products">
                    <div class="section_title">
                        <h1>БЛИЖАЙШИЕ АНАЛОГИ</h1>
                    </div>
                    <div class="scrollbox-slider">
                        <?php $count = 0; foreach ($products as $product) { $count++; } ?>
                        <? if ($count > 4) { ?>
                        <div class="controls">
                            <button class="control btn backward" id="scrollbox-slider-backward"><i class="fa fa-angle-left"></i></button>
                            <button class="control btn forward" id="scrollbox-slider-forward"><i class="fa fa-angle-right"></i></button>
                        </div>
                        <? } ?>
                        <div class="scrollbox-container ">
                            <div class="scrollbox">
                                <?php foreach ($products as $product) { ?>
                                  <div class="element">
                                      <div class="product <? if ($product['cart_id']) { ?>added<? } ?>">
									  
									  <?php if ($product['stock_status'] == 'В наличии в аптеке') { ?>
									<div style="position: absolute;top: 0;left: 0;z-index: 2;text-align: right;" class="in_stock_sticker">
										<img src="catalog/view/theme/default/stylesheet/images/in_stock.png" />
									</div>
								<? } ?>
								
                                          <?php if ($product['special']) { ?>
                                            <div class="action">
                                              <div class="discount"><? echo round((($product['price']-$product['special'])/$product['price'])*100);?>%</div>
                                            </div>
                                          <? } ?>
                                          <a href="<?php echo $product['href']; ?>" class="img-container bg-item" data-image-src="<?php echo $product['thumb']; ?>"></a>
                                          <div class="description">
                                              <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                              <p><?php echo $product['description']; ?></p>
                                              <?php if ($product['price']) { ?>
                                              <div class="price">
                                                <?php if (!$product['special']) { ?>
                                                <strong><?php echo $product['price']; ?></strong>
                                                <?php } else { ?>
                                                <strong><?php echo $product['special']; ?></strong> <i><?php echo $product['price']; ?></i>
                                                <?php } ?>
                                              </div>
                                              <?php } ?>
                                              <button onclick="
                                              cart.add('<?php echo $product['product_id']; ?>'); 
                                              $('.quantity-<?php echo $product['product_id']; ?>').val('1'); 
                                              $('.quantity-<?php echo $product['product_id']; ?>').parent().parent().show(); 
                                              $('.quantity-<?php echo $product['product_id']; ?>').parent().parent().prev().hide(); 
                                              $('.quantity-<?php echo $product['product_id']; ?>').parent().parent().addClass('cartid');
                                              $('.quantity-<?php echo $product['product_id']; ?>').parent().parent().parent().parent().addClass('added');
                                              " class="btn btn-primary text-uppercase" <? if ($product['cart_id']) { ?>style="display:none;"<? } ?>>В корзину</button>

                                              
                                              <div class="added" <? if ($product['cart_id']) { ?>style="display:block;"<? } ?>>
                                                  <p>Добавлен <a href="index.php?route=checkout/cart">в корзину</a></p>
                                                  <div class="quantity_count">
                                                      <button class="btn quantity-down" type="button"><i class="fa fa-minus"></i></button>
                                                      <input cartid="<?php echo $product['cart_id']; ?>" prodid="quantity-<?php echo $product['product_id']; ?>" disabled="disabled" type="text" name="quantity" class="form-control quantity-form quantity-<?php echo $product['product_id']; ?>" value="<?php echo $product['quan']; ?>">
                                                      <button class="btn quantity-up" type="button"><i class="fa fa-plus"></i></button>
                                                      <button onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="btn btn-like"><i class="icon-heart2"></i></button>
                                                  </div>
                                              </div>
                                              
                                          </div>
                                      </div>
                                  </div>
                                  <? } ?>
                                

                            </div>
                        </div>
                    </div>
                </section>
                <? } ?>
                <section class="product_details">
                    <div class="site_tab">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <? if ($descriptionprov) { ?>
                            <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Описание </a></li>
                            <? } ?>
                            <?php if ($attribute_groups) { ?>
                            <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Характеристики</a></li>
                            <? } ?>
                            <? if ($instrukprov) { ?>
                            <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Инструкция по применению</a></li>
                            <? } ?>
                            <li role="presentation" <? if (!$descriptionprov) { ?> class="active"<? } ?>><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">Отзывы</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <? if ($descriptionprov) { ?>
                            <div role="tabpanel" class="tab-pane active" id="tab1">
                                <div class="row">
                                    <div class="col-sm-8 col-lg-9">
                                        <div class="instruction" id="instruction">
                                            <span itemprop="description"> <? echo $description; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-lg-3">
                                        <? echo $column_right; ?>
                                    </div>
                                </div>
                            </div>
                            <? } ?>

                            <?php if ($attribute_groups) { ?>
                            <div role="tabpanel" class="tab-pane" id="tab2">
                              
                              <div class="tab-pane" id="tab-specification">
                                <table class="table table-bordered">
                                  <?php foreach ($attribute_groups as $attribute_group) { ?>
                                  <thead>
                                    <tr>
                                      <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                    <tr>
                                      <td><?php echo $attribute['name']; ?></td>
                                      <td><?php echo $attribute['text']; ?></td>
                                    </tr>
                                    <?php } ?>
                                  </tbody>
                                  <?php } ?>
                                </table>
                              </div>
                              
                            </div>
                            <?php } ?>
                            <? if ($instrukprov) { ?>
                            <div role="tabpanel" class="tab-pane" id="tab3"><? echo $instruk; ?></div>
                            <?php } ?>
                            <div role="tabpanel" class="tab-pane<? if (!$descriptionprov) { ?> active<? } ?>" id="tab4">
                                <?php if ($review_status) { ?>
                                <div class="tab-pane" id="tab-review">
                                  <form class="form-horizontal" id="form-review">
                                    <div id="review"></div>
                                    <h2><?php echo $text_write; ?></h2>
                                    <?php if ($review_guest) { ?>
                                    <div class="form-group required">
                                      <div class="col-sm-12">
                                        <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                                        <input type="text" name="name" value="" id="input-name" class="form-control" />
                                      </div>
                                    </div>
                                    <div class="form-group required">
                                      <div class="col-sm-12">
                                        <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                                        <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                                        <div class="help-block"><?php echo $text_note; ?></div>
                                      </div>
                                    </div>
                                    <div class="form-group required">
                                      <div class="col-sm-12">
                                        <label class="control-label"><?php echo $entry_rating; ?></label>
                                        &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                                        <input type="radio" name="rating" value="1" />
                                        &nbsp;
                                        <input type="radio" name="rating" value="2" />
                                        &nbsp;
                                        <input type="radio" name="rating" value="3" />
                                        &nbsp;
                                        <input type="radio" name="rating" value="4" />
                                        &nbsp;
                                        <input type="radio" name="rating" value="5" />
                                        &nbsp;<?php echo $entry_good; ?></div>
                                    </div>
                                    <?php echo $captcha; ?>
                                    <div class="buttons clearfix">
                                      <div class="pull-right">
                                        <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                                      </div>
                                    </div>
                                    <?php } else { ?>
                                    <?php echo $text_login; ?>
                                    <?php } ?>
                                  </form>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </section>


<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('.right_info input[type=\'text\'], .right_info input[type=\'hidden\']'),
		dataType: 'json',
		beforeSend: function() {
			//$('#button-cart').button('loading');
		},
		complete: function() {
			//$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
				$('.bread_crumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('.cart .text').html(json['total']);

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > ul').load('index.php?route=common/cart/info ul li');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script>

<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			//$('#button-review').button('loading');
		},
		complete: function() {
			//$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>
<?php echo $footer; ?>
