<?php echo $header; ?>

<section class="inner_page">
	<nav class="bread_crumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<? if ($breadcrumb == end($breadcrumbs)) { ?>
				<span><?php echo $breadcrumb['text']; ?></span>
				<? } else { ?>
				<a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
			<? } ?>
		<?php } ?>
	</nav>
	
	<div class="page_title text-uppercase">
		<h1>ВАША КОРЗИНА</h1>
	</div>
	<div class="cart_section">
		
		<?php if (!empty($attention)) { ?>
            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<?php if (!empty($success)) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<?php if (!empty($error_warning)) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		
		<table>
			<thead>
				<tr>
					<th>Фото</th>
					<th>Наименование и описание:</th>
					<th>Цена за шт</th>
					<th>Количество</th>
					<th>Сумма</th>
					<th>Действие</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($products as $product) { ?>
					<tr>
						<td>
							<a href="<?php echo $product['href']; ?>" class="img-container bg-item" data-image-src="<?php echo $product['thumb']; ?>"></a>
						</td>
						<td>
							<div class="description">
								<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?>
									<?php if (!$product['stock']) { ?>
										<span class="text-danger">***</span>
									<?php } ?>
								</a>
								<p><? echo $product['description']; ?></p>
							</div>
						</td>
						<td>
							<strong class="title">Цена за шт</strong>
							<div class="price">
								<? if ($product['special']) { ?>
									<strong><? echo $product['special']; ?></strong>
									<i><? echo $product['price']; ?></i>
									<div class="discount"><? echo round((($product['price']-$product['special'])/$product['price'])*100);?>%</div>
									<? } else { ?>
									<strong><? echo $product['price']; ?></strong>
								<? } ?>
								
							</div>
						</td>
						
						<td>
							<strong class="title">Количество</strong>
							<div class="quantity_count">
								<button class="btn quantity-down" type="button"><i class="fa fa-minus"></i></button>
								<input name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" type="text" class="form-control quantity-form">
								<button class="btn quantity-up" type="button"><i class="fa fa-plus"></i></button>
							</div>
						</td>
						<td>
							<strong class="title">Сумма</strong>
							<div class="price">
								<strong><?php echo $product['total']; ?></strong>
							</div>
						</td>
						
						<td>
							<strong class="title">Действие</strong>
							
							<button onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="btn btn-like"><img src="catalog/view/theme/default/stylesheet/images/icon_06.png" /></button>
							<input type="hidden" value="<?php echo $product['cart_id']; ?>">
							<button class="btn btn-remove"><img src="catalog/view/theme/default/stylesheet/images/icon_03.png" /></button>
						</td>
					</tr>
				<? } ?>
				
			</tbody>
		</table>
		
		<div class="clearfix">
			<?php if ($coupon || $voucher || $reward || $shipping) { ?>
				<?php echo $coupon; ?><?php echo $voucher; ?><?php echo $reward; ?>
			<?php } ?>
			<div class="total">
				<p>Итого к оплате:</p>
				
				<div class="price"><strong><?php echo $sub_total; ?></strong></div>
				
				
				<a href="<?php echo $checkout; ?>" class="btn btn-primary text-uppercase">Оформить</a>
			</div>
		</div>
		
		
	</div>
	
	<? echo $content_bottom; ?>
	
</section>


<?php echo $footer; ?>
