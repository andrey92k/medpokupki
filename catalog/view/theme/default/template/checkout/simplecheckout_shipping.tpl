   <div class="simplecheckout-warning-block" <?php echo $display_error && $has_error_shipping ? '' : 'style="display:none"' ?>><?php echo $error_shipping ?></div>
	
	<?php if ($error_recept_shipping) { ?>
		<div class="simplecheckout-warning-block"><?php echo $error_recept_shipping ?></div>
	<?php } ?>
	
	<?php if ($error_weight_product) { ?>

				<table>
			<thead>
				<tr>
					<th>Фото</th>
					<th>Наименование и описание:</th>
					<th>Цена за шт</th>
					<th>Количество</th>
					<th>Сумма</th>
					<th>Действие</th>
				</tr>
			</thead>
			<tbody>

				<?php foreach ($error_weight_product as $product) { ?>
					<tr>
						<td>
							<a href="<?php echo $product['href']; ?>" class="img-container bg-item" data-image-src="<?php echo $product['thumb']; ?>"></a>
						</td>
						<td>
							<div class="description">
								<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?>
									<?php if (!$product['stock']) { ?>
										<span class="text-danger">***</span>
									<?php } ?>
								</a>
								<p><? echo $product['description']; ?></p>

							</div>
						</td>
						<td>
							<strong class="title">Цена за шт</strong>
							<div class="price">
								<? if ($product['special']) { ?>
									<strong><? echo $product['special']; ?></strong>
									<i><? echo $product['price']; ?></i>
									<div class="discount"><? echo round((($product['price']-$product['special'])/$product['price'])*100);?>%</div>
									<? } else { ?>
									<strong><? echo $product['price']; ?></strong>
								<? } ?>
								
							</div>
						</td>
						
						<td>
							<strong class="title">Количество</strong>
							<div class="quantity_count">
								<button class="btn quantity-down" type="button"><i class="fa fa-minus"></i></button>
								<input name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" type="text" class="form-control quantity-form">
								<button class="btn quantity-up" type="button"><i class="fa fa-plus"></i></button>
							</div>
						</td>
						<td>
							<strong class="title">Сумма</strong>
							<div class="price">
								<strong><?php echo $product['total']; ?></strong>
							</div>
						</td>
						
						<td>
							<strong class="title">Действие</strong>
							
							<button onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="btn btn-like"><img src="catalog/view/theme/default/stylesheet/images/icon_06.png" /></button>
							<input type="hidden" value="<?php echo $product['cart_id']; ?>">
							<button class="btn btn-remove"><img src="catalog/view/theme/default/stylesheet/images/icon_03.png" /></button>
						</td>
					</tr>
				<? } ?>
				
			</tbody>
		</table>
	<?php } ?>

    <div class="variants">
        <div class="row">
			<?php if (!empty($shipping_methods)) { ?>
				<?php if ($display_type == 2 ) { ?>
					<?php $current_method = false; ?>
					<select data-onchange="reloadAll" name="shipping_method">
						<?php foreach ($shipping_methods as $shipping_method) { ?>
							<?php if (!empty($shipping_method['title'])) { ?>
								<optgroup label="<?php echo $shipping_method['title']; ?>">
								<?php } ?>
								<?php if (empty($shipping_method['error'])) { ?>
									<?php foreach ($shipping_method['quote'] as $quote) { ?>
									<option value="<?php echo $quote['code']; ?>" <?php echo !empty($quote['dummy']) ? 'disabled="disabled"' : '' ?> <?php echo !empty($quote['dummy']) ? 'data-dummy="true"' : '' ?> <?php if ($quote['code'] == $code) { ?>selected="selected"<?php } ?>><?php echo $quote['title']; ?><?php echo !empty($quote['text']) ? ' - '.$quote['text'] : ''; ?></option>
									<?php if ($quote['code'] == $code) { $current_method = $quote; } ?>
								<?php } ?>
								<?php } else { ?>
								<option value="<?php echo $shipping_method['code']; ?>" disabled="disabled"><?php echo $shipping_method['error']; ?></option>
								<?php } ?>
								<?php if (!empty($shipping_method['title'])) { ?>
								</optgroup>
							<?php } ?>
						<?php } ?>
					</select>
					<?php if ($current_method) { ?>
						<?php if (!empty($current_method['description'])) { ?>
							<div class="simplecheckout-methods-description"><?php echo $current_method['description']; ?></div>
						<?php } ?>
						<?php if (!empty($rows)) { ?>
							<?php foreach ($rows as $row) { ?>
								<?php echo $row ?>
							<?php } ?>
						<?php } ?>
					<?php } ?>
					<?php } else { ?>
					
					<?php foreach ($shipping_methods as $shipping_method) { ?>
						<?php if (!empty($shipping_method['title'])) { ?>
							<p><b><?php echo $shipping_method['title']; ?></b></p>
						<?php } ?>
						<?php if (!empty($shipping_method['warning'])) { ?>
							<div class="simplecheckout-error-text"><?php echo $shipping_method['warning']; ?></div>
						<?php } ?>
						<?php if (empty($shipping_method['error'])) { ?>
							<?php $count1 = 0; foreach ($shipping_method['quote'] as $quote) { $count1++; ?>
								<div class="col-sm-6">
									<label for="<?php echo $quote['code']; ?>">  
										<input class="radiodost" type="radio" data-inviz="opisanie<? echo $count1; ?>" data-onchange="reloadAll" name="shipping_method" <?php echo !empty($quote['dummy']) ? 'disabled="disabled"' : '' ?> <?php echo !empty($quote['dummy']) ? 'data-dummy="true"' : '' ?> value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" <?php if ($quote['code'] == $code) { ?>checked="checked"<?php } ?> />
										<span class="radiospan"><?php if (!empty($quote['img'])) { ?>
											<img src="<?php echo $quote['img']; ?>" width="60" height="32" border="0" style="display:block;margin:3px;">
										<?php } ?>
										<strong><?php echo !empty($quote['title']) ? $quote['title'] : ''; ?></strong><?php echo !empty($quote['text']) ? '' . $quote['text'] : ''; ?></span>
									</label>
								</div>
							<?php } ?>
							<?php $count = 0; foreach ($shipping_method['quote'] as $quote) { $count++; ?>
								<?php if (!empty($quote['description'])) { ?>
									<? if ($quote['description'] !="<p><br></p>") { ?>
										<div class="form-group col-sm-12 opisanie<? echo $count; ?> inviz">
											<label for="<?php echo $quote['code']; ?>"><?php echo $quote['description']; ?></label>
										</div>
									<? } ?>
								<?php } ?>
								<?php if ($quote['code'] == $code && !empty($rows)) { ?>
								<div class="form-group col-sm-12 shipping_custom_rows">
                                    <?php foreach ($rows as $row) { ?>
										<?php echo $row ?>
									<?php } ?>
								</div>
								<?php } ?>
							<?php } ?>
							
							
							<?php } else { ?>
							<div class="simplecheckout-error-text"><?php echo $shipping_method['error']; ?></div>
						<?php } ?>
					<?php } ?>
					
				<?php } ?>
				<input type="hidden" name="shipping_method_current" value="<?php echo $code ?>" />
				<input type="hidden" name="shipping_method_checked" value="<?php echo $checked_code ?>" />
			<?php } ?>
			<?php if (empty($shipping_methods) && $address_empty && $display_address_empty) { ?>
				<div class="simplecheckout-warning-text"><?php echo $text_shipping_address; ?></div>
			<?php } ?>
			<?php if (empty($shipping_methods) && !$address_empty) { ?>
				<div class="simplecheckout-warning-text"><?php echo $error_no_shipping; ?></div>
			<?php } ?>
		</div>
	</div>
</section>
</div>