<div class="col-md-4 col-lg-3">
    <div class="orders">
        <h4>Состав заказа</h4>
        <hr/>
        <?php foreach ($products as $product) { ?>
			<div class="product">
				<div class="img-container bg-item" data-image-src="<?php echo $product['thumb']; ?>"></div>
				<div class="text">
					<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
					<p><?php echo $product['quantity']; ?> шт * <?php echo $product['price']; ?></p>
					<?php if ($product['rec_status_id']) { ?>
						<p class="text-danger" style="font-weight: bold; color: red;">
							Строго рецептурный товар!
						</p>
					<?php } ?>
				</div>
				
			</div>
		<? } ?>
        <hr/>
        
      <a href="#collapse-coupon" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion">У меня есть промокод <i class="fa fa-caret-down"></i></a>
  <div id="collapse-coupon" class="panel-collapse collapse">
    <div class="panel-body">
     
      <div class="input-group col-sm-6">
        <input type="text" name="coupon" value="<?php echo $coupon; ?>" placeholder="<?php echo $entry_coupon; ?>" id="input-coupon" class="form-control" />
        <span class="input-group-btn">
        <input type="button" value="<?php echo $button_coupon; ?>" id="button-coupon" data-loading-text="<?php echo $text_loading; ?>"  class="btn btn-primary" />
        </span></div>
      <script type="text/javascript"><!--
$('#button-coupon').on('click', function() {
    $.ajax({
        url: 'index.php?route=total/coupon/coupon',
        type: 'post',
        data: 'coupon=' + encodeURIComponent($('input[name=\'coupon\']').val()),
        dataType: 'json',
        beforeSend: function() {
            //$('#button-coupon').button('loading');
        },
        complete: function() {
            //$('#button-coupon').button('reset');
        },
        success: function(json) {
            $('.alert').remove();

            if (json['error']) {
                $('header').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }

            if (json['redirect']) {
                location = json['redirect'];
            }
        }
    });
});
//--></script>
    </div>
  </div>
        <p><strong>Выбранный способ доставки:</strong></p>
        <?php foreach ($totals as $total) { ?>
            <? if ($total == end($totals)) { ?>
                <hr />
                <strong>Итого к оплате:</strong>
                <div class="price"><strong><?php echo $total['text']; ?></strong></div>
				
				<? } else { ?>
				<p><?php echo $total['title']; ?>:  <?php echo $total['text']; ?></p> 
			<? } ?>       
		<? } ?>
		<button class="btn btn-primary text-uppercase" data-onclick="createOrder" id="simplecheckout_button_confirm">Оформить</button>
	</div>
</div>

<!--
	
	<div class="simplecheckout-block" id="simplecheckout_summary">
	<?php if ($display_header) { ?>
		<div class="checkout-heading panel-heading"><?php echo $text_summary ?></div>
	<?php } ?>
	<div class="table-responsive">
	<table class="simplecheckout-cart">
	<colgroup>
	<col class="image">
	<col class="name">
	<col class="model">
	<col class="quantity">
	<col class="price">
	<col class="total">
	</colgroup>
	<thead>
	<tr>
	<th class="image"><?php echo $column_image; ?></th>
	<th class="name"><?php echo $column_name; ?></th>
	<th class="model"><?php echo $column_model; ?></th>
	<th class="quantity"><?php echo $column_quantity; ?></th>
	<th class="price"><?php echo $column_price; ?></th>
	<th class="total"><?php echo $column_total; ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($products as $product) { ?>
		<tr>
		<td class="image">
		<?php if ($product['thumb']) { ?>
			<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
		<?php } ?>
		</td>
		<td class="name">
		<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
		<div class="options">
		<?php foreach ($product['option'] as $option) { ?>
			&nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small><br />
		<?php } ?>
		</div>
		<?php if ($product['reward']) { ?>
			<small><?php echo $product['reward']; ?></small>
		<?php } ?>
		</td>
		<td class="model"><?php echo $product['model']; ?></td>
		<td class="quantity"><?php echo $product['quantity']; ?></td>
		<td class="price"><?php echo $product['price']; ?></td>
		<td class="total"><?php echo $product['total']; ?></td>
		</tr>
	<?php } ?>
	<?php foreach ($vouchers as $voucher_info) { ?>
		<tr>
		<td class="image"></td>
		<td class="name"><?php echo $voucher_info['description']; ?></td>
		<td class="model"></td>
		<td class="quantity">1</td>
		<td class="price"><?php echo $voucher_info['amount']; ?></td>
		<td class="total"><?php echo $voucher_info['amount']; ?></td>
		</tr>
	<?php } ?>
	</tbody>
	</table>
	</div>
	
	<?php foreach ($totals as $total) { ?>
		<div class="simplecheckout-cart-total" id="total_<?php echo $total['code']; ?>">
		<span><b><?php echo $total['title']; ?>:</b></span>
		<span class="simplecheckout-cart-total-value"><?php echo $total['text']; ?></span>
		</div>
	<?php } ?>
	
	<?php if ($summary_comment) { ?>
		<table class="simplecheckout-cart simplecheckout-summary-info">
		<thead>
		<tr>
		<th class="name"><?php echo $text_summary_comment; ?></th>
		</tr>
		</thead>
		<tbody>
		<tr>
		<td><?php echo $summary_comment; ?></td>
		</tr>
		</tbody>
		</table>
	<?php } ?>
	<?php if ($summary_payment_address || $summary_shipping_address) { ?>
		<table class="simplecheckout-cart simplecheckout-summary-info">
		<thead>
		<tr>
		<th class="name"><?php echo $text_summary_payment_address; ?></th>
		<?php if ($summary_shipping_address) { ?>
			<th class="name"><?php echo $text_summary_shipping_address; ?></th>
		<?php } ?>
		</tr>
		</thead>
		<tbody>
		<tr>
		<td><?php echo $summary_payment_address; ?></td>
		<?php if ($summary_shipping_address) { ?>
			<td><?php echo $summary_shipping_address; ?></td>
		<?php } ?>
		</tr>
		</tbody>
		</table>
	<?php } ?>
</div>-->			