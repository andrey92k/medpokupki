<!-- Carousel -->
        <div id="homeCarousel" class="carousel slide">
            <!-- Items -->
            <div class="carousel-inner">
                <!-- Item 1 -->
                <?php $count = 0; foreach ($banners as $banner) { $count++; ?>
                <div class="item <? if ($count == 1) { ?>active<? } ?>">
                  <?php if ($banner['link']) { ?>
                  <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
                  <?php } else { ?>
                  <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
                  <?php } ?>
                </div>
                <?php } ?>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#homeCarousel" data-slide="prev">
                <span class="fa fa-angle-left"></span>
            </a>
            <a class="right carousel-control" href="#homeCarousel" data-slide="next">
                <span class="fa fa-angle-right"></span>
            </a>

                        <!-- Menu -->
            <ul class="carousel-indicators">
                            <?php $count = -1; foreach ($banners as $banner) { $count++; ?>
                <li data-target="#homeCarousel" data-slide-to="<? echo $count; ?>" <? if ($count == 0) { ?>class="active"<? } ?>><? echo $count+1; ?></li>
                <?php } ?>
            </ul>
        </div>
        <!-- Carousel -->

