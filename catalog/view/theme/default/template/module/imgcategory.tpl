<!--Info Slider-->
        <section class="section_block info_slider">
            <div class="section_title text-center">
                <h1>ЧТО МОЖНО ПРИНЯТЬ В СЛУЧАЕ</h1>
                <p>Подборки лекарств и препаратов которые могут вам помочь в том или ином случае</p>
            </div>
            <div class="scrollbox-slider">
                <div class="controls">
                    <button class="control btn backward" id="Button3"><i class="fa fa-angle-left"></i></button>
                    <button class="control btn forward" id="Button4"><i class="fa fa-angle-right"></i></button>
                </div>
                <div class="scrollbox-container ">
                    <div class="scrollbox">
                       <?php foreach ($categories as $category) { ?>
                        <div class="element">
                            <div class="item">
                                <a href="<?php echo $category['href']; ?>" class="img-container">
                                    <img src="<?php echo $category['thumb']; ?>" /></a>
                                <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                                <p>Товаров: <?php echo $category['kolvo']; ?></p>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <!--Info Slider-->

