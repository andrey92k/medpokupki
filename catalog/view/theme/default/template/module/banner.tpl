<!--Articles-->
<div class="section_block articles">
	<div class="row">
		<?php foreach ($banners as $banner) { ?>
			<div class="col-sm-6">
				<article class="banner">
					<?php if ($banner['html']) { ?>
							<?php echo $banner['html']; ?>
						<?php } else { ?>
							<?php if ($banner['link']) { ?>
								<a href="<?php echo $banner['link']; ?>" class="img-container">
									<img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
								</a>
								<?php } else { ?>
								<img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
							<?php } ?>
					<?php } ?>
				</article>
			</div>
		<?php } ?>
	</div>
	</div>
<!--Articles-->

