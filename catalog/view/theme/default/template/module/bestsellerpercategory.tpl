<?php if (isset($products) && count($products)) { ?>

<div class="products">
      <h5>Также покупают:</h5>
      <?php foreach ($products as $product) { ?>
      <div class="product">
          <div class="action"></div>
          <a href="<?php echo $product['href']; ?>" class="img-container bg-item" data-image-src="<?php echo $product['thumb']; ?>"></a>
          <div class="description">
              <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
              <p><?php echo $product['description']; ?></p>
              <?php if ($product['price']) { ?>
              <div class="price">
                <?php if (!$product['special']) { ?>
                <strong><?php echo $product['price']; ?></strong>
                <?php } else { ?>
                <strong><?php echo $product['special']; ?></strong> <i><?php echo $product['price']; ?></i>
                <?php } ?>
              </div>
              <?php } ?>
              <button onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-primary text-uppercase" <? if ($product['cart_id']) { ?>style="display:none;"<? } ?>>В корзину</button>
              <div class="added" <? if ($product['cart_id']) { ?>style="display:block;"<? } ?>>
                  <p>Добавлен <a href="index.php?route=checkout/cart">в корзину</a></p>
                  <div class="quantity_count">
                      <button onclick="cart.updateone('<?php echo $product['cart_id']; ?>','-1');" class="btn quantity-down" type="button"><i class="fa fa-minus"></i></button>
                      <input disabled="disabled" type="text" class="form-control quantity-form quantity-<?php echo $product['cart_id']; ?>" value="<?php echo $product['quan']; ?>">
                      <button onclick="cart.updateone('<?php echo $product['cart_id']; ?>','1');" class="btn quantity-up" type="button"><i class="fa fa-plus"></i></button>
                      <button onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="btn btn-like"><i class="icon-heart2"></i></button>
                  </div>
              </div>
          </div>
      </div>
      <?php } ?>
  </div>
<?php } ?>

