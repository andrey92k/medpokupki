<!--Products-->
        <section class="section_block products">
            <div class="section_title text-center color-1">
                <h1><?php echo $heading_title; ?></h1>
                <? echo $information_description; ?>
            </div>
            <div class="scrollbox-slider">
                <div class="controls">
                    <button class="control btn backward" id="scrollbox-slider-backward"><i class="fa fa-angle-left"></i></button>
                    <button class="control btn forward" id="scrollbox-slider-forward"><i class="fa fa-angle-right"></i></button>
                </div>
                <div class="scrollbox-container ">
                    <div class="scrollbox">
                        <?php foreach ($products as $product) { ?>
                        <div class="element">
                            <div class="product <? if ($product['cart_id']) { ?>added<? } ?>">
							<div class="image"></div>
							<?php if ($product['stock_status'] == 'В наличии в аптеке') { ?>
									<div style="position: absolute;top: 0;left: 0;z-index: 2;text-align: right;" class="in_stock_sticker">
										<img src="catalog/view/theme/default/stylesheet/images/in_stock.png" />
									</div>
								<? } ?>
								
                                <?php if ($product['special']) { ?>
                                      <div class="action">
                                        <div class="discount"><? echo round((($product['price']-$product['special'])/$product['price'])*100);?>%</div>
                                      </div>
                                <? } ?>
                                <a href="<?php echo $product['href']; ?>" class="img-container bg-item" data-image-src="<?php echo $product['thumb']; ?>"></a>
                                <div class="description">
                                    <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                    <p><?php echo $product['description']; ?></p>
                                    <?php if ($product['price']) { ?>
                                    <div class="price">
                                      <?php if (!$product['special']) { ?>
                                      <strong><?php echo $product['price']; ?></strong>
                                      <?php } else { ?>
                                      <strong><?php echo $product['special']; ?></strong> <i><?php echo $product['price']; ?></i>
                                      <?php } ?>
                                    </div>
                                    <?php } ?>
                                    <button onclick="
                                    cart.add('<?php echo $product['product_id']; ?>'); 
                                    $('.quantity-<?php echo $product['product_id']; ?>').val('1'); 
                                    $('.quantity-<?php echo $product['product_id']; ?>').parent().parent().show(); 
                                    $('.quantity-<?php echo $product['product_id']; ?>').parent().parent().prev().hide(); 
                                    $('.quantity-<?php echo $product['product_id']; ?>').parent().parent().addClass('cartid');
                                    $('.quantity-<?php echo $product['product_id']; ?>').parent().parent().parent().parent().addClass('added');
                                    " class="btn btn-primary text-uppercase" <? if ($product['cart_id']) { ?>style="display:none;"<? } ?>>В корзину</button>

                                    
                                    <div class="added" <? if ($product['cart_id']) { ?>style="display:block;"<? } ?>>
                                        <p>Добавлен <a href="index.php?route=checkout/cart">в корзину</a></p>
                                        <div class="quantity_count">
                                            <button class="btn quantity-down" type="button"><i class="fa fa-minus"></i></button>
                                            <input cartid="<?php echo $product['cart_id']; ?>" prodid="quantity-<?php echo $product['product_id']; ?>" disabled="disabled" type="text" name="quantity" class="form-control quantity-form quantity-<?php echo $product['product_id']; ?>" value="<?php echo $product['quan']; ?>">
                                            <button class="btn quantity-up" type="button"><i class="fa fa-plus"></i></button>
                                            <button onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="btn btn-like"><i class="icon-heart2"></i></button>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <? } ?>

                    </div>
                </div>
            </div>
        </section>
        <!--Products-->
