<!--Brands-->
        <section class="section_block brands">
            <div class="section_title text-center">
                <h1>ВЫБРАТЬ НУЖНЫЙ ВАМ БРЕНД</h1>
                <div class="sort">
                    <div class="buttons">
                    	<?php foreach ($categories as $category) { ?>
                        <a class='sl' data-id='<?php echo $category['category_id'];?>' href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a> 
                        <?php } ?>     
                    </div>
                </div>
            </div>
            <?php $i=0;foreach ($categories as $category) { $i++;?>
            <div class="scrollbox-slider sl_cat" <?php if ($i>1)echo "style='display:none;'"?> id='cat<?php echo $category['category_id'];?>'>
                <div class="controls">
                    <button class="control btn backward" id="Button1"><i class="fa fa-angle-left"></i></button>
                    <button class="control btn forward" id="Button2"><i class="fa fa-angle-right"></i></button>
                </div>
                <div class="scrollbox-container ">
                    <div class="scrollbox">
                    	<?php foreach ($category['mans'] as $manufacturer) { ?>              
                        <div class="element">
                            <a rel="nofollow" title="<?php echo $manufacturer['name']; ?>" href="<?php echo $manufacturer['href']; ?>"><img src="<?php echo $manufacturer['thumb']; ?>" title="<?php echo $manufacturer['name']; ?>" alt="<?php echo $manufacturer['name']; ?>" /></a>
                            <a rel="nofollow" title="<?php echo $manufacturer['name']; ?>" href="<?php echo $manufacturer['href']; ?>" style="
    display: block;
    font-size: 14px;
    margin-top: 20px;
    text-decoration: underline;
"><?php echo $manufacturer['name']; ?><Br><span>(<?php echo $manufacturer['total'];?>)</span></a>

                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php }?>
        </section>
<script type='text/javascript'>
$(document).ready(function()
{
  
   $("a.sl").click(function()
   {
      var id=$(this).attr('data-id');
      $('.sl_cat').hide();
      $(".sl_cat#cat"+id).show();
     return false;
   });
});
</script>        
        <!--Brands-->
