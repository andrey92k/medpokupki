﻿<!--Brands-->
        <section class="section_block brands">
            <div class="section_title text-center">
                <h1>ВЫБРАТЬ НУЖНЫЙ ВАМ БРЕНД</h1>
                <div class="sort">
                    <div class="buttons">
                    	<?php foreach ($categories as $category) { ?>
                        <a href="index.php?route=product/manufacturer#<?php echo $category['name']; ?>"><?php echo $category['name']; ?></a> 
                        <?php } ?>     
                    </div>
                </div>
            </div>
            <div class="scrollbox-slider ">
                <div class="controls">
                    <button class="control btn backward" id="Button1"><i class="fa fa-angle-left"></i></button>
                    <button class="control btn forward" id="Button2"><i class="fa fa-angle-right"></i></button>
                </div>
                <div class="scrollbox-container ">
                    <div class="scrollbox">
                    	<?php foreach ($manufactureres as $manufacturer) { ?>              
                        <div class="element">
                            <a rel="nofollow" title="<?php echo $manufacturer['name']; ?>" href="<?php echo $manufacturer['href']; ?>"><img src="<?php echo $manufacturer['thumb']; ?>" title="<?php echo $manufacturer['name']; ?>" alt="<?php echo $manufacturer['name']; ?>" /></a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <!--Brands-->
