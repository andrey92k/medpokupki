<div class="row">
                    <section class="section_block products">
                        <div class="page_title text-uppercase">
                            <h1>Рекомендуем также купить</h1>
                        </div>
                        <div class="row">
                        <?php foreach ($products as $product) { ?>
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <div class="product <? if ($product['cart_id']) { ?>added<? } ?>">
                                <?php if ($product['special']) { ?>
                                      <div class="action">
                                        <div class="discount"><? echo round((($product['price']-$product['special'])/$product['price'])*100);?>%</div>
                                      </div>
                                <? } ?>
                                <a href="<?php echo $product['href']; ?>" class="img-container bg-item" data-image-src="<?php echo $product['thumb']; ?>"></a>
                                <div class="description">
                                    <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                    <p><?php echo $product['description']; ?></p>
                                    <?php if ($product['price']) { ?>
                                    <div class="price">
                                      <?php if (!$product['special']) { ?>
                                      <strong><?php echo $product['price']; ?></strong>
                                      <?php } else { ?>
                                      <strong><?php echo $product['special']; ?></strong> <i><?php echo $product['price']; ?></i>
                                      <?php } ?>
                                    </div>
                                    <?php } ?>
                                    <button onclick="
                                    cart.add('<?php echo $product['product_id']; ?>'); 
                                    $('.quantity-<?php echo $product['product_id']; ?>').val('1'); 
                                    $('.quantity-<?php echo $product['product_id']; ?>').parent().parent().show(); 
                                    $('.quantity-<?php echo $product['product_id']; ?>').parent().parent().prev().hide(); 
                                    $('.quantity-<?php echo $product['product_id']; ?>').parent().parent().addClass('cartid');
                                    $('.quantity-<?php echo $product['product_id']; ?>').parent().parent().parent().parent().addClass('added'); setTimeout(function () { location.reload(); }, 600);
                                    " class="btn btn-primary text-uppercase" <? if ($product['cart_id']) { ?>style="display:none;"<? } ?>>В корзину</button>

                                    
                                    <div class="added" <? if ($product['cart_id']) { ?>style="display:block;"<? } ?>>
                                        <p>Добавлен <a href="index.php?route=checkout/cart">в корзину</a></p>
                                        <div class="quantity_count">
                                            <button onclick="setTimeout(function () { location.reload(); }, 600);" class="btn quantity-down" type="button"><i class="fa fa-minus"></i></button>
                                            <input cartid="<?php echo $product['cart_id']; ?>" prodid="quantity-<?php echo $product['product_id']; ?>" disabled="disabled" type="text" name="quantity" class="form-control quantity-form quantity-<?php echo $product['product_id']; ?>" value="<?php echo $product['quan']; ?>">
                                            <button onclick="setTimeout(function () { location.reload(); }, 600);" class="btn quantity-up" type="button"><i class="fa fa-plus"></i></button>
                                            <button onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="btn btn-like"><i class="icon-heart2"></i></button>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <? } ?>
                        </div>
                    </section>
            </div>