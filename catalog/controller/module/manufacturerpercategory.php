<?php
// *	@copyright	OCSHOP.CMS \ ocshop.net 2011 - 2015.
// *	@demo	http://ocshop.net
// *	@blog	http://ocshop.info
// *	@forum	http://forum.ocshop.info
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerModuleManufacturerpercategory extends Controller {
	public function index($setting) {
		$this->load->language('module/manufacturerpercategory');
		
    	$data['heading_title'] = $this->language->get('heading_title');
		
		if (isset($this->request->get['path'])) {
            $path = '';
            $parts = explode('_', (string)$this->request->get['path']);
            foreach ($parts as $path_id) {
                if (!$path) {
                    $path = $path_id;
                } else {
                    $path .= '_' . $path_id;
                }
            }

            $category_id = array_pop($parts);
        } else {
            $category_id = null;
        }
		
		$this->load->model('module/manufacturerpercategory');					
		$this->load->model('catalog/manufacturer');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
        $this->load->model('catalog/category');
        
        $data['categories'] = array();

		$data['categories-1'] = array();

        $categories = $this->model_catalog_category->getCategories(0);
        
        foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();
        $mans=Array();
          $arr=$this->model_catalog_category->getCategoriesWithM($category['category_id']);
        
          foreach($arr['manufacturers'] as $m)
         {
         
         $q=$this->db->query("select count(*) as total from ".DB_PREFIX."product where manufacturer_id=".$m['manufacturer_id']." and status=1");
         
          if ($m['image'])
            $mans[]=array(
                'name'=>$m['name'],
                'total'=>$q->row['total'],
                'href' => $this->url->link('product/manufacturer/info','manufacturer_id='.$m['manufacturer_id'],false),
                          'thumb'=>$this->model_tool_image->resize($m['image'], 120, 70));
         };
				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
          'mans' =>$mans,
					'children' => $children_data,
          'category_id'=>$category['category_id'],
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}
   /*
		$results = $this->model_catalog_manufacturer->getManufacturers();

		foreach ($results as $result) {
			if (is_numeric(utf8_substr($result['name'], 0, 1))) {
				$key = '0 - 9';
			} else {
				$key = utf8_substr(utf8_strtoupper($result['name']), 0, 1);
			}

			if (!isset($data['categories-1'][$key])) {
				$data['categories-1'][$key]['name'] = $key;
			}

			$data['categories-1'][$key]['manufacturer'][] = array(
				'name' => $result['name'],
				'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id'])
			);
		}
		
		$data['manufactureres'] = array();
					
		if (isset($category_id)) {
		
		$results = $this->model_module_manufacturerpercategory->getManufacturersByCategory($category_id);
		} else {
		   	$results = $this->model_catalog_manufacturer->getManufacturers(0);
		}

		foreach($results as $result)
		{
			
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], 158, 74);
					$data['manufactureres'][] = array(
						'thumb'       => $image,
						'manufacturer_id' => $result['manufacturer_id'],
						'name'        => $result['name'] ,
						'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id'])
				
					);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', 158, 74);
				}
			
			
		}
    */
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/manufacturerpercategory.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/manufacturerpercategory.tpl', $data);
		} else {
			return $this->load->view('default/template/module/manufacturerpercategory.tpl', $data);
		}
		
  	}
}
?>