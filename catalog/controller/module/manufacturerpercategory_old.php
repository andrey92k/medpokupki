<?php
// *	@copyright	OCSHOP.CMS \ ocshop.net 2011 - 2015.
// *	@demo	http://ocshop.net
// *	@blog	http://ocshop.info
// *	@forum	http://forum.ocshop.info
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerModuleManufacturerpercategory extends Controller {
	public function index($setting) {
		$this->load->language('module/manufacturerpercategory');
		
    	$data['heading_title'] = $this->language->get('heading_title');
		
		if (isset($this->request->get['path'])) {
            $path = '';
            $parts = explode('_', (string)$this->request->get['path']);
            foreach ($parts as $path_id) {
                if (!$path) {
                    $path = $path_id;
                } else {
                    $path .= '_' . $path_id;
                }
            }

            $category_id = array_pop($parts);
        } else {
            $category_id = null;
        }
		
		$this->load->model('module/manufacturerpercategory');					
		$this->load->model('catalog/manufacturer');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');


		$data['categories'] = array();

		$results = $this->model_catalog_manufacturer->getManufacturers();

		foreach ($results as $result) {
			if (is_numeric(utf8_substr($result['name'], 0, 1))) {
				$key = '0 - 9';
			} else {
				$key = utf8_substr(utf8_strtoupper($result['name']), 0, 1);
			}

			if (!isset($data['categories'][$key])) {
				$data['categories'][$key]['name'] = $key;
			}

			$data['categories'][$key]['manufacturer'][] = array(
				'name' => $result['name'],
				'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id'])
			);
		}
		
		$data['manufactureres'] = array();
					
		if (isset($category_id)) {
		
		$results = $this->model_module_manufacturerpercategory->getManufacturersByCategory($category_id);
		} else {
		   	$results = $this->model_catalog_manufacturer->getManufacturers(0);
		}

		foreach($results as $result)
		{
			
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], 158, 74);
					$data['manufactureres'][] = array(
						'thumb'       => $image,
						'manufacturer_id' => $result['manufacturer_id'],
						'name'        => $result['name'] ,
						'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id'])
				
					);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', 158, 74);
				}
			
			
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/manufacturerpercategory.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/manufacturerpercategory.tpl', $data);
		} else {
			return $this->load->view('default/template/module/manufacturerpercategory.tpl', $data);
		}
		
  	}
}
?>