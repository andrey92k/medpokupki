<?php
class ControllerApiShiptor extends Controller {
	public function __construct($registry) {
		parent::__construct($registry);
		$registry->set('shiptor', new Shiptor($registry));
	}

	public function kladr() {
		if (! empty($this->request->post['kladr_id'])) {
			$this->session->data['shiptor']['kladr_id'] = substr($this->request->post['kladr_id'], 0, -2);
		}
	}

	public function address() {
		$this->language->load('shipping/shiptor');

		$json = array();
		
		$shipping_method = false;
		
		if (isset($this->request->post['shipping_method'])) {
			$shipping_method = $this->request->post['shipping_method'];
		}

		$order_id = false;

		if (isset($this->request->post['order_id'])) {
			$order_id = $this->request->post['order_id'];
		}
		
		if (! $shipping_method) {
			$json['error'] = $this->language->get('error_shipping_method');
		}
		
		if (! $order_id && !$this->request->get['token']) {
			$json['error'] = $this->language->get('error_order_id');
		}
		
		// сбор данных по товарам из заказа
		
		$this->load->model('account/order');
		$this->load->model('catalog/product');
		
		$products = $this->model_account_order->getOrderProducts($order_id);
		
		foreach ($products as $id => $product) {
			$product_info = $this->model_catalog_product->getProduct($product['product_id']);
			
			$products[$id]['weight']		= $product_info['weight'];
			$products[$id]['weight_class_id']	= $product_info['weight_class_id'];
			$products[$id]['length']		= $product_info['length'];
			$products[$id]['width']			= $product_info['width'];
			$products[$id]['height']		= $product_info['height'];
		}
		
		if (! $json) {
			$this->load->model('shipping/shiptor');
			
			if ($shipping = $this->model_shipping_shiptor->getValidate($shipping_method)) {
				if (! isset($this->session->data['shiptor']['kladr_id'])) {
					$json['error'] = $this->language->get('error_kladr');			
				} else {
					$kladr_id = $this->session->data['shiptor']['kladr_id'];
				}
				
				if (! $json) {
					if ($shipping['shipping'] == 'P') {
						
						$json['heading_title'] = $this->language->get('heading_point');
						
						$params = array(
							'kladr_id'		=> $kladr_id,
							'shipping_method'	=> $shipping['courier_id'],
						);

						// если товар один, и указаны его размеры - берем их
						// если товар один, и размеров нет - ставим нули, в библиотеке подставятся дефолтные размеры
						// если товаров несколько - пропускаем блок, в библиотеке подставятся дефолтные размеры

						if (1 == sizeof($products)) {
							$product = reset($products);

							if (1 == $product['quantity']) {
								$params['length'] = $this->length->convert($product['length'], 1, 1);
								$params['width']  = $this->length->convert($product['width'],  1, 1);
								$params['height'] = $this->length->convert($product['height'], 1, 1);
							}
						}

						$weight = 0;

						foreach ($products as $product) {
							if (0 != $product['weight']) {
								$weight += $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('shiptor_weight_class_id'));
							} else {
								$weight += $product['quantity'] * $this->config->get('shiptor_weight');
							}
						}

						$params['weight'] = $weight;
						
						if ($points = $this->shiptor->getDeliveryPoints($params)) {
							foreach ($points as $key => $point) {
								if (! isset($point['cod']) && ! isset($point['card'])) {
									continue;
								}

								$json['success'][$key]['id']      = $point['id'];
								$json['success'][$key]['address'] = $point['address'];

								$json['success'][$key]['cod'] = 0;
								
								if (isset($point['cod']) && $point['cod']) {
									$json['success'][$key]['cod'] = 1;
								}	
								
								$json['success'][$key]['card'] = 0;

								if (isset($point['card']) && $point['card']) {
									$json['success'][$key]['card'] = 1;
								}

								$json['success'][$key]['courier_id'] = $shipping['courier_id'];
							}
						} else {
							$json['success'] = array(array('id' => 0, 'address' => 'Адрес доставки клиента', 'cod' => 1, 'card' => 0, 'courier_id' => $shipping['courier_id']));
						}						
					}					
				}
			}
		}
		

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function save() {
		$this->language->load('shipping/shiptor');

		$json = array();		
		
		if (isset($this->request->post['shipping_method']) && preg_match('/^shiptor/i', $this->request->post['shipping_method'])) {
			if (empty($this->request->post['courier_id'])) {
				$json['error'] = 'Не определена служба доставки';
			} else {
				$this->session->data['shiptor']['address']['courier_id'] = $this->request->post['courier_id'];
				
				if (isset($this->request->post['point_id'])) {
					$this->session->data['shiptor']['address']['point_id'] = $this->request->post['point_id'];
				}
				
				if (isset($this->request->post['address'])) {
					$this->session->data['shiptor']['address']['point'] = $this->request->post['address'];
				}

				if (isset($this->request->post['cod'], $this->request->post['card'])) {
					if ($this->request->post['cod'] && $this->request->post['card']) {
						$this->session->data['shiptor']['payment'][$this->request->post['courier_id']] = 'all_types';
					} else if (! $this->request->post['cod'] && $this->request->post['card']) {
						$this->session->data['shiptor']['payment'][$this->request->post['courier_id']] = 'card';
					} else if ($this->request->post['cod'] && ! $this->request->post['card']) {
						$this->session->data['shiptor']['payment'][$this->request->post['courier_id']] = 'cod';
					}
				}
				
				$json['success'] = $this->language->get('text_api_shipping_point');
			}
		}
	
		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function recalculate() {
		if (!isset($this->request->post['payment_method'])
		|| !isset($this->request->post['shipping_method'])
		|| !isset($this->session->data['shipping_address'])
		|| !isset($this->session->data['payment_methods'][$this->request->post['payment_method']])) return;
		
		// Shipping Methods
		$method_data = array();
		
		$this->session->data['payment_method']['code'] = $this->request->post['payment_method'];

		$this->load->model('extension/extension');

		$results = $this->model_extension_extension->getExtensions('shipping');

		foreach ($results as $result) {
			if ($this->config->get($result['code'] . '_status')) {
				$this->load->model('shipping/' . $result['code']);

				$quote = $this->{'model_shipping_' . $result['code']}->getQuote($this->session->data['shipping_address']);
		
				if ($quote) {
					$method_data[$result['code']] = array(
						'title'      => $quote['title'],
						'quote'      => $quote['quote'],
						'sort_order' => $quote['sort_order'],
						'error'      => $quote['error']
					);
				}
			}
		}
		
		$sort_order = array();

		foreach ($method_data as $key => $value) {
			$sort_order[$key] = $value['sort_order'];
		}

		array_multisort($sort_order, SORT_ASC, $method_data);

		$this->session->data['shipping_methods'] = $method_data;
		
		$shipping = explode('.', $this->request->post['shipping_method']);

		if (!isset($shipping[0]) || !isset($shipping[1]) || !isset($this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]])) {
			return;
		}
		
		$this->session->data['shipping_method'] = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];
	}
}
