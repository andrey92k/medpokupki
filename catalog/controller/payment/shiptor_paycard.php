<?php
# Разработчик: Билалов Ильсур
# E-mail: bilalovi@gmail.com
# Skype: oc-designer
# ВК: https://vk.com/ocdesign
# Shiptor - Агрегатор служб доставки

class ControllerPaymentShiptorPaycard extends Controller {
	public function index() {
		$data['button_confirm'] = $this->language->get('button_confirm');

		$data['text_loading'] = $this->language->get('text_loading');

		$data['continue'] = $this->url->link('checkout/success');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/shiptor_paycard.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/payment/shiptor_paycard.tpl', $data);
		} else {
			return $this->load->view('default/template/payment/shiptor_paycard.tpl', $data);
		}
	}

	public function confirm() {
		if ($this->session->data['payment_method']['code'] == 'shiptor_paycard') {
			$this->load->model('checkout/order');

			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('shiptor_paycard_order_status_id'));
		}
	}
}