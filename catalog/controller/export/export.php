<?php
	// *	@copyright	OPENCART.PRO 2011 - 2015.
	// *	@forum	http://forum.opencart.pro
	// *	@source		See SOURCE.txt for source and other copyright.
	// *	@license	GNU General Public License version 3; see LICENSE.txt
	
	class ControllerExportExport extends Controller {
		public function do_export() {
			
			
			if (empty ($this->request->get['key']) 
			|| $this->request->get['key'] != 'TxTY}9wq3s8@vE@qhuCmVLsYD?c~$6')
			{
				header("HTTP/1.0 404 Not Found");
				$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');
				exit;
				
			}
			
			$this->load->model('catalog/product');
			ini_set('display_errors',1);
			error_reporting(E_ALL);
			
			
			set_time_limit(0);
			ini_set('memory_limit', '5048M');
			
			// $this->db->query('SET OPTION SQL_BIG_SELECTS = 1');
			$this->db->query('SET SQL_BIG_SELECTS = 1');
			
			$products = $this->db->query("SELECT p.product_id,  pd.name AS pd_name, 
			m.name AS p_manufacturer_name, 
			p.mpn AS mpn_manufacturer_name, 
			p.price, p.quantity, p.model, p.stock_status_id, 
			(SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND pd2.quantity <= 1 AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount_price  
			FROM " . DB_PREFIX . "product p 
			LEFT JOIN " . DB_PREFIX . "product_description pd 
			ON (pd.product_id = p.product_id) 
			LEFT JOIN " . DB_PREFIX . "manufacturer m 
			ON (m.manufacturer_id = p.manufacturer_id) 
			WHERE p.stock_status_id >= 6 
			AND p.status = 1 
			AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
			ORDER BY p.product_id");
			
			// echo "<pre>";
			// print_r($products->rows);
			
			// exit;
			
			$txt_filename = DIR_VIGRUZKA . 'medpokupki_products.txt';
			$zip_filename = DIR_VIGRUZKA . 'medpokupki_products.zip';
			
			if (file_exists($txt_filename))
			{
				unlink($txt_filename);
			}
			
			
			if (file_exists($zip_filename))
			{
				unlink($zip_filename);
			}
			
			
			$fp = fopen(DIR_VIGRUZKA . 'medpokupki_products.txt', 'w');
			
			fwrite($fp, "TYP | APTEKA | PREPARAT | PROIZV | STRANA | CENA | OST | SROK | HREF" . PHP_EOL);
			
			// echo '<br>TYP | APTEKA | PREPARAT | PROIZV | STRANA | CENA | OST | SROK | HREF<br>';
			
			foreach ($products->rows as $product)
			{
				if ($product['mpn_manufacturer_name'])
				{
					$manufacturer_name = $product['mpn_manufacturer_name'];
				}
				else
				{
					$manufacturer_name = $product['p_manufacturer_name'];
				}
				
				if ($product['discount_price'])
				{
					$price = $product['discount_price'];
				}
				else
				{
					$price = $product['price'];
				}
				
				$product_url = htmlspecialchars_decode($this->url->link('product/product', 'product_id=' . $product['product_id']));
				
				
				// echo "остатки | МедПокупки.РУ | {$product['pd_name']} | {$manufacturer_name} | | {$price} | {$product['quantity']} | | {$product_url}<br>";
				
				fwrite($fp, "остатки | МедПокупки.РУ | {$product['pd_name']} | {$manufacturer_name} | | {$price} | {$product['quantity']} | | {$product_url}" . PHP_EOL);
			}
			
			fclose($fp);
			
			
			$zip = new ZipArchive();
			
			if ($zip->open($zip_filename, ZipArchive::CREATE)!==TRUE) {
				exit("Невозможно открыть <$zip_filename>\n");
			}
			
			$zip->addFile($txt_filename, 'medpokupki_products.txt');
			
			// echo "numfiles: " . $zip->numFiles . "\n";
			// echo "status:" . $zip->status . "\n";
			$zip->close();
			
			if (file_exists($txt_filename))
			{
				unlink($txt_filename);
			}
			
			echo 'done';
			
			
		}
	}								