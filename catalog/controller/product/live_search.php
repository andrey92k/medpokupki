<?php
class ControllerProductLiveSearch extends Controller {
	public function index() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$search = $this->request->get['filter_name'];
		} else {
			$search = '';
		}
		$tag           = $search;
		$description   = '';
		// $description   = true;
		$category_id   = 0;
		$sub_category  = '';
		$sort          = 'p.sort_order';
		$order         = 'ASC';
		$page          = 1;
		$limit         = $this->config->get('live_search_limit');
		$search_result = 0;
		$error         = false;
		if (version_compare(VERSION, '2.0.0.0', '>=') && version_compare(VERSION, '2.2.0.0', '<')) {
			$currency_code = null;
		}
		elseif(version_compare(VERSION, '2.2.0.0', '>=') && version_compare(VERSION, '2.3.0.0', '<')){
			$currency_code = $this->session->data['currency'];
		}
		else{
			$error = true;
			$json[] = array(
				'product_id' => 0,
				'image'      => null,
				'name'       => 'Version Error: '.VERSION,
				'extra_info' => null,
				'price'      => 0,
				'special'    => 0,
				'url'        => '#'
			);
		}

		if(!$error){
			if (isset($this->request->get['filter_name'])) {
				$this->load->model('catalog/product');
				
				$results = $this->model_catalog_product->getLiveSearchProducts($search);
				
				
				// $search_result = $this->model_catalog_product->getTotalProducts($filter_data);
				$search_result = 0;
				$image_width        = $this->config->get('live_search_image_width');
				$image_height       = $this->config->get('live_search_image_height');
				$title_length       = $this->config->get('live_search_title_length');
				$description_length = $this->config->get('live_search_description_length');
				
				$json['total'] = (int)$search_result;
				
				foreach ($results as $result) {

					$json['products'][] = array(
						'product_id'  => $result['product_id'],
						'image'       => '',
						'name' => utf8_substr(strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')), 0, $title_length) . '..',
						'extra_info' => '',
						'price'       => '',
						'special'     => '',
						'url'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
					);
				}
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
