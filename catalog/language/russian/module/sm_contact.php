<?php
// Heading 
$_['text_name']			 = 'Ваше имя';
$_['text_email']		 = 'Ваш e-mail';
$_['text_message']		 = 'Сообщение';
$_['text_button']		 = 'Отправить';

$_['error_email']		 = 'Введите E-mail';
$_['error_message']		 = 'Напишите сообщение';
$_['error_verification'] = 'Неправильный ответ';
$_['text_success']		 = 'Ваше сообщение отправлено';

$_['email_subject']		 = 'Новое сообщение от %s';