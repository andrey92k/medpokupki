<?php
# Разработчик: Билалов Ильсур
# E-mail: bilalovi@gmail.com
# Skype: oc-designer
# ВК: https://vk.com/ocdesign
# Shiptor - Агрегатор служб доставки

$_['text_title'] = 'Оплата картой при получении';
$_['text_title_insurance'] = 'Оплата картой при получении <span>+%s комиссия за финансовые операции</span>';
$_['text_point'] = '<br/> %s';