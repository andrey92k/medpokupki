<?php
// *	@copyright	OPENCART.PRO 2011 - 2015.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']        = 'Ваш заказ сформирован!';

// Text
$_['text_basket']          = 'Корзина покупок';
$_['text_checkout']        = 'Оформление заказа';
$_['text_success']         = 'Успешно';
$_['text_customer'] 	   = '<p>Ваш заказ успешно создан!</p><p>Вы зарегистрированы и можете просматривать историю заказов в <a href="%s">Личном кабинете</a>.</p><p>Спасибо за покупки в нашем интернет-магазине!</p>';
$_['text_guest']    	   = '<p>Ваш заказ успешно создан!</p><p>Зарегистрированным пользователям доступен просмотр состояния заказа и прошлых покупок.</p><p>Спасибо за покупки в нашем интернет-магазине!</p>';