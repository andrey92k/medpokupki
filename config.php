<?php
// HTTP
define('HTTP_SERVER', 'http://test/');

// HTTPS
define('HTTPS_SERVER', 'http://test/');

// DIR
define('DIR_APPLICATION', 'C:/OpenServer/OSPanel/domains/test/catalog/');
define('DIR_SYSTEM', 'C:/OpenServer/OSPanel/domains/test/system/');
define('DIR_LANGUAGE', 'C:/OpenServer/OSPanel/domains/test/catalog/language/');
define('DIR_TEMPLATE', 'C:/OpenServer/OSPanel/domains/test/catalog/view/theme/');
define('DIR_CONFIG', 'C:/OpenServer/OSPanel/domains/test/system/config/');
define('DIR_IMAGE', 'C:/OpenServer/OSPanel/domains/test/image/');
define('DIR_CACHE', 'C:/OpenServer/OSPanel/domains/test/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/OpenServer/OSPanel/domains/test/system/storage/download/');
define('DIR_LOGS', 'C:/OpenServer/OSPanel/domains/test/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/OpenServer/OSPanel/domains/test/system/storage/modification/');
define('DIR_UPLOAD', 'C:/OpenServer/OSPanel/domains/test/system/storage/upload/');
define('DIR_VIGRUZKA', 'C:/OpenServer/OSPanel/domains/test/vigruzka/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'medical');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
