<?php
/*
 * Shoputils
 *
 * ПРИМЕЧАНИЕ К ЛИЦЕНЗИОННОМУ СОГЛАШЕНИЮ
 *
 * Этот файл связан лицензионным соглашением, которое можно найти в архиве,
 * вместе с этим файлом. Файл лицензии называется: LICENSE.2.0.x-2.1.x-2.2.x-2.3.x.RUS.TXT
 * Так же лицензионное соглашение можно найти по адресу:
 * https://opencart.market/LICENSE.2.0.x-2.1.x-2.2.x-2.3.x.RUS.TXT
 * 
 * =================================================================
 * OPENCART/ocStore 2.0.x-2.1.x-2.2.x-2.3.x ПРИМЕЧАНИЕ ПО ИСПОЛЬЗОВАНИЮ
 * =================================================================
 *  Этот файл предназначен для Opencart/ocStore 2.0.x-2.1.x-2.2.x-2.3.x. Shoputils не
 *  гарантирует правильную работу этого расширения на любой другой 
 *  версии Opencart/ocStore, кроме Opencart/ocStore 2.0.x-2.1.x-2.2.x-2.3.x. 
 *  Shoputils не поддерживает программное обеспечение для других 
 *  версий Opencart/ocStore.
 * =================================================================
*/
class ModelShoputilsDiscounts extends Model {
    public function getList() {
        return $this->db->query('SELECT * FROM '.DB_PREFIX.'shoputils_discounts ORDER BY sort_order, name')->rows;
    }

    public function getDiscounts($discounts_id) {
        return $this->db->query('SELECT * FROM '.DB_PREFIX.'shoputils_discounts WHERE discounts_id = "'.(int)$discounts_id.'"')->row;
    }

    public function applyDiscounts() {
        $cnt = 0;

        $sql = "DELETE FROM ". DB_PREFIX . "product_discount WHERE priority >= 0";
        $this->db->query($sql);

        $sql = 'SELECT * FROM '.DB_PREFIX.'shoputils_discounts WHERE enabled = 1 ORDER BY sort_order, name';
        $query = $this->db->query($sql);

        $this->load->model('catalog/product');

        foreach ($query->rows as $discount){
            $products = array();
            if ($discount['objects_type'] == 1) {
                $category_ids = explode(',', $discount['categories']);
                foreach ($category_ids as $category_id){
                    $products_category = $this->model_catalog_product->getProductsByCategoryId($category_id);
                    $products = array_udiff($products, $products_category, array($this, 'compareProductIds')); //Отсекаем от основного массива дубли товаров
                    $products = array_merge($products, $products_category); //Добавляем товары из новой категории
                }
            } else if ($discount['objects_type'] == 2) {
                $this->load->model('shoputils/product');
                $products = $this->model_shoputils_product->getProducts(explode(',', $discount['products']));
            } else if ($discount['objects_type'] == 3) {
                $this->load->model('shoputils/product');
                $products = $this->model_shoputils_product->getProductsByManufacturersId(explode(',', $discount['manufacturers']));
            } else {
                $products = $this->getAllProducts();
            }

            $cnt += count($products);

            $customer_group_ids = explode(',', $discount['customer_group_ids']);

            foreach ($customer_group_ids as $customer_group_id) {
                foreach ($products as $product){
                    //$price = round($product['price'] - $product['price'] * ($discount['percent'] / 100));
                    $price = floor($product['price'] - $product['price'] * ($discount['percent'] / 100));
                    //$price = $this->currency->format(floor($product['price'] - $product['price'] * ($discount['percent'] / 100)), '', '', false);
                    $sql = "INSERT INTO " .
                                DB_PREFIX . "product_discount
                            SET
                                product_id = '" . (int)$product['product_id'] . "',
                                customer_group_id = '" . (int)$customer_group_id . "',
                                priority = '" . (int)$discount['priority'] . "',
                                quantity = '" . (int) $discount['quantity'] . "',
                                price = '" . (float)$price . "',
                                date_start = '" . $this->db->escape($discount['date_start']) . "',
                                date_end = '" . $this->db->escape($discount['date_end']) . "'";
                    $this->db->query($sql);
                }
            }
        }

        $this->cache->delete('product');
        $this->cache->delete('store');
        $this->cache->delete('category');
		
		$this->load->model('tool/nitro');
		$this->model_tool_nitro->clearPageCache($product_id);

        return $cnt;
    }

    public function makeInstall() {
       $this->db->query('CREATE TABLE IF NOT EXISTS `'.DB_PREFIX.'shoputils_discounts` (
          `discounts_id` int(11) NOT NULL AUTO_INCREMENT,
          `name` varchar(512) NOT NULL,
          `customer_group_ids` text NOT NULL,
          `priority` int(5) NOT NULL,
          `quantity` int(11) NOT NULL,
          `percent` decimal(10,2) NOT NULL,
          `date_start` date NOT NULL,
          `date_end` date NOT NULL,
          `objects_type` int(1) NOT NULL,
          `categories` text NOT NULL,
          `products` text NOT NULL,
          `manufacturers` text NOT NULL,
          `enabled` int(1) NOT NULL DEFAULT 1,
          `sort_order` int(11) NOT NULL,
          PRIMARY KEY (`discounts_id`)
        ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');

        $query = $this->db->query('SHOW INDEXES FROM '.DB_PREFIX.'product');
        $found = false;
        foreach ($query->rows as $row){
            if ($row['Column_name'] == 'manufacturer_id'){
                $found = true;
                break;
            }
        }
        if (!$found){
            $this->db->query('ALTER TABLE '.DB_PREFIX.'product ADD INDEX IDX_manufacturer_id (manufacturer_id)');
        }
    }

    protected function getAllProducts() {
        return $this->db->query("SELECT product_id, price FROM `" . DB_PREFIX . "product`")->rows;
    }

    function compareProductIds($a, $b) {
        return ($a['product_id'] - $b['product_id']);
    }
}
?>