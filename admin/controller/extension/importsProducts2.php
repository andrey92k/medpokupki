<?php

class ControllerExtensionImportsProducts2 extends Controller
{
    public function index($args)
    {
        $this->load->language('extension/installer');
        $this->load->model('catalog/category');
        $categories = $this->model_catalog_category->getAllCategories();
        $data['categories'] = $this->model_catalog_category->getCategories($categories);
        $this->document->setTitle("Импорт товаров 2.0");
        $data['heading_title'] = "Импорт товаров 2.0";
        $data['text_loading'] = $this->language->get('text_loading');
        $data['entry_upload'] = $this->language->get('entry_upload');
        $data['entry_overwrite'] = $this->language->get('entry_overwrite');
        $data['entry_progress'] = $this->language->get('entry_progress');
        $data['help_upload'] = $this->language->get('help_upload');
        $data['button_upload'] = $this->language->get('button_upload');
        $data['button_clear'] = $this->language->get('button_clear');
        $data['button_continue'] = $this->language->get('button_continue');
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['breadcrumbs'][] = array(
            'text' => "Импорт товаров 2.0",
            'href' => $this->url->link('extension/importsProducts', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['token'] = $this->session->data['token'];
        $directories = glob(DIR_UPLOAD . 'temp-*', GLOB_ONLYDIR);
        if ($directories) {
            $data['error_warning'] = $this->language->get('error_temporary');
        } else {
            $data['error_warning'] = '';
        }
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $data_type = empty($args['type']) ? 1 : $args['type'];
        $data_db = $this->db->query("SELECT * FROM `import_settings` WHERE `key` = 'setting_type_$data_type' LIMIT 1")->row;
        if (!empty($data_db)) $data_db = unserialize($data_db['value']);
        else $data_db = [];
        $data['data'] = $data_db;
        $data['status'] = $this->db->query("SELECT * FROM `import_process_status` WHERE `file_type` = '$data_type' ORDER BY `id` DESC LIMIT 0, 10")->rows;
        if($data_type == 3) $this->response->setOutput($this->load->view('extension/importsProducts2-file-2.tpl', $data));
        elseif ($data_type == 2) $this->response->setOutput($this->load->view('extension/importsProducts2-file.tpl', $data));
        else $this->response->setOutput($this->load->view('extension/importsProducts2.tpl', $data));
    }

    public function file2()
    {
        return $this->index(['type' => 3]);
    }

    public function file(){
        return $this->index(['type' => 2]);
    }

    public function saveData()
    {
        if (empty($this->request->post)) {
            $data = [];
        } else {
            $data = $this->request->post;
        }
        if (empty($data['type'])) return;
        $this->db->query("DELETE FROM `import_settings` WHERE `key` = 'setting_type_" . (int)$data['type'] . "'");
        $this->db->query("INSERT INTO `import_settings`(`key`, `value`) VALUES ('setting_type_" . (int)$data['type'] . "', '" . serialize($data) . "')");
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput('');
    }
}