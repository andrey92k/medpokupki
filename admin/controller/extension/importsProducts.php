<?php

class ControllerExtensionImportsProducts extends Controller
{
    public function index()
    {
        $this->load->language('extension/installer');
        $this->load->model('catalog/category');
        $categories = $this->model_catalog_category->getAllCategories();
        $data['categories'] = $this->model_catalog_category->getCategories($categories);

        $this->document->setTitle("Импорт товаров");

        $data['heading_title'] = "Импорт товаров";
        $data['text_loading'] = $this->language->get('text_loading');
        $data['entry_upload'] = $this->language->get('entry_upload');
        $data['entry_overwrite'] = $this->language->get('entry_overwrite');
        $data['entry_progress'] = $this->language->get('entry_progress');
        $data['help_upload'] = $this->language->get('help_upload');
        $data['button_upload'] = $this->language->get('button_upload');
        $data['button_clear'] = $this->language->get('button_clear');
        $data['button_continue'] = $this->language->get('button_continue');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => "Импорт товаров",
            'href' => $this->url->link('extension/importsProducts', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['token'] = $this->session->data['token'];

        $directories = glob(DIR_UPLOAD . 'temp-*', GLOB_ONLYDIR);

        if ($directories) {
            $data['error_warning'] = $this->language->get('error_temporary');
        } else {
            $data['error_warning'] = '';
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/importsProducts.tpl', $data));
    }

    public function upload()
    {
        $json['console'] = "-----------------------------------------------------------" . PHP_EOL;
        $fileValidate = $this->isValidFile();
        if ($fileValidate === true) {
            $json['console'] = $this->fileProcessing($json['console']);
        } else {
            $json['console'] .= $fileValidate . PHP_EOL;
        }
        $json['console'] .= "-----------------------------------------------------------";
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function isCreateColumn()
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "product` LIMIT 0, 1";
        $row = $this->db->query($sql)->row;
        if (isset($row['cmp_code'])) return;
        $sql = "ALTER TABLE `" . DB_PREFIX . "product` ADD COLUMN `cmp_code` int NULL DEFAULT 0";
        $this->db->query($sql);
    }

    private function isValidType($type)
    {

    }

    private function addNomenclature($data)
    {
        $this->load->model('catalog/product');
        /** @var ModelCatalogProduct $model */
        $model = $this->model_catalog_product;
        $data = [
            'upc' => $data['name'],
            'quantity' => 0,
            'stock_status_id' => 7,
            'shipping' => 1,
            'sort_order' => 1,
            'status' => 0,
            'noindex' => 0,
            'sku' => $data['nnt'], //ННТ
            'product_description' => [1 => [
                'name' => $data['name'],
                'description' => $data['name'],
                'tag' => '',
                'meta_title' => '',
                'meta_h1' => '',
                'meta_description' => '',
                'meta_keyword' => ''
            ]],
            //-----------------------------------------------------
            // Поля которые не используем                        ||
            //-----------------------------------------------------
            'model' => '',
            'ean' => '',
            'jan' => '',
            'isbn' => '',
            'mpn' => '',
            'location' => '',
            'minimum' => '',
            'subtract' => '',
            'date_available' => '',
            'manufacturer_id' => '',
            'price' => '',
            'points' => '',
            'weight' => '',
            'weight_class_id' => '',
            'length' => '',
            'width' => '',
            'height' => '',
            'length_class_id' => '',
            'tax_class_id' => ''
        ];
        $model->addProduct($data);
    }

    private function updateNomenclature($data, $update_list)
    {
        if (!empty($update_list)) {
            foreach ($update_list as $item) {
                if ($item['upc'] == $data['name']) continue;
                $sql = "UPDATE `" . DB_PREFIX . "product` SET `upc` = '" . $this->db->escape($data['name']) . "' WHERE `product_id` = " . $item['product_id'] . ";";
                $this->db->query($sql);
                $sql = "UPDATE `" . DB_PREFIX . "product_description` SET `name` = '" . $this->db->escape($data['name']) . "', `description` = '" . $this->db->escape($data['name']) . "' WHERE `product_id` = " . $item['product_id'] . ";";
                $this->db->query($sql);
            }
        }
    }

    private function getProductFromNNT($nnt_id)
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "product` WHERE `sku` = '" . $nnt_id . "'";
        return $this->db->query($sql)->rows;
    }

    private function fileProcessing($console)
    {
        ini_set('max_execution_time', 60 * 20);
        $file = $this->request->files['file'];
        $show_info = fopen($file['tmp_name'], 'r');
        $numStr = 1;
        $updateStr = 0;
        $insertStr = 0;
        $first = true;
        $type = $this->request->post['type'];
        while (true) {
            $str = fgets($show_info);
            if (empty($str)) break;
            $str = str_replace("\r\n", '', $str);
            $data_str = explode(";", $str);
            if ($type == 1 and $first) {
                if ($data_str[0] != "ННТ" and $data_str[1] != "Наименование") {
                    $console .= "Ошибка. Файл не соответсвует формату" . PHP_EOL;
                    return $console;
                }
                if (!is_dir(DIR_UPLOAD . "import_data")) mkdir(DIR_UPLOAD . "import_data", 0777, true);
                if (move_uploaded_file($file['tmp_name'], DIR_UPLOAD . "import_data/nomenclature.csv")) {
                    $console .= "Файл успешно загружен и будет обработан в ближайшее время." . PHP_EOL;
                    return $console;
                } else {
                    $console .= "Ошибка загрузки файла." . PHP_EOL;
                    return $console;
                }
            }
            if ($type == 2) {
                $nameValid = [
                    "﻿Code",
                    "Наименование",
                    "Производитель",
                    "Цена",
                    "Кол-во",
                    "Срок годности",
                    "ШтрихКод",
                    "Cmp_UniCode",
                    "RR_IDD",
                    "Cmp_Name",
                    "ЖНВЛС",
                    "ss1",
                    "ss2",
                ];
                foreach ($data_str as $index => $item_data){
                    if($nameValid[$index] != mb_convert_encoding($item_data,"UTF-8")){
                        $console .= "Ошибка. Файл не соответсвует формату" . PHP_EOL;
                        return $console;
                    }
                }
                if (!is_dir(DIR_UPLOAD . "import_data")) mkdir(DIR_UPLOAD . "import_data", 0777, true);
                if (move_uploaded_file($file['tmp_name'], DIR_UPLOAD . "import_data/sell.csv")) {
                    if(!isset($this->request->post['data_tariff'])){
                        $data_tariff = [];
                    }else{
                        $data_tariff = $this->request->post['data_tariff'];
                    }
                    file_put_contents(DIR_UPLOAD . "import_data/sell.data", json_encode($data_tariff));
                    $console .= "Файл успешно загружен и будет обработан в ближайшее время." . PHP_EOL;
                    return $console;
                } else {
                    $console .= "Ошибка загрузки файла." . PHP_EOL;
                    return $console;
                }
            }
            if ($type == 3) {
                $nameValid = [
                    "﻿Code",
                    "Наименование",
                    "Производитель",
                    "Цена",
                    "Кол-во",
                    "Срок годности",
                    "ШтрихКод",
                    "ss1",
                    "ss2",
                ];
                foreach ($data_str as $index => $item_data){
                    if($nameValid[$index] != mb_convert_encoding($item_data,"UTF-8")){
                        $console .= "Ошибка. Файл не соответсвует формату" . PHP_EOL;
                        return $console;
                    }
                }
                if (!is_dir(DIR_UPLOAD . "import_data")) mkdir(DIR_UPLOAD . "import_data", 0777, true);
                if (move_uploaded_file($file['tmp_name'], DIR_UPLOAD . "import_data/res.csv")) {
                    $console .= "Файл успешно загружен и будет обработан в ближайшее время." . PHP_EOL;
                    return $console;
                } else {
                    $console .= "Ошибка загрузки файла." . PHP_EOL;
                    return $console;
                }
            }
            break;
        }
        $console .= "Записей обновлено: $updateStr" . PHP_EOL;
        $console .= "Записей добавлено: $insertStr" . PHP_EOL;
        return $console;
    }

    private function handleNomenclatureFile($data_str, $console, $updateStr, $insertStr)
    {

    }

    private function isValidFile()
    {
        if (empty($this->request->post['type']) or ($this->request->post['type'] != 1 and $this->request->post['type'] != 2 and $this->request->post['type'] != 3)) return "Выберите тип файла";
        if (!empty($this->request->files['file']['name'])) {
            $file = $this->request->files['file'];
            if ($file['error'] != UPLOAD_ERR_OK) {
                return $this->language->get('error_upload_' . $this->request->files['file']['error']);
            }
            if (mime_content_type($file['tmp_name']) != 'text/plain') {
                return "Не верный формат файла";
            }
            if (pathinfo($file['name'], PATHINFO_EXTENSION) != 'csv') {
                return "Не верный формат файла";
            }
            return true;
        } else {
            return "Файл не найден";
        }
    }

    private function addNomenclatureOLD($data)
    {
        $sql = "INSERT INTO " . DB_PREFIX . "nomenclature SET `name` = '" . $this->db->escape($data['name']) . "', `nnt` = '" . $data['nnt'] . "'";
        $this->db->query($sql);
        return $sql;
    }

    private function updateNomenclatureOLD($data, $id)
    {
        $sql = "UPDATE `" . DB_PREFIX . "nomenclature` SET `name` = '" . $this->db->escape($data['name']) . "' WHERE `nnt` = " . $id . ";";
        $this->db->query($sql);
        return $sql;
    }

    private function getNomenclatureOLD($id)
    {
        $sql = "SELECT * FROM `" . DB_PREFIX . "nomenclature` WHERE `nnt` = '" . $id . "' LIMIT 0, 1";
        return $this->db->query($sql)->row;
    }

    private function informationUpdate()
    {

    }

    private function parsePageInformation()
    {

    }
}