<?php
/*
 * Shoputils
 *
 * ПРИМЕЧАНИЕ К ЛИЦЕНЗИОННОМУ СОГЛАШЕНИЮ
 *
 * Этот файл связан лицензионным соглашением, которое можно найти в архиве,
 * вместе с этим файлом. Файл лицензии называется: LICENSE.2.0.x-2.1.x-2.2.x-2.3.x.RUS.TXT
 * Так же лицензионное соглашение можно найти по адресу:
 * https://opencart.market/LICENSE.2.0.x-2.1.x-2.2.x-2.3.x.RUS.TXT
 * 
 * =================================================================
 * OPENCART/ocStore 2.0.x-2.1.x-2.2.x-2.3.x ПРИМЕЧАНИЕ ПО ИСПОЛЬЗОВАНИЮ
 * =================================================================
 *  Этот файл предназначен для Opencart/ocStore 2.0.x-2.1.x-2.2.x-2.3.x. Shoputils не
 *  гарантирует правильную работу этого расширения на любой другой 
 *  версии Opencart/ocStore, кроме Opencart/ocStore 2.0.x-2.1.x-2.2.x-2.3.x. 
 *  Shoputils не поддерживает программное обеспечение для других 
 *  версий Opencart/ocStore.
 * =================================================================
*/

class ControllerShoputilsDiscounts extends Controller {
    const FILE_NAME_LIC = 'shoputils_discounts.lic';

    public function __construct($registry) {
        parent::__construct($registry);
        $this->load->language('shoputils/discounts');
        $this->load->model('shoputils/discounts');
    }

    public function menu_discounts() {
        if ($this->user->hasPermission('access', 'shoputils/discounts')) {
            $data['menu_selector_id'] = version_compare(VERSION, '2.3', '<') ? 'catalog' : 'menu-catalog';
            $data['menu_discounts'] = $this->language->get('menu_discounts');
            $data['menu_discounts_href'] = $this->makeUrl('shoputils/discounts/settings');

            if (version_compare(VERSION, '2.2.0.0', '<')) {
                return $this->load->view('shoputils/discounts_menu.tpl', $data);
            } else {
                return $this->load->view('shoputils/discounts_menu', $data);
            }
        }
    }

    public function settings() {
        $this->getList();
    }

    protected function getList() {
        if (!is_file(DIR_APPLICATION . self::FILE_NAME_LIC)) {
            $this->response->redirect($this->url->link('shoputils/discounts/lic', '&token=' . $this->session->data['token'], 'SSL'));
        }

        register_shutdown_function(array($this, 'licShutdownHandler'));
        $this->load->model('shoputils/discounts_post');

        $this->model_shoputils_discounts->makeInstall();

        $this->load->model('catalog/category');

        $data['breadcrumbs'][] = array(
            'href' => $this->makeUrl('common/dashboard'),
            'text' => $this->language->get('text_home')
        );

        $data['breadcrumbs'][] = array(
            'href' => $this->makeUrl('shoputils/discounts/settings'),
            'text' => $this->language->get('heading_title')
        );

        $this->document->setTitle($this->language->get('heading_title'));

        $data = array_merge($data, $this->_setData(array(
            'heading_title'   => $this->language->get('heading_title'),
            'insert'          => $this->makeUrl('shoputils/discounts/insert'),
            'delete'          => $this->makeUrl('shoputils/discounts/delete'),
            'apply'           => $this->makeUrl('shoputils/discounts/apply'),
            'text_copyright'  => sprintf($this->language->get('text_copyright'), $this->language->get('heading_title'), date('Y', time())),
            'text_info'       => sprintf($this->language->get('text_info'), DB_PREFIX . 'product_discount', DB_DATABASE),
            'button_insert',
            'button_delete',
            'button_apply',
            'question_apply',
            'column_name',
            'column_enabled',
            'column_objects',
            'column_percent',
            'column_date_start',
            'column_date_end',
            'column_sort_order',
            'column_action',
            'text_no_results',
            'text_confirm'
        )));

        $data['error_warning'] = isset($this->error['warning']) ? $this->error['warning'] : '';

        $data['discounts'] = array();

        $results = $this->model_shoputils_discounts->getList();

        foreach ($results as $result) {
            $action = array();

            $action[] = array(
                'text' => $this->language->get('button_edit'),
                'href' => $this->makeUrl('shoputils/discounts/update', '&discounts_id=' . $result['discounts_id'])
            );

            $data['discounts'][] = array(
                'discounts_id' => $result['discounts_id'],
                'name' => $result['name'],
                'enabled' => (int) $result['enabled'] == 1 ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
                'objects_type' => $this->getObjectTypeText($result['objects_type']),
                'percent' => $result['percent'],
                'date_start' => $result['date_start'],
                'date_end' => $result['date_end'],
                'sort_order' => $result['sort_order'],
                'action' => $action
            );
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data = array_merge($data, $this->_setData(
            array(
                 'header'       => $this->load->controller('common/header'),
                 'column_left'  => $this->load->controller('common/column_left'),
                 'footer'       => $this->load->controller('common/footer')
            )
        ));

        if (version_compare(VERSION, '2.2.0.0', '<')) {
            $this->response->setOutput($this->load->view('shoputils/discounts_settings.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('shoputils/discounts_settings', $data));
        }
    }

    protected function getForm() {
        if (!is_file(DIR_APPLICATION . self::FILE_NAME_LIC)) {
            $this->response->redirect($this->url->link('shoputils/discounts/lic', '&token=' . $this->session->data['token'], 'SSL'));
        }

        register_shutdown_function(array($this, 'licShutdownHandler'));
        $this->load->model('shoputils/discounts_post');

        $this->document->addScript('view/javascript/shoputils/jquery.cookie.min.js');
        $this->document->addScript('view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addStyle('view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');
        $this->document->addStyle('view/stylesheet/discounts.css');

        $data['breadcrumbs'][] = array(
            'href' => $this->makeUrl('common/dashboard'),
            'text' => $this->language->get('text_home')
        );

        $data['breadcrumbs'][] = array(
            'href' => $this->makeUrl('shoputils/discounts/settings'),
            'text' => $this->language->get('heading_title')
        );

        if (!isset($this->request->get['discounts_id'])) {
            $data['action'] = $this->makeUrl('shoputils/discounts/insert');

            $this->document->setTitle($this->language->get('heading_title_insert'));

            $data['breadcrumbs'][] = array(
                'href' => $this->makeUrl('shoputils/discounts/insert'),
                'text' => $this->language->get('heading_title_insert')
            );

            $discounts = array();
        } else {
            $this->document->setTitle($this->language->get('heading_title_update'));

            $data['action'] = $this->makeUrl('shoputils/discounts/update', '&discounts_id=' . $this->request->get['discounts_id']);

            $data['breadcrumbs'][] = array(
                'href' => $this->makeUrl('shoputils/discounts/update'),
                'text' => $this->language->get('heading_title_update')
            );

            $discounts = $this->model_shoputils_discounts->getDiscounts($this->request->get['discounts_id']);
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['error_warning'] = isset($this->error['warning']) ? $this->error['warning'] : '';
        $data['error_name'] = isset($this->error['error_name']) ? $this->error['error_name'] : '';

        if (isset($discounts['customer_group_ids'])) {
            $discounts['customer_group_ids'] = explode(',', $discounts['customer_group_ids']);
        } else {
            $discounts['customer_group_ids'] = array();
        }

        if (isset($discounts['products'])) {
            $discounts['products_array'] = explode(',', $discounts['products']);
        } else {
            $discounts['products'] = '';
            $discounts['products_array'] = array();
        }

        if (isset($discounts['manufacturers'])) {
            $discounts['manufacturers_array'] = explode(',', $discounts['manufacturers']);
        } else {
            $discounts['manufacturers_array'] = array();
        }
        $this->load->model('catalog/manufacturer');

        $discounts['manufacturers'] = $this->model_catalog_manufacturer->getManufacturers();

        if (isset($discounts['categories'])) {
            $discounts['categories_array'] = explode(',', $discounts['categories']);
        } else {
            $discounts['categories'] = '';
            $discounts['categories_array'] = array();
        }

        $data = array_merge($data, $this->_setData(array(
            'categories_dialog' => $this->makeUrl('shoputils/categories_selector/index'),
            'products_dialog'   => $this->makeUrl('shoputils/products_selector/index'),
            'products_table'    => $this->makeUrl('shoputils/products_selector/table', '&field=products_input&selected={selected}'),
            'categories_table'  => $this->makeUrl('shoputils/categories_selector/table', '&field=categories_input&selected={selected}'),
            'cancel'            => $this->makeUrl('shoputils/discounts/settings'),
            'text_copyright'    => sprintf($this->language->get('text_copyright'), $this->language->get('heading_title'), date('Y', time())),
            'entry_name',
            'entry_status',
            'entry_quantity',
            'entry_sort_order',
            'entry_percent',
            'entry_priority',
            'entry_customer_group_ids',
            'entry_date_start',
            'entry_date_end',
            'entry_objects_type',
            'entry_objects_id_0',
            'entry_objects_id_1',
            'entry_objects_id_2',
            'entry_objects_id_3',
            'help_name',
            'help_status',
            'help_quantity',
            'help_sort_order',
            'help_percent',
            'help_priority',
            'help_customer_group_ids',
            'help_date_start',
            'help_date_end',
            'help_objects_type',
            'help_objects_categories',
            'help_objects_products',
            'help_objects_manufacturers',
            'column_objects_product_sku',
            'column_objects_product_name',
            'column_objects_product_price',
            'column_objects_category_name',
            'button_save',
            'button_cancel',
            'button_change',
            'text_enabled',
            'text_disabled',
            'text_select_all',
            'text_unselect_all',
            'text_no_categories',
            'text_no_products',
            'text_select_categories',
            'text_select_products'
        )));

        $data = array_merge($data, $this->_updateData(
            array(
                'name',
                'enabled',
                'percent',
                'sort_order',
                'priority',
                'quantity',
                'customer_group_ids',
                'date_start',
                'date_end',
                'objects_type',
                'products',
                'categories',
                'manufacturers',
                'products_array',
                'categories_array',
                'manufacturers_array',
            ),
            $discounts
        ));

        if (version_compare(VERSION, '2.1.0.0', '<')) {
            $this->load->model('sale/customer_group');
            $data['customer_groups'] = $this->model_sale_customer_group->getCustomerGroups();
        } else {
            $this->load->model('customer/customer_group');
            $data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();
        }

        $data = array_merge($data, $this->_setData(
            array(
                 'header'       => $this->load->controller('common/header'),
                 'column_left'  => $this->load->controller('common/column_left'),
                 'footer'       => $this->load->controller('common/footer')
            )
        ));

        if (version_compare(VERSION, '2.2.0.0', '<')) {
            $this->response->setOutput($this->load->view('shoputils/discounts_form.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('shoputils/discounts_form', $data));
        }
    }

    public function insert() {
        $data['text_error'] = '';

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            if (!is_file(DIR_APPLICATION . self::FILE_NAME_LIC)) {
                $this->response->redirect($this->url->link('shoputils/discounts/lic', '&token=' . $this->session->data['token'], 'SSL'));
            }

            register_shutdown_function(array($this, 'licShutdownHandler'));
            $this->load->model('shoputils/discounts_post');

            $this->model_shoputils_discounts_post->addDiscounts($this->request->post);
			
			// $this->load->model('tool/nitro');
			// $this->model_tool_nitro->clearPageCache();
			
			// $this->clearMegaFilterCache();

            $this->session->data['success'] = $this->language->get('text_success_insert');

            $this->response->redirect($this->makeUrl('shoputils/discounts/settings'));
        }

        $this->getForm();
    }

    public function delete() {
        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            if (!is_file(DIR_APPLICATION . self::FILE_NAME_LIC)) {
                $this->response->redirect($this->url->link('shoputils/discounts/lic', '&token=' . $this->session->data['token'], 'SSL'));
            }

            register_shutdown_function(array($this, 'licShutdownHandler'));
            $this->load->model('shoputils/discounts_post');

            foreach ($this->request->post['selected'] as $discounts_id) {
                $this->model_shoputils_discounts_post->deleteDiscounts($discounts_id);
            }
			
			// $this->load->model('tool/nitro');
			// $this->model_tool_nitro->clearPageCache();
			
			// $this->clearMegaFilterCache();
			

            $this->session->data['success'] = $this->language->get('text_success_delete');

            $this->response->redirect($this->makeUrl('shoputils/discounts/settings'));
        }

        $this->getList();
    }

    public function update() {
        $data['text_error'] = '';

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            if (!is_file(DIR_APPLICATION . self::FILE_NAME_LIC)) {
                $this->response->redirect($this->url->link('shoputils/discounts/lic', '&token=' . $this->session->data['token'], 'SSL'));
            }

            register_shutdown_function(array($this, 'licShutdownHandler'));
            $this->load->model('shoputils/discounts_post');

            $this->model_shoputils_discounts_post->editDiscounts($this->request->get['discounts_id'], $this->request->post);
			
			// $this->load->model('tool/nitro');
			// $this->model_tool_nitro->clearPageCache();
			
			// $this->clearMegaFilterCache();

            $this->session->data['success'] = $this->language->get('text_success_update');

            $this->response->redirect($this->makeUrl('shoputils/discounts/settings'));
        }

        $this->getForm();
    }

    public function apply() {
        if ($this->validateApply()){
            $this->session->data['success'] = sprintf(
                $this->language->get('text_success_apply'),
                $this->model_shoputils_discounts->applyDiscounts()
            );
			
			$this->load->model('tool/nitro');
			$this->model_tool_nitro->clearDBCache();
			$this->model_tool_nitro->clearPageCache();
			
			// $this->clearMegaFilterCache();
		
        }
        $this->getList();
    }

    public function lic() {
        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            if (!$this->user->hasPermission('modify', 'shoputils/discounts')) {
                $this->session->data['warning'] = sprintf($this->language->get('error_permission'), $this->language->get('heading_title'));
            } elseif (!empty($this->request->post['lic_data'])) {
                if (!is_writable(DIR_APPLICATION)) {
                    if (function_exists('chmod')) {
                        $perms = fileperms(DIR_APPLICATION);
                        chmod(DIR_APPLICATION, 0777);
                    }
                }
                
                $lic = '------ LICENSE FILE DATA -------' . "\n";
                $lic .= trim($this->request->post['lic_data']) . "\n";
                $lic .= '--------------------------------' . "\n";
                $file = DIR_APPLICATION . self::FILE_NAME_LIC;
                $handle = @fopen($file, 'w'); 
                fwrite($handle, $lic);
                fclose($handle); 
                if (isset($perms)) {
                    chmod(DIR_APPLICATION, $perms);
                }

                if (!is_file($file)) {
                    $this->session->data['warning'] = sprintf($this->language->get('error_dir_perm'), DIR_APPLICATION);
                    $this->response->redirect($this->url->link('shoputils/discounts/lic', '&token=' . $this->session->data['token'], 'SSL'));
                }

                register_shutdown_function(array($this, 'licShutdownHandler'));
                $this->load->model('shoputils/discounts_post');

                $this->response->redirect($this->url->link('shoputils/discounts/settings', '&token=' . $this->session->data['token'], 'SSL'));
            }
        }

        $this->document->setTitle($this->language->get('heading_title'));

        $domain = str_replace('http://', '', HTTP_SERVER);
        $domain = explode('/', str_replace('https://', '', $domain));
        
        $loader = extension_loaded('ionCube Loader');

        $loader_min_version = '4.5';
        $loader_version = function_exists('ioncube_loader_version') ? ioncube_loader_version() : $loader_min_version;
        $loader_compare = version_compare($loader_version, $loader_min_version, '>=');

        $php_min_version = '5.3';
        $php_version = phpversion();
        $php_compare = version_compare($php_version, $php_min_version, '>=');

        $data = $this->_setData(array(
            'heading_title',
            'button_save',
            'button_cancel',
            'text_ok',
            'text_error',
            'text_get_key',
            'entry_key',
            'error_key',
            'error_php_version',
            'error_loader'          => sprintf($this->language->get('error_loader'), $loader_min_version),
            'error_loader_version'  => sprintf($this->language->get('error_loader_version'), $loader_min_version),
            'error'                 => !($loader && $loader_compare && $php_compare),
            'text_domain'           => sprintf($this->language->get('text_domain'), $domain[0]),
            'text_loader'           => sprintf($this->language->get('text_loader'), $loader_version, $loader_min_version),
            'text_php'              => sprintf($this->language->get('text_php'), $php_version, $php_min_version),
            'action'                => $this->url->link('shoputils/discounts/lic', '&token=' . $this->session->data['token'], 'SSL'),
            'cancel'                => $this->url->link('common/dashboard', '&token=' . $this->session->data['token'], 'SSL'),
            'loader'                => $loader,
            'icon'                  => 'view/image/shoputils_discounts.png',
            'loader_compare'        => $loader_compare,
            'php_compare'           => $php_compare
        ));
        
        if (isset($this->session->data['warning'])) {
          $data['error_warning'] = $this->session->data['warning'];
          unset($this->session->data['warning']);
          if (is_file(DIR_APPLICATION . self::FILE_NAME_LIC)) {
              @unlink(DIR_APPLICATION . self::FILE_NAME_LIC);
          }
        } else {
          $data['error_warning'] = '';
        }

        $data = array_merge($data, $this->_setData(
            array(
                 'header'       => $this->load->controller('common/header'),
                 'column_left'  => $this->load->controller('common/column_left'),
                 'footer'       => $this->load->controller('common/footer')
            )
        ));
        
        if (version_compare(VERSION, '2.2.0.0', '<')) {
            $this->response->setOutput($this->load->view('shoputils/admin/discounts.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('shoputils/admin/discounts', $data));
        }
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'shoputils/discounts')) {
            $data['warning'] = $this->language->get('error_permission');
            return false;
        } elseif (!trim($this->request->post['name'])) {
            $data['warning'] = $data['error_name'] = $this->language->get('error_name');
            return false;
        }
        return true;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'shoputils/discounts')) {
            $data['warning'] = $this->language->get('error_permission');
            return false;
        }
        return true;
    }

    protected function validateApply() {
        if (!$this->user->hasPermission('modify', 'shoputils/discounts')) {
            $data['warning'] = $this->language->get('error_permission');
            return false;
        }
        return true;
    }

    function licShutdownHandler() {
        if (@is_array($e = @error_get_last())) {
            $code = isset($e['type']) ? $e['type'] : 0;
            $msg = isset($e['message']) ? $e['message'] : '';
            if(($code > 0) && (strpos($msg, 'requires a license file') || strpos($msg, 'is not valid for this server'))) {
                $this->session->data['warning'] = $this->language->get('error_key');
                $this->response->redirect($this->makeUrl('shoputils/discounts/lic'));
            }
        }
    }

    protected function _setData($values) {
        $data = array();
        foreach ($values as $key => $value) {
            if (is_int($key)) {
                $data[$value] = $this->language->get($value);
            } else {
                $data[$key] = $value;
            }
        }
        return $data;
    }

    protected function _updateData($keys, $info) {
        $data = array();
        foreach ($keys as $key) {
            if (isset($this->request->post[$key])) {
                $data[$key] = $this->request->post[$key];
            } elseif (isset($info[$key])) {
                $data[$key] = $info[$key];
            } else {
                $data[$key] = $this->config->get($key);
            }
        }
        return $data;
    }

    protected function getObjectTypeText($object_type_id) {
        return $this->language->get('entry_objects_id_' . $object_type_id);
    }

    protected function makeUrl($route, $url = ''){
        if (isset($this->session->data['token'])){
            return str_replace('&amp;', '&', $this->url->link($route, $url.'&token=' . $this->session->data['token'], 'SSL'));
        } else {
            return str_replace('&amp;', '&', $this->url->link($route, $url, 'SSL'));
        }
    }
}