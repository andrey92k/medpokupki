<?php
# Разработчик: Билалов Ильсур
# E-mail: bilalovi@gmail.com
# Skype: oc-designer
# ВК: https://vk.com/ocdesign
# Shiptor - Агрегатор служб доставки

class ControllerShippingShiptor extends Controller {
	private $error;

	public function __construct($registry) {
		parent::__construct($registry);

		$registry->set('shiptor', new Shiptor($registry));
	}

	public function index() {
		$this->load->language('shipping/shiptor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');
		$this->load->model('shipping/shiptor');
		$this->load->model('localisation/weight_class');
		# Получаем списки всех методов доставки
		// $shipping_methods = $this->shiptor->getShippingMethods();
		
		$shipping_methods = $this->getShippingMethods();
		
		// $this->log->write(print_r($shipping_methods, true));
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			if($shipping_methods)$this->model_shipping_shiptor->setShippingMethods($shipping_methods);
			$this->model_setting_setting->editSetting('shiptor', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('shipping/shiptor', 'token=' . $this->session->data['token'], true));
		}

		$data['heading_title']		= $this->language->get('heading_title');

		$data['text_edit']		= $this->language->get('text_edit');
		$data['text_faq']		= $this->language->get('text_faq');
		$data['text_none']		= $this->language->get('text_none');
		$data['text_enabled']		= $this->language->get('text_enabled');
		$data['text_disabled']		= $this->language->get('text_disabled');
		$data['text_none']		= $this->language->get('text_none');
		$data['text_select']		= $this->language->get('text_select');
		$data['text_loading']		= $this->language->get('text_loading');
		$data['text_order_customer']	= $this->language->get('text_order_customer');
		$data['text_order_store']	= $this->language->get('text_order_store');
		$data['text_order_shiptor']	= $this->language->get('text_order_shiptor');
		$data['text_percent']		= $this->language->get('text_percent');
		$data['text_amount']		= $this->language->get('text_amount');

		$data['tab_general']		= $this->language->get('tab_general');
		$data['tab_delivery']		= $this->language->get('tab_delivery');
		$data['tab_setting']		= $this->language->get('tab_setting');

		$data['button_save']		= $this->language->get('button_save');
		$data['button_cancel']		= $this->language->get('button_cancel');
		$data['button_authorization']	= $this->language->get('button_authorization');

		# Default settings
		$data['entry_authorization']	= $this->language->get('entry_authorization');
		$data['entry_country']		= $this->language->get('entry_country');
		$data['entry_logger']		= $this->language->get('entry_logger');
		$data['entry_region']		= $this->language->get('entry_region');

		$data['entry_weight']		= $this->language->get('entry_weight');
		$data['entry_weight_class_id']	= $this->language->get('entry_weight_class_id');
		$data['entry_default_dimensions'] = $this->language->get('entry_default_dimensions');
		$data['entry_status']		= $this->language->get('entry_status');
		$data['entry_sort_order']	= $this->language->get('entry_sort_order');
		$data['entry_days']		= $this->language->get('entry_days');
		$data['entry_time']		= $this->language->get('entry_time');
		$data['entry_cache']		= $this->language->get('entry_cache');
		$data['entry_comment']		= $this->language->get('entry_comment');
		$data['entry_my_comment']	= $this->language->get('entry_my_comment');
		$data['entry_product']		= $this->language->get('entry_product');

		$data['help_authorization']	= $this->language->get('help_authorization');
		$data['help_country']		= $this->language->get('help_country');
		$data['help_region']		= $this->language->get('help_region');
		$data['help_logger']		= $this->language->get('help_logger');
		$data['help_time']		= $this->language->get('help_time');
		$data['help_comment']		= $this->language->get('help_comment');
		$data['help_my_comment']	= $this->language->get('help_my_comment');
		$data['help_product']		= $this->language->get('help_product');
		$data['help_dimensions']	= $this->language->get('help_dimensions');
		$data['help_weight_class_id']	= $this->language->get('help_weight_class_id');
		$data['help_default_dimensions'] = $this->language->get('help_default_dimensions');
		$data['help_increase_days']	= $this->language->get('help_increase_days');

		# Shipping settings
		$data['entry_shipping_status']	= $this->language->get('entry_shipping_status');
		$data['entry_shipping_time']	= $this->language->get('entry_shipping_time');
		$data['entry_method_shipping']	= $this->language->get('entry_method_shipping');

		$data['help_shipping_status']	= $this->language->get('help_shipping_status');
		$data['help_shipping_time']	= $this->language->get('help_shipping_time');
		$data['help_method_shipping']	= $this->language->get('help_method_shipping');

		# Region settings
		$data['entry_region_status']	= $this->language->get('entry_region_status');
		$data['entry_region_hide']	= $this->language->get('entry_region_hide');

		$data['help_region_status']	= $this->language->get('help_region_status');
		$data['help_region_hide']	= $this->language->get('help_region_hide');

		$data['entry_setting']		= $this->language->get('entry_setting');
		$data['entry_weight_price']	= $this->language->get('entry_weight_price');
		$data['entry_total_free']	= $this->language->get('entry_total_free');
		$data['entry_total_min']	= $this->language->get('entry_total_min');
		$data['entry_total_max']	= $this->language->get('entry_total_max');
		$data['entry_total_min_weight']	= $this->language->get('entry_total_min_weight');
		$data['entry_total_max_weight']	= $this->language->get('entry_total_max_weight');
		$data['entry_fixed']		= $this->language->get('entry_fixed');
		$data['entry_type']		= $this->language->get('entry_type');
		$data['entry_markup']		= $this->language->get('entry_markup');
		$data['entry_markup_surcharge']	= $this->language->get('entry_markup_surcharge');
		$data['entry_status']		= $this->language->get('entry_status');

		$data['help_setting']		= $this->language->get('help_setting');
		$data['help_weight_price']	= $this->language->get('help_weight_price');
		$data['help_total_free']	= $this->language->get('help_total_free');
		$data['help_total_min']		= $this->language->get('help_total_min');
		$data['help_total_max']		= $this->language->get('help_total_max');
		$data['help_total_min_weight']	= $this->language->get('help_total_min_weight');
		$data['help_total_max_weight']	= $this->language->get('help_total_max_weight');
		$data['help_fixed']		= $this->language->get('help_fixed');
		$data['help_type']		= $this->language->get('help_type');
		$data['help_markup']		= $this->language->get('help_markup');
		$data['help_markup_surcharge']	= $this->language->get('help_markup_surcharge');
		$data['help_status']		= $this->language->get('help_status');

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->session->data['warning'])) {
			$data['error'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$data['error'] = '';
		}

 		if (isset($this->error['warning'])) {
			$data['error'] = $this->error['warning'];
		} else {
			$data['error'] = '';
		}

		if (isset($this->error['error'])) {
			$data['error'] = $this->language->get('error_warning');

			foreach ($this->error['error'] as $name => $message) {
				$data['error_' . $name] = $message;
			}
		}

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array('text' => $this->language->get('text_home'), 'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true));
		$data['breadcrumbs'][] = array('text' => $this->language->get('text_shipping'), 'href' => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], true));
		$data['breadcrumbs'][] = array('text' => $this->language->get('heading_title') . $this->language->get('text_version'), 'href' => $this->url->link('shipping/shiptor', 'token=' . $this->session->data['token'], true));

		$data['customer'] = $this->url->link('shipping/shiptor/customer', 'token=' . $this->session->data['token'], true);
		$data['order']    = $this->url->link('shipping/shiptor/order', 'token=' . $this->session->data['token'], true);
		$data['shiptor']  = $this->url->link('shipping/shiptor/shiptor', 'token=' . $this->session->data['token'], true);
		$data['action']   = $this->url->link('shipping/shiptor', 'token=' . $this->session->data['token'], true);
		$data['cancel']   = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], true);

		$data['token'] = $this->session->data['token'];

		# Default settings
		if (isset($this->request->post['shiptor_authorization'])) {
			$data['authorization'] = $this->request->post['shiptor_authorization'];
		} else {
			$data['authorization'] = $this->config->get('shiptor_authorization');
		}

		if (isset($this->request->post['shiptor_logger'])) {
			$data['logger'] = $this->request->post['shiptor_logger'];
		} else {
			$data['logger'] = $this->config->get('shiptor_logger');
		}

		if (isset($this->request->post['shiptor_cache'])) {
			$data['cache'] = $this->request->post['shiptor_cache'];
		} else {
			$data['cache'] = $this->config->get('shiptor_cache');
		}

		if (isset($this->request->post['shiptor_status'])) {
			$data['status'] = $this->request->post['shiptor_status'];
		} else {
			$data['status'] = $this->config->get('shiptor_status');
		}

		if (isset($this->request->post['shiptor_sort_order'])) {
			$data['sort_order'] = $this->request->post['shiptor_sort_order'];
		} else {
			$data['sort_order'] = $this->config->get('shiptor_sort_order');
		}

		if (isset($this->request->post['shiptor_weight'])) {
			$data['weight'] = $this->request->post['shiptor_weight'];
		} elseif ($this->config->get('shiptor_weight')) {
			$data['weight'] = $this->config->get('shiptor_weight');
		} else {
			$data['weight'] = 0;
		}

		if (isset($this->request->post['shiptor_default_length'])) {
			$data['default_length'] = $this->request->post['shiptor_default_length'];
		} elseif ($this->config->get('shiptor_default_length')) {
			$data['default_length'] = $this->config->get('shiptor_default_length');
		} else {
			$data['default_length'] = '';
		}

		if (isset($this->request->post['shiptor_default_width'])) {
			$data['default_width'] = $this->request->post['shiptor_default_width'];
		} elseif ($this->config->get('shiptor_default_width')) {
			$data['default_width'] = $this->config->get('shiptor_default_width');
		} else {
			$data['default_width'] = '';
		}

		if (isset($this->request->post['shiptor_default_height'])) {
			$data['default_height'] = $this->request->post['shiptor_default_height'];
		} elseif ($this->config->get('shiptor_default_height')) {
			$data['default_height'] = $this->config->get('shiptor_default_height');
		} else {
			$data['default_height'] = '';
		}
		
		$data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();

		if (isset($this->request->post['shiptor_weight_class_id'])) {
			$data['weight_class_id'] = (int)$this->request->post['shiptor_weight_class_id'];
		} else {
			$data['weight_class_id'] = (int)$this->config->get('shiptor_weight_class_id');
		}

		if (isset($this->request->post['shiptor_days'])) {
			$data['days'] = $this->request->post['shiptor_days'];
		} else {
			$data['days'] = $this->config->get('shiptor_days');
		}
		
		if (isset($this->request->post['shiptor_increase_days'])) {
			$data['increase_days'] = (int)$this->request->post['shiptor_increase_days']<0?0:abs($this->request->post['shiptor_increase_days']);
		} elseif ($this->config->get('shiptor_increase_days')) {
			$data['increase_days'] = (int)$this->config->get('shiptor_increase_days')<0?0:abs($this->config->get('shiptor_increase_days'));
		} else {
			$data['increase_days'] = 0;
		}

		if (isset($this->request->post['shiptor_time'])) {
			$data['time'] = $this->request->post['shiptor_time'];
		} else {
			$data['time'] = $this->config->get('shiptor_time');
		}

		# Получаем списки всех стран доставки
		$data['countries'] = $this->model_shipping_shiptor->getCountries();

		if (isset($this->request->post['shiptor_country'])) {
			$data['country_code'] = $this->request->post['shiptor_country'];
		} elseif ($this->config->get('shiptor_country')) {
			$data['country_code'] = $this->config->get('shiptor_country');
		} else {
			$data['country_code'] = 'RU';
		}

		if (isset($this->request->post['shiptor_regions'])) {
			$regions = $this->request->post['shiptor_regions'];
		} elseif ($this->config->get('shiptor_regions')) {
			$regions = $this->config->get('shiptor_regions');
		} else {
			$regions = array();
		}

		$data['regions'] = array();

		if ($regions) {
			foreach ($regions as $id) {
				if ($region_info = $this->model_shipping_shiptor->getRegionId($id)) {
					$data['regions'][] = array('id' => $region_info['id'], 'name' => $region_info['name']);
				}
			}
		}

		$data['shipping_methods'] = array();

		
		# Shipping settings
		if ($shipping_methods) {
			foreach ($shipping_methods as $shipping_method) {
			
			
				$shipping_method['name'] = $shipping_method['simple_name'] ? $shipping_method['simple_name'] : $shipping_method['name'];
				
				// Иключаем ненужные методы
							if ( !in_array($shipping_method['category'], array('door-to-door', 'door-to-delivery-point')) )
								continue;
							
				// if(stristr($shipping_method['category'],'-to-') || $shipping_method['courier']=='aramex')continue;
				if(!stristr($shipping_method['category'],'-to-') || $shipping_method['courier']=='aramex')continue;
				$data['shipping_methods'][] = $shipping_method;
			}
			
			foreach ($shipping_methods as $shipping_method) {
				
				if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_status'])) {
					$data['status_' . $shipping_method['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_status'];
				} else {
					$data['status_' . $shipping_method['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_status');
				}

				if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_weight_price'])) {
					$data['weight_price_' . $shipping_method['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_weight_price'];
				} else {
					$data['weight_price_' . $shipping_method['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_weight_price');
				}

				if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_total_free'])) {
					$data['total_free_' . $shipping_method['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_total_free'];
				} else {
					$data['total_free_' . $shipping_method['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_total_free');
				}

				if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_total_min'])) {
					$data['total_min_' . $shipping_method['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_total_min'];
				} else {
					$data['total_min_' . $shipping_method['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_total_min');
				}

				if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_total_max'])) {
					$data['total_max_' . $shipping_method['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_total_max'];
				} else {
					$data['total_max_' . $shipping_method['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_total_max');
				}

				if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_total_min_weight'])) {
					$data['total_min_weight_' . $shipping_method['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_total_min_weight'];
				} else {
					$data['total_min_weight_' . $shipping_method['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_total_min_weight');
				}

				if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_total_max_weight'])) {
					$data['total_max_weight_' . $shipping_method['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_total_max_weight'];
				} else {
					$data['total_max_weight_' . $shipping_method['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_total_max_weight');
				}

				if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_fixed'])) {
					$data['fixed_' . $shipping_method['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_fixed'];
				} else {
					$data['fixed_' . $shipping_method['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_fixed');
				}

				if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_type'])) {
					$data['type_' . $shipping_method['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_type'];
				} else {
					$data['type_' . $shipping_method['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_type');
				}

				if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_markup'])) {
					$data['markup_' . $shipping_method['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_markup'];
				} else {
					$data['markup_' . $shipping_method['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_markup');
				}

				if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_markup_surcharge'])) {
					$data['markup_surcharge_' . $shipping_method['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_markup_surcharge'];
				} else {
					$data['markup_surcharge_' . $shipping_method['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_markup_surcharge');
				}

				if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_sort_order'])) {
					$data['sort_order_' . $shipping_method['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_sort_order'];
				} else {
					$data['sort_order_' . $shipping_method['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_sort_order');
				}

				# Shiptor 1-day Курьер
				if ($shipping_method['courier'] == 'shiptor-one-day') {
					if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_time'])) {
						$data['time_' . $shipping_method['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_time'];
					} else {
						$data['time_' . $shipping_method['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_time');
					}
				}

				if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_comment'])) {
					$data['comment_' . $shipping_method['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_comment'];
				} else {
					$data['comment_' . $shipping_method['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_comment');
				}

				if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_my_comment'])) {
					$data['my_comment_' . $shipping_method['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_my_comment'];
				} else {
					$data['my_comment_' . $shipping_method['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_my_comment');
				}

				# Geo zone settings
				foreach ($data['regions'] as $region) {
					if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_weight_price'])) {
						$data['weight_price_' . $shipping_method['id'] . '_' . $region['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_weight_price'];
					} else {
						$data['weight_price_' . $shipping_method['id'] . '_' . $region['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_weight_price');
					}

					if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_total_free'])) {
						$data['total_free_' . $shipping_method['id'] . '_' . $region['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_total_free'];
					} else {
						$data['total_free_' . $shipping_method['id'] . '_' . $region['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_total_free');
					}

					if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_total_min'])) {
						$data['total_min_' . $shipping_method['id'] . '_' . $region['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_total_min'];
					} else {
						$data['total_min_' . $shipping_method['id'] . '_' . $region['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_total_min');
					}

					if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_total_max'])) {
						$data['total_max_' . $shipping_method['id'] . '_' . $region['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_total_max'];
					} else {
						$data['total_max_' . $shipping_method['id'] . '_' . $region['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_total_max');
					}

					if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_total_min_weight'])) {
						$data['total_min_weight_' . $shipping_method['id'] . '_' . $region['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_total_min_weight'];
					} else {
						$data['total_min_weight_' . $shipping_method['id'] . '_' . $region['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_total_min_weight');
					}

					if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_total_max_weight'])) {
						$data['total_max_weight_' . $shipping_method['id'] . '_' . $region['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_total_max_weight'];
					} else {
						$data['total_max_weight_' . $shipping_method['id'] . '_' . $region['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_total_max_weight');
					}

					if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_fixed'])) {
						$data['fixed_' . $shipping_method['id'] . '_' . $region['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_fixed'];
					} else {
						$data['fixed_' . $shipping_method['id'] . '_' . $region['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_fixed');
					}

					if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_markup'])) {
						$data['markup_' . $shipping_method['id'] . '_' . $region['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_markup'];
					} else {
						$data['markup_' . $shipping_method['id'] . '_' . $region['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_markup');
					}

					if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_type'])) {
						$data['type_' . $shipping_method['id'] . '_' . $region['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_type'];
					} else {
						$data['type_' . $shipping_method['id'] . '_' . $region['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_type');
					}

					if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_status'])) {
						$data['status_' . $shipping_method['id'] . '_' . $region['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_status'];
					} else {
						$data['status_' . $shipping_method['id'] . '_' . $region['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_status');
					}

					if (isset($this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_hide'])) {
						$data['hide_' . $shipping_method['id'] . '_' . $region['id']] = $this->request->post['shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_hide'];
					} else {
						$data['hide_' . $shipping_method['id'] . '_' . $region['id']] = $this->config->get('shiptor_' . $shipping_method['id'] . '_' . $region['id'] . '_hide');
					}
				}
			}
		}

		$data['header'] 	 = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] 	 = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('shipping/shiptor.tpl', $data));
	}

	# Список заказов Shiptor
	public function shiptor() {
		$this->load->language('shipping/shiptor');

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array('text' => $this->language->get('text_home'), 'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true));
		$data['breadcrumbs'][] = array('text' => $this->language->get('text_shipping'), 'href' => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], true));
		$data['breadcrumbs'][] = array('text' => $this->language->get('heading_title'), 'href' => $this->url->link('shipping/shiptor', 'token=' . $this->session->data['token'], true));
		$data['breadcrumbs'][] = array('text' => $this->language->get('text_order_shiptor'), 'href' => $this->url->link('shipping/shiptor/shiptor', 'token=' . $this->session->data['token'], true));

		$data['token'] = $this->session->data['token'];

		$data['module']    = $this->url->link('shipping/shiptor', 'token=' . $this->session->data['token'], true);
		$data['customer'] = $this->url->link('shipping/shiptor/customer', 'token=' . $this->session->data['token'], true);
		$data['order']    = $this->url->link('shipping/shiptor/order', 'token=' . $this->session->data['token'], true);


		$data['heading_title']		= $this->language->get('text_order_shiptor');

		$data['text_module_setting']	= $this->language->get('text_module_setting');
		$data['text_order_customer']	= $this->language->get('text_order_customer');
		$data['text_order_store']	= $this->language->get('text_order_store');

		$data['column_shiptor_id']	= $this->language->get('column_shiptor_id');
		$data['column_status']		= $this->language->get('column_status');
		$data['column_order_id']	= $this->language->get('column_order_id');
		$data['column_label']		= $this->language->get('column_label');
		$data['column_customer']	= $this->language->get('column_customer');
		$data['column_phone']		= $this->language->get('column_phone');
		$data['column_email']		= $this->language->get('column_email');
		$data['column_address']		= $this->language->get('column_address');
		$data['column_tracking']	= $this->language->get('column_tracking');
		$data['column_weight']		= $this->language->get('column_weight');
		$data['column_cod']		= $this->language->get('column_cod');
		$data['column_delivery']	= $this->language->get('column_delivery');
		$data['column_time']		= $this->language->get('column_time');
		$data['column_limits']		= $this->language->get('column_limits');
		$data['column_history']		= $this->language->get('column_history');

		$data['header']      = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer']      = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('shipping/shiptor/shiptor.tpl', $data));
	}

	public function shiptor_table() {
		$json = array();

		if ($this->user->hasPermission('modify', 'shipping/shiptor')) {
			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else {
				$page = 0;
			}

			if (isset($this->request->get['per_page'])) {
				$per_page = $this->request->get['per_page'];
			} else {
				$per_page = 50;
			}

			if (isset($this->request->get['filter'])) {
				$filter = $this->request->get['filter'];
			} else {
				$filter = null;
			}

			if ($filter == 'delivered') {
				$params['per_page'] = true;
			}

			if ($filter == 'returned') {
				$params['returned'] = true;
			}

			$params['per_page'] = $per_page;
			$params['page']		= $page / $per_page + 1;

			$orders = $this->shiptor->getPackages($params);

			$total = $this->shiptor->getPackagesCount($params);

			$json['total'] = 0;

			if (isset($total['count'])) {
				$json['total'] = $total['count'];
			}

			$json['rows'] = array();

			if ($orders) {
				foreach ($orders as $order) {
					$customer = '';

					if (! empty($order['departure']['address']['name'])) {
						$customer .= $order['departure']['address']['name'];
					}

					if (! empty($order['departure']['address']['surname'])) {
						$customer .= ' ' . $order['departure']['address']['surname'];
					}

					if (! empty($order['departure']['address']['patronymic'])) {
						$customer .= ' ' . $order['departure']['address']['patronymic'];
					}

					$address = '';

					if (! empty($order['departure']['address']['postal_code'])) {
						$address .= ' ' . $order['departure']['address']['postal_code'];
					}

					if (! empty($order['departure']['address']['administrative_area'])) {
						$address .= ' ' . $order['departure']['address']['administrative_area'];
					}

					if (! empty($order['departure']['address']['settlement'])) {
						$address .= ' ' . $order['departure']['address']['settlement'];
					}

					if (! empty($order['departure']['address']['street'])) {
						$address .= ' ' . $order['departure']['address']['street'];
					}

					if (! empty($order['departure']['address']['house'])) {
						$address .= ' ' . $order['departure']['address']['house'];
					}

					if (! empty($order['departure']['address']['apartment'])) {
						$address .= ' ' . $order['departure']['address']['apartment'];
					}

					$history = array();

					if (isset($order['history'])) {
						foreach ($order['history'] as $value) {
							$history[] = array('date' => date('d-m-Y H:s:i', strtotime($value['date'])), 'event' => $value['event']);
						}
					}

					$json['rows'][] = array(
						'shiptor_id' 	  => $order['id'],
						'status' 	      => $order['status'],
						'label_url'   	  => $order['label_url'],
						'order'   	      => array('order_id' => $order['external_id'], 'href' => $this->url->link('sale/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $order['external_id'], true)),
						'tracking'     	  => $order['tracking_number'],
						'weight'    	  => $order['weight'],
						'cod'     		  => $order['cod'],
						'shipping_method' => $order['departure']['shipping_method']['name'],
						'delivery_time'   => $order['delivery_time'],
						'name'   		  => $customer,
						'email'   		  => $order['departure']['address']['email'],
						'phone'   		  => $order['departure']['address']['phone'],
						'address'   	  => $address,
						'limits'   	  	  => $order['departure']['delivery_point']['limits'],
						'history'   	  => $history,
					);
				}
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function customer() {
		$this->load->language('shipping/shiptor');

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array('text' => $this->language->get('text_home'), 'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true));
		$data['breadcrumbs'][] = array('text' => $this->language->get('text_shipping'), 'href' => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], true));
		$data['breadcrumbs'][] = array('text' => $this->language->get('heading_title'), 'href' => $this->url->link('shipping/shiptor', 'token=' . $this->session->data['token'], true));
		$data['breadcrumbs'][] = array('text' => $this->language->get('text_order_customer'), 'href' => $this->url->link('shipping/shiptor/customer', 'token=' . $this->session->data['token'], true));

		$data['token'] = $this->session->data['token'];

		$data['module']  = $this->url->link('shipping/shiptor', 'token=' . $this->session->data['token'], true);
		$data['order']   = $this->url->link('shipping/shiptor/order', 'token=' . $this->session->data['token'], true);
		$data['shiptor'] = $this->url->link('shipping/shiptor/shiptor', 'token=' . $this->session->data['token'], true);

		$data['heading_title']		= $this->language->get('text_order_customer');

		$data['text_module_setting']	= $this->language->get('text_module_setting');
		$data['text_order_store']	= $this->language->get('text_order_store');
		$data['text_order_shiptor']	= $this->language->get('text_order_shiptor');

		$data['error_is_numeric']	= $this->language->get('error_is_numeric');

		$data['button_filter']		= $this->language->get('button_filter');

		$data['column_recipient']	= $this->language->get('column_recipient');
		$data['column_customer']	= $this->language->get('column_customer');
		$data['column_address']		= $this->language->get('column_address');
		$data['column_kladr_id']	= $this->language->get('column_kladr_id');

		$data['text_filter_customer']	= $this->language->get('text_filter_customer');
		$data['text_filter_email']	= $this->language->get('text_filter_email');
		$data['text_filter_telephone']	= $this->language->get('text_filter_telephone');
		$data['text_filter_kladr_id']	= $this->language->get('text_filter_kladr_id');

		$data['header']      = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer']      = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('shipping/shiptor/customer.tpl', $data));
	}

	public function customer_table() {
		$json = array();

		$this->load->language('shipping/shiptor');

		if ($this->user->hasPermission('modify', 'shipping/shiptor')) {
			$this->load->model('shipping/shiptor');

			if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
				if (isset($this->request->post['type'], $this->request->post['address_id'])) {
					if ($this->request->post['type'] == 'remove') {
						$this->model_shipping_shiptor->removeCustomer($this->request->post['address_id']);

						$json['success'] = $this->language->get('success_remove');
					}

					if ($this->request->post['type'] == 'save') {
						if (isset($this->request->post['kladr_id'])) {
							$this->model_shipping_shiptor->editCustomer($this->request->post['address_id'], $this->request->post['customer_id'], $this->request->post['kladr_id']);

							$json['success'] = $this->language->get('success_update');
						}
					}

					// TODO: перепроверить
					$this->response->addHeader('Content-Type: application/json');
					return $this->response->setOutput(json_encode($json));
				}
			}

			if (isset($this->request->get['filter_customer'])) {
				$filter_customer = $this->request->get['filter_customer'];
			} else {
				$filter_customer = null;
			}

			if (isset($this->request->get['filter_email'])) {
				$filter_email = $this->request->get['filter_email'];
			} else {
				$filter_email = null;
			}

			if (isset($this->request->get['filter_telephone'])) {
				$filter_telephone = $this->request->get['filter_telephone'];
			} else {
				$filter_telephone = null;
			}

			if (isset($this->request->get['filter_kladr_id'])) {
				$filter_kladr_id = $this->request->get['filter_kladr_id'];
			} else {
				$filter_kladr_id = null;
			}

			if (isset($this->request->get['per_page'])) {
				$per_page = $this->request->get['per_page'];
			} else {
				$per_page = 50;
			}

			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else {
				$page = 1;
			}

			$filter_data = array(
				'filter_customer'  => $filter_customer,
				'filter_email'     => $filter_email,
				'filter_telephone' => $filter_telephone,
				'filter_kladr_id'  => $filter_kladr_id,
				'start'            => ($page / $per_page) * $per_page,
				'limit'            => $per_page
			);

			$json['total'] = $this->model_shipping_shiptor->getTotalCustomer($filter_data);

			$json['rows'] = array();

			$customers = $this->model_shipping_shiptor->getCustomer($filter_data);

			if ($customers) {
				foreach ($customers as $customer) {
					$json['rows'][] = array(
						'address_id'  => $customer['address_id'],
						'recipient'   => $customer['recipient'],
						'customer_id' => $customer['customer_id'],
						'customer'    => $customer['customer'],
						'address'     => $customer['address'],
						'kladr_id'    => $customer['kladr_id']
					);
				}
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function order() {
		$this->load->language('shipping/shiptor');

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array('text' => $this->language->get('text_home'), 'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true));
		$data['breadcrumbs'][] = array('text' => $this->language->get('text_shipping'), 'href' => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], true));
		$data['breadcrumbs'][] = array('text' => $this->language->get('heading_title'), 'href' => $this->url->link('shipping/shiptor', 'token=' . $this->session->data['token'], true));
		$data['breadcrumbs'][] = array('text' => $this->language->get('text_order_store'), 'href' => $this->url->link('shipping/shiptor/order', 'token=' . $this->session->data['token'], true));

		$data['module']   = $this->url->link('shipping/shiptor', 'token=' . $this->session->data['token'], true);
		$data['shiptor']  = $this->url->link('shipping/shiptor/shiptor', 'token=' . $this->session->data['token'], true);
		$data['customer'] = $this->url->link('shipping/shiptor/customer', 'token=' . $this->session->data['token'], true);

		$data['token'] = $this->session->data['token'];

		$data['heading_title']		= $this->language->get('text_order_store');

		$data['text_module_setting']	= $this->language->get('text_module_setting');
		$data['text_order_customer']	= $this->language->get('text_order_customer');
		$data['text_order_shiptor']	= $this->language->get('text_order_shiptor');

		$data['text_not_selected']	= $this->language->get('text_not_selected');

		$data['error_is_numeric']	= $this->language->get('error_is_numeric');
		$data['error_is_telephone']	= $this->language->get('error_is_telephone');
		$data['error_is_email']		= $this->language->get('error_is_email');

		$data['button_filter']		= $this->language->get('button_filter');

		$data['column_order_id']	= $this->language->get('column_order_id');
		$data['column_shiptor_id']	= $this->language->get('column_shiptor_id');
		$data['column_customer']	= $this->language->get('column_customer');
		$data['column_email']		= $this->language->get('column_email');
		$data['column_telephone']	= $this->language->get('column_telephone');
		$data['column_delivery']	= $this->language->get('column_delivery');
		$data['column_time']		= $this->language->get('column_time');
		$data['column_kladr_id']	= $this->language->get('column_kladr_id');
		$data['column_point_address']	= $this->language->get('column_point_address');
		$data['column_street']		= $this->language->get('column_street');
		$data['column_house']		= $this->language->get('column_house');
		$data['column_apartment']	= $this->language->get('column_apartment');
		$data['column_weight']		= $this->language->get('column_weight');
		$data['column_cod']		= $this->language->get('column_cod');
		$data['column_payment']		= $this->language->get('column_payment');
		$data['column_total']		= $this->language->get('column_total');
		$data['column_date_added']	= $this->language->get('column_date_added');

		$data['text_filter_customer']	= $this->language->get('text_filter_customer');
		$data['text_filter_email']	= $this->language->get('text_filter_email');
		$data['text_filter_telephone']	= $this->language->get('text_filter_telephone');
		$data['text_filter_date_added']	= $this->language->get('text_filter_date_added');

		$data['times'] = $this->language->get('array_time');

		$data['header']      = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer']      = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('shipping/shiptor/order.tpl', $data));
	}

	public function order_table() {
		$json = array();

		$this->load->language('shipping/shiptor');

		if ($this->user->hasPermission('modify', 'shipping/shiptor')) {
			$this->load->model('shipping/shiptor');

			if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

				if (isset($this->request->post['type'], $this->request->post['order_id'])) {
					if ($this->request->post['type'] == 'remove') {
						$this->model_shipping_shiptor->removeOrder($this->request->post['order_id']);

						$json['success'] = $this->language->get('success_remove');
					}

					if ($this->request->post['type'] == 'save') {
						$this->model_shipping_shiptor->editOrder($this->request->post['order_id'], $this->request->post);

						$json['success'] = $this->language->get('success_update');
					}

					if ($this->request->post['type'] == 'shiptor') {
						$json = $this->setShiptor($this->request->post['order_id']);
					}

					$this->response->addHeader('Content-Type: application/json');
					return $this->response->setOutput(json_encode($json));
				}
			}

			if (isset($this->request->get['filter_customer'])) {
				$filter_customer = $this->request->get['filter_customer'];
			} else {
				$filter_customer = null;
			}

			if (isset($this->request->get['filter_email'])) {
				$filter_email = $this->request->get['filter_email'];
			} else {
				$filter_email = null;
			}

			if (isset($this->request->get['filter_telephone'])) {
				$filter_telephone = $this->request->get['filter_telephone'];
			} else {
				$filter_telephone = null;
			}

			if (isset($this->request->get['filter_date_added'])) {
				$filter_date_added = $this->request->get['filter_date_added'];
			} else {
				$filter_date_added = null;
			}

			if (isset($this->request->get['per_page'])) {
				$per_page = $this->request->get['per_page'];
			} else {
				$per_page = 10;
			}

			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else {
				$page = 1;
			}

			$filter_data = array(
				'filter_customer'   => $filter_customer,
				'filter_email'      => $filter_email,
				'filter_telephone'  => $filter_telephone,
				'filter_date_added' => $filter_date_added,
				'start'             => ($page / $per_page) * $per_page,
				'limit'             => $per_page
			);

			$json['total'] = $this->model_shipping_shiptor->getTotalOrders($filter_data);

			$json['rows'] = array();
			$json['params'] = $filter_data;
			$orders = $this->model_shipping_shiptor->getOrders($filter_data);

			if ($orders) {
				foreach ($orders as $order) {
					if ($order['delivery_point']) {
						$delivery = $this->language->get('text_pickup');
					} else {
						$delivery = $this->language->get('text_courier');
					}

					if ($order['street']) {
						$street = $order['street'];
					} else if ($order['shipping_address_1']) {
						$street = $order['shipping_address_1'];
					} else {
						$street = '';
					}

					$time = '';

					$times = $this->language->get('array_time');

					if ($times && is_array($times)) {
						foreach ($times as $key => $value) {
							if ($key == $order['time']) {
								$time = $value;
								break;
							}
						}
					}

					$payment = $order['payment_method'];

					$json['rows'][] = array(
						'order_id'   => (int)$order['order_id'],
						'shiptor_id' => (int)$order['shiptor_id'],
						'customer'   => $order['firstname'] . ' ' . $order['lastname'],
						'email'      => $order['email'],
						'telephone'  => $order['telephone'],
						'delivery'   => $delivery,
						'time'	 	 => $time,
						'point_id'   => (int)$order['delivery_point'],
						'address'    => $order['address'],
						'kladr_id'   => $order['kladr_id'],
						'street'     => $street,
						'house'      => $order['house'],
						'apartment'  => $order['apartment'],
						'weight'     => (float)$order['weight'],
						'payment'    => $payment,
						'total'      => false !== strpos($order['payment_code'], 'shiptor_') ? (float)$order['total'] : 0,
						'date_added' => date($this->language->get('date_format_short'), strtotime($order['date_added'])),
						'view'       => $this->url->link('sale/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $order['order_id'], true),
						'edit'       => $this->url->link('sale/order/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $order['order_id'], true)
					);
				}
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	private function setShiptor($order_id) {
		$this->load->language('shipping/shiptor');

		$message = array();

		if ($order_id) {
			$params = array();

			$this->load->model('shipping/shiptor');

			$params['external_id'] = $order_id;

			$order = $this->model_shipping_shiptor->getOrder($order_id);

			$params['delivery_time'] = $order['time'];

			$weight = trim($order['weight']);

			if ($weight && is_numeric($weight)) {
				$params['weight'] = (float)$weight;
			}

			$shipping_method = trim($order['shipping_method']);

			if ($shipping_method && is_numeric($shipping_method)) {
				$params['shipping_method'] = $shipping_method;
			} else {
				$message['error']['shipping'] = $this->language->get('error_is_shipping');
			}
			
			if(stristr($order['shipping_code'],'shiptor.russian-post-') && $order['lastname']==''){
				$message['error']['shipping'] = $this->language->get('error_is_surname');
			}

			$delivery_point = trim($order['delivery_point']);

			if ($delivery_point) {
				$params['delivery_point'] = $delivery_point;
			} else {
				if ($order['street']) {
					$params['street'] = trim($order['street']);
				} else if ($order['shipping_address_1']) {
					$this->model_shipping_shiptor->editOrder($order_id, array('street' => trim($order['shipping_address_1'])));

					$params['street'] = trim($order['shipping_address_1']);
				}

				if ($order['house']) {
					$params['house'] = trim($order['house']);
				}

				if ($order['apartment']) {
					$params['apartment'] = trim($order['apartment']);
				}
			}

			if ($order['firstname']) {
				$params['name'] = $order['firstname'];
			}

			if ($order['lastname']) {
				$params['surname'] = $order['lastname']!=''?$order['lastname']:$order['firstname'];
			}
			else{
				$params['surname'] = $order['firstname'];
			}

			$email = utf8_strtolower(trim($order['email']));

			if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$params['email'] = $email;
			} else {
				$message['error']['email'] = $this->language->get('error_is_email');
			}

			if ($order['telephone']) {
				if (preg_match('/^((\+79|79|89|\+77|77|87)+([0-9]){9})$/ui', $order['telephone'])) {
					$params['phone'] = trim($order['telephone']);
				} else {
					$message['error']['telephone'] = $this->language->get('error_is_telephone');
				}
			}

			$kladr_id = trim($order['kladr_id']);

			if ($kladr_id && is_numeric($kladr_id)) {
				$params['kladr_id'] = $kladr_id;
			} else {
				$message['error']['kladr_id'] = $this->language->get('error_is_kladr');
			}

			// вычисляем наложенный платеж и объявленную стоимость

			if ($order['total']) {
				$params['declared_cost'] = $order['total'];

				$params['cod'] = (false !== strpos($order['payment_code'], 'shiptor_')) ? $order['total'] : 0;
			}

			if ($order['payment_code'] == 'shiptor_paycard') {
				$params['cashless_payment'] = 1;
			}
			
			// Пока отправляем только RU
			if ($order['country']) {
				$params['country_code'] = $order['country'];
			}
			
			if ($order['shipping_zone']) {
				$params['administrative_area'] = $order['shipping_zone'];
			}
			
			if ($order['shipping_city']) {
				$params['settlement'] = $order['shipping_city'];
			}

			// проверка наличия услуги с артикулом магазина (если нет - создаем)
			// артикул = url сайта

			$shop_article = mb_substr(HTTPS_CATALOG, 0, 64);

			if (!$this->shiptor->checkServiceByArticle($shop_article)) {
				$data = array('name' => $this->language->get('text_shipping'), 'shopArticle' => $shop_article, 'price' => 0);
				$this->shiptor->addService($data);
			}

			if ($products = $this->model_shipping_shiptor->productsOrder($order_id)) {
				$this->load->model('catalog/product');

				// передаем номенклатуру

				foreach ($products as $product) {
					$product_info = $this->model_catalog_product->getProduct($product['product_id']);

					// TODO: проверить, откуда данные - из product или product_info

					$product_shiptor_params = array(
						'name'          => $product['name'],
						'article'       => empty($product['sku']) ? '' : $product['sku'],
						'shopArticle'   => empty($product['model']) ? '' : $product['model'],
						'length'	=> empty($product['length']) ? '' : $this->length->convert($product['length'], 1, 1),
						'width'         => empty($product['width']) ? '' : $this->length->convert($product['width'],  1, 1),
						'height'        => empty($product['height']) ? '' : $this->length->convert($product['height'], 1, 1),
						'weight'        => empty($product['weight']) ? '' : $this->length->convert($product['weight'], 1, 1),
						'price'         => $product['price'],
					);

					$this->shiptor->addProduct($product_shiptor_params);
				}

				// если товар один, и указаны его размеры - берем их
				// если товар один, и размеров нет - ставим нули, в библиотеке подставятся дефолтные размеры
				// если товаров несколько - пропускаем блок, в библиотеке подставятся дефолтные размеры

				if (1 == sizeof($products)) {
					$product = reset($products);

					if (1 == $product['quantity']) {
						$product_info = $this->model_catalog_product->getProduct($product['product_id']);
						$params = array_merge($params,$this->model_shipping_shiptor->length($product_info));
					}
				}

				# Cписок продуктов
				$params['products'] = array();

				foreach ($products as $product) {
					if (empty($product['quantity'])) {
						$message['error']['product'] = $this->language->get('error_is_quantity');
						continue;
					}

					if (empty($product['price'])) {
						$message['error']['product'] = $this->language->get('error_is_price');
						continue;
					}

					$params['products'][] = array(
						'shopArticle' => isset($product['model']) ? $product['model'] : ''  ,
						'count'       => (int)$product['quantity'],
						'price'       => (int)$product['price']
					);
				}
			}
			
			if (! $message) {
				$shiptor = $this->shiptor->addPackage($params);

				if (! empty($shiptor['id'])) {
					$this->model_shipping_shiptor->editOrder($order_id, array('shiptor_id' => $shiptor['id']));

					$message['success'] = $this->language->get('success_shiptor');
				} else {
					$message['error']['shiptor'] = $this->language->get('error_is_shiptor');
				}
			}
		} else {
			$message['error']['order'] = $this->language->get('error_is_order');
		}

		return $message;
	}



	private function validate() {
		if (! $this->user->hasPermission('modify', 'shipping/shiptor')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (! extension_loaded('curl')) {
			$this->error['warning'] = $this->language->get('error_extension');
		}

		if (empty($this->request->post['shiptor_authorization'])) {
			$this->error['error']['authorization'] = $this->language->get('error_authorization');
		}
		
		// при активации модуля данные еще не передаются, но валидация уже идет (?!), поэтому используем isset на переменные
		
		if (isset($this->request->post['shiptor_weight'])) {
			if (empty($this->request->post['shiptor_weight'])) {
				$this->error['error']['default_weight'] = $this->language->get('error_default_weight');
			}
		}

		if (isset($this->request->post['shiptor_default_length'], $this->request->post['shiptor_default_width'], $this->request->post['shiptor_default_height'])) {
			if (empty($this->request->post['shiptor_default_length'])
			&& empty($this->request->post['shiptor_default_width'])
			&& empty($this->request->post['shiptor_default_height'])) {
				$this->error['error']['default_dimensions'] = $this->language->get('error_default_dimensions');
			}
		}
		
		foreach($this->getShippingMethods() as $shipping_method){
			if(isset($this->request->post['shiptor_'.$shipping_method['id'].'_type']) && $this->request->post['shiptor_'.$shipping_method['id'].'_type']=='P' && !empty($this->request->post['shiptor_'.$shipping_method['id'].'_markup']) &&
				((int)$this->request->post['shiptor_'.$shipping_method['id'].'_markup']>100 || (int)$this->request->post['shiptor_'.$shipping_method['id'].'_markup']<-100)){
				$this->error['error'][$shipping_method['id'].'_markup'] = $this->language->get('error_is_markup');
			}
			if(isset($this->request->post['shiptor_'.$shipping_method['id'].'_type']) && $this->request->post['shiptor_'.$shipping_method['id'].'_type']=='P' && !empty($this->request->post['shiptor_'.$shipping_method['id'].'_markup_surcharge']) &&
				((int)$this->request->post['shiptor_'.$shipping_method['id'].'_markup_surcharge']>100 || (int)$this->request->post['shiptor_'.$shipping_method['id'].'_markup_surcharge']<-100)){
				$this->error['error'][$shipping_method['id'].'_markup_surcharge'] = $this->language->get('error_is_markup');
			}
		}
		return ! $this->error;
	}

	public function regions() {
		$json = array();

		if (isset($this->request->get['filter_name']) && $this->user->hasPermission('modify', 'shipping/shiptor')) {

			$this->load->model('shipping/shiptor');

			$data = array('filter_name' => $this->request->get['filter_name'], 'start' => 0, 'limit' => 20);

			$results = $this->model_shipping_shiptor->getRegions($data);

			foreach ($results as $result) {
				$json[] = array('id' => $result['id'], 'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')));
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function auth() {
		$this->load->language('shipping/shiptor');

		$json = array();

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->user->hasPermission('modify', 'shipping/shiptor')) {
			if ($this->config->get('shiptor_authorization')) {
				$this->load->model('shipping/shiptor');

				$shipping_methods = $this->getShippingMethods();

				if ($shipping_methods) {
					$this->model_shipping_shiptor->setShippingMethods($shipping_methods);

					$сountries = $this->shiptor->getCountries();
					$this->model_shipping_shiptor->setCountries($сountries);

					$json['success'] = true;
				} else {
					$json['error'] = $this->language->get('error_key');
				}
			} else {
				$json['error'] = $this->language->get('error_setting');
			}
		} else {
			$json['error'] = $this->language->get('error_permission');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function install() {
		$this->load->model('shipping/shiptor');
		$this->model_shipping_shiptor->install();
	}

	public function uninstall() {
		$this->load->model('shipping/shiptor');
		$this->model_shipping_shiptor->uninstall();
	}
	
	
	# получение данных о доставке
		private function getShippingMethods() {
			return $this->db->query('SELECT * FROM `' . DB_PREFIX . 'shipping_shiptor_couriers`')->rows;
		}
		
}
