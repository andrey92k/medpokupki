<?php
// Heading
$_['heading_title']        = 'Contact Form';

//Text
$_['text_module']          = 'Modules';
$_['text_success']         = 'Success: You have modified Contact Form';
$_['text_edit']            = 'Edit Contact Form';

//Entry
$_['entry_status']         = 'Status';
$_['entry_title']         = 'Module Title';
$_['entry_text']         = 'Message above form (optional)';


//Error
$_['error_permission']     = 'Warning: You do not have permission to modify Contact Form';