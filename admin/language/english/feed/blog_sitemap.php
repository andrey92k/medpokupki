<?php
// *	@copyright	OCSHOP.CMS \ ocshop.net 2011 - 2015.
// *	@demo	http://ocshop.net
// *	@blog	http://ocshop.info
// *	@forum	http://forum.ocshop.info
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']    = 'Blog Sitemap';

// Text
$_['text_feed']        = 'Feeds';
$_['text_success']     = 'Success: You have modified Blog Sitemap feed!';
$_['text_edit']        = 'Edit Blog Sitemap';

// Entry
$_['entry_status']     = 'Status';
$_['entry_data_feed']  = 'Data Feed Url';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Blog Sitemap feed!';