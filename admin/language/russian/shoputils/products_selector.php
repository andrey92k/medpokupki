<?php
/*
 * Shoputils
 *
 * ПРИМЕЧАНИЕ К ЛИЦЕНЗИОННОМУ СОГЛАШЕНИЮ
 *
 * Этот файл связан лицензионным соглашением, которое можно найти в архиве,
 * вместе с этим файлом. Файл лицензии называется: LICENSE.2.0.x-2.1.x-2.2.x-2.3.x.RUS.TXT
 * Так же лицензионное соглашение можно найти по адресу:
 * https://opencart.market/LICENSE.2.0.x-2.1.x-2.2.x-2.3.x.RUS.TXT
 * 
 * =================================================================
 * OPENCART/ocStore 2.0.x-2.1.x-2.2.x-2.3.x ПРИМЕЧАНИЕ ПО ИСПОЛЬЗОВАНИЮ
 * =================================================================
 *  Этот файл предназначен для Opencart/ocStore 2.0.x-2.1.x-2.2.x-2.3.x. Shoputils не
 *  гарантирует правильную работу этого расширения на любой другой 
 *  версии Opencart/ocStore, кроме Opencart/ocStore 2.0.x-2.1.x-2.2.x-2.3.x. 
 *  Shoputils не поддерживает программное обеспечение для других 
 *  версий Opencart/ocStore.
 * =================================================================
*/

$_['button_filter'] = 'Фильтровать';
$_['entry_filter_name'] = 'Название';
$_['button_cancel'] = 'Отмена';
$_['button_select'] = 'Выбрать';
$_['column_id'] = 'ID';
$_['column_sku'] = 'Код';
$_['column_name'] = 'Название';
$_['column_price'] = 'Цена';
$_['text_no_products'] = 'Нет товаров';
$_['text_products_selected'] = 'Всего товаров выбрано: ';
$_['text_delete']   = 'Удалить';
$_['text_edit']     = 'Редактировать';
$_['heading_title'] = 'Выбор товаров';
?>