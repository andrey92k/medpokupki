<?php
# Разработчик: Билалов Ильсур
# E-mail: bilalovi@gmail.com
# Skype: oc-designer
# ВК: https://vk.com/ocdesign
# Shiptor - Агрегатор служб доставки

$_['heading_title']     = 'Shiptor - Оплата с сайта';
$_['heading_name']     = 'Shiptor - Оплата с сайта';

$_['text_total']       = 'Оплата с сайта';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Настройки';
$_['text_faq']         = 'Документация';

$_['entry_status']     = 'Статус';
$_['entry_sort_order'] = 'Порядок сортировки';
$_['entry_insuranse']  = 'Считать страховку';

$_['error_permission'] = 'У Вас нет прав для управления данным модулем!';