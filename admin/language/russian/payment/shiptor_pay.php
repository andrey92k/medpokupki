<?php
# Разработчик: Билалов Ильсур
# E-mail: bilalovi@gmail.com
# Skype: oc-designer
# ВК: https://vk.com/ocdesign
# Shiptor - Агрегатор служб доставки

$_['heading_title']    	 = 'Shiptor - Оплата наличными при получении';

$_['text_payment']		 = 'Оплата';
$_['text_success']		 = 'Настройки модуля успешно обновлены!';
$_['text_edit']          = 'Редактирование модуля';
$_['text_faq']           = 'Документация';

$_['entry_order_status'] = 'Статус заказа после оплаты';
$_['entry_status'] 		 = 'Статус';
$_['entry_sort_order']   = 'Порядок сортировки';

$_['error_permission']	 = 'У Вас нет прав для управления этим модулем!';