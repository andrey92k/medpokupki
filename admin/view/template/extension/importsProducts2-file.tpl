<?php $i = 1; ?>
<?php echo $header; ?>
    <script src="/admin/view/javascript/jquery.inputmask.bundle.js"></script>
    <script>
        $(document).ready(function () {
            $(".time").inputmask({"alias":"Regex","regex":"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$"});
        });
    </script>
<?php echo $column_left; ?>
    <div id="content">
        <div class="page-header">
            <div class="container-fluid">
                <h1><?php echo $heading_title; ?></h1>
                <div class="pull-right">
                    <button type="submit" form="form-product" data-toggle="tooltip" title="Добавить время" class="btn btn-success add-item-tariff">
                        <i class="fa fa-plus"></i>
                    </button>
                    <button type="submit" form="form-product" data-toggle="tooltip" title="Сохранить" class="btn btn-primary save-data">
                        <i class="fa fa-save"></i>
                    </button>
                </div>
                <ul class="breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <?php if ($error_warning) { ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            <?php } ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-puzzle-piece"></i> <?php echo $heading_title; ?></h3>
                </div>
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li><a href="index.php?route=extension/importsProducts2&token=<?php echo $token; ?>">Номенклатура</a></li>
                        <li class="active"><a href="index.php?route=extension/importsProducts2/file&token=<?php echo $token; ?>">Товары поставщиков</a></li>
                        <li><a href="index.php?route=extension/importsProducts2/file2&token=<?php echo $token; ?>">Товары в аптеке</a></li>
                    </ul>
                    <form class="form-horizontal" enctype="text/plain">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-category">Файл:</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="type_file" disabled="disabled">
                                    <option value="0"> --- Не выбрано ---</option>
                                    <option value="1"> Номенклатура</option>
                                    <option value="2" selected="selected"> Товары поставщиков</option>
                                    <option value="3"> Товары в аптеке</option>
                                </select>
                                <input type="hidden" name="type" value="2">
                            </div>
                        </div>
                        <div class="tariff form-group" style="margin: 0; padding: 0">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-category">Тип наценки:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="type_tariff">
                                        <option value="1" selected="selected"> Диапазонная</option>
                                        <option value="2"> Фиксированная</option>
                                        <option value="3"> Диапазонная по категориям</option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <button type="button" class="btn btn-success add-item-tariff-second">Добавить</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group tariff-item">
                            <label class="col-sm-2 control-label" for="input-category">Путь:</label>
                            <div class="col-sm-10">
                                <input type="text" name="path" value="<?= !empty($data['path']) ? $data['path'] : '' ?>" placeholder="/var/www/" class="form-control" autocomplete="off">
                            </div>
                        </div>

                        <div class="tariff form-group" style="margin: 0; padding: 0">
                            <div class="params-tariff form-group" style="margin: 0; padding: 0">
                                <?php if(!empty($data['time'])){ ?>
                                    <?php foreach ($data['time'] as $time){ ?>
                                        <div class="form-group tariff-item">
                                            <label class="col-sm-2 control-label" for="input-category">Запуск импорта в:</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="time[<?= $i ?>]" value="<?= $time ?>" placeholder="15:00" class="form-control time" autocomplete="off">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button" class="btn btn-danger remove-tariff">Удалить</button>
                                            </div>
                                        </div>
                                        <?php $i++; ?>
                                    <?php } ?>
                                <?php } ?>
                                <?php if(!empty($data['data_tariff'])) { ?>
                                    <?php foreach ($data['data_tariff'] as $tariff) { ?>
                                        <?php if($tariff['type'] == 1){ ?>
                                            <div class="form-group tariff-item">
                                                <label class="col-sm-2 control-label" for="input-category">Диапазонная:</label>
                                                <div class="col-sm-2">
                                                    <input type="text" name="data_tariff[<?= $i ?>][from]" value="<?= !empty($tariff['from']) ? $tariff['from'] : '' ?>" placeholder="Цена от" class="form-control" autocomplete="off">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" name="data_tariff[<?= $i ?>][to]" value="<?= !empty($tariff['to']) ? $tariff['to'] : '' ?>" placeholder="Цено до" class="form-control" autocomplete="off">
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" name="data_tariff[<?= $i ?>][value]" value="<?= !empty($tariff['value']) ? $tariff['value'] : '' ?>" placeholder="Значение" class="form-control" autocomplete="off">
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="button" class="btn btn-danger remove-tariff">Удалить</button>
                                                </div>
                                                <input type="hidden" name="data_tariff[<?= $i ?>][type]" value="1">
                                            </div>
                                        <?php } ?>
                                        <?php if($tariff['type'] == 2){ ?>
                                            <div class="form-group tariff-item">
                                                <label class="col-sm-2 control-label" for="input-category">Фиксированная:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" name="data_tariff[<?= $i ?>][sku]" value="<?= !empty($tariff['sku']) ? $tariff['sku'] : '' ?>" placeholder="Артикул" class="form-control" autocomplete="off">
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" name="data_tariff[<?= $i ?>][value]" value="<?= !empty($tariff['value']) ? $tariff['value'] : '' ?>" placeholder="Значение" class="form-control" autocomplete="off">
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="button" class="btn btn-danger remove-tariff">Удалить</button>
                                                    <input type="hidden" name="data_tariff[<?= $i ?>][type]" value="2">
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php if($tariff['type'] == 3){ ?>
                                            <div class="form-group tariff-item">
                                                <label class="col-sm-2 control-label" for="input-category">Диапазонная по категориям:</label>
                                                <div class="col-sm-2">
                                                    <select class="form-control" name="category_id">
                                                        <option value="0">--- Не выбрано ---</option>
                                                        <?php foreach ($categories as $category) { ?>
                                                            <option <?php if(!empty($tariff['category_id']) and $category['category_id'] == $tariff['category_id']) echo 'selected="selected"'; ?> value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" name="from" value="<?= !empty($tariff['from']) ? $tariff['from'] : '' ?>" placeholder="Цена от" class="form-control" autocomplete="off">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" name="to" value="<?= !empty($tariff['to']) ? $tariff['to'] : '' ?>" placeholder="Цено до" class="form-control" autocomplete="off">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" name="data_tariff[<?= $i ?>][value]" value="<?= !empty($tariff['value']) ? $tariff['value'] : '' ?>" placeholder="Значение" class="form-control" autocomplete="off">
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="button" class="btn btn-danger remove-tariff">Удалить</button>
                                                </div>
                                                <input type="hidden" name="data_tariff[<?= $i ?>][type]" value="3">
                                            </div>
                                        <?php } ?>
                                        <?php $i++; ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </form>
                    <?php if(!empty($status)){ ?>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <td class="text-left">Время операции</td>
                                <td class="text-left">Статус</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($status as $st){ ?>
                                <tr>
                                    <td class="text-left"><?= DateTime::createFromFormat('Y-m-d H:i:s', $st['create_time'])->format('d.m.Y H:i') ?></td>
                                    <td class="text-left">
                                        <?= $st['status'] == 0 ? 'В обработке' : '' ?>
                                        <?= $st['status'] == 1 ? 'Готово' : '' ?>
                                        <?= $st['status'] == 2 ? 'Ошибка' : '' ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="hide data-row">
            <div class="form-group tariff-item">
                <label class="col-sm-2 control-label" for="input-category">Диапазонная:</label>
                <div class="col-sm-2">
                    <input type="text" name="from" value="" placeholder="Цена от" class="form-control" autocomplete="off">
                </div>
                <div class="col-sm-2">
                    <input type="text" name="to" value="" placeholder="Цено до" class="form-control" autocomplete="off">
                </div>
                <div class="col-sm-4">
                    <input type="text" name="value" value="" placeholder="Значение" class="form-control" autocomplete="off">
                </div>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-danger remove-tariff">Удалить</button>
                </div>
                <input type="hidden" name="type" value="1">
            </div>
            <div class="form-group tariff-item">
                <label class="col-sm-2 control-label" for="input-category">Фиксированная:</label>
                <div class="col-sm-4">
                    <input type="text" name="sku" value="" placeholder="Артикул" class="form-control" autocomplete="off">
                </div>
                <div class="col-sm-4">
                    <input type="text" name="value" value="" placeholder="Значение" class="form-control" autocomplete="off">
                </div>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-danger remove-tariff">Удалить</button>
                    <input type="hidden" name="type" value="2">
                </div>
            </div>
            <div class="form-group tariff-item">
                <label class="col-sm-2 control-label" for="input-category">Диапазонная по категориям:</label>
                <div class="col-sm-2">
                    <select class="form-control" name="category_id">
                        <option value="0" selected="selected">--- Не выбрано ---</option>
                        <?php foreach ($categories as $category) { ?>
                            <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-sm-2">
                    <input type="text" name="from" value="" placeholder="Цена от" class="form-control" autocomplete="off">
                </div>
                <div class="col-sm-2">
                    <input type="text" name="to" value="" placeholder="Цено до" class="form-control" autocomplete="off">
                </div>
                <div class="col-sm-2">
                    <input type="text" name="value" value="" placeholder="Значение" class="form-control" autocomplete="off">
                </div>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-danger remove-tariff">Удалить</button>
                </div>
                <input type="hidden" name="type" value="3">
            </div>
            <div class="form-group tariff-item">
                <label class="col-sm-2 control-label" for="input-category">Запуск импорта в:</label>
                <div class="col-sm-8">
                    <input type="text" name="time" value="" placeholder="15:00" class="form-control time" autocomplete="off">
                </div>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-danger remove-tariff">Удалить</button>
                </div>
            </div>
        </div>
        <script type="text/javascript"><!--
            var count_add_item = <?= $i ?>;
            $(document).on('click', ".add-item-tariff", function () {
                var t = 4;
                var d = $('.data-row .form-group').eq(t - 1).clone().appendTo(".params-tariff");
                d.find("input, select").each(function () {
                    var name = $(this).attr('name');
                    $(this).attr('name', 'time[' + count_add_item + ']');
                });
                count_add_item++;
                $(".time").inputmask({"alias":"Regex","regex":"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$"});
                return false;
            });

            $(document).on('click', ".add-item-tariff-second", function () {
                var t = $('select[name="type_tariff"]').val();
                var d = $('.data-row .form-group').eq(t-1).clone().appendTo(".params-tariff");
                d.find("input, select").each(function(){
                    var name = $(this).attr('name');
                    $(this).attr('name', 'data_tariff['+count_add_item+']['+name+']');
                });
                count_add_item++;
                return false;
            });

            $(document).on('click', ".save-data", function () {
                $("#form-upload .params-tariff").remove();
                if ($('select[name="type_file"]').val() == 2) {
                    $('.tariff .params-tariff').clone().appendTo("#form-upload");
                }
                $.post('index.php?route=extension/importsProducts2/saveData&token=<?php echo $token; ?>', $('.form-horizontal').serialize(), function () {
                    location.reload();
                });
                return false;
            });

            $(document).on('click', '.remove-tariff', function () {
                $(this).parents('.tariff-item').remove();
            });
            //--></script>
    </div>
<?php echo $footer; ?>