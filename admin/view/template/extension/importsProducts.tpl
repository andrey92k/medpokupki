<?php echo $header; ?><?php echo $column_left; ?>
    <div id="content">
        <div class="page-header">
            <div class="container-fluid">
                <h1><?php echo $heading_title; ?></h1>
                <ul class="breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <?php if ($error_warning) { ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            <?php } ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-puzzle-piece"></i> <?php echo $heading_title; ?></h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-category">Файл:</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="type_file">
                                    <option value="0" selected="selected"> --- Не выбрано ---</option>
                                    <option value="1"> Номенклатура</option>
                                    <option value="2"> Товары поставщиков</option>
                                    <option value="3"> Товары в аптеке</option>
                                </select>
                            </div>
                        </div>

                        <div class="tariff hide form-group" style="margin: 0; padding: 0">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-category">Тип наценки:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="type_tariff">
                                        <option value="1" selected="selected"> Диапазонная</option>
                                        <option value="2"> Фиксированная</option>
                                        <option value="3"> Диапазонная по категориям</option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <button type="button" class="btn btn-success add-item-tariff">Добавить</button>
                                </div>
                            </div>
                            <div class="params-tariff form-group" style="margin: 0; padding: 0">

                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_upload; ?>:</label>
                            <div class="col-sm-10">
                                <button type="button" id="button-upload" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_upload; ?></button>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Консоль:</label>
                            <div class="col-sm-10">
                                <textarea rows="10" readonly id="overwrite" class="form-control"></textarea>
                                <br/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="hide data-row">
            <div class="form-group tariff-item">
                <label class="col-sm-2 control-label" for="input-category">Диапазонная:</label>
                <div class="col-sm-2">
                    <input type="text" name="from" value="" placeholder="Цена от" class="form-control" autocomplete="off">
                </div>
                <div class="col-sm-2">
                    <input type="text" name="to" value="" placeholder="Цено до" class="form-control" autocomplete="off">
                </div>
                <div class="col-sm-4">
                    <input type="text" name="value" value="" placeholder="Значение" class="form-control" autocomplete="off">
                </div>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-danger remove-tariff">Удалить</button>
                </div>
                <input type="hidden" name="type" value="1">
            </div>
            <div class="form-group tariff-item">
                <label class="col-sm-2 control-label" for="input-category">Фиксированная:</label>
                <div class="col-sm-4">
                    <input type="text" name="sku" value="" placeholder="Артикул" class="form-control" autocomplete="off">
                </div>
                <div class="col-sm-4">
                    <input type="text" name="value" value="" placeholder="Значение" class="form-control" autocomplete="off">
                </div>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-danger remove-tariff">Удалить</button>
                    <input type="hidden" name="type" value="2">
                </div>
            </div>
            <div class="form-group tariff-item">
                <label class="col-sm-2 control-label" for="input-category">Диапазонная по категориям:</label>
                <div class="col-sm-4">
                    <select class="form-control" name="category_id">
                        <option value="0" selected="selected">--- Не выбрано ---</option>
                        <?php foreach ($categories as $category) { ?>
                            <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-sm-4">
                    <input type="text" name="value" value="" placeholder="Значение" class="form-control" autocomplete="off">
                </div>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-danger remove-tariff">Удалить</button>
                </div>
                <input type="hidden" name="type" value="3">
            </div>
        </div>
        <script type="text/javascript"><!--
            var count_add_item = 1;
            $(document).on('click', ".add-item-tariff", function () {
                var t = $('select[name="type_tariff"]').val();
                var d = $('.data-row .form-group').eq(t-1).clone().appendTo(".params-tariff");
                d.find("input, select").each(function(){
                    var name = $(this).attr('name');
                    $(this).attr('name', 'data_tariff['+count_add_item+']['+name+']');
                });
                count_add_item++;
                return false;
            });
            $(document).on('change', 'select[name="type_file"]', function () {
                if ($(this).val() == 2) {
                    $('.tariff').removeClass("hide");
                } else {
                    $('.tariff').addClass("hide");
                }
            });
            $(document).on('click', '.remove-tariff', function(){
                $(this).parents('.tariff-item').remove();
            });
            $('#button-upload').on('click', function () {
                $('#form-upload').remove();

                $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /><input type="text" name="type" /></form>');
                $("#form-upload .params-tariff").remove();
                if($('select[name="type_file"]').val() == 2){
                    $('.tariff .params-tariff').clone().appendTo("#form-upload");
                }


                $('#form-upload input[name=\'file\']').trigger('click');

                if (typeof timer != 'undefined') {
                    clearInterval(timer);
                }

                timer = setInterval(function () {
                    if ($('#form-upload input[name=\'file\']').val() != '') {
                        clearInterval(timer);
                        $('input[name="type"]').val($('select[name="type_file"]').val());

                        // Reset everything
                        $('.alert').remove();
                        $('#progress-bar').css('width', '0%');
                        $('#progress-bar').removeClass('progress-bar-danger progress-bar-success');
                        $('#progress-text').html('');

                        $.ajax({
                            url: 'index.php?route=extension/importsProducts/upload&token=<?php echo $token; ?>',
                            type: 'post',
                            dataType: 'json',
                            data: new FormData($('#form-upload')[0]),
                            cache: false,
                            contentType: false,
                            processData: false,
                            beforeSend: function () {
                                $('#button-upload').button('loading');
                            },
                            complete: function () {
                                $('#button-upload').button('reset');
                            },
                            success: function (json) {
                                if (json['console']) {
                                    $("#overwrite").text(json['console']);
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                }, 500);
            });
            //--></script>
    </div>
<?php echo $footer; ?>