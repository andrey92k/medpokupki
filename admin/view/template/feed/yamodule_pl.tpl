<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
		<div class="pull-right"> <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
		<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
		</ul>
		</div>
	</div>
	<div class="container-fluid">
				<div class="panel panel-default">
					<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-pencil"></i>Я.Маркет Advanced YML</h3>
				</div>
				<ul class="nav nav-tabs">
					<li><a href="#tab-market" data-toggle="tab"><?php echo $market; ?></a></li>
					<li><a href="#tab-product-options" data-toggle="tab">Размерная сетка</a></li>
				</ul>
				<div class="tab-content bootstrap">
					<div class="tab-pane" id="tab-product-options">
						<?php foreach ($product_options_status as $m) { echo $m; } ?>
						<div class="panel panel-default">
							<div class="panel-body">
								<form id="product-options_form" action="<?php echo $action; ?>" method="POST" class="form-horizontal">
									<input type="hidden" value="product_options" name="type_data"/>
									<table class="table table-condensed">
										<thead>
											<tr>
												<td>Название</td>
												<td>Unit</td>
											</tr>
										</thead>
										<tbody>
											<?php foreach($ya_market_pl_product_options_unit as $option_val): ?>
											<tr>
												<td><label for="" class="col-sm-3z"><?=$option_val['name'];?></label> </td>
												<td>
													<input class="form-control" type="text" name="ya_market_pl_product_options_unit[<?=$option_val['id'];?>]" value="<?=$option_val['unit'];?>">
												</td>
											</tr>
											<?php endforeach;?>
										</tbody>
									</table>
									<button type="button" onclick="$('#product-options_form').submit(); return false;" class="btn btn-default">
										<i class="process-icon-save"></i> <?php echo $market_sv; ?>
									</button>									
								</form>
							</div>
						</div>
					</div>

					<div class="tab-pane" id="tab-market">
						<?php foreach ($market_status as $m) { echo $m; } ?>
						<div class="panel panel-default">
							<div class="panel-body">
							
								<form action="<?php echo $action; ?>" method="POST" id="form-seting" class="market_form form-horizontal">
									<input type="hidden" value="market" name="type_data"/>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="ya_market_shopname"><?php echo $market_s_name; ?></label>
										<div class="col-sm-8">
											<input type="text" name="ya_market_pl_shopname" value="<?php echo $ya_market_pl_shopname; ?>" id="ya_market_shopname" class="form-control"/>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label"><?php echo $market_prostoy; ?></label>
										<div class="col-sm-8">
											<label class="radio-inline">
												<input type="radio" <?php echo ($ya_market_pl_prostoy ? ' checked="checked"' : ''); ?> name="ya_market_pl_prostoy" value="1"/> <?php echo $active_on; ?></label>
											<label class="radio-inline">
												<input type="radio" <?php echo (!$ya_market_pl_prostoy ? ' checked="checked"' : ''); ?> name="ya_market_pl_prostoy" value="0"/> <?php echo $active_off; ?></label>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label"><div class="help" data-toggle="tooltip" title="<? echo $market_cron; ?>" >Планировщик задач <i class="fa fa-question-circle" data-key=""></i></div></label>
										<div class="col-sm-8">
											<label class="radio-inline">
												<input type="radio" <?php echo ($ya_market_pl_cron ? ' checked="checked"' : ''); ?> name="ya_market_pl_cron" value="1"/> <?php echo $active_on; ?></label>
											<label class="radio-inline">
												<input type="radio" <?php echo (!$ya_market_pl_cron ? ' checked="checked"' : ''); ?> name="ya_market_pl_cron" value="0"/> <?php echo $active_off; ?></label>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label"><div class="help" data-toggle="tooltip" title="<? echo $market_valuta; ?>" >Синхронизация с ВАЛЮТА ПЛЮС <i class="fa fa-question-circle" data-key=""></i></div></label>
										<div class="col-sm-8">
											<label class="radio-inline">
												<input type="radio" <?php echo ($ya_market_pl_valuta ? ' checked="checked"' : ''); ?> name="ya_market_pl_valuta" value="1"/> <?php echo $active_on; ?></label>
											<label class="radio-inline">
												<input type="radio" <?php echo (!$ya_market_pl_valuta ? ' checked="checked"' : ''); ?> name="ya_market_pl_valuta" value="0"/> <?php echo $active_off; ?></label>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label"><div class="help" data-toggle="tooltip" title="<? echo $market_cpa; ?>" >Заказ на Яндекс.Маркет <i class="fa fa-question-circle" data-key=""></i></div></label>
										<div class="col-sm-8">
											<label class="radio-inline">
												<input type="radio" <?php echo ($ya_market_pl_cpa ? ' checked="checked"' : ''); ?> name="ya_market_pl_cpa" value="1"/> <?php echo $active_on; ?></label>
											<label class="radio-inline">
												<input type="radio" <?php echo (!$ya_market_pl_cpa ? ' checked="checked"' : ''); ?> name="ya_market_pl_cpa" value="0"/> <?php echo $active_off; ?></label>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-4 control-label"><div class="help" data-toggle="tooltip" title="<? echo $market_oldprice; ?>" >Учитывать скидки <i class="fa fa-question-circle" data-key=""></i></div></label>
										<div class="col-sm-8">
											<label class="radio-inline">
												<input type="radio" <?php echo ($ya_market_pl_oldprice ? ' checked="checked"' : ''); ?> name="ya_market_pl_oldprice" value="1"/> <?php echo $active_on; ?></label>
											<label class="radio-inline">
												<input type="radio" <?php echo (!$ya_market_pl_oldprice ? ' checked="checked"' : ''); ?> name="ya_market_pl_oldprice" value="0"/> <?php echo $active_off; ?></label>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label"><div class="help" data-toggle="tooltip" title="<? echo $market_mini; ?>" >Минимальное количество в заказе <i class="fa fa-question-circle" data-key=""></i></div></label>
										<div class="col-sm-8">
											<label class="radio-inline">
												<input type="radio" <?php echo ($ya_market_pl_mini ? ' checked="checked"' : ''); ?> name="ya_market_pl_mini" value="1"/> <?php echo $active_on; ?></label>
											<label class="radio-inline">
												<input type="radio" <?php echo (!$ya_market_pl_mini ? ' checked="checked"' : ''); ?> name="ya_market_pl_mini" value="0"/> <?php echo $active_off; ?></label>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label"><div class="help" data-toggle="tooltip" title="<? echo $market_dostavka; ?>" >Индивидульная доставка <i class="fa fa-question-circle" data-key=""></i></div></label>
										<div class="col-sm-8">
											<label class="radio-inline">
												<input type="radio" <?php echo ($ya_market_pl_dostavka ? ' checked="checked"' : ''); ?> name="ya_market_pl_dostavka" value="1"/> <?php echo $active_on; ?></label>
											<label class="radio-inline">
												<input type="radio" <?php echo (!$ya_market_pl_dostavka ? ' checked="checked"' : ''); ?> name="ya_market_pl_dostavka" value="0"/> <?php echo $active_off; ?></label>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label"><div class="help" data-toggle="tooltip" title="<? echo $market_outlet; ?>" >Пункты самовывоза <i class="fa fa-question-circle" data-key=""></i></div></label>
										<div class="col-sm-8">
											<label class="radio-inline">
												<input type="radio" <?php echo ($ya_market_pl_outlet ? ' checked="checked"' : ''); ?> name="ya_market_pl_outlet" value="1"/> <?php echo $active_on; ?></label>
											<label class="radio-inline">
												<input type="radio" <?php echo (!$ya_market_pl_outlet ? ' checked="checked"' : ''); ?> name="ya_market_pl_outlet" value="0"/> <?php echo $active_off; ?></label>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label"><div class="help" data-toggle="tooltip" title="<? echo $market_barcode; ?>" >Штрихкоды производителя <i class="fa fa-question-circle" data-key=""></i></div></label>
										<div class="col-sm-8">
											<label class="radio-inline">
												<input type="radio" <?php echo ($ya_market_pl_barcode ? ' checked="checked"' : ''); ?> name="ya_market_pl_barcode" value="1"/> <?php echo $active_on; ?></label>
											<label class="radio-inline">
												<input type="radio" <?php echo (!$ya_market_pl_barcode ? ' checked="checked"' : ''); ?> name="ya_market_pl_barcode" value="0"/> <?php echo $active_off; ?></label>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label"><div class="help" data-toggle="tooltip" title="<? echo $market_garanty; ?>" >Гарантия производителя <i class="fa fa-question-circle" data-key=""></i></div></label>
										<div class="col-sm-8">
											<label class="radio-inline">
												<input type="radio" <?php echo ($ya_market_pl_garanty ? ' checked="checked"' : ''); ?> name="ya_market_pl_garanty" value="1"/> <?php echo $active_on; ?></label>
											<label class="radio-inline">
												<input type="radio" <?php echo (!$ya_market_pl_garanty ? ' checked="checked"' : ''); ?> name="ya_market_pl_garanty" value="0"/> <?php echo $active_off; ?></label>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label"><div class="help" data-toggle="tooltip" title="<? echo $market_adult; ?>" >Товары имеющие возрастное ограничение <i class="fa fa-question-circle" data-key=""></i></div></label>
										<div class="col-sm-8">
											<label class="radio-inline">
												<input type="radio" <?php echo ($ya_market_pl_adult ? ' checked="checked"' : ''); ?> name="ya_market_pl_adult" value="1"/> <?php echo $active_on; ?></label>
											<label class="radio-inline">
												<input type="radio" <?php echo (!$ya_market_pl_adult ? ' checked="checked"' : ''); ?> name="ya_market_pl_adult" value="0"/> <?php echo $active_off; ?></label>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-4"><?php echo $market_set; ?></label>
										<div class="col-sm-8">
											<div class="checkbox">
												<label for="ya_market_available"><input type="checkbox" <?php echo ($ya_market_pl_available? ' checked="checked"' : ''); ?> name="ya_market_pl_available" id="ya_market_available" class="" value="1"/> <?php echo $market_set_1; ?></label>
											</div>
											<div class="checkbox">
												<label for="ya_market_combination"><input type="checkbox" <?php echo ($ya_market_pl_combination ? ' checked="checked"' : ''); ?> name="ya_market_pl_combination" id="ya_market_combination" class="" value="1"/> <?php echo $market_set_3; ?></label>
											</div>
											<div class="checkbox">
												<label for="ya_market_features"><input type="checkbox" <?php echo ($ya_market_pl_features ? ' checked="checked"' : ''); ?> name="ya_market_pl_features" id="ya_market_features" class="" value="1"/> <?php echo $market_set_4; ?></label>
											</div>
											<div class="checkbox">
												<label for="ya_market_dimensions"><input type="checkbox" <?php echo ($ya_market_pl_dimensions ? ' checked="checked"' : ''); ?> name="ya_market_pl_dimensions" id="ya_market_dimensions" class="" value="1"/> <?php echo $market_set_5; ?></label>
											</div>
											<div class="checkbox">
												<label for="ya_market_allcurrencies"><input type="checkbox" <?php echo ($ya_market_pl_allcurrencies ? ' checked="checked"' : ''); ?> name="ya_market_pl_allcurrencies" id="ya_market_allcurrencies" class="" value="1"/> <?php echo $market_set_6; ?></label>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label"><?php echo $market_dostup; ?></label>
										<div class="col-sm-8">
											<label class="radio-inline">
												<input type="radio" <?php echo ($ya_market_pl_set_available == 1 ? ' checked="checked"' : ''); ?> name="ya_market_pl_set_available" value="1"/> <?php echo $market_dostup_1; ?></label>
											<label class="radio-inline">
												<input type="radio" <?php echo ($ya_market_pl_set_available == 2 ? ' checked="checked"' : ''); ?> name="ya_market_pl_set_available" value="2"/> <?php echo $market_dostup_2; ?></label>
											<label class="radio-inline">
												<input type="radio" <?php echo ($ya_market_pl_set_available == 3 ? ' checked="checked"' : ''); ?> name="ya_market_pl_set_available" value="3"/> <?php echo $market_dostup_3; ?></label>
											<label class="radio-inline">
												<input type="radio" <?php echo ($ya_market_pl_set_available == 4 ? ' checked="checked"' : ''); ?> name="ya_market_pl_set_available" value="4"/> <?php echo $market_dostup_4; ?></label>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="">Массовый статус доставки</label>
										<div class="col-sm-8">
											<table class="table">
												<tr>
													<div class="form-group">
														<label class="col-sm-4 control-label" for="ya_market_localcoast"><?php echo $market_d_cost; ?></label>
														<div class="col-sm-8">
															<input type="text" name="ya_market_pl_localcoast" value="<?php echo $ya_market_pl_localcoast; ?>"
																   id="ya_market_localcoast" class="form-control"/>
														</div>
													</div>
												</tr>
												<tr>
													<div class="form-group">
														<label class="col-sm-4 control-label" for="ya_market_localdays"><?php echo $market_d_days; ?></label>
														<div class="col-sm-8">
															<input type="text" name="ya_market_pl_localdays" value="<?php echo $ya_market_pl_localdays; ?>"
																   id="ya_market_localdays" class="form-control"/>
														</div>
													</div>
												</tr>
												<tr>
													<div class="form-group">
														<label class="col-sm-4 control-label" for="ya_market_localtimes">Время заказа "до" в домашнем регионе</label>
														<div class="col-sm-8">
															<input type="text" name="ya_market_pl_localtimes" value="<?php echo $ya_market_pl_localtimes; ?>"
																   id="ya_market_localtimes" class="form-control"/>
														</div>
													</div>
												</tr>
											</table>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="ya_market_localdays"><?php echo "Индивидуальный статус доставки"; ?></label>
										<div class="col-sm-8">
											<table class="table">
												<tr>
													<th>Статус товара на складе</th>
													<th>Срок доставки</th>
													<th>Время заказа "до"</th>
													<th>Стоимость доставки </th>
												</tr>
												<?php
												foreach ($stockstatuses as $stock) {?>
													<tr>
														<td>
															<?php echo $stock['name']; ?>
														</td>
														<td>
															<input type="text" name="ya_market_pl_stock_days[<?php echo $stock['id']; ?>]" value="<?php echo $ya_market_pl_stock_days[$stock['id']]; ?>" class="form-control"/>
														</td>
														<td>
															<input type="text" name="ya_market_pl_stock_times[<?php echo $stock['id']; ?>]" value="<?php echo $ya_market_pl_stock_times[$stock['id']]; ?>" class="form-control"/>
														</td>
														<td>
															<input type="text" name="ya_market_pl_stock_cost[<?php echo $stock['id']; ?>]" value="<?php echo $ya_market_pl_stock_cost[$stock['id']]; ?>" class="form-control"/>
														</td>
													</tr>
												<?php } ?>
											</table>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label"><?php echo $market_color_option; ?></label>
										<div class="col-sm-8">
											<div class="scrollbox" style="height: 100px; overflow-x: auto; width: 100%;">
												<?php $class = 'odd'; ?>
												<?php foreach ($options as $option) { ?>
												<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
												<div class="<?php echo $class; ?>">
													<?php if (in_array($option['option_id'], $ya_market_pl_color_options)) { ?>
													<input type="checkbox" name="ya_market_pl_color_options[]" value="<?php echo $option['option_id']; ?>" checked="checked" />
													<?php echo $option['name']; ?>
													<?php } else { ?>
													<input type="checkbox" name="ya_market_pl_color_options[]" value="<?php echo $option['option_id']; ?>" />
													<?php echo $option['name']; ?>
													<?php } ?>
												</div>
												<?php } ?>
											</div>
											<a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label"><?php echo $market_size_option; ?><br/><?php echo $market_size_unit; ?></label>
										<div class="col-sm-8">
											<div class="scrollbox" style="height: 160px; overflow-x: auto; width: 100%;">
												<?php $class = 'odd'; ?>
												<?php foreach ($options as $option) { ?>
												<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
												<div class="<?php echo $class; ?>">
													<?php if (in_array($option['option_id'], $ya_market_pl_size_options)) { ?>
													<input type="checkbox" name="ya_market_pl_size_options[]" value="<?php echo $option['option_id']; ?>" checked="checked" />
													<?php echo $option['name']; ?>
													<?php } else { ?>
													<input type="checkbox" name="ya_market_pl_size_options[]" value="<?php echo $option['option_id']; ?>" />
													<?php echo $option['name']; ?>
													<?php } ?>
												</div>
												<?php } ?>
											</div>
											<a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label"><?php echo $market_out; ?></label>
										<div class="col-sm-8">
											<label class="radio-inline">
												<input type="radio" <?php echo ($ya_market_pl_catall ? ' checked="checked"' : ''); ?> name="ya_market_pl_catall" value="1"/> <?php echo $market_out_all; ?></label>
											<label class="radio-inline">
												<input type="radio" <?php echo (!$ya_market_pl_catall ? ' checked="checked"' : ''); ?> name="ya_market_pl_catall" value="0"/> <?php echo $market_out_sel; ?></label>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-sm-4"><?php echo $market_cat; ?></label>
										<div class="col-sm-8">
											<div class="panel panel-default">
												<div class="tree-panel-heading tree-panel-heading-controls clearfix">
													<div class="tree-actions pull-right">
														<a onclick="hidecatall($('#categoryBox')); return false;" id="collapse-all-categoryBox" class="btn btn-default">
															<i class="icon-collapse-alt"></i><?php echo $market_sv_all; ?>
														</a>
														<a onclick="showcatall($('#categoryBox')); return false;" id="expand-all-categoryBox" class="btn btn-default">
															<i class="icon-expand-alt"></i><?php echo $market_rv_all; ?>
														</a>
														<a onclick="checkAllAssociatedCategories($('#categoryBox')); return false;" id="check-all-categoryBox" class="btn btn-default">
															<i class="icon-check-sign"></i><?php echo $market_ch_all; ?>
														</a>
														<a onclick="uncheckAllAssociatedCategories($('#categoryBox')); return false;" id="uncheck-all-categoryBox" class="btn btn-default">
															<i class="icon-check-empty"></i><?php echo $market_unch_all; ?>
														</a>
													</div>
												</div>
												<ul id="categoryBox" class="tree">
													<?php echo $market_cat_tree; ?>
												</ul>
											</div>
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-4 control-label" for="ya_market_dynamic"><?php echo $market_lnk_yml; ?></label>
										<div class="col-sm-8">
											<input type="hidden" name="ya_market_pl_dynamic" value="<?php echo $ya_market_pl_lnk_yml; ?>" id="ya_market_dynamic"/>
											<div disabled="disabled" class="form-control">
												<?php echo $ya_market_pl_lnk_yml; ?>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label"><?php echo $market_lnk_cron; ?></label>
										<div class="col-sm-8">
											<div disabled="disabled" class="form-control">
												<?php echo $ya_market_pl_lnk_cron; ?>
											</div>
										</div>
									</div>
								</form>
								<div class="panel-footer clearfix">
									<button type="button" onclick="$('.market_form').submit(); return false;" value="1" id="module_form_submit_btn_3" name="submitmarketModule" class="btn btn-default">
										<i class="process-icon-save"></i> <?php echo $market_sv; ?>
									</button>
									<!-- <button type="submit" class="btn btn-default btn btn-default" name="generatemanual"><i class="process-icon-refresh"></i> <?php echo $market_gen; ?></button> -->
								</div>
							</div>
						</div>
					</div>
				</div>
	</div>
</div>
<?php echo $footer; ?>
<script type="text/javascript"><!--
var step = new Array();
var total = 0;
	triggerInvoiceSetting();
	function triggerInvoiceSetting(){
		if($('input[name=ya_kassa_inv]:checked').val()==1){
			$('.invoice-setting').show();
		}else{
			$('.invoice-setting').hide();
		}
	}
$('input[name=ya_kassa_inv]').bind('click', triggerInvoiceSetting);
$('#mws_csr_gen').bind('click', function(e) {
	if (confirm('<?php echo $lbl_mws_alert; ?>')) {
		e.preventDefault();
		e.stopPropagation();
		$.ajax({
			url: 'index.php?route=tool/mws/generate&token=<?php echo $token; ?>',
			cache: false,
			success: function(json) {
				location.reload();
			}
		});
	}
});
$('#mws_crt_load').on('click', function() {
	$('#form-upload').remove();
	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);
			$('.alert').remove();
			$.ajax({
				url: 'index.php?route=tool/mws/upload&token=<?php echo $token; ?>',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('#mws_crt_load').button('loading');
				},
				complete: function() {
					$('#mws_crt_load').button('reset');
				},
				success: function(json) {
					if (!json.error){
						$('#mws_form').submit();
					} else {
						$('#mws_form').prepend("<div class='alert alert-danger'>"+ json.error +"</div>");
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});

</script>

<script type="text/javascript">
    function hideTaxes() {
        var inside = $('input[name="ya_kassa_send_check"]:checked').val();

        if (inside == 1)
        {
            $('.taxesArea').slideDown('slow');
        } else {
            $('.taxesArea').slideUp('slow');
        }
    }

function showcatall($tree)
{
$tree.find("ul.tree").each(
	function()
	{
		$(this).slideDown();
	}
);
}

function hidecatall($tree)
{
$tree.find("ul.tree").each(
	function()
	{
		$(this).slideUp();
	}
);
}
function checkAllAssociatedCategories($tree)
{
$tree.find(":input[type=checkbox]").each(
	function()
	{
		$(this).prop("checked", true);
		$(this).parent().addClass("tree-selected");
	}
);
}

function uncheckAllAssociatedCategories($tree)
{
$tree.find(":input[type=checkbox]").each(
	function()
	{
		$(this).prop("checked", false);
		$(this).parent().removeClass("tree-selected");
	}
);
}

$(document).ready(function(){
	$('.nav-tabs a:first').tab('show');
	var view = $.totalStorage('tab_ya');
	if(view == null)
		$.totalStorage('tab_ya', 'tab-product-options');
	else
		$('.nav-tabs li a[href="#'+ view +'"]').click();

	$('.nav-tabs li').click(function(){
		var view = $(this).find('a').first().attr('href').replace('#', '');
		$.totalStorage('tab_ya', view);
	});

	$('.tree-item-name label').click(function(){
		$(this).parent().find('input').click();
	});

    hideTaxes();
	$('input[name="ya_kassa_send_check"]').change(function(){
	    hideTaxes();
    });

	$('.tree-folder-name input').change(function(){
		if ($(this).prop("checked"))
		{
			$(this).parent().addClass("tree-selected");
			$(this).parents('.tree-folder').first().find('ul input').prop("checked", true).parent().addClass("tree-selected");
		}
		else
		{
			$(this).parent().removeClass("tree-selected");
			$(this).parents('.tree-folder').first().find('ul input').prop("checked", false).parent().removeClass("tree-selected");
		}
	});

	$('.tree-toggler').click(function(){
		$(this).parents('.tree-folder').first().find('ul').slideToggle('slow');
	});

	$('.tree input').change(function(){
		if ($(this).prop("checked"))
		{
			$(this).parent().addClass("tree-selected");
		}
		else
		{
			$(this).parent().removeClass("tree-selected");
		}
	});
	//
	var market_cat = JSON.parse('[<?php echo $ya_market_pl_categories; ?>]');
	console.log(market_cat);
	for (i in market_cat)
		$('#categoryBox input[value="'+ market_cat[i] +'"]').prop("checked", true).change();
    // Show/hide payment methods for some modes
    var $funcMode = function (){
        if ($(":input[name=ya_kassa_paymode][type=radio]:checked").val() == '1') {
            $(".kassa-wo-select").show();
            $(".kassa-w-select").hide();
        } else {
            $(".kassa-w-select").show();
            $(".kassa-wo-select").hide();
        }
    };
    $funcMode.call();
    $(":input[name=ya_kassa_paymode][type=radio]").click($funcMode);
});
</script>
<script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter27746007 = new Ya.Metrika({ id:27746007 }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script>
<noscript><div><img src="//mc.yandex.ru/watch/27746007" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<style>
.bootstrap .tree-panel-heading-controls {
    line-height: 2.2em;
    font-size: 1.1em;
    color: #00aff0;
}

.bootstrap .tree-panel-heading-controls i {
    font-size: 14px;
}

.bootstrap .tree {
    list-style: none;
    padding: 0 0 0 20px;
}

.bootstrap .tree input {
    vertical-align: baseline;
    margin-right: 4px;
    line-height: normal;
}

.bootstrap .tree i {
    font-size: 14px;
}

.bootstrap .tree .tree-item-name,.bootstrap .tree .tree-folder-name {
    padding: 2px 5px;
    -webkit-border-radius: 4px;
    border-radius: 4px;
}

.bootstrap .tree .tree-item-name label,.bootstrap .tree .tree-folder-name label {
    font-weight: 400;
}

.bootstrap .tree .tree-item-name:hover,.bootstrap .tree .tree-folder-name:hover {
    background-color: #eee;
    cursor: pointer;
}

.bootstrap .tree .tree-selected {
    color: #fff;
    background-color: #00aff0;
}

.bootstrap .tree .tree-selected:hover {
    background-color: #009cd6;
}

.bootstrap .tree .tree-selected i.tree-dot {
    background-color: #fff;
}

.bootstrap .panel-footer {
	height: 73px;
	border-color: #eee;
	background-color: #fcfdfe;
}

.bootstrap .tree i.tree-dot {
    display: inline-block;
    position: relative;
    width: 6px;
    height: 6px;
    margin: 0 4px;
    background-color: #ccc;
    -webkit-border-radius: 6px;
    border-radius: 6px;
}

.bootstrap .tree .tree-item-disable,.bootstrap .tree .tree-folder-name-disable {
    color: #ccc;
}

.bootstrap .tree .tree-item-disable:hover,.bootstrap .tree .tree-folder-name-disable:hover {
    color: #ccc;
    background-color: none;
}

.bootstrap .tree-actions {
    display: inline-block;
}

.bootstrap .tree-panel-heading-controls {
    padding: 5px;
    border-bottom: solid 1px #dfdfdf;
}

.bootstrap .tree-actions .twitter-typeahead {
    padding: 0 0 0 4px;
}

.bootstrap #categoryBox {
	padding: 10px 5px 5px 20px;
}

.bootstrap .tree-panel-label-title {
    font-weight: 400;
    margin: 0;
    padding: 0 0 0 8px;
}

.scrollbox > div {
	height: 23px;
}
</style>
<script type="text/javascript" src="view/javascript/jquery.total-storage.js"></script>