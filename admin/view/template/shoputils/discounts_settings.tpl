<?php
/*
 * Shoputils
 *
 * ���������� � ������������� ����������
 *
 * ���� ���� ������ ������������ �����������, ������� ����� ����� � ������,
 * ������ � ���� ������. ���� �������� ����������: LICENSE.2.0.x-2.1.x-2.2.x-2.3.x.RUS.TXT
 * ��� �� ������������ ���������� ����� ����� �� ������:
 * https://opencart.market/LICENSE.2.0.x-2.1.x-2.2.x-2.3.x.RUS.TXT
 * 
 * =================================================================
 * OPENCART/ocStore 2.0.x-2.1.x-2.2.x-2.3.x ���������� �� �������������
 * =================================================================
 *  ���� ���� ������������ ��� Opencart/ocStore 2.0.x-2.1.x-2.2.x-2.3.x. Shoputils ��
 *  ����������� ���������� ������ ����� ���������� �� ����� ������ 
 *  ������ Opencart/ocStore, ����� Opencart/ocStore 2.0.x-2.1.x-2.2.x-2.3.x. 
 *  Shoputils �� ������������ ����������� ����������� ��� ������ 
 *  ������ Opencart/ocStore.
 * =================================================================
*/
?>
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo $insert; ?>" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo $button_insert; ?></a>
        <button type="button" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form').submit() : false;"><i class="fa fa-trash-o"></i> <?php echo $button_delete; ?></button>
        <button type="button" class="btn btn-success" onclick="if (confirm('<?php echo $question_apply; ?>')) {$('#form').attr('action', '<?php echo $apply; ?>');$('#form').submit();}"><i class="fa fa-check"></i> <?php echo $button_apply; ?></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>

    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>

    <?php if ($text_info) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $text_info; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><img src="view/image/shoputils_discounts.png"><?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                    <td class="text-left" width="20%"><?php echo $column_name; ?></td>
                    <td class="text-left"><?php echo $column_enabled; ?></td>
                    <td class="text-left"><?php echo $column_objects; ?></td>
                    <td class="text-right"><?php echo $column_percent; ?></td>
                    <td class="text-right"><?php echo $column_date_start; ?></td>
                    <td class="text-right"><?php echo $column_date_end; ?></td>
                    <td class="text-right"><?php echo $column_sort_order; ?></td>
                    <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($discounts) { ?>
                <?php foreach ($discounts as $discount) { ?>
                  <tr>
                    <td class="text-center"><input type="checkbox" name="selected[]" value="<?php echo $discount['discounts_id']; ?>"/></td>
                    <td class="text-left"><?php echo $discount['name']; ?></td>
                    <td class="text-left"><?php echo $discount['enabled']; ?></td>
                    <td class="text-left"><?php echo $discount['objects_type']; ?></td>
                    <td class="text-right"><?php echo $discount['percent']; ?></td>
                    <td class="text-right"><?php echo $discount['date_start']; ?></td>
                    <td class="text-right"><?php echo $discount['date_end']; ?></td>
                    <td class="text-right"><?php echo $discount['sort_order']; ?></td>
                    <td class="text-right">
                      <?php foreach ($discount['action'] as $action) { ?>
                          <a href="<?php echo $action['href']; ?>" data-toggle="tooltip" title="<?php echo $action['text']; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                      <?php } ?>
                    </td>
                    </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="9"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div style="padding: 15px 15px; border:1px solid #ccc; margin-top: 15px; box-shadow:0 0px 5px rgba(0,0,0,0.1);"><?php echo $text_copyright; ?></div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>