<?php
/*
 * Shoputils
 *
 * ���������� � ������������� ����������
 *
 * ���� ���� ������ ������������ �����������, ������� ����� ����� � ������,
 * ������ � ���� ������. ���� �������� ����������: LICENSE.2.0.x-2.1.x-2.2.x-2.3.x.RUS.TXT
 * ��� �� ������������ ���������� ����� ����� �� ������:
 * https://opencart.market/LICENSE.2.0.x-2.1.x-2.2.x-2.3.x.RUS.TXT
 * 
 * =================================================================
 * OPENCART/ocStore 2.0.x-2.1.x-2.2.x-2.3.x ���������� �� �������������
 * =================================================================
 *  ���� ���� ������������ ��� Opencart/ocStore 2.0.x-2.1.x-2.2.x-2.3.x. Shoputils ��
 *  ����������� ���������� ������ ����� ���������� �� ����� ������ 
 *  ������ Opencart/ocStore, ����� Opencart/ocStore 2.0.x-2.1.x-2.2.x-2.3.x. 
 *  Shoputils �� ������������ ����������� ����������� ��� ������ 
 *  ������ Opencart/ocStore.
 * =================================================================
*/
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"><?php echo $title; ?></h4>
        </div><!-- <div class="modal-header"> -->
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-8">
                    <div class="input-group">
                        <input type="text" value="<?php echo $filter; ?>" id="filter_name" placeholder="<?php echo $entry_filter_name; ?>" class="form-control" />
                        <span class="input-group-btn">
                        <a data-toggle="tooltip" title="<?php echo $button_filter; ?>" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i></a>
                        </span>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="pull-right">
                        <a id="select" class="btn btn-primary"><i class="fa fa-check"></i> <?php echo $button_select; ?></a>
                        <button type="button" aria-hidden="true" id="cancel" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></button>
                    </div>
                </div>
            </div><!-- <div class="row">  -->
            <hr />

            <div class="row">
                <div class="col-sm-12">
                    <div id="status"><?php echo $text_categories_selected; ?>0</div>
                </div>
            </div><!-- <div class="row">  -->
            <hr />

            <div class="row">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <td style="text-align: center; width:1px;"><input type="checkbox" id="checkboxall" onclick="checkboxall(this);" style="width:10px;" /></td>
                                <td class="text-left"><?php echo $column_name; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($categories)) { ?>
                            <?php foreach ($categories as $category) { ?>
                            <tr>
                                <td width="1" style="text-align: center;">
                                    <input type="checkbox" name="selected" id="category_<?php echo $category['category_id']; ?>" value="<?php echo $category['category_id']; ?>"/>
                                </td>
                                <td class="text-left"><label for='category_<?php echo $category['category_id']; ?>'><?php echo $category['name']; ?></label></td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr><td class="text-center" colspan="4"><?php echo $text_no_categories; ?></td></tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- <div class="row">  -->
        </div><!-- </div class="modal-body"> -->
        <div class="modal-footer"></div>
    </div><!-- </div class="modal-content">  -->
</div><!-- </div class="modal-dialog modal-lg">  -->
<script type="text/javascript"><!--
  //$(document).ready(function () {
      var selected = $('#categories_input-selected');
      if (!selected.val()){
          selected.val($('#categories_input').val());
      }

      updateStats();
      updateChecked();

      $('#cancel').click(function() {
          $.cookie('filter', '');
          selected.val('');
          $('#modal-categories_selector').modal('hide');
      });
      
      $('#select').click(function() {
          $('#categories_input').val(selected.val());
          updateCategoriesTable();
          $.cookie('filter', '');
          selected.val('');
          $('#modal-categories_selector').modal('hide');
      });

      $('#filter_name').on('keydown', function(e) {
        if (e.which == 13) {
          $('#button-filter').trigger('click');
        }
      });

      $('#button-filter').click(function() {
          $.cookie('filter', $('#filter_name').val());
          url = '<?php echo $action_filter; ?>';
          $('#modal-categories_selector').load(url);
    });

      if(!Array.indexOf){
          Array.prototype.indexOf = function(obj){
              for(var i=0; i<this.length; i++){
                  if(this[i]==obj){
                      return i;
                  }
              }
              return -1;
          }
      }

      function checkboxall(el) {
           var checked_status = el.checked;
           $("input[name=selected]").each(function() {
              this.checked = checked_status;
              if ($(this).is(':checked')){
                  categoryAdd($(this).val());
              } else {
                  categoryRemove($(this).val());
              }
           });
      }

      function categoryAdd(id) {
          var ids = selected.val().split(',');
          if (ids.indexOf(id) == -1){
              ids.push(id);
              selected.val(ids.join(','));
              updateStats();
          }
      }

      function categoryRemove(id) {
          var ids = new String(selected.val()).split(',');
          // bug #7654 fixed
          while (ids.indexOf(id) != -1) {
              ids.splice(ids.indexOf(id), 1);
          }
          selected.val(ids.join(','));
          updateStats();
      }

      $('input[name*=\'select\']').change(function() {
          if ($(this).is(':checked')){
              categoryAdd($(this).val());
          } else {
              categoryRemove($(this).val());
          }
      });

      function updateStats(){
          var ids = new String(selected.val()).split(',');
          if (ids.length > 0 && ids[0] == ''){
              ids.splice(0,1);
          }
          $('#status').html('<?php echo $text_categories_selected; ?>' + (ids.length));
      }

      function updateChecked(){
          var ids = new String(selected.val()).split(',');
          if (ids.length > 0 && ids[0] == ''){
              ids.splice(0,1);
          }
          $("input[name=selected]").each(function(){
              $(this).attr('checked', ids.indexOf($(this).val()) != -1);
          });
      }

  //});
//--></script>
