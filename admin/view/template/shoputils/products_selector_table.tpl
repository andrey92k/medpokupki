<?php
/*
 * Shoputils
 *
 * ���������� � ������������� ����������
 *
 * ���� ���� ������ ������������ �����������, ������� ����� ����� � ������,
 * ������ � ���� ������. ���� �������� ����������: LICENSE.2.0.x-2.1.x-2.2.x-2.3.x.RUS.TXT
 * ��� �� ������������ ���������� ����� ����� �� ������:
 * https://opencart.market/LICENSE.2.0.x-2.1.x-2.2.x-2.3.x.RUS.TXT
 * 
 * =================================================================
 * OPENCART/ocStore 2.0.x-2.1.x-2.2.x-2.3.x ���������� �� �������������
 * =================================================================
 *  ���� ���� ������������ ��� Opencart/ocStore 2.0.x-2.1.x-2.2.x-2.3.x. Shoputils ��
 *  ����������� ���������� ������ ����� ���������� �� ����� ������ 
 *  ������ Opencart/ocStore, ����� Opencart/ocStore 2.0.x-2.1.x-2.2.x-2.3.x. 
 *  Shoputils �� ������������ ����������� ����������� ��� ������ 
 *  ������ Opencart/ocStore.
 * =================================================================
*/
?>
<?php $table_id = rand(1, 99999999); ?>
<table class="table table-bordered table-hover table-striped table-<?php echo $table_id; ?>" style="width:80%">
    <thead>
    <tr>
        <td style="text-align: center; width:1px;">
            <input type="checkbox" id="checkboxall" style="width:10px;"/>
        </td>
        <td style="min-width:7%;"></td>
        <td class="text-left" style="min-width:10%;"><?php echo $column_id; ?></td>
        <td class="text-left" style="min-width:20%;"><?php echo $column_sku; ?></td>
        <td class="text-left"><?php echo $column_name; ?></td>
        <td class="text-left" style="min-width:20%;"><?php echo $column_price; ?></td>
    </tr>
    </thead>
    <?php if (count($selected)) { ?>
    <?php foreach($selected as $values) { ?>
    <tr>
        <td class="text-center" style="width:1px;">
            <input type="checkbox" name="selected" id="product_<?php echo $values['product_id']; ?>"
                   value="<?php echo $values['product_id']; ?>"/>
        </td>
        <td class="text-center"style="min-width:7%;">
            <i title="<?php echo $text_delete; ?>" class="fa fa-minus-circle delete_product" style="cursor:pointer;" product_id="<?php echo $values['product_id']?>" ></i>&nbsp;
            <a href="<?php echo $values['href']?>" title="<?php echo $text_edit; ?>"><i class="fa fa-pencil"></i></a>
        </td>
        <td class="text-left"><?php echo $values['product_id']?></td>
        <td class="text-left"><?php echo $values['sku']?></td>
        <td class="text-left"><?php echo $values['name']?></td>
        <td class="text-left"><?php echo $values['price']?></td>
    </tr>
    <?php } ?>
    <tr>
        <td style="padding:3px;" colspan="6">
            <a href="#" class="btn btn-danger" id="products_delete"><i class="fa fa-minus-circle"></i> <?php echo $button_delete; ?></a>
        </td>
    </tr>
    <?php } else { ?>
    <tr>
        <td style="padding:3px;" colspan="6">
            <?php echo $text_no_products; ?>
        </td>
    </tr>
    <?php } ?>
</table>

<?php if(isset($field)) { ?>
<script type="text/javascript"><!--
    <?php
    $result = array();
    foreach($selected as $values) {
        $result[] = $values['product_id'];
    }
    ?>
    $('#<?php echo $field; ?>').val('<?php echo implode(',', $result)?>');


    $('.delete_product').click(function(){
        var id = $(this).attr('product_id');
        var ids = new String($('#<?php echo $field; ?>').val()).split(',');
       // bug #7654 fixed
        while (ids.indexOf(id) != -1) {
            ids.splice(ids.indexOf(id), 1);
        }
        $('#<?php echo $field; ?>').val(ids.join(','));
        updateProductsTable();
    });

    $('#products_delete').click(function(event){
        event.preventDefault();
        var deleted = false;
        $('.table-<?php echo $table_id?> input[name=selected]:checked').each(function(){
            var id = $(this).val();
            var ids = new String($('#<?php echo $field; ?>').val()).split(',');
            // bug #7654 fixed
            while (ids.indexOf(id) != -1) {
                ids.splice(ids.indexOf(id), 1);
            }
            $('#<?php echo $field; ?>').val(ids.join(','));
            deleted = true;
        });
        if (deleted){
            updateProductsTable();
        }
    });

    $(".table-<?php echo $table_id?> #checkboxall").click(function() {
         var checked_status = this.checked;
         $(".table-<?php echo $table_id?> input[name=selected]").each(function() {
            this.checked = checked_status;
         });
    });

//--></script>

<?php } ?>
