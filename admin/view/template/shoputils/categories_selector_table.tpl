<?php
/*
 * Shoputils
 *
 * ���������� � ������������� ����������
 *
 * ���� ���� ������ ������������ �����������, ������� ����� ����� � ������,
 * ������ � ���� ������. ���� �������� ����������: LICENSE.2.0.x-2.1.x-2.2.x-2.3.x.RUS.TXT
 * ��� �� ������������ ���������� ����� ����� �� ������:
 * https://opencart.market/LICENSE.2.0.x-2.1.x-2.2.x-2.3.x.RUS.TXT
 * 
 * =================================================================
 * OPENCART/ocStore 2.0.x-2.1.x-2.2.x-2.3.x ���������� �� �������������
 * =================================================================
 *  ���� ���� ������������ ��� Opencart/ocStore 2.0.x-2.1.x-2.2.x-2.3.x. Shoputils ��
 *  ����������� ���������� ������ ����� ���������� �� ����� ������ 
 *  ������ Opencart/ocStore, ����� Opencart/ocStore 2.0.x-2.1.x-2.2.x-2.3.x. 
 *  Shoputils �� ������������ ����������� ����������� ��� ������ 
 *  ������ Opencart/ocStore.
 * =================================================================
*/
?>
<?php $table_id = rand(1, 99999999); ?>
<table class="table table-bordered table-hover table-striped table-<?php echo $table_id?>" style="width:80%">
    <thead>
    <tr>
        <td style="text-align: center; width:1px;">
            <input type="checkbox" class="checkboxall" style="width:10px;"/>
        </td>
        <td style="min-width:7%;"></td>
        <td class="text-left"><?php echo $column_name; ?></td>
    </tr>
    </thead>
    <?php if (count($selected)) { ?>
    <?php foreach($selected as $values) { ?>
    <tr>
        <td class="text-center" style="width:1px;">
            <input type="checkbox" name="selected" id="category_<?php echo $values['category_id']; ?>"
                   value="<?php echo $values['category_id']; ?>"/>
        </td>
        <td class="text-center"style="min-width:7%;">
            <i title="<?php echo $text_delete; ?>" class="fa fa-minus-circle delete_category" style="cursor:pointer;" category_id="<?php echo $values['category_id']?>" ></i>&nbsp;
            <a href="<?php echo $values['href']?>" title="<?php echo $text_edit; ?>"><i class="fa fa-pencil"></i></a>
        </td>
        <td class="text-left"><?php echo $values['name']?></td>
    </tr>
    <?php } ?>
    <tr>
        <td style="padding:3px;" colspan="3">
            <a href="#" class="btn btn-danger" id="categories_delete"><i class="fa fa-minus-circle"></i> <?php echo $button_delete; ?></a>
        </td>
    </tr>
    <?php } else { ?>
    <tr>
        <td style="padding:3px;" colspan="3">
            <?php echo $text_no_categories; ?>
        </td>
    </tr>
    <?php } ?>
</table>

<?php if(isset($field)) { ?>
<script type="text/javascript"><!--
    <?php
    $result = array();
    foreach($selected as $values) {
        $result[] = $values['category_id'];
    }
    ?>
    $('#<?php echo $field; ?>').val('<?php echo implode(',', $result)?>');


    $('.delete_category').click(function(){
        var id = $(this).attr('category_id');
        var ids = new String($('#<?php echo $field; ?>').val()).split(',');
       // bug #7654 fixed
        while (ids.indexOf(id) != -1) {
            ids.splice(ids.indexOf(id), 1);
        }
        $('#<?php echo $field; ?>').val(ids.join(','));
        updateCategoriesTable();
    });

    $('#categories_delete').click(function(event){
        event.preventDefault();
        var deleted = false;
        $('.table-<?php echo $table_id?> input[name=selected]:checked').each(function(){
            var id = $(this).val();
            var ids = new String($('#<?php echo $field; ?>').val()).split(',');
            // bug #7654 fixed
            while (ids.indexOf(id) != -1) {
                ids.splice(ids.indexOf(id), 1);
            }
            $('#<?php echo $field; ?>').val(ids.join(','));
            deleted = true;
        });
        if (deleted){
            updateCategoriesTable();
        }
    });

    $(".table-<?php echo $table_id?> .checkboxall").click(function() {
         var checked_status = this.checked;
         $(".table-<?php echo $table_id?> input[name=selected]").each(function() {
            this.checked = checked_status;
         });
    });

//--></script>

<?php } ?>
