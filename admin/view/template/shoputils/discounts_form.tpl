<?php
/*
 * Shoputils
 *
 * ���������� � ������������� ����������
 *
 * ���� ���� ������ ������������ �����������, ������� ����� ����� � ������,
 * ������ � ���� ������. ���� �������� ����������: LICENSE.2.0.x-2.1.x-2.2.x-2.3.x.RUS.TXT
 * ��� �� ������������ ���������� ����� ����� �� ������:
 * https://opencart.market/LICENSE.2.0.x-2.1.x-2.2.x-2.3.x.RUS.TXT
 * 
 * =================================================================
 * OPENCART/ocStore 2.0.x-2.1.x-2.2.x-2.3.x ���������� �� �������������
 * =================================================================
 *  ���� ���� ������������ ��� Opencart/ocStore 2.0.x-2.1.x-2.2.x-2.3.x. Shoputils ��
 *  ����������� ���������� ������ ����� ���������� �� ����� ������ 
 *  ������ Opencart/ocStore, ����� Opencart/ocStore 2.0.x-2.1.x-2.2.x-2.3.x. 
 *  Shoputils �� ������������ ����������� ����������� ��� ������ 
 *  ������ Opencart/ocStore.
 * =================================================================
*/
?>
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
					<a onclick="$('#form').submit();" class="btn btn-success"><i class="fa fa-save"></i> <?php echo $button_save; ?></a>
          <a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-danger"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
      </div>
      <h1><img src="view/image/shoputils_discounts.png"><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>

  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>

    <div class="panel panel-default">
      <div class="panel-body">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form" class="form-horizontal">
      
        <div class="form-group required">
          <label class="col-sm-2 control-label"><?php echo $entry_name; ?></label>
          <div class="col-sm-10">
            <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
            <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
            <?php } ?>
            <span class="help-block"><?php echo $help_name; ?></span>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
          <div class="col-sm-10">
            <select name="enabled" id="input-status" class="form-control">
              <option value="1"<?php echo $enabled ? ' selected="selected"' : ''; ?>><?php echo $text_enabled; ?></option>
              <option value="0"<?php echo !$enabled ? ' selected="selected"' : ''; ?>><?php echo $text_disabled; ?></option>
            </select>
            <span class="help-block"><?php echo $help_status; ?></span>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-quantity"><?php echo $entry_quantity; ?></label>
          <div class="col-sm-10">
            <input type="text" name="quantity" value="<?php echo $quantity; ?>" placeholder="<?php echo $entry_quantity; ?>" id="input-quantity" class="form-control" />
            <span class="help-block"><?php echo $help_quantity; ?></span>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-sort_order"><?php echo $entry_sort_order; ?></label>
          <div class="col-sm-10">
            <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort_order" class="form-control" />
            <span class="help-block"><?php echo $help_sort_order; ?></span>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-percent"><?php echo $entry_percent; ?></label>
          <div class="col-sm-10">
            <input type="text" name="percent" value="<?php echo $percent; ?>" placeholder="<?php echo $entry_percent; ?>" id="input-percent" class="form-control" />
            <span class="help-block"><?php echo $help_percent; ?></span>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-priority"><?php echo $entry_priority; ?></label>
          <div class="col-sm-10">
            <input type="text" name="priority" value="<?php echo $priority; ?>" placeholder="<?php echo $entry_priority; ?>" id="input-priority" class="form-control" />
            <span class="help-block"><?php echo $help_priority; ?></span>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-customer_group_ids"><?php echo $entry_customer_group_ids; ?></label>
          <div class="col-sm-10">
                <div class="scrollbox">
                  <?php $class = 'odd'; ?>
                  <?php foreach ($customer_groups as $customer_group) { ?>
                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                    <div class="<?php echo $class; ?>">
                      <?php if (isset($customer_group_ids) && in_array($customer_group['customer_group_id'], $customer_group_ids)) { ?>
                        <label><input type="checkbox" name="customer_group_ids[]" value="<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
                          <?php echo $customer_group['name']; ?></label>
                      <?php } else { ?>
                        <label><input type="checkbox" name="customer_group_ids[]" value="<?php echo $customer_group['customer_group_id']; ?>" />
                          <?php echo $customer_group['name']; ?></label>
                      <?php } ?>
                    </div>
                  <?php } ?>
                </div>
                <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
            <span class="help-block"><?php echo $help_customer_group_ids; ?></span>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-date_start"><?php echo $entry_date_start; ?></label>
          <div class="col-sm-10">
            <div class="input-group date">
              <input type="text" name="date_start" value="<?php echo $date_start; ?>" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" id="input-date_start" class="form-control" />
              <span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span>
            </div>
            <span class="help-block"><?php echo $help_date_start; ?></span>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-date_end"><?php echo $entry_date_end; ?></label>
          <div class="col-sm-10">
            <div class="input-group date">
              <input type="text" name="date_end" value="<?php echo $date_end; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" id="input-date_end" class="form-control" />
              <span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span>
            </div>
            <span class="help-block"><?php echo $help_date_end; ?></span>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-objects_type"><?php echo $entry_objects_type; ?></label>
          <div class="col-sm-10">
            <select name="objects_type" id="input-objects_type" class="form-control">
              <option value="0"<?php echo (int)$objects_type == 0 ? ' selected="selected"' : ''; ?>><?php echo $entry_objects_id_0; ?></option>
              <option value="1"<?php echo (int)$objects_type == 1 ? ' selected="selected"' : ''; ?>><?php echo $entry_objects_id_1; ?></option>
              <option value="2"<?php echo (int)$objects_type == 2 ? ' selected="selected"' : ''; ?>><?php echo $entry_objects_id_2; ?></option>
              <option value="3"<?php echo (int)$objects_type == 3 ? ' selected="selected"' : ''; ?>><?php echo $entry_objects_id_3; ?></option>
            </select>
            <span class="help-block"><?php echo $help_objects_type; ?></span>
          </div>
        </div>

        <div class="form-group"<?php echo (int)$objects_type != 1 ? ' style="display:none"':''?> id="categories_tr">
          <label class="col-sm-2 control-label"><?php echo $entry_objects_id_1; ?></label>
          <div class="col-sm-10">
            <span class="help-block"><?php echo $help_objects_categories; ?></span>
            <input type="hidden" name="categories" id="categories_input" value="<?php echo $categories; ?>"/>
            <input type="hidden" id="categories_input-selected">
            <div id="categories_list"></div>
            <a data-toggle="tooltip" title="<?php echo $button_change; ?>" id="categories_button" class="btn btn-info"><i class="fa fa-bars"></i> <?php echo $button_change; ?></a>
          </div>
        </div>

        <div class="form-group"<?php echo (int)$objects_type != 2 ? ' style="display:none"':''?> id="products_tr">
          <label class="col-sm-2 control-label"><?php echo $entry_objects_id_2; ?></label>
          <div class="col-sm-10">
            <span class="help-block"><?php echo $help_objects_products; ?></span>
            <input type="hidden" name="products" id="products_input" value="<?php echo $products; ?>"/>
            <input type="hidden" id="products_input-selected">
            <div id="products_list"></div>
            <a id="products_button" class="btn btn-info"><i class="fa fa-tasks"></i> <?php echo $button_change; ?></a>
          </div>
        </div>

        <div class="form-group"<?php echo (int)$objects_type != 3 ? ' style="display:none"':''?> id="manufacturers_tr">
          <label class="col-sm-2 control-label"><?php echo $entry_objects_id_3; ?></label>
          <div class="col-sm-10">
            <span class="help-block"><?php echo $help_objects_manufacturers; ?></span>
            <div class="well well-sm" style="min-height: 150px;max-height: 500px;overflow: auto;">
              <table class="table table-striped table-hover">
              <?php foreach ($manufacturers as $manufacturer) { ?>
                <tr>
                  <td class="checkbox">
                    <label>
                      <input type="checkbox" name="manufacturers[]" 
                            value="<?php echo $manufacturer['manufacturer_id']; ?>" <?php echo in_array($manufacturer['manufacturer_id'], $manufacturers_array) ? 'checked' : ''?>/>
                      <?php echo $manufacturer['name']; ?>
                    </label>
                  </td>
                </tr>
              <?php } ?>
              </table>
            </div>
            <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a></div>
        </div>

      </form>
      <?php echo sprintf($text_copyright, $heading_title); ?>


      </div><!-- </div class="panel-body"> -->
    </div><!-- </div class="panel panel-default"> -->
  </div><!-- </div class="container-fluid"> -->
</div><!-- </div id="content"> -->

<script type="text/javascript"><!--
		$(document).ready(function() {
				updateCategoriesTable();
				updateProductsTable();
		});

    $('.date').datetimepicker({
        pickTime: false
    });

    $('#input-objects_type').change(function(){
        if ($(this).val() == 0){
            $('#categories_tr').hide();
            $('#products_tr').hide()
            $('#manufacturers_tr').hide()
        } else if ($(this).val() == 1){
            $('#categories_tr').show();
            $('#products_tr').hide()
            $('#manufacturers_tr').hide()
        } else if ($(this).val() == 2){
            $('#categories_tr').hide();
            $('#products_tr').show();
            $('#manufacturers_tr').hide()
        } else if ($(this).val() == 3){
            $('#categories_tr').hide();
            $('#products_tr').hide();
            $('#manufacturers_tr').show()
        }
    });

/*    $('#categories_button').click(function(event){
        event.preventDefault();
        //dialogObjectsSelector('<?php echo $text_select_categories; ?>', 'categories_dialog', 'categories_input', '<?php echo $categories_dialog; ?>')
    });
*/
/*    $('#products_button').click(function(event){
        event.preventDefault();
        dialogObjectsSelector('<?php echo $text_select_products; ?>', 'products_dialog', 'products_input', '<?php echo $products_dialog; ?>')
    });
*/
		$('#categories_button').on('click', function() {
			$('#modal-categories_selector').remove();
			$('#modal-products_selector').remove();

			$.ajax({
				url: 'index.php?route=shoputils/categories_selector&token=' + getURLVar('token'),
				dataType: 'html',
				beforeSend: function() {
					$('#categories_button i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
					$('#categories_button').prop('disabled', true);
				},
				complete: function() {
					$('#categories_button i').replaceWith('<i class="fa fa-bars"></i>');
					$('#categories_button').prop('disabled', false);
				},
				success: function(html) {
					$('body').append('<div id="modal-categories_selector" class="modal">' + html + '</div>');

					$('#modal-categories_selector').modal('show');
				}
			});

		});

		$('#products_button').on('click', function() {
			$('#modal-categories_selector').remove();
			$('#modal-products_selector').remove();

			$.ajax({
				url: 'index.php?route=shoputils/products_selector&token=' + getURLVar('token'),
				dataType: 'html',
				beforeSend: function() {
					$('#products_button i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
					$('#products_button').prop('disabled', true);
				},
				complete: function() {
					$('#products_button i').replaceWith('<i class="fa fa-tasks"></i>');
					$('#products_button').prop('disabled', false);
				},
				success: function(html) {
					$('body').append('<div id="modal-products_selector" class="modal">' + html + '</div>');

					$('#modal-products_selector').modal('show');
				}
			});

		});

    function updateCategoriesTable(){
        $('#categories_list').html('<img src="view/image/ajaxLoader.gif">');
        var table_action = '<?php echo $categories_table; ?>';
        $.ajax({
            url: table_action.replace('{selected}', $('#categories_input').val()),
            dataType: 'html',
            success: function(data) {
                $('#categories_list').html(data);
            }
        });
    }

    function updateProductsTable(){
        $('#products_list').html('<img src="view/image/ajaxLoader.gif">');
        var table_action = '<?php echo $products_table; ?>';
    console.log(table_action.replace('{selected}', $('#products_input').val()));
        $.ajax({
            url: table_action.replace('{selected}', $('#products_input').val()),
            dataType: 'html',
            success: function(data) {
                $('#products_list').html(data);
            }
        });
    }
//--></script>
<?php echo $footer; ?>