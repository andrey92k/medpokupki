<?php
/*
 * Shoputils
 *
 * ���������� � ������������� ����������
 *
 * ���� ���� ������ ������������ �����������, ������� ����� ����� � ������,
 * ������ � ���� ������. ���� �������� ����������: LICENSE.2.0.x-2.1.x-2.2.x-2.3.x.RUS.TXT
 * ��� �� ������������ ���������� ����� ����� �� ������:
 * https://opencart.market/LICENSE.2.0.x-2.1.x-2.2.x-2.3.x.RUS.TXT
 * 
 * =================================================================
 * OPENCART/ocStore 2.0.x-2.1.x-2.2.x-2.3.x ���������� �� �������������
 * =================================================================
 *  ���� ���� ������������ ��� Opencart/ocStore 2.0.x-2.1.x-2.2.x-2.3.x. Shoputils ��
 *  ����������� ���������� ������ ����� ���������� �� ����� ������ 
 *  ������ Opencart/ocStore, ����� Opencart/ocStore 2.0.x-2.1.x-2.2.x-2.3.x. 
 *  Shoputils �� ������������ ����������� ����������� ��� ������ 
 *  ������ Opencart/ocStore.
 * =================================================================
*/
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"><?php echo $title; ?></h4>
        </div><!-- <div class="modal-header"> -->
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-8">
                    <div class="input-group">
                        <input type="text" value="<?php echo $filter; ?>" id="filter_name" placeholder="<?php echo $entry_filter_name; ?>" class="form-control" />
                        <span class="input-group-btn">
                        <a data-toggle="tooltip" title="<?php echo $button_filter; ?>" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i></a>
                        </span>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="pull-right">
                        <a id="select" class="btn btn-primary"><i class="fa fa-check"></i> <?php echo $button_select; ?></a>
                        <button type="button" aria-hidden="true" id="cancel" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></button>
                    </div>
                </div>
            </div><!-- <div class="row">  -->
            <hr />

            <div class="row">
                <div class="col-sm-12">
                    <div id="status"><?php echo $text_products_selected; ?>0</div>
                </div>
            </div><!-- <div class="row">  -->
            <hr />

            <div class="row">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <td style="text-align: center; width:1px;"><input type="checkbox" id="checkboxall" onclick="checkboxall(this);" style="width:10px;" /></td>
                                <td class="text-left"><?php echo $column_id; ?></td>
                                <td class="text-left"><?php echo $column_sku; ?></td>
                                <td class="text-left"><?php echo $column_name; ?></td>
                                <td class="text-left"><?php echo $column_price; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($products)) { ?>
                            <?php foreach ($products as $product) { ?>
                            <tr>
                                <td width="1" style="text-align: center;">
                                    <input type="checkbox" name="selected" id="product_<?php echo $product['product_id']; ?>" value="<?php echo $product['product_id']; ?>"/>
                                </td>
                                <td class="text-left"><label for='product_<?php echo $product['product_id']; ?>'><?php echo $product['product_id']; ?></label></td>
                                <td class="text-left"><label for='product_<?php echo $product['product_id']; ?>'><?php echo $product['sku']; ?></label></td>
                                <td class="text-left"><label for='product_<?php echo $product['product_id']; ?>'><?php echo $product['name']; ?></label></td>
                                <td class="text-left"><label for='product_<?php echo $product['product_id']; ?>'><?php echo $product['price']; ?></label></td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr><td class="text-center" colspan="4"><?php echo $text_no_products; ?></td></tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- <div class="row">  -->
        </div><!-- </div class="modal-body"> -->
        <div class="modal-footer"><?php echo $pagination; ?></div>
    </div><!-- </div class="modal-content">  -->
</div><!-- </div class="modal-dialog modal-lg">  -->
<script type="text/javascript"><!--
  //$(document).ready(function () {
      var selected = $('#products_input-selected');
      if (!selected.val()){
          selected.val($('#products_input').val());
      }

      updateStats();
      updateChecked();

      $('#cancel').click(function() {
          $.cookie('filter', '');
          selected.val('');
          $('#modal-products_selector').modal('hide');
      });
      
      $('#select').click(function() {
          $('#products_input').val(selected.val());
          updateProductsTable();
          $.cookie('filter', '');
          selected.val('');
          $('#modal-products_selector').modal('hide');
      });

      $('#filter_name').on('keydown', function(e) {
        if (e.which == 13) {
          $('#button-filter').trigger('click');
        }
      });

      $('#button-filter').click(function() {
          $.cookie('filter', $('#filter_name').val());
          url = '<?php echo $action_filter; ?>';
          $('#modal-products_selector').load(url);
    });

    $('.pagination a').on('click', function(e) {
      e.preventDefault();
      $('#modal-products_selector').load($(this).attr('href'));
    });

      if(!Array.indexOf){
          Array.prototype.indexOf = function(obj){
              for(var i=0; i<this.length; i++){
                  if(this[i]==obj){
                      return i;
                  }
              }
              return -1;
          }
      }

      function checkboxall(el) {
           var checked_status = el.checked;
           $("input[name=selected]").each(function() {
              this.checked = checked_status;
              if ($(this).is(':checked')){
                  productAdd($(this).val());
              } else {
                  productRemove($(this).val());
              }
           });
      }

      function productAdd(id) {
          var ids = selected.val().split(',');
          if (ids.indexOf(id) == -1){
              ids.push(id);
              selected.val(ids.join(','));
              updateStats();
          }
      }

      function productRemove(id) {
          var ids = new String(selected.val()).split(',');
          // bug #7654 fixed
          while (ids.indexOf(id) != -1) {
              ids.splice(ids.indexOf(id), 1);
          }
          selected.val(ids.join(','));
          updateStats();
      }

      $('input[name*=\'select\']').change(function() {
          if ($(this).is(':checked')){
              productAdd($(this).val());
          } else {
              productRemove($(this).val());
          }
      });

      function updateStats(){
          var ids = new String(selected.val()).split(',');
          if (ids.length > 0 && ids[0] == ''){
              ids.splice(0,1);
          }
          $('#status').html('<?php echo $text_products_selected; ?>' + (ids.length));
      }

      function updateChecked(){
          var ids = new String(selected.val()).split(',');
          if (ids.length > 0 && ids[0] == ''){
              ids.splice(0,1);
          }
          $("input[name=selected]").each(function(){
              $(this).attr('checked', ids.indexOf($(this).val()) != -1);
          });
      }

  //});
//--></script>
