<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<a href="<?php echo $customer; ?>" class="btn btn-default"><?php echo $text_order_customer; ?></a> 
				<a href="<?php echo $order; ?>" class="btn btn-default"><?php echo $text_order_store; ?></a> 
				<a href="<?php echo $shiptor; ?>" class="btn btn-default"><?php echo $text_order_shiptor; ?></a>			
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
				<button type="submit" form="form-shiptor" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
			</div>

			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<?php if (!empty($breadcrumb['separator'])) : ?>
						<li><?php echo $breadcrumb['separator']; ?></li>
					<?php endif; ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	
	<div class="container-fluid">
		<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>		
		<?php if ($error) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>		
	 
		<link href="view/javascript/shipping/shiptor/shiptor.css" rel="stylesheet" />
		<link href="view/javascript/shipping/shiptor/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" />

		<script src="view/javascript/shipping/shiptor/datetimepicker/moment.min.js"></script>	
		<script src="view/javascript/shipping/shiptor/datetimepicker/ru.min.js"></script>
		<script src="view/javascript/shipping/shiptor/datetimepicker/bootstrap-datetimepicker.min.js"></script>
		
		<div class="panel panel-default">
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-shiptor" class="form-horizontal">
				<?php if ($shipping_methods) { ?>
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
						<li><a href="#tab-delivery" data-toggle="tab"><?php echo $tab_delivery; ?></a></li>
						<li><a href="#tab-setting" data-toggle="tab"><?php echo $tab_setting; ?></a></li>
					</ul>

					<div style="float: right;">
						<a href="https://help.shiptor.ru/knowledge_base/item/100062" target="_blank"><?= $text_faq; ?></a>
					</div>

					<div class="tab-content">

						<!-- методы доставки -->

						<div class="tab-pane active" id="tab-general">
							<div class="col-sm-2">
								<ul class="nav nav-pills nav-stacked">
								<?php foreach ($shipping_methods as $key => $shipping_method) { ?>
									<li <?php echo ! $key ? 'class="active"' : ''; ?>><a href="#tab-<?php echo $shipping_method['id']; ?>" data-toggle="tab"><?php echo $shipping_method['name']; ?></a></li></li>
								<?php } ?>
								</ul>
							</div>

							<div class="col-sm-10">
								<div class="tab-content">		
								<?php foreach ($shipping_methods as $key => $shipping_method) { ?>
									<div class="tab-pane <?php echo ! $key ? 'active' : ''; ?>" id="tab-<?php echo $shipping_method['id']; ?>">

										<h3><?php echo $shipping_method['name']; ?></h3>

										<div class="form-group">
											<div class="sh-flex">
												<label class="col-sm-2 control-label"><?php echo $entry_shipping_status; ?></label>
												<div class="col-sm-3">
													<div class="onoffswitch">
														<?php if (${'status_' . $shipping_method['id']}) { ?>
															<input type="checkbox" name="shiptor_<?php echo $shipping_method['id']; ?>_status" id="<?php echo $shipping_method['id']; ?>_status" value="1" class="onoffswitch-checkbox" checked="checked" />
														<?php } else { ?>
															<input type="checkbox" name="shiptor_<?php echo $shipping_method['id']; ?>_status" id="<?php echo $shipping_method['id']; ?>_status" value="1" class="onoffswitch-checkbox" />
														<?php } ?>
														 <label class="onoffswitch-label" for="<?php echo $shipping_method['id']; ?>_status"></label>
													</div>
												</div>
												<div class="col-sm-7">
													<div class="sh-help"><?php echo $help_shipping_status; ?></div>
												</div>
											</div>
										</div>
										<?php if ($shipping_method['courier'] == 'shiptor-one-day') { ?>
										<div class="form-group">
											<div class="sh-flex">
												<label class="col-sm-2 control-label"><?php echo $entry_shipping_time; ?></label>
												<div class="col-sm-3">
													<div class="input-group" id="datetimepicker">
														<input type="text" name="shiptor_<?php echo $shipping_method['id']; ?>_time" value="<?php echo preg_replace('/[a-zAZ\.*+? ]/' , '', ${'time_' . $shipping_method['id']}); ?>" class="form-control" />
														<span class="input-group-btn"><button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button></span>
													</div>
												</div>
												<div class="col-sm-7">
													<div class="sh-help"><?php echo $help_shipping_time; ?></div>
												</div>
											</div>
										</div>
										<?php } ?>

										
										<div class="form-group">
											<div class="sh-flex">
												<label class="col-sm-2 control-label"><?php echo $entry_total_free; ?></label>
												<div class="col-sm-3">
													<input name="shiptor_<?php echo $shipping_method['id']; ?>_total_free" value="<?php echo ${'total_free_' . $shipping_method['id']}; ?>" class="form-control" />
												</div>
												<div class="col-sm-7">
													<div class="sh-help"><?php echo $help_total_free; ?></div>
												</div>
											</div>		
										</div>		
										<div class="form-group">
											<div class="sh-flex">
												<label class="col-sm-2 control-label"><?php echo $entry_total_min; ?></label>
												<div class="col-sm-3">
													<input name="shiptor_<?php echo $shipping_method['id']; ?>_total_min" value="<?php echo ${'total_min_' . $shipping_method['id']}; ?>" class="form-control" />
												</div>
												<div class="col-sm-7">
													<div class="sh-help"><?php echo $help_total_min; ?></div>
												</div>
											</div>		
										</div>	
										<div class="form-group">
											<div class="sh-flex">
												<label class="col-sm-2 control-label"><?php echo $entry_total_max; ?></label>
												<div class="col-sm-3">
													<input name="shiptor_<?php echo $shipping_method['id']; ?>_total_max" value="<?php echo ${'total_max_' . $shipping_method['id']}; ?>" class="form-control" />
												</div>
												<div class="col-sm-7">
													<div class="sh-help"><?php echo $help_total_max; ?></div>
												</div>
											</div>		
										</div>								
										<div class="form-group">
											<div class="sh-flex">
												<label class="col-sm-2 control-label"><?php echo $entry_total_min_weight; ?></label>
												<div class="col-sm-3">
													<input name="shiptor_<?php echo $shipping_method['id']; ?>_total_min_weight" value="<?php echo ${'total_min_weight_' . $shipping_method['id']}; ?>" class="form-control" />
												</div>
												<div class="col-sm-7">
													<div class="sh-help"><?php echo $help_total_min_weight; ?></div>
												</div>
											</div>		
										</div>	
										<div class="form-group">
											<div class="sh-flex">
												<label class="col-sm-2 control-label"><?php echo $entry_total_max_weight; ?></label>
												<div class="col-sm-3">
													<input name="shiptor_<?php echo $shipping_method['id']; ?>_total_max_weight" value="<?php echo ${'total_max_weight_' . $shipping_method['id']}; ?>" class="form-control" />
												</div>
												<div class="col-sm-7">
													<div class="sh-help"><?php echo $help_total_max_weight; ?></div>
												</div>
											</div>		
										</div>	
										<div class="form-group">
											<div class="sh-flex">
												<label class="col-sm-2 control-label"><?php echo $entry_fixed; ?></label>
												<div class="col-sm-3">
													<input name="shiptor_<?php echo $shipping_method['id']; ?>_fixed" value="<?php echo ${'fixed_' . $shipping_method['id']}; ?>" class="form-control" />
												</div>
												<div class="col-sm-7">
													<div class="sh-help"><?php echo $help_fixed; ?></div>
												</div>
											</div>	
										</div>
										<div class="form-group">
											<div class="sh-flex">
												<label class="col-sm-2 control-label"><?php echo $entry_weight_price; ?></label>
												<div class="col-sm-3">
													<textarea name="shiptor_<?php echo $shipping_method['id']; ?>_weight_price" class="form-control"><?php echo ${'weight_price_' . $shipping_method['id']}; ?></textarea>
												</div>
												<div class="col-sm-7">
													<div class="sh-help"><?php echo $help_weight_price; ?></div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="sh-flex">
												<label class="col-sm-2 control-label"><?php echo $entry_type; ?></label>
												<div class="col-sm-3">
													<select name="shiptor_<?php echo $shipping_method['id']; ?>_type" class="form-control">
														<option value="0"><?php echo $text_select; ?></option>
														<?php if (${'type_' . $shipping_method['id']} == 'P') { ?>
															<option value="P" selected="selected"><?php echo $text_percent; ?></option>
														<?php } else { ?>
															<option value="P"><?php echo $text_percent; ?></option>
														<?php } ?>
														<?php if (${'type_' . $shipping_method['id']} == 'F') { ?>
															<option value="F" selected="selected"><?php echo $text_amount; ?></option>
														<?php } else { ?>
															<option value="F"><?php echo $text_amount; ?></option>
														<?php } ?>
													</select>
												</div>
												<div class="col-sm-7">
													<div class="sh-help"><?php echo $help_type; ?></div>
												</div>
											</div>											
										</div>													
										<div class="form-group">
											<div class="sh-flex">
												<label class="col-sm-2 control-label"><?php echo $entry_markup; ?></label>
												<div class="col-sm-3">
													<input type="text" name="shiptor_<?php echo $shipping_method['id']; ?>_markup" value="<?php echo ${'markup_' . $shipping_method['id']}; ?>" class="form-control" />
													<?php if (isset(${'error_'.$shipping_method['id'].'_markup'})) { ?><span class="text-danger"><?php echo ${'error_'.$shipping_method['id'].'_markup'}; ?></span><?php } ?>
												</div>
												<div class="col-sm-7">
													<div class="sh-help"><?php echo $help_markup; ?></div>
												</div>
											</div>
										</div>
										<!--<div class="form-group">
											<div class="sh-flex">
												<label class="col-sm-2 control-label"><?php echo $entry_markup_surcharge; ?></label>
												<div class="col-sm-3">
													<input type="text" name="shiptor_<?php echo $shipping_method['id']; ?>_markup_surcharge" value="<?php echo ${'markup_surcharge_' . $shipping_method['id']}; ?>" class="form-control" />
													<?php if (isset(${'error_'.$shipping_method['id'].'_markup_surcharge'})) { ?><span class="text-danger"><?php echo ${'error_'.$shipping_method['id'].'_markup_surcharge'}; ?></span><?php } ?>
												</div>
												<div class="col-sm-7">
													<div class="sh-help"><?php echo $help_markup_surcharge; ?></div>
												</div>
											</div>
										</div>-->
										<div class="form-group">
											<div class="sh-flex">
												<label class="col-sm-2 control-label"><?php echo $entry_comment; ?></label>
												<div class="col-sm-3">
													<div class="onoffswitch">
														<?php if (${'comment_' . $shipping_method['id']}) { ?>
															<input type="checkbox" name="shiptor_<?php echo $shipping_method['id']; ?>_comment" id="<?php echo $shipping_method['id']; ?>_comment" value="1" class="onoffswitch-checkbox" checked="checked" />
														<?php } else { ?>
															<input type="checkbox" name="shiptor_<?php echo $shipping_method['id']; ?>_comment" id="<?php echo $shipping_method['id']; ?>_comment" value="1" class="onoffswitch-checkbox" />
														<?php } ?>
														 <label class="onoffswitch-label comment-change" for="<?php echo $shipping_method['id']; ?>_comment"></label>
													</div>

												</div>
												<div class="col-sm-7">
													<div class="sh-help"><?php echo $help_comment; ?></div>
												</div>
											</div>
										</div>
										<div class="form-group my-comment">
											<div class="sh-flex">
												<label class="col-sm-2 control-label"><?php echo $entry_my_comment; ?></label>
												<div class="col-sm-3">
													<input type="text" name="shiptor_<?php echo $shipping_method['id']; ?>_my_comment" value="<?php echo ${'my_comment_' . $shipping_method['id']}; ?>" class="form-control" />
												</div>
												<div class="col-sm-7">
													<div class="sh-help"><?php echo $help_my_comment; ?></div>
												</div>
											</div>	
										</div>							
										<div class="form-group">
											<div class="sh-flex">
												<label class="col-sm-2 control-label"><?php echo $entry_sort_order; ?></label>										
												<div class="col-sm-3">
													<input type="text" name="shiptor_<?php echo $shipping_method['id']; ?>_sort_order" value="<?php echo ${'sort_order_' . $shipping_method['id']}; ?>" class="form-control" />									
												</div>
												<div class="col-sm-7">
												</div>
											</div>
										</div>
										
										<?php if (!empty($regions)) { ?>
										<div class="form-group">
											<div class="col-sm-2">
												<ul class="nav nav-pills nav-stacked">
												<?php foreach ($regions as $i => $region) { ?>
												<li <?php echo ! $i ? 'class="active"' : ''; ?>><a href="#tab-<?php echo $shipping_method['id']; ?>-region-<?php echo $region['id']; ?>" data-toggle="tab"><?php echo $region['name']; ?></a></li>
												<?php } ?>
												</ul>
											</div>
											<div class="col-sm-10">
												<div class="tab-content">
													<?php foreach ($regions as $i => $region) { ?>
														<div class="tab-pane <?php echo ! $i ? 'active' : ''; ?>" id="tab-<?php echo $shipping_method['id']; ?>-region-<?php echo $region['id']; ?>">
															<div class="form-group">
																<div class="sh-flex">
																	<label class="col-sm-2 control-label"><?php echo $entry_weight_price; ?></label>
																	<div class="col-sm-3">
																		<textarea name="shiptor_<?php echo $shipping_method['id']; ?>_<?php echo $region['id']; ?>_weight_price" class="form-control"><?php echo ${'weight_price_' . $shipping_method['id'] . '_' . $region['id']}; ?></textarea>
																	</div>
																	<div class="col-sm-7">
																		<div class="sh-help"><?php echo $help_weight_price; ?></div>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<div class="sh-flex">
																	<label class="col-sm-2 control-label"><?php echo $entry_total_free; ?></label>
																	<div class="col-sm-3">
																		<input name="shiptor_<?php echo $shipping_method['id']; ?>_<?php echo $region['id']; ?>_total_free" value="<?php echo ${'total_free_' . $shipping_method['id'] . '_' . $region['id']}; ?>" class="form-control" />
																	</div>
																	<div class="col-sm-7">
																		<div class="sh-help"><?php echo $help_total_free; ?></div>
																	</div>
																</div>		
															</div>
															<div class="form-group">
																<div class="sh-flex">
																	<label class="col-sm-2 control-label"><?php echo $entry_total_min; ?></label>
																	<div class="col-sm-3">
																		<input name="shiptor_<?php echo $shipping_method['id']; ?>_<?php echo $region['id']; ?>_total_min" value="<?php echo ${'total_min_' . $shipping_method['id'] . '_' . $region['id']}; ?>" class="form-control" />
																	</div>
																	<div class="col-sm-7">
																		<div class="sh-help"><?php echo $help_total_min; ?></div>
																	</div>
																</div>		
															</div>
															<div class="form-group">
																<div class="sh-flex">
																	<label class="col-sm-2 control-label"><?php echo $entry_total_max; ?></label>
																	<div class="col-sm-3">
																		<input name="shiptor_<?php echo $shipping_method['id']; ?>_<?php echo $region['id']; ?>_total_max" value="<?php echo ${'total_max_' . $shipping_method['id'] . '_' . $region['id']}; ?>" class="form-control" />
																	</div>
																	<div class="col-sm-7">
																		<div class="sh-help"><?php echo $help_total_max; ?></div>
																	</div>
																</div>		
															</div>
															<div class="form-group">
																<div class="sh-flex">
																	<label class="col-sm-2 control-label"><?php echo $entry_fixed; ?></label>
																	<div class="col-sm-3">
																		<input name="shiptor_<?php echo $shipping_method['id']; ?>_<?php echo $region['id']; ?>_fixed" value="<?php echo ${'fixed_' . $shipping_method['id'] . '_' . $region['id']}; ?>" class="form-control" />
																	</div>
																	<div class="col-sm-7">
																		<div class="sh-help"><?php echo $help_fixed; ?></div>
																	</div>
																</div>	
															</div>	
															<div class="form-group">
																<div class="sh-flex">
																	<label class="col-sm-2 control-label"><?php echo $entry_type; ?></label>
																	<div class="col-sm-3">
																		<select name="shiptor_<?php echo $shipping_method['id']; ?>_<?php echo $region['id']; ?>_type" class="form-control">
																			<option value="0"><?php echo $text_select; ?></option>
																			<?php if (${'type_' . $shipping_method['id'] . '_' . $region['id']} == 'P') { ?>
																				<option value="P" selected="selected"><?php echo $text_percent; ?></option>
																			<?php } else { ?>
																				<option value="P"><?php echo $text_percent; ?></option>
																			<?php } ?>
																			<?php if (${'type_' . $shipping_method['id'] . '_' . $region['id']} == 'F') { ?>
																				<option value="F" selected="selected"><?php echo $text_amount; ?></option>
																			<?php } else { ?>
																				<option value="F"><?php echo $text_amount; ?></option>
																			<?php } ?>
																		</select>
																	</div>
																	<div class="col-sm-7">
																		<div class="sh-help"><?php echo $help_type; ?></div>
																	</div>
																</div>													
															</div>													
															<div class="form-group">
																<div class="sh-flex">
																	<label class="col-sm-2 control-label"><?php echo $entry_markup; ?></label>
																	<div class="col-sm-3">
																		<input type="text" name="shiptor_<?php echo $shipping_method['id']; ?>_<?php echo $region['id']; ?>_markup" value="<?php echo ${'markup_' . $shipping_method['id'] . '_' . $region['id']}; ?>" class="form-control" />
																	</div>
																	<div class="col-sm-7">
																		<div class="sh-help"><?php echo $help_markup; ?></div>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<div class="sh-flex">
																	<label class="col-sm-2 control-label"><?php echo $entry_region_status; ?></label>
																	<div class="col-sm-3">
																		<div class="onoffswitch">
																			<?php if (${'status_' . $shipping_method['id'] . '_' . $region['id']}) { ?>
																				<input type="checkbox" name="shiptor_<?php echo $shipping_method['id']; ?>_<?php echo $region['id']; ?>_status" id="<?php echo $shipping_method['id']; ?>_<?php echo $region['id']; ?>_status" value="1" class="onoffswitch-checkbox" checked="checked" />
																			<?php } else { ?>
																				<input type="checkbox" name="shiptor_<?php echo $shipping_method['id']; ?>_<?php echo $region['id']; ?>_status" id="<?php echo $shipping_method['id']; ?>_<?php echo $region['id']; ?>_status" value="1" class="onoffswitch-checkbox" />
																			<?php } ?>
																			 <label class="onoffswitch-label" for="<?php echo $shipping_method['id']; ?>_<?php echo $region['id']; ?>_status"></label>
																		</div>
																	</div>
																	<div class="col-sm-7">
																		<div class="sh-help"><?php echo $help_region_status; ?></div>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<div class="sh-flex">
																	<label class="col-sm-2 control-label"><?php echo $entry_region_hide; ?></label>
																	<div class="col-sm-3">
																		<div class="onoffswitch">
																			<?php if (${'hide_' . $shipping_method['id'] . '_' . $region['id']}) { ?>
																				<input type="checkbox" name="shiptor_<?php echo $shipping_method['id']; ?>_<?php echo $region['id']; ?>_hide" id="<?php echo $shipping_method['id']; ?>_<?php echo $region['id']; ?>_hide" value="1" class="onoffswitch-checkbox" checked="checked" />
																			<?php } else { ?>
																				<input type="checkbox" name="shiptor_<?php echo $shipping_method['id']; ?>_<?php echo $region['id']; ?>_hide" id="<?php echo $shipping_method['id']; ?>_<?php echo $region['id']; ?>_hide" value="1" class="onoffswitch-checkbox" />
																			<?php } ?>
																			 <label class="onoffswitch-label" for="<?php echo $shipping_method['id']; ?>_<?php echo $region['id']; ?>_hide"></label>
																		</div>
																	</div>
																	<div class="col-sm-7">
																		<div class="sh-help"><?php echo $help_region_hide; ?></div>
																	</div>
																</div>
															</div>
														</div>
													<?php } ?>
												</div>
											</div>
										</div>
										<?php } ?>
									</div><!-- / tab-general -->

								<?php } ?>				
								</div><!-- / tab-content -->
							</div>

						</div>

						<!-- /методы доставки -->

						<!-- параметры доставки -->

						<div class="tab-pane" id="tab-delivery">
							<div class="form-group required">
								<div class="sh-flex">
									<label class="col-sm-2 control-label"><?php echo $entry_weight; ?></label>
									<div class="col-sm-3">
										<input type="text" name="shiptor_weight" value="<?php echo $weight; ?>" class="form-control" />
										<?php if (isset($error_default_weight)) { ?><span class="text-danger"><?php echo $error_default_weight; ?></span><?php } ?>
									</div>
									<div class="col-sm-7">
										<div class="sh-help"><?php echo $help_dimensions; ?></div>
									</div>
								</div>
							</div>
							<div class="form-group required">
								<div class="sh-flex">
									<label class="col-sm-2 control-label"><?php echo $entry_default_dimensions; ?></label>
									<div class="col-sm-3">
										<input type="text" name="shiptor_default_length" value="<?php echo $default_length; ?>" class="form-control" style="width: 25%; display: inline;" />
										&nbsp;x&nbsp;
										<input type="text" name="shiptor_default_width" value="<?php echo $default_width; ?>" class="form-control" style="width: 25%; display: inline;" />
										&nbsp;x&nbsp;
										<input type="text" name="shiptor_default_height" value="<?php echo $default_height; ?>" class="form-control" style="width: 25%; display: inline;" />
										<?php if (isset($error_default_dimensions)) { ?><span class="text-danger"><?php echo $error_default_dimensions; ?></span><?php } ?>
									</div>
									<div class="col-sm-7">
										<div class="sh-help"><?php echo $help_default_dimensions; ?></div>
									</div>
								</div>
							</div>

							<?php /* единицы измерения веса - строго килограммы */ ?>
							<input type="hidden" name="shiptor_weight_class_id" value="1">

							<div class="form-group">	
								<div class="sh-flex">
									<label class="col-sm-2 control-label"><?php echo $entry_region; ?></label>
									<div class="col-sm-3">
										<input type="text" name="shiptor_regions" value="" placeholder="Автодополнение" class="form-control" />
										<div id="shiptor-regions" class="well well-sm" style="height: 150px; overflow: auto;">
										<?php foreach ($regions as $region) { ?>
											<div id="shiptor-regions<?php echo $region['id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $region['name']; ?><input type="hidden" name="shiptor_regions[]" value="<?php echo $region['id']; ?>" /></div>
										<?php } ?>
										</div>
									</div>
									<div class="col-sm-7">
										<div class="sh-help"><?php echo $help_region; ?></div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="sh-flex">
									<label class="col-sm-2 control-label"><?php echo $entry_days; ?></label>
									<div class="col-sm-3">								
										<div class="onoffswitch">
											<?php if ($days) { ?>
												<input type="checkbox" name="shiptor_days" id="days" value="1" class="onoffswitch-checkbox" checked="checked" />
											<?php } else { ?>
												<input type="checkbox" name="shiptor_days" id="days" value="1" class="onoffswitch-checkbox" />
											<?php } ?>
											 <label class="onoffswitch-label" for="days"></label>
										
										</div>
									</div>
									<div class="col-sm-7">
										<div class="increase">
										<label>
										<?php echo $help_increase_days; ?>
										</label>
										</div>
										<input name="shiptor_increase_days" value="<?php echo $increase_days;?>" class="form-control" style="width: 10%; display: inline;">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="sh-flex">
									<label class="col-sm-2 control-label"><?php echo $entry_time; ?></label>
									<div class="col-sm-3">
										<div class="onoffswitch">
											<?php if ($time) { ?>
												<input type="checkbox" name="shiptor_time" id="time" value="1" class="onoffswitch-checkbox" checked="checked" />
											<?php } else { ?>
												<input type="checkbox" name="shiptor_time" id="time" value="1" class="onoffswitch-checkbox" />
											<?php } ?>
											 <label class="onoffswitch-label" for="time"></label>
										</div>
									</div>
									<div class="col-sm-7">
										<div class="sh-help"><?php echo $help_time; ?></div>
									</div>
								</div>
							</div>
						</div>

						<!-- /параметры доставки -->


						<!-- настройки -->

						<div class="tab-pane" id="tab-setting">
							<div class="form-group required">
								<div class="sh-flex">
									<label class="col-sm-2 control-label"><?php echo $entry_authorization; ?></label>
									<div class="col-sm-3">
										<input type="text" name="shiptor_authorization" value="<?php echo $authorization; ?>" class="form-control" />									
										<?php if (isset($error_authorization)) { ?><span class="text-danger"><?php echo $error_authorization; ?></span><?php } ?>
									</div>
									<div class="col-sm-7">
										<div class="sh-help"><?php echo $help_authorization; ?></div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="sh-flex">
									<label class="col-sm-2 control-label"><?php echo $entry_logger; ?></label>
									<div class="col-sm-3">							
										<div class="onoffswitch">
											<?php if ($logger) { ?>
												<input type="checkbox" name="shiptor_logger" id="logger" value="1" class="onoffswitch-checkbox" checked="checked" />
											<?php } else { ?>
												<input type="checkbox" name="shiptor_logger" id="logger" value="1" class="onoffswitch-checkbox" />
											<?php } ?>
											 <label class="onoffswitch-label" for="logger"></label>
										</div>
									</div>
									<div class="col-sm-7">
										<div class="sh-help"><?php echo $help_logger; ?></div>
									</div>
								</div>
							</div>
							<!--<div class="form-group">
								<div class="sh-flex">
									<label class="col-sm-2 control-label"><?php echo $entry_cache; ?></label>
									<div class="col-sm-3">
										<div class="onoffswitch">
											<?php if ($cache) { ?>
												<input type="checkbox" name="shiptor_cache" id="cache" value="1" class="onoffswitch-checkbox" checked="checked" />
											<?php } else { ?>
												<input type="checkbox" name="shiptor_cache" id="cache" value="1" class="onoffswitch-checkbox" />
											<?php } ?>
											 <label class="onoffswitch-label" for="cache"></label>
										</div>
									</div>
									<div class="col-sm-7">
									</div>
								</div>
							</div>-->
							<div class="form-group">
								<div class="sh-flex">
									<label class="col-sm-2 control-label"><?php echo $entry_status; ?></label>
									<div class="col-sm-3">
										<div class="onoffswitch">
											<?php if ($status) { ?>
												<input type="checkbox" name="shiptor_status" id="status" value="1" class="onoffswitch-checkbox" checked="checked" />
											<?php } else { ?>
												<input type="checkbox" name="shiptor_status" id="status" value="1" class="onoffswitch-checkbox" />
											<?php } ?>
											 <label class="onoffswitch-label" for="status"></label>
										</div>
									</div>
									<div class="col-sm-7">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="sh-flex">
									<label class="col-sm-2 control-label"><?php echo $entry_sort_order; ?></label>
									<div class="col-sm-3">
										<input type="text" name="shiptor_sort_order" value="<?php echo $sort_order; ?>" class="form-control" />
									</div>
									<div class="col-sm-7">
									</div>
								</div>
							</div>	
						</div>

						<!-- /настройки -->

					</div><!-- / tab-content -->

				<?php } else { ?>

					<div class="form-horizontal">
						<div class="form-group required">
							<div class="sh-flex">
								<label class="col-sm-2 control-label"><?php echo $entry_authorization; ?></label>
								<div class="col-sm-3">
									<input type="text" name="shiptor_authorization" value="<?php echo $authorization; ?>" class="form-control" />
								</div>
								<div class="col-sm-7">
									<div class="sh-help"><?php echo $help_authorization; ?></div>
								</div>
							</div>				
						</div>				
						<div class="form-group">
							<div class="col-sm-push-3 col-sm-9"><a onclick="Auth();" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-success"><?php echo $button_authorization; ?></a></div>
						</div>			
					</div>			
				<?php } ?>			
				</form>
			</div>
		</div>
	</div>
</div>

<script>
$('#datetimepicker').datetimepicker({language: 'ru', pickDate: false, icons: {time: "fa fa-clock-o", date: "fa fa-calendar", up: "fa fa-arrow-up", down: "fa fa-arrow-down"}});

$('input[name=shiptor_regions]').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=shipping/shiptor/regions&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return { label: item.name, value: item.id }
				}));
			}
		});
	},
	select: function(item) {
		$('[name=shiptor_regions]').val('');
		$('#shiptor-regions' + item.value).remove();
		$('#shiptor-regions').append('<div id="shiptor-regions' + item.value + '"><i class="fa fa-minus-circle"></i> ' + item.label + '<input type="hidden" name="shiptor_regions[]" value="' + item.value + '" /></div>');	
	}
});
	
$('#shiptor-regions').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

function Auth() {
	$.ajax({
		url: 'index.php?route=shipping/shiptor/auth&token=<?php echo $token; ?>',
		type: 'POST',
		dataType: 'json',
		beforeSend: function() {			
			$('.text-danger').remove();
		},
		success: function(json) {
			if (json.error) {
				$('input[name=shiptor_authorization]').after('<span class="text-danger">' + json.error + '</span>');
			}			
			if (json.success) {
				document.location.reload(true);
			}
		}
	});
}
function setCorrectPercent(id){
	if($('select[name=shiptor_'+id+'_type]').val()=='P' &&
	   ($('input[name=shiptor_'+id+'_markup]').val()>100 || $('input[name=shiptor_'+id+'_markup]').val()<-100)){
		
		if($('input[name=shiptor_'+id+'_markup]').val().indexOf('-')==-1){
			if($('input[name=shiptor_'+id+'_markup]').val()>100){
				$('input[name=shiptor_'+id+'_markup]').val('100');
			}
		}
		else {
			if($('input[name=shiptor_'+id+'_markup]').val()<-100){
				$('input[name=shiptor_'+id+'_markup]').val('-100');
			}
		}
	}
	$('#tab-'+id+' input.form-control:not(input[name=shiptor_'+id+'_markup],input[name=shiptor_'+id+'_my_comment],input[name=shiptor_'+id+'_sort_order])').each(function(){
		if($(this).val().indexOf('-')!=-1){
			$(this).val($(this).val().split('-')[1]);
		}
	});
}
$('#form-shiptor').on('focusout','input.form-control',function(){
	setCorrectPercent();
});
$('#form-shiptor').on('submit', function(event){
	var id = $('#tab-general .tab-content .tab-pane.active').attr('id').split('-')[1];
	if($('select[name=shiptor_'+id+'_type]').val()!='0' && $('input[name=shiptor_'+id+'_markup]').val()==''){
		$('select[name=shiptor_'+id+'_type]').val(0);
	}
	event.preventDefault();
	setCorrectPercent(id);
	$(this).unbind('submit').submit();
});
</script>

<?php echo $footer; ?>
