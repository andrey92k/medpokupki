jQuery(function(){

jQuery("#callmeform .btn").mouseover(function(){ //обработка прозрачности кнопки
	jQuery(this).css({ opacity: 0.9 });
}).mouseleave(function(){
	jQuery(this).css({ opacity: 1 });
});

jQuery(".callme_viewform").click(function(){
	callmeShowForm();
});

jQuery("#cme_cls").click(function(){//кнопка "закрыть" 
	callmeShowForm();
}).mouseover(function(){
	jQuery(this).css({ opacity: 0.8 });
}).mouseleave(function(){
	jQuery(this).css({ opacity: 1 });
});

jQuery("#viewform").hover(function(){ 
	jQuery(this).addClass("callmeform_hover");
},function(){
	jQuery(this).removeClass("callmeform_hover");
}

);
});

function callmeShowForm(){ //скрываем и показываем форму
	jQuery("#callmeform").fadeToggle("fast");
	jQuery("#callme_result").html("");
}

function sendMail() {
	var cnt = jQuery.Storage.get('callme-sent'); //getting last sent time from storage
	if (!cnt) { cnt = 0; }
	
	jQuery.getJSON("/callme/index.php", {
		cname: jQuery("#cname").val(), 
		cphone: jQuery("#cphone").val(), 
		ccmnt: jQuery("#ccmnt").val(), 
		ctime: cnt, 
		url: location.href 
	}, function(data) {	
		message = "<div class='" + data.cls + "'>" + data.message +"</div>";
		jQuery("#callme_result").html(message);
		
		if (data.result == "success") {
			jQuery.Storage.set("callme-sent", data.time);
			jQuery("#callmeform .btn").attr("disabled", "disabled");
			setTimeout( function(){ callmeShowForm(); }, 4000);
			setTimeout( function(){ jQuery("#callmeform .txt").val(""); }, 5000);
		}
	});
}

jQuery(document).ready(function(){

jQuery("#callmeform .btn").click(function(){
	jQuery("#callme_result").html("<div class='sending'>Отправка...</div>");
	setTimeout( function(){ sendMail(); }, 2000);
	return false;
});	
});

jQuery(document).keyup(function(a) { //обработка esc
	if (a.keyCode == 27) {
		if(jQuery("#callmeform").is(":visible")) {
			callmeShowForm();
		}
	}
});